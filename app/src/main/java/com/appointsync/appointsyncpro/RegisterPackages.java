package com.appointsync.appointsyncpro;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.appointsync.appointsyncpro.Adapters.PackagePagerAdapter;
import com.appointsync.appointsyncpro.Adapters.PackagePagerModel;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class RegisterPackages extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_packages);


    }


    public void showDialog(View v){

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.package_layout);


        List<PackagePagerModel> pagerArr = new ArrayList<>();
        pagerArr.add(new PackagePagerModel("1", "Pager Item #1", "hi", "$14.99"));
        pagerArr.add(new PackagePagerModel("2", "Pager Item #2", "Hello \n bye", "$24.99"));

        PackagePagerAdapter adapter = new PackagePagerAdapter(this, pagerArr);

        AutoScrollViewPager pager = (AutoScrollViewPager) dialog.findViewById(R.id.pager);

        pager.setAdapter(adapter);

        final CirclePageIndicator pageIndicator = (CirclePageIndicator) dialog.findViewById(R.id.page_indicator);

        pageIndicator.setViewPager(pager);
        pageIndicator.setCurrentItem(0);

        pageIndicator.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){


                //this will log the page number that was click
                Log.i("TAG", "This page was clicked: ");
//                Log.i("TAG", "This page was clicked: " + pageIndicator.getId());
            }
        });

        dialog.show();



    }

}

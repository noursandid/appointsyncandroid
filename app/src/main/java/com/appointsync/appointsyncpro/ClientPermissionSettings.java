package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClientPermissions;
import com.appointsync.appointsyncpro.Class.Main.GetPermissions;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedPermission;
import com.appointsync.appointsyncpro.Class.Main.PostUpdateClientPermissions;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePermission;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientPermissionSettings extends Fragment {
    private Switch singleClientPermissionRequestNewAppointment, singleClientPermissionAcceptRequestedAppointment, singleClientPermissionAmendAppointment, singleClientPermissionCancelAppointment;
    private ImageView singleClientPermissionSettingsBackArrow;
    private TextView singleClientPermissionText;
    private ProgressDialog pd;
    MappedClient mappedClient;
    private long mLastClickTime =0;
    private Boolean canCreate = true,canAmend = true, canCancel= true,canAccept = true;

    public ClientPermissionSettings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_permission_settings, container, false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        singleClientPermissionRequestNewAppointment = view.findViewById(R.id.singleClientPermissionRequestNewAppointment);
        singleClientPermissionAcceptRequestedAppointment = view.findViewById(R.id.singleClientPermissionAcceptRequestedAppointment);
        singleClientPermissionAmendAppointment = view.findViewById(R.id.singleClientPermissionAmendAppointment);
        singleClientPermissionCancelAppointment = view.findViewById(R.id.singleClientPermissionCancelAppointment);
        singleClientPermissionSettingsBackArrow = view.findViewById(R.id.singleClientPermissionSettingsBackArrow);
        singleClientPermissionText = view.findViewById(R.id.singleClientPermissionText);

        singleClientPermissionText.setText("The above permissions are assigned for " +mappedClient.getUsername()+ " and will override the default clients permissions in your settings.");

        pd = new ProgressDialog(getContext());

        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        Call<AppointSyncResponse<MappedPermission>> call = RetrofitInstance.getRetrofitInstance(new GetClientPermissions(mappedClient.getWebID()));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<MappedPermission>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedPermission>> call, Response<AppointSyncResponse<MappedPermission>> response) {

                Controller.clientPermission = response.body().getData();

                updateView();
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<MappedPermission>> call, Throwable t) {

                System.out.println("FAILLL");

            }
        });


        singleClientPermissionRequestNewAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.clientPermission.getCanCreateAppointment().equals(1)) {
                    updateClientPermission("0", "1");
                } else if (Controller.clientPermission.getCanCreateAppointment().equals(0)) {
                    updateClientPermission("1", "1");
                }

            }

        });

        singleClientPermissionAcceptRequestedAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.clientPermission.getCanAcceptAppointment().equals(1)) {
                    updateClientPermission("0", "2");
                } else if (Controller.clientPermission.getCanAcceptAppointment().equals(0)) {
                    updateClientPermission("1", "2");
                }


            }

        });

        singleClientPermissionAmendAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.clientPermission.getCanAmendAppointment().equals(1)) {
                    updateClientPermission("0", "3");
                } else if (Controller.clientPermission.getCanAmendAppointment().equals(0)) {
                    updateClientPermission("1", "3");
                }


            }

        });

        singleClientPermissionCancelAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.clientPermission.getCanCancelAppointment().equals(1)) {
                    updateClientPermission("0", "4");
                } else if (Controller.clientPermission.getCanCancelAppointment().equals(0)) {
                    updateClientPermission("1", "4");
                }


            }

        });


        singleClientPermissionSettingsBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ClientPermissionSettings f = (ClientPermissionSettings) fm.findFragmentByTag("seeClientSettingsPermissionFragment");

                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });

        return view;
    }

    public void updateClientPermission(final String permissionValue, final String permission) {

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostUpdateClientPermissions(mappedClient.getWebID(),permission, permissionValue));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                if (permission.equals("1")) {
                    Controller.clientPermission.setCanCreateAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("2")) {
                    Controller.clientPermission.setCanAcceptAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("3")) {
                    Controller.clientPermission.setCanAmendAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("4")) {
                    Controller.clientPermission.setCanCancelAppointment(Integer.parseInt(permissionValue));
                }

                updateView();

                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void updateView(){


        if (Controller.clientPermission.getCanCreateAppointment().equals(1)) {
            singleClientPermissionRequestNewAppointment.setChecked(true);
        } else if (Controller.clientPermission.getCanCreateAppointment().equals(0)) {
            singleClientPermissionRequestNewAppointment.setChecked(false);
        }

        if (Controller.clientPermission.getCanAcceptAppointment().equals(1)) {
            singleClientPermissionAcceptRequestedAppointment.setChecked(true);
        } else if (Controller.clientPermission.getCanAcceptAppointment().equals(0)) {
            singleClientPermissionAcceptRequestedAppointment.setChecked(false);
        }

        if (Controller.clientPermission.getCanAmendAppointment().equals(1)) {
            singleClientPermissionAmendAppointment.setChecked(true);
        } else if (Controller.clientPermission.getCanAmendAppointment().equals(0)) {
            singleClientPermissionAmendAppointment.setChecked(false);
        }

        if (Controller.clientPermission.getCanCancelAppointment().equals(1)) {
            singleClientPermissionCancelAppointment.setChecked(true);
        } else if (Controller.clientPermission.getCanCancelAppointment().equals(0)) {
            singleClientPermissionCancelAppointment.setChecked(false);
        }
    }

}

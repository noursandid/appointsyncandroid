package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.ClientsObjects.ClientRecyclerAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Clients extends Fragment implements Observer {

//    private List<MappedClient> rv_list = new ArrayList<MappedClient>();
//    private RecyclerView recyclerView;
//    ClientRecyclerAdapter mAdapter = new ClientRecyclerAdapter(rv_list);

    private RecyclerView recyclerView;
    private ClientRecyclerAdapter adapter;
    private List<MappedClient> developersLists;
    String profilePicture, userName = "";
    public ButtonSelectedProtocol delegate;
    private SearchView searchView;
    private ImageView ClientAddNewClient;
    private SwipeRefreshLayout swipeClientRefresh;
    private ProgressDialog pd;
    private TextView clientListNoClients;
    private long mLastClickTime = 0;
//    adapter = new ClientRecyclerAdapter

    ////    ConstraintLayout ClientLayoutItem;
//
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    public Clients() {
        // Required empty public constructor
    }

    public void didFinishAddingClient() {
        fetchTimelineAsync(0);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_clients, container, false);

        swipeClientRefresh = view.findViewById(R.id.swipeClientRefresh);

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        SearchView svMovies = view.findViewById(R.id.svMovies);


        recyclerView = view.findViewById(R.id.home_rv);
        ClientAddNewClient = view.findViewById(R.id.ClientAddNewClient);
        clientListNoClients = view.findViewById(R.id.clientListNoClients);
//        ClientLayoutItem = view.findViewById(R.id.home_rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        developersLists = new ArrayList<>();

        if (Controller.clientList != null) {


            for (int i = 0; i < Controller.clientList.size(); i++) {

                developersLists.add(Controller.client = Controller.clientList.get(i));
                Collections.sort(developersLists, MappedClient.StuNameComparator);


            }


            adapter = new ClientRecyclerAdapter(developersLists, getActivity().getApplicationContext(), delegate);

            recyclerView.setAdapter(adapter);
            swipeClientRefresh.setRefreshing(false);
            pd.dismiss();


        } else {

            Call<AppointSyncResponse<ArrayList<MappedClient>>> call = RetrofitInstance.getRetrofitInstance(new GetClients());
            pd.show();
            call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClient>>>() {
                @Override
                public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Response<AppointSyncResponse<ArrayList<MappedClient>>> response) {

                    if (response.body().getErrorCode() == 0) {
                        if (Controller.clientList != null) {
                            Controller.clientList.clear();
                        }
                        if (developersLists != null) {
                            developersLists.clear();
                        }

                        Controller.clientList = response.body().getData();
//                Collections.sort(Controller.clientList.get);

                        for (Iterator<MappedClient> i = response.body().getData().iterator(); i.hasNext(); ) {
                            MappedClient mappedClient = i.next();

                            profilePicture = mappedClient.getProfilePic().toString();
                            if (profilePicture.isEmpty()) {
                                profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
                            }

                            developersLists.add(Controller.client = mappedClient);

                            Collections.sort(developersLists, MappedClient.StuNameComparator);
                            System.out.println("FML " + mappedClient.getFirstName());
                        }


                        adapter = new ClientRecyclerAdapter(developersLists, getActivity().getApplicationContext(), delegate);

                        recyclerView.setAdapter(adapter);
                        swipeClientRefresh.setRefreshing(false);
                        pd.dismiss();

                        if(Controller.clientList==null||Controller.clientList.size()==0){
                            clientListNoClients.setText("You don't have any clients.");
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Throwable t) {
                }

            });

        }

        svMovies.setActivated(true);
        svMovies.setQueryHint("Search Client");
        svMovies.onActionViewExpanded();
        svMovies.setIconified(false);
        svMovies.clearFocus();

        svMovies.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

//        final Clients minime = this;
        ClientAddNewClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

//                Intent intent = new Intent(getActivity(), AddClient.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("String3ade", minime);
//                intent.putExtras(bundle);
//                startActivity(intent);

                delegate.seeAddNewClient();

            }

        });

        swipeClientRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchTimelineAsync(0);

            }
        });

        swipeClientRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return view;

    }

//    @Override
//    public void onBackPressed() {
//        // close search view on back button pressed
//        if (!searchView.isIconified()) {
//            searchView.setIconified(true);
//            return;
//        }
//        super.onBackPressed();
//    }


    @Override
    public void onResume() {
        super.onResume();
        HideKeyboardFunction.hideKeyboard(getActivity());
    }

    @Override
    public void update() {


        System.out.println("SANDID");
        fetchTimelineAsync(0);
    }

    public void fetchTimelineAsync(int page) {
        // Send the network request to fetch the updated data
        // `client` here is an instance of Android Async HTTP
        // getHomeTimeline is an example endpoint.
        Call<AppointSyncResponse<ArrayList<MappedClient>>> call = RetrofitInstance.getRetrofitInstance(new GetClients());
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClient>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Response<AppointSyncResponse<ArrayList<MappedClient>>> response) {

                if (Controller.clientList != null) {
                    Controller.clientList.clear();
                }
                if (developersLists != null) {
                    developersLists.clear();
                }

                Controller.clientList = response.body().getData();
//                Collections.sort(Controller.clientList.get);

                for (Iterator<MappedClient> i = response.body().getData().iterator(); i.hasNext(); ) {
                    MappedClient mappedClient = i.next();

                    profilePicture = mappedClient.getProfilePic().toString();
                    if (profilePicture.isEmpty()) {
                        profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
                    }

                    developersLists.add(Controller.client = mappedClient);

                    Collections.sort(developersLists, MappedClient.StuNameComparator);

                    System.out.println("FML " + mappedClient.getFirstName());

                }


                adapter = new ClientRecyclerAdapter(developersLists, getActivity().getApplicationContext(), delegate);

                recyclerView.setAdapter(adapter);
                swipeClientRefresh.setRefreshing(false);

                if(Controller.clientList!=null||Controller.clientList.size()>0){
                    clientListNoClients.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Throwable t) {
            }

        });


    }
}

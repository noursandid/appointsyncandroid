package com.appointsync.appointsyncpro;


import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetAppointments;
import com.appointsync.appointsyncpro.Class.Main.GetNotification;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;
import com.appointsync.appointsyncpro.Class.Main.PostReadNotifications;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.NotificationAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notifications extends Fragment implements UpdateAllObserver {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<MappedNotification> notificationListNew;
    private List<MappedNotification> notificationListToday;
    private List<MappedNotification> notificationListThisMonth;
    private List<MappedNotification> notificationListOlder;
    private List<MappedNotification> notificationList;
    public ButtonSelectedProtocol delegate;
    private SwipeRefreshLayout swipeContainer;
    private TextView notificationNoNotification;

    private ProgressDialog pd;

    ////    ConstraintLayout ClientLayoutItem;
//
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public Notifications() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        NotificationManager manager = (NotificationManager) getContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();



        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerNotification);

        recyclerView = view.findViewById(R.id.notification_recycler_view);
        notificationNoNotification = view.findViewById(R.id.notificationNoNotification);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        Controller.registerAsAnObserverToUpdateAll(this);

        pd.show();

        getNotifications();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchTimelineAsync(0);

            }
        });


        return view;
    }


    @Override
    public void updateAll() {

        if(getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    getNotifications();
                }
            });
        }
    }

    public void getNotifications(){

        Call<AppointSyncResponse<ArrayList<MappedNotification>>> call = RetrofitInstance.getRetrofitInstance(new GetNotification());
//        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedNotification>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Response<AppointSyncResponse<ArrayList<MappedNotification>>> response) {

                if (response.body().getErrorCode() == 0) {

                    notificationListNew = new ArrayList<>();
                    notificationListToday = new ArrayList<>();
                    notificationListThisMonth = new ArrayList<>();
                    notificationListOlder = new ArrayList<>();
                    notificationList = new ArrayList<>();

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    String dateToday = dateFormat.format(date);


                    DateFormat dateFormatMonth = new SimpleDateFormat("yyyy/MM");
                    Date dateMonth = new Date();
                    String dateStringMonth = dateFormatMonth.format(dateMonth);

                    List<MappedNotification> mappedNotifications = response.body().getData();

                    int mappedNotificationsCounter = mappedNotifications.size();
                    for (int i = 0; i < mappedNotificationsCounter; i++) {

                        notificationList.add(mappedNotifications.get(i));

                        if (mappedNotifications.get(i).getStatus().equals("0")) {
                            notificationListNew.add(mappedNotifications.get(i));
                        } else if ((UnixDateConverter.UnixDateConverterDateDayCompare(mappedNotifications.get(i).getDate())).equals(dateToday)) {
                            notificationListToday.add(mappedNotifications.get(i));
                        } else if ((UnixDateConverter.UnixDateConverterDateMonthCompare(mappedNotifications.get(i).getDate())).equals(dateStringMonth)) {
                            notificationListThisMonth.add(mappedNotifications.get(i));
                        } else {
                            notificationListOlder.add(mappedNotifications.get(i));
                        }

//                    notificationList.add(mappedNotifications.get(i));
//                    System.out.println("Fragment NOTIFICATION->" + mappedNotifications.get(i).getDescription());

                    }


                    adapter = new NotificationAdapter(notificationList, notificationListNew, notificationListToday, notificationListThisMonth, notificationListOlder, getActivity().getApplicationContext(), delegate);


                    recyclerView.setAdapter(adapter);

                    pd.dismiss();

                    if(mappedNotifications.size() == 0){

                        notificationNoNotification.setText("You don't have any notifications.");

                    }
                }

                readNotifications();
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Throwable t) {

            }
        });
    }

    public void fetchTimelineAsync(int page) {
        // Send the network request to fetch the updated data
        // `client` here is an instance of Android Async HTTP
        // getHomeTimeline is an example endpoint.
        Call<AppointSyncResponse<ArrayList<MappedNotification>>> call = RetrofitInstance.getRetrofitInstance(new GetNotification());
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedNotification>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Response<AppointSyncResponse<ArrayList<MappedNotification>>> response) {

                if(response.body().getErrorCode() == 0) {

                    notificationListNew.clear();
                    notificationListToday.clear();
                    notificationListThisMonth.clear();
                    notificationListOlder.clear();
                    notificationList.clear();

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                    Date date = new Date();
                    String dateToday = dateFormat.format(date);


                    DateFormat dateFormatMonth = new SimpleDateFormat("yyyy/MM");
                    Date dateMonth = new Date();
                    String dateStringMonth = dateFormatMonth.format(dateMonth);

                    List<MappedNotification> mappedNotifications = response.body().getData();

                    int mappedNotificationsCounter = mappedNotifications.size();
                    for (int i = 0; i < mappedNotificationsCounter; i++) {

                        notificationList.add(mappedNotifications.get(i));

                        if (mappedNotifications.get(i).getStatus().equals("0")) {
                            notificationListNew.add(mappedNotifications.get(i));
                        } else if ((UnixDateConverter.UnixDateConverterDateDayCompare(mappedNotifications.get(i).getDate())).equals(dateToday)) {
                            notificationListToday.add(mappedNotifications.get(i));
                        } else if ((UnixDateConverter.UnixDateConverterDateMonthCompare(mappedNotifications.get(i).getDate())).equals(dateStringMonth)) {
                            notificationListThisMonth.add(mappedNotifications.get(i));
                        } else {
                            notificationListOlder.add(mappedNotifications.get(i));
                        }

//                    notificationList.add(mappedNotifications.get(i));
//                    System.out.println("Fragment NOTIFICATION->" + mappedNotifications.get(i).getDescription());

                    }

                    adapter.notifyDataSetChanged();
                    // ...the data has come back, a
                    // Now we call setRefreshing(false) to signal refresh has finished
                    swipeContainer.setRefreshing(false);

                    if(mappedNotifications.size() == 0){

                        notificationNoNotification.setVisibility(View.GONE);

                    }
                }

                readNotifications();
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Throwable t) {

            }

            public void onFailure(Throwable e) {
                Log.d("DEBUG", "Fetch timeline error: " + e.toString());
            }
        });
    }

    private void readNotifications() {
        Call<AppointSyncResponse<ArrayList<Success>>> call = RetrofitInstance.getRetrofitInstance(new PostReadNotifications());
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<Success>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<Success>>> call, Response<AppointSyncResponse<ArrayList<Success>>> response) {

                if (response.body().getErrorCode() == 0) {
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<Success>>> call, Throwable t) {

            }

            public void onFailure(Throwable e) {
                Log.d("DEBUG", "Fetch timeline error: " + e.toString());
            }
        });
    }

}

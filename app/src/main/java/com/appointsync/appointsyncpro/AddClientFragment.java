package com.appointsync.appointsyncpro;


import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.appointsync.appointsyncpro.Adapters.AddClientPageAdapter;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddClientFragment extends Fragment implements Observer,Subject {
    private AddClientPageAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ArrayList<Observer> observers = new ArrayList<>();
    private ImageView AddClientCloseView;
    private long mLastClickTime = 0;

    public AddClientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_client, container, false);

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        AddClientCloseView = view.findViewById(R.id.AddClientCloseView);
        adapter = new AddClientPageAdapter(getFragmentManager());
        AddOfflineClientTab addOfflineClientTab= new AddOfflineClientTab();
        AddOnlineClientTab addOnlineClientTab = new AddOnlineClientTab();
        addOnlineClientTab.register(this);
        addOfflineClientTab.register(this);
        adapter.addFragment(addOfflineClientTab, "Offline Client");
        adapter.addFragment(addOnlineClientTab, "Online Client");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);


        AddClientCloseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                AddClientFragment f = (AddClientFragment) fm.findFragmentByTag("addNewClientFragment");

                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });

        return view;
    }

    @Override
    public void update() {
        notifyObservers();
    }

    @Override
    public void register(Observer observer) {
        if (observer != null && !observers.contains(observer)){
            observers.add(observer);
        }
    }

    @Override
    public void unregister(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers){
            observer.update();
        }
    }

}

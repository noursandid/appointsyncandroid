package com.appointsync.appointsyncpro;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.Country;
import com.appointsync.appointsyncpro.Class.CountryList;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.PostAddOfflineClient;
import com.appointsync.appointsyncpro.Class.Main.PostLinkOfflineClient;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddOfflineClientTab extends Fragment implements Subject {
    private TextInputEditText fragmentAddOfflineClientFirstNameText, fragmentAddOfflineClientMiddleNameText, fragmentAddOfflineClientLastNameText;
    private Spinner spinnerAddOfflineClientGender, addOfflineClientSpinnerCountryCode;
    private Button dateOfBirthOfflineClientRegistration, addOfflineClientPost;
    private EditText addOfflineClientNumberRegistration;
    private String selectedGender;
    private DatePickerDialog datePickerDialog;
    private long mLastClickTime = 0;
    private List<Observer> observers = new ArrayList<>();

    public AddOfflineClientTab() {
        // Required empty public constructor
        observers = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_offline_client_tab, container, false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        fragmentAddOfflineClientFirstNameText = view.findViewById(R.id.fragmentAddOfflineClientFirstNameText);
        fragmentAddOfflineClientMiddleNameText = view.findViewById(R.id.fragmentAddOfflineClientMiddleNameText);
        fragmentAddOfflineClientLastNameText = view.findViewById(R.id.fragmentAddOfflineClientLastNameText);
        spinnerAddOfflineClientGender = view.findViewById(R.id.spinnerAddOfflineClientGender);
        addOfflineClientSpinnerCountryCode = view.findViewById(R.id.addOfflineClientSpinnerCountryCode);
        dateOfBirthOfflineClientRegistration = view.findViewById(R.id.dateOfBirthOfflineClientRegistration);
        addOfflineClientPost = view.findViewById(R.id.addOfflineClientPost);
        addOfflineClientNumberRegistration = view.findViewById(R.id.addOfflineClientNumberRegistration);


        InputStream inputStream = this.getResources().openRawResource(R.raw.countrycodes);
        String jsonString = readJsonFile(inputStream);

        Gson countryGson = new Gson();
        final CountryList countryList = countryGson.fromJson(jsonString, CountryList.class);

        final ArrayList<Country> allCountryList = countryList.countries;

        List<String> list = new ArrayList<String>();
        for (Country country : allCountryList) {

            list.add(country.name.toString() + " (+" + country.dial_code.toString() + ")");

        }

        ArrayAdapter<String> dataAdapterPhone = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        dataAdapterPhone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addOfflineClientSpinnerCountryCode.setAdapter(dataAdapterPhone);


        dateOfBirthOfflineClientRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                dateOfBirthOfflineClientRegistration.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


//        Gender Spinner

        String[] plants = new String[]{
                "Select a gender...",
                "Male",
                "Female",
                "Other"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerAddOfflineClientGender.setAdapter(spinnerArrayAdapter);

        spinnerAddOfflineClientGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGender = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
//                    Toast.makeText
//                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
//                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


        addOfflineClientPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (fragmentAddOfflineClientFirstNameText.getText().toString().equals("") || fragmentAddOfflineClientLastNameText.getText().toString().equals("") || selectedGender.equals("Select a gender...") ||
                        dateOfBirthOfflineClientRegistration.getText().toString().equals("Date of birth") || addOfflineClientNumberRegistration.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Make sure to fill all fields.", Toast.LENGTH_LONG).show();
                } else {

                    Integer countryCodeSpinnerSelect = addOfflineClientSpinnerCountryCode.getSelectedItemPosition();
                    String selectedCountryCodeSpinnerSelectedID = allCountryList.get(countryCodeSpinnerSelect).dial_code.toString();

                    AppointsyncDate date = new AppointsyncDate(dateOfBirthOfflineClientRegistration.getText().toString() + " 14:00");
                    String dateOfBirthUnixMS = date.getUnix();
                    String dateOfBirthUnix = dateOfBirthUnixMS.substring(0, dateOfBirthUnixMS.length() - 3);


                    Call<AppointSyncResponse<MappedClient>> call = RetrofitInstance.getRetrofitInstance(new PostAddOfflineClient(addOfflineClientNumberRegistration.getText().toString(), fragmentAddOfflineClientFirstNameText.getText().toString(),
                            fragmentAddOfflineClientLastNameText.getText().toString(), fragmentAddOfflineClientMiddleNameText.getText().toString(), dateOfBirthUnix, selectedGender, selectedCountryCodeSpinnerSelectedID));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<MappedClient>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedClient>> call, Response<AppointSyncResponse<MappedClient>> response) {

                            if (response.body().getErrorCode() == 0) {

                                Toast.makeText(getActivity(), "Client added successfully.", Toast.LENGTH_SHORT).show();
                                Controller.clientList.add(response.body().getData());
                                notifyObservers();
                                pd.dismiss();

                                FragmentManager fm = getFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                AddClientFragment f = (AddClientFragment) fm.findFragmentByTag("addNewClientFragment");
                                ft.remove(f);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                ft.commit();



                            } else {
                                pd.dismiss();
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure
                                (Call<AppointSyncResponse<MappedClient>> call, Throwable t) {

                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                        }
                    });
                }


            }

        });


        return view;
    }

    private String readJsonFile(InputStream inputStream) {
// TODO Auto-generated method stub
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte bufferByte[] = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(bufferByte)) != -1) {
                outputStream.write(bufferByte, 0, length);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {

        }
        return outputStream.toString();
    }

    @Override
    public void register(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(final Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

}

package com.appointsync.appointsyncpro;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.DatabaseAccess;
import com.appointsync.appointsyncpro.Adapters.PackagePagerAdapter;
import com.appointsync.appointsyncpro.Adapters.PackagePagerModel;
import com.appointsync.appointsyncpro.Adapters.RegistrationPackagesAdapter;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.LoginRequest;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedProfile;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationToken;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.Network.UploadFile;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.viewpagerindicator.CirclePageIndicator;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private String inputUsernameString = "", inputPasswordString = "", inputPasswordStringEncrypt;
    private long mLastClickTime = 0;
    private EditText inputUsername, inputPassword;
    private Button loginButton, registerButton;
    private TextView forgotPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        inputUsername = findViewById(R.id.loginUsername);
        inputPassword = findViewById(R.id.loginPassword);
        loginButton = findViewById(R.id.loginButton);
        registerButton = findViewById(R.id.registerButton);
        forgotPassword = findViewById(R.id.forgotPasswordTV);


        registerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Login.this, RegistrationPackages.class);
                startActivity(intent);


            }

        });


        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                try {
                    inputUsernameString = inputUsername.getText().toString();
                } catch (Exception e) {
                    Toast.makeText(Login.this, "Fill all fields! ", Toast.LENGTH_SHORT).show();
                }


                try {
                    inputPasswordString = inputPassword.getText().toString();
                } catch (Exception e) {
                    Toast.makeText(Login.this, "Fill all fields! ", Toast.LENGTH_SHORT).show();
                }

                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] data = md.digest(inputPasswordString.getBytes());
                    StringBuilder encryptedPassword = new StringBuilder();
                    for (int i = 0; i < data.length; i++) {
                        encryptedPassword.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    inputPasswordStringEncrypt = encryptedPassword.toString();
                    System.out.println(encryptedPassword);
                    System.out.println(inputPasswordString.toString());
                } catch (Exception e) {
                    System.out.println(e);
                }

                if (inputUsernameString == "" || inputPasswordString == "" || inputPasswordString == null || inputUsernameString == null || inputPasswordString.isEmpty() || inputUsernameString.isEmpty()) {

                    Toast.makeText(Login.this, "Fill all fields! ", Toast.LENGTH_SHORT).show();

                } else {

                    inputUsername.setEnabled(false);
                    inputPassword.setEnabled(false);
                    loginButton.setEnabled(false);
                    registerButton.setEnabled(false);
                    forgotPassword.setEnabled(false);

                    try {
                        Call<AppointSyncResponse<MappedProfile>> call = RetrofitInstance.getRetrofitInstance(new LoginRequest(inputUsernameString, inputPasswordStringEncrypt));
                        call.enqueue(new Callback<AppointSyncResponse<MappedProfile>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<MappedProfile>> call, Response<AppointSyncResponse<MappedProfile>> response) {
                                final int errorCode = response.body().getErrorCode();
                                if (errorCode == 0) {

                                    Controller.noActiveSubscription = false;

                                    FirebaseInstanceId.getInstance().getInstanceId()
                                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                    if (!task.isSuccessful()) {
                                                        return;
                                                    }

                                                    // Get new Instance ID token
                                                    String token = task.getResult().getToken();

                                                    Controller.loggedInuserToken = token;
                                                    Call<AppointSyncResponse<MappedProfile>> call = RetrofitInstance.getRetrofitInstance(new PostNotificationToken(token));
                                                    call.enqueue(new Callback<AppointSyncResponse<MappedProfile>>() {
                                                        @Override
                                                        public void onResponse(Call<AppointSyncResponse<MappedProfile>> call, Response<AppointSyncResponse<MappedProfile>> response) {

                                                            if (response.body().getErrorCode() == 0) {

                                                            }

                                                        }

                                                        @Override
                                                        public void onFailure(Call<AppointSyncResponse<MappedProfile>> call, Throwable t) {


                                                        }
                                                    });

                                                }
                                            });

                                    Controller.user = response.body().getData();
                                    RetrofitInstance retrofitInstance = new RetrofitInstance();
                                    retrofitInstance.userID = Controller.user.getId();
                                    UploadFile.userID = Controller.user.getId();

                                    saveToLoginTable(inputUsernameString, inputPasswordStringEncrypt);
                                    Intent intent = new Intent(Login.this, Dashboard.class);
                                    startActivity(intent);
                                } else if (errorCode == 476300) {
                                    inputUsername.setEnabled(true);
                                    inputPassword.setEnabled(true);
                                    loginButton.setEnabled(true);
                                    registerButton.setEnabled(true);
                                    forgotPassword.setEnabled(true);
                                    Toast.makeText(Login.this, "Invalid username or password!", Toast.LENGTH_LONG).show();
                                } else {
                                    inputUsername.setEnabled(true);
                                    inputPassword.setEnabled(true);
                                    loginButton.setEnabled(true);
                                    registerButton.setEnabled(true);
                                    forgotPassword.setEnabled(true);
                                    Toast.makeText(Login.this, "Something went wrong, please try again later. ", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<MappedProfile>> call, Throwable t) {

                                Toast.makeText(Login.this, "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();

                                inputUsername.setEnabled(true);
                                inputPassword.setEnabled(true);
                                loginButton.setEnabled(true);
                                registerButton.setEnabled(true);
                                forgotPassword.setEnabled(true);

                            }
                        });
                    } catch (Exception e) {
                        Toast.makeText(Login.this, "Fill all fields! ", Toast.LENGTH_SHORT).show();
                    }

                }
            }

        });


        forgotPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Intent intent = new Intent(Login.this, ForgotPassword.class);
                startActivity(intent);

            }

        });


//       Button registerHost = findViewById(R.id.registerButton);
//        registerHost.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(Login.this, RegisterPackages.class);
//                startActivity(intent);
//            }
//        });

    }

    public void saveToLoginTable(String username, String password) {
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.insertLogin(username, password);
        databaseAccess.close();
    }


}

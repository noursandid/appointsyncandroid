package com.appointsync.appointsyncpro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingHelp extends Fragment {


    public ProfileSettingHelp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_profile_setting_help, container, false);

//        final ImageView profileSettingSubscriptionBackArrow = view.findViewById(R.id.profileSettingSubscriptionBackArrow);
//
//
//        profileSettingSubscriptionBackArrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                ProfileSettingSubscription f = (ProfileSettingSubscription) fm.findFragmentByTag("profileSettingSubscriptionFragment");
//                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//                ft.remove(f);
//                ft.commit();
//
//
//            }
//        });



        return view;
    }

}

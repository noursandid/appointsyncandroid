package com.appointsync.appointsyncpro;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.os.storage.StorageManager;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetAppointmentAttachment;
import com.appointsync.appointsyncpro.Class.Main.GetAppointmentNotes;
import com.appointsync.appointsyncpro.Class.Main.GetMessage;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentAttachments;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentNotes;
import com.appointsync.appointsyncpro.Class.Main.MappedMessage;
import com.appointsync.appointsyncpro.Class.Main.MappedNote;
import com.appointsync.appointsyncpro.Class.Main.PostAppointmentNote;
import com.appointsync.appointsyncpro.Class.Main.RejectAppointment;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.AppointmentNotesAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.MessageViewAdapter;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.Network.UploadFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentNotes extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private MappedAppointment mappedAppointment;
    private List<MappedAppointmentNotes> mappedAppointmentNotesList;
    private List<MappedAppointmentAttachments> mappedAppointmentAttachmentsList;
    private ImageView appointmentNoteAddNote, appointmentNotesAddAttachment, appointmentNotesViewBackArrow;
    private long mLastClickTime = 0;
    private ProgressDialog pd;
    private SwipeRefreshLayout swipeAppointmentNotesRefresh;
    private Fragment minime;
    private String filePickerLocation = "";
    private File fileToDelete;
    private TextView appointmentNotesNoNotes;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2, PICK_PDF_FILE = 3;
    private static final int MY_CAMERA_REQUEST_CODE = 100;


    public AppointmentNotes() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_appointment_notes, container, false);

        mappedAppointment = (MappedAppointment) getArguments().getSerializable(AppointmentView.APPOINTMENT_OBJECT);

        minime = this;
        appointmentNoteAddNote = view.findViewById(R.id.appointmentNoteAddNote);
        appointmentNotesAddAttachment = view.findViewById(R.id.appointmentNotesAddAttachment);
        swipeAppointmentNotesRefresh = view.findViewById(R.id.appointmentNotesSwipeContainer);
        appointmentNotesViewBackArrow = view.findViewById(R.id.appointmentNotesViewBackArrow);
        appointmentNotesNoNotes = view.findViewById(R.id.appointmentNotesNoNotes);

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        pd.show();
        Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call = RetrofitInstance.getRetrofitInstance(new GetAppointmentNotes(mappedAppointment.getWebID()));
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call, Response<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> response) {
                if (mappedAppointmentNotesList != null) {
                    mappedAppointmentNotesList.clear();
                }
                List<MappedAppointmentNotes> mappedAppointmentNotes = response.body().getData();

                int mappedMessageCounter = mappedAppointmentNotes.size();
                for (int i = 0; i < mappedMessageCounter; i++) {

                    mappedAppointmentNotesList.add(mappedAppointmentNotes.get(i));

                }

                Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach = RetrofitInstance.getRetrofitInstance(new GetAppointmentAttachment(mappedAppointment.getWebID()));
                callAttach.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach, Response<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> response) {

                        if (response.body().getErrorCode() == 0) {
                            if (mappedAppointmentAttachmentsList != null) {
                                mappedAppointmentAttachmentsList.clear();
                            }

                            List<MappedAppointmentAttachments> mappedAppointmentAttachments = response.body().getData();

                            int mappedNoteAttachmentCounter = mappedAppointmentAttachments.size();
                            for (int i = 0; i < mappedNoteAttachmentCounter; i++) {

                                mappedAppointmentAttachmentsList.add(mappedAppointmentAttachments.get(i));

                            }

                            adapter = new AppointmentNotesAdapter(mappedAppointmentNotesList, mappedAppointmentAttachmentsList, getActivity().getApplicationContext());
                            recyclerView.setAdapter(adapter);
                            pd.dismiss();

                            if (mappedAppointmentNotesList.size() == 0 && mappedAppointmentAttachmentsList.size() == 0) {
                                appointmentNotesNoNotes.setText("No notes for this appointment.");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach, Throwable t) {
                    }

                });

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call, Throwable t) {
            }

        });


        recyclerView = view.findViewById(R.id.appointment_note_recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        mappedAppointmentNotesList = new ArrayList<>();
        mappedAppointmentAttachmentsList = new ArrayList<>();


        appointmentNotesAddAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                selectImage();

            }
        });


        appointmentNotesViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (minime != null) {
                    ft.remove(minime);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });


        appointmentNoteAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Add note");

                final EditText input = new EditText(getActivity());
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
                input.setSingleLine(false);  //add this
                input.setMaxLines(7);
                input.setGravity(Gravity.LEFT | Gravity.TOP);
                input.setHorizontalScrollBarEnabled(false); //this
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                builder.setView(input);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String appointmentNote = input.getText().toString();
                        if (!appointmentNote.equals("")||appointmentNote.length()>0) {
                            appointmentNote = appointmentNote.replaceAll("\n", "<br>");

                            pd.show();
                            Call<AppointSyncResponse<MappedAppointmentNotes>> call = RetrofitInstance.getRetrofitInstance(new PostAppointmentNote(mappedAppointment.getWebID(), appointmentNote));
                            call.enqueue(new Callback<AppointSyncResponse<MappedAppointmentNotes>>() {
                                @Override
                                public void onResponse(Call<AppointSyncResponse<MappedAppointmentNotes>> call, Response<AppointSyncResponse<MappedAppointmentNotes>> response) {
                                    if (response.body().getErrorCode() == 0) {
                                        updateNotesView();
                                        pd.dismiss();
                                    } else {
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                                        pd.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AppointSyncResponse<MappedAppointmentNotes>> call, Throwable t) {
                                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                    pd.dismiss();

                                }
                            });
                        }else{
                            Toast.makeText(getActivity(), "Please enter a note.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

        swipeAppointmentNotesRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchTimelineAsync(0);

            }
        });

        swipeAppointmentNotesRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        return view;
    }

    public void fetchTimelineAsync(int page) {

        updateNotesView();

    }

    private void updateNotesView() {

        Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call = RetrofitInstance.getRetrofitInstance(new GetAppointmentNotes(mappedAppointment.getWebID()));
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call, Response<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> response) {
                if (mappedAppointmentNotesList != null) {
                    mappedAppointmentNotesList.clear();
                }

                List<MappedAppointmentNotes> mappedAppointmentNotes = response.body().getData();

                int mappedMessageCounter = mappedAppointmentNotes.size();
                for (int i = 0; i < mappedMessageCounter; i++) {

                    mappedAppointmentNotesList.add(mappedAppointmentNotes.get(i));

                }

                Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach = RetrofitInstance.getRetrofitInstance(new GetAppointmentAttachment(mappedAppointment.getWebID()));
                callAttach.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach, Response<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> response) {
                        if (mappedAppointmentAttachmentsList != null) {
                            mappedAppointmentAttachmentsList.clear();
                        }

                        List<MappedAppointmentAttachments> mappedAppointmentAttachments = response.body().getData();

                        int mappedNoteAttachmentCounter = mappedAppointmentAttachments.size();
                        for (int i = 0; i < mappedNoteAttachmentCounter; i++) {

                            mappedAppointmentAttachmentsList.add(mappedAppointmentAttachments.get(i));

                        }

                        adapter = new AppointmentNotesAdapter(mappedAppointmentNotesList, mappedAppointmentAttachmentsList, getActivity().getApplicationContext());
                        recyclerView.setAdapter(adapter);

                        if (mappedAppointmentNotesList.size() != 0 || mappedAppointmentAttachmentsList.size() != 0) {
                            appointmentNotesNoNotes.setVisibility(View.GONE);
                        }

                    }


                    @Override
                    public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> callAttach, Throwable t) {
                    }

                });

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> call, Throwable t) {
            }

        });

        swipeAppointmentNotesRefresh.setRefreshing(false);

    }


    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            final int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Choose From Files", "Cancel"};
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {


                        if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                            dialog.dismiss();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {
                                    Log.v(TAG, "Permission is granted");

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                                } else {

                                    Log.v(TAG, "Permission is revoked");
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                    Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                                }
                            } else { //permission is automatically granted on sdk<23 upon installation
                                Log.v(TAG, "Permission is granted");

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);

                            }

                        } else {

                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_REQUEST_CODE);
//                                Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                        }
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG, "Permission is granted");

                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                            } else {

                                Log.v(TAG, "Permission is revoked");
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                            }
                        } else { //permission is automatically granted on sdk<23 upon installation
                            Log.v(TAG, "Permission is granted");

                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                        }
                    } else if (options[item].equals("Choose From Files")) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG, "Permission is granted");

                                Intent intent = new Intent();
                                intent.setType("application/pdf");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_FILE);

                            } else {

                                Log.v(TAG, "Permission is revoked");
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                            }
                        } else { //permission is automatically granted on sdk<23 upon installation
                            Log.v(TAG, "Permission is granted");

                            Intent intent = new Intent();
                            intent.setType("application/pdf");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_FILE);

                        }
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            filePickerLocation = "PICK_IMAGE_CAMERA";
            if (data != null) {
                try {

                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
//
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//                    Toast.makeText(getActivity(), "THIS IS SPARRTA" + TAG, Toast.LENGTH_LONG).show();
                    File destination = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");

                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    fileToDelete = file;
                    Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

                    uploadFile(uri);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            filePickerLocation = "PICK_IMAGE_GALLERY";
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    uploadFile(selectedImage);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            }
        } else if (requestCode == PICK_PDF_FILE) {
            filePickerLocation = "PICK_PDF_FILE";
            if (data != null) {
                File file = new File(getRealPathFromURI(getContext(), data.getData()));
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getAbsoluteFile());

                MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                Toast.makeText(getActivity(), "Uploading...", Toast.LENGTH_LONG).show();
                RequestBody appointmentWebID = RequestBody.create(MediaType.parse("text/plain"), mappedAppointment.getWebID().toString());

                Call<AppointSyncResponse<Success>> call = UploadFile.getImageRetrofitInstance().uploadAppointmentNoteAttachment(appointmentWebID, multipartBody);

                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {
                        System.out.println("SUCCEEDED");
                        if (response.body().getErrorCode() == 0) {
                            Toast.makeText(getActivity(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                            updateNotesView();
                        } else if(response.body().getErrorCode() == 120) {
                            Toast.makeText(getActivity(), "File size must be less than 2MB", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                        t.printStackTrace();
                    }
                });
            }
        }
    }

    public String getRealPathFromURI(final Context context, final Uri uri) {
        if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
            System.out.println(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + uri.getLastPathSegment().toString());
            return Environment.getExternalStorageDirectory().getPath().toString() + File.separator + uri.getLastPathSegment().toString();
        } else {
            //check for KITKAT or above
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {
                    return getDownloadFilePath(context, uri);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };
                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
            return null;
        }

    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getDownloadFilePath(Context context, Uri uri) {
        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                String fileName = cursor.getString(0);
                String path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                if (!TextUtils.isEmpty(path)) {
                    return path;
                }
            }
        } finally {
            cursor.close();
        }
        String id = DocumentsContract.getDocumentId(uri);
        if (id.startsWith("raw:")) {
            return id.replaceFirst("raw:", "");
        }
        Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads"), java.lang.Long.valueOf(id));
        return getDataColumn(context, contentUri, null, null);
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Log.v(TAG, "Permission is granted");

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                    } else {

                        Log.v(TAG, "Permission is revoked");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.v(TAG, "Permission is granted");

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                }

            } else {

                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
    }


    //end onRequestPermissionsResult


    private void uploadFile(Uri fileUri) {

        //creating a file
        File file = new File(getRealPathFromURI(getContext(), fileUri));

        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);


        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        RequestBody appointmentWebID = RequestBody.create(MediaType.parse("text/plain"), mappedAppointment.getWebID().toString());
        Toast.makeText(getActivity(), "Uploading...", Toast.LENGTH_LONG).show();
        Call<AppointSyncResponse<Success>> call = UploadFile.getImageRetrofitInstance().uploadAppointmentNoteAttachment(appointmentWebID, filePart);

        //finally performing the call
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {
                System.out.println("SUCCEEDED");
                if (response.body().getErrorCode() == 0) {
                    Toast.makeText(getActivity(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                    updateNotesView();
                } else if (response.body().getErrorCode() == 120) {
                    Toast.makeText(getActivity(), "File size must be less than 2MB", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                }
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }
        });
    }

}

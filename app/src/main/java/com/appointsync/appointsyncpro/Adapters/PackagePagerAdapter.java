package com.appointsync.appointsyncpro.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.R;
import com.appointsync.appointsyncpro.RegisterHostPremium;
import com.appointsync.appointsyncpro.RegisterHostPro;

import java.util.List;

/**
 * Created by cgsawma on 11/3/18.
 */

public class PackagePagerAdapter extends PagerAdapter {


    Context context;

    List<PackagePagerModel> pagerArr;
    LayoutInflater inflater;

    public PackagePagerAdapter(Context context, List<PackagePagerModel> pagerArr) {
        this.context = context;
        this.pagerArr = pagerArr;

        inflater = ((Activity) context).getLayoutInflater();
    }

    @Override
    public int getCount() {
        return pagerArr.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = inflater.inflate(R.layout.package_list_item, container, false);

        TextView tv = (TextView) view.findViewById(R.id.packageTitle);
        TextView tvt = (TextView) view.findViewById(R.id.packageBody);
        TextView tvtt = (TextView) view.findViewById(R.id.packagePrice);

        view.setTag(position);


        ((ViewPager) container).addView(view);

        final PackagePagerModel model = pagerArr.get(position);

        tv.setText(model.getTitle());
        tvt.setText(model.getText());
        tvtt.setText(model.getPrice());

        view.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
               String packageFlag = model.getId().toString();
               if(packageFlag == "1"){

                   Intent intent = new Intent(context, RegisterHostPro.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(intent);

               }else if(packageFlag == "2"){

                   Intent intent = new Intent(context, RegisterHostPremium.class);
                   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   context.startActivity(intent);

               }
            }
        });

        return view;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((View)object);

    }

}

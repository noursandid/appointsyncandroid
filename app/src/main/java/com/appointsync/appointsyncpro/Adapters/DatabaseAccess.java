package com.appointsync.appointsyncpro.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cgsawma on 11/9/18.
 */

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */



    public void insertLogin(String username, String password) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        String sql1 = "INSERT OR REPLACE INTO " + "LOGIN" + " (ID, USERNAME, PASSWORD) values('1', '"+username+"','"+password+"');";
        db.execSQL(sql1);
    }


    public List<String> getLogin() {
        List<String> login = new ArrayList<>();
        Cursor loginCursor = database.rawQuery("SELECT * FROM LOGIN", null);
        loginCursor.moveToFirst();
        while (!loginCursor.isAfterLast()) {
            login.add(loginCursor.getString(0));
            login.add(loginCursor.getString(1));
            login.add(loginCursor.getString(2));
            loginCursor.moveToNext();
        }
        loginCursor.close();
        return login;
    }


    public List<String> getQuotes() {
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM SQLITETEST", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            list.add(cursor.getString(1));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


    public List<String> deleteLogin() {
        List<String> login = new ArrayList<>();
        Cursor loginCursor = database.rawQuery("DELETE FROM LOGIN WHERE 1", null);
        loginCursor.moveToFirst();

        loginCursor.close();
        return login;
    }



}

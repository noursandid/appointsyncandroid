package com.appointsync.appointsyncpro.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.R;

import java.util.ArrayList;

/**
 * Created by cgsawma on 11/26/18.
 */

public class CustomListAdapter extends BaseAdapter {

    private static ArrayList<MappedClient> listContact;

    private LayoutInflater mInflater;

    public CustomListAdapter(Context photosFragment, ArrayList<MappedClient> results){
        listContact = results;
        mInflater = LayoutInflater.from(photosFragment);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listContact.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return listContact.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.listview_row, null);
            holder = new ViewHolder();
            holder.txtname = (TextView) convertView.findViewById(R.id.nameTextViewID);
            holder.txtphone = (TextView) convertView.findViewById(R.id.infoTextViewID);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtname.setText(listContact.get(position).getFirstName());
        holder.txtphone.setText(listContact.get(position).getUsername());

        return convertView;
    }

    static class ViewHolder{
        TextView txtname, txtphone;
    }

}



package com.appointsync.appointsyncpro.Adapters;

/**
 * Created by cgsawma on 11/3/18.
 */

public class PackagePagerModel {

    String id;
    String title;
    String text;
    String price;

    public PackagePagerModel(String id, String title, String text, String price) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.price = price;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}

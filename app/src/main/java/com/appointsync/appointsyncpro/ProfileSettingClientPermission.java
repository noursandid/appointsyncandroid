package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.LoginRequest;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetPermissions;
import com.appointsync.appointsyncpro.Class.Main.MappedPermission;
import com.appointsync.appointsyncpro.Class.Main.MappedProfile;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePermission;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingClientPermission extends Fragment {
    ProgressDialog pd;
    Switch clientPermissionRequestNewAppointment,clientPermissionAcceptRequestedAppointment,clientPermissionAmendAppointment,clientPermissionCancelAppointment;
    private long mLastClickTime = 0;
    public ProfileSettingClientPermission() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting_client_permission, container, false);

        final ImageView profileSettingClientPermissionBackArrow = view.findViewById(R.id.profileSettingClientPermissionBackArrow);

        pd = new ProgressDialog(getContext());

        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        clientPermissionRequestNewAppointment = view.findViewById(R.id.clientPermissionRequestNewAppointment);
        clientPermissionAcceptRequestedAppointment = view.findViewById(R.id.clientPermissionAcceptRequestedAppointment);
        clientPermissionAmendAppointment = view.findViewById(R.id.clientPermissionAmendAppointment);
        clientPermissionCancelAppointment = view.findViewById(R.id.clientPermissionCancelAppointment);


        Call<AppointSyncResponse<MappedPermission>> call = RetrofitInstance.getRetrofitInstance(new GetPermissions());
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<MappedPermission>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedPermission>> call, Response<AppointSyncResponse<MappedPermission>> response) {

                Controller.permissions = response.body().getData();
                updateView();
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<MappedPermission>> call, Throwable t) {

                System.out.println("FAILLL");

            }
        });

//

        clientPermissionRequestNewAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.permissions.getCanCreateAppointment().equals(1)) {
                    updatePermission("0", "1");
                } else if (Controller.permissions.getCanCreateAppointment().equals(0)) {
                    updatePermission("1", "1");
                }

            }

        });

        clientPermissionAcceptRequestedAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.permissions.getCanAcceptAppointment().equals(1)) {
                    updatePermission("0", "2");
                } else if (Controller.permissions.getCanAcceptAppointment().equals(0)) {
                    updatePermission("1", "2");
                }


            }

        });

        clientPermissionAmendAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.permissions.getCanAmendAppointment().equals(1)) {
                    updatePermission("0", "3");
                } else if (Controller.permissions.getCanAmendAppointment().equals(0)) {
                    updatePermission("1", "3");
                }


            }

        });

        clientPermissionCancelAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                if (Controller.permissions.getCanCancelAppointment().equals(1)) {
                    updatePermission("0", "4");
                } else if (Controller.permissions.getCanCancelAppointment().equals(0)) {
                    updatePermission("1", "4");
                }


            }

        });

        profileSettingClientPermissionBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingClientPermission f = (ProfileSettingClientPermission) fm.findFragmentByTag("profileSettingClientPermissionFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });


        return view;
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }

    public void updatePermission(final String permissionValue, final String permission) {

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostUpdatePermission(permissionValue, permission));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                if (permission.equals("1")) {
                    Controller.permissions.setCanCreateAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("2")) {
                    Controller.permissions.setCanAcceptAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("3")) {
                    Controller.permissions.setCanAmendAppointment(Integer.parseInt(permissionValue));
                } else if (permission.equals("4")) {
                    Controller.permissions.setCanCancelAppointment(Integer.parseInt(permissionValue));
                }

                updateView();

                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

                System.out.println("FAILLL");

            }
        });

    }

    public void updateView(){


        if (Controller.permissions.getCanAcceptAppointment().equals(1)) {
            clientPermissionAcceptRequestedAppointment.setChecked(true);
        } else if (Controller.permissions.getCanAcceptAppointment().equals(0)) {
            clientPermissionAcceptRequestedAppointment.setChecked(false);
        }

        if (Controller.permissions.getCanAmendAppointment().equals(1)) {
            clientPermissionAmendAppointment.setChecked(true);
        } else if (Controller.permissions.getCanAmendAppointment().equals(0)) {
            clientPermissionAmendAppointment.setChecked(false);
        }

        if (Controller.permissions.getCanCancelAppointment().equals(1)) {
            clientPermissionCancelAppointment.setChecked(true);
        } else if (Controller.permissions.getCanCancelAppointment().equals(0)) {
            clientPermissionCancelAppointment.setChecked(false);
        }

        if (Controller.permissions.getCanCreateAppointment().equals(1)) {
            clientPermissionRequestNewAppointment.setChecked(true);
        } else if (Controller.permissions.getCanCreateAppointment().equals(0)) {
            clientPermissionRequestNewAppointment.setChecked(false);
        }
    }

}

package com.appointsync.appointsyncpro;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ClientProfilePage extends AppCompatActivity {

    private String profilePicture = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile_page);

//
//        final ImageView profileImageView = findViewById(R.id.clientProfileImage);
//
//        final TextView clientName = findViewById(R.id.clientProfileFirstName);
//        final TextView clientNumber = findViewById(R.id.clientProfileNumber);
//        final TextView clientUsername = findViewById(R.id.clientProfileUsername);
//        final TextView clientEmail = findViewById(R.id.clientProfileEmail);
//        final TextView clientDOB = findViewById(R.id.clientProfileDOB);
//        final TextView clientGender = findViewById(R.id.clientProfileGender);
//        final TextView clientPhoneNumber = findViewById(R.id.clientProfilePhoneNumber);
//
//
//        final Button addAppointment = findViewById(R.id.clientProfileAddAppointment);
//        final Button clientFile = findViewById(R.id.clientProfileClientFile);
//        final Button sendMessage = findViewById(R.id.clientProfileSendMessage);
//        final Button settingsButton = findViewById(R.id.clientProfileSettings);
//        final Button showAppointment = findViewById(R.id.clientProfileShowAppointments);
//        final Button linkToOnlineAccount = findViewById(R.id.clientProfileLinkToOnlineAccount);
//
//        profileImageView.setVisibility(View.INVISIBLE);
//        clientName.setVisibility(View.INVISIBLE);
//
//        Intent intent = getIntent();
//        MappedClient mappedClient = (MappedClient) intent.getSerializableExtra(ClientRecyclerAdapter.CLIENT_OBJECT);
//
//        int clientType = mappedClient.getType();
//
//
//        if (mappedClient.getMiddleName().isEmpty()) {
//            clientName.setText("Name: " + mappedClient.getFirstName() + " " + mappedClient.getLastName());
//        } else {
//            clientName.setText("Name: " + mappedClient.getFirstName() + " " + mappedClient.getMiddleName() + " " + mappedClient.getLastName());
//        }
//
//        profilePicture = mappedClient.getProfilePic();
//        if (profilePicture.isEmpty()) {
//            profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
//        }
//
//        Picasso.get().load(profilePicture).into(profileImageView);
//
//        clientNumber.setText("Client Number: "+mappedClient.getClientNumber().toString());
//
//        if(clientType == 2) {
//            clientUsername.setText("Username: " + mappedClient.getUsername());
//            clientEmail.setText("Email: " + mappedClient.getEmail());
//            linkToOnlineAccount.setVisibility(View.GONE);
//        }else if(clientType == 4){
//            clientUsername.setVisibility(View.GONE);
//            clientEmail.setVisibility(View.GONE);
//            settingsButton.setVisibility(View.GONE);
//            sendMessage.setVisibility(View.GONE);
//        }
//
//        UnixDateConverter unixDateConverter = new UnixDateConverter();
//        String formattedDate = unixDateConverter.UnixDateConverter(mappedClient.getDob());
//
//
//        clientDOB.setText("Date of Birth: "+formattedDate);
//        clientGender.setText("Gender: "+mappedClient.getGender());
//        clientPhoneNumber.setText("Phone Number: "+mappedClient.getPhoneNumber().toString());
//
//        profileImageView.setVisibility(View.VISIBLE);
//        clientName.setVisibility(View.VISIBLE);
//
//        final ImageView cancelRegister = findViewById(R.id.clientBackArrow);
//        cancelRegister.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//
//                finish();
//
//            }
//
//        });
//
//
//
    }
}


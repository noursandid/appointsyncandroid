package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.PasswordConvertClass;
import com.appointsync.appointsyncpro.Class.Main.PostSecureFileFlag;
import com.appointsync.appointsyncpro.Class.Main.PostTemplatePin;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePermission;
import com.appointsync.appointsyncpro.Class.Main.PostVerifyPin;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.text.NumberFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingFileSecurity extends Fragment {
    ProgressDialog pd;
    String pinCode;
    Switch profileSettingSecurityPinSwitch;
    private long mLastClickTime = 0;
    public ProfileSettingFileSecurity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_profile_setting_file_security, container, false);

        pd = new ProgressDialog(getContext());

        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        final ImageView profileSettingFileSecurityBackArrow = view.findViewById(R.id.profileSettingFileSecurityBackArrow);


        profileSettingSecurityPinSwitch = view.findViewById(R.id.profileSettingSecurityPinSwitch);

        updateView();

        profileSettingSecurityPinSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.user.getHiddenFiles().equals(0)) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Set Pin");

                    final EditText input = new EditText(getActivity());
                    input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    builder.setView(input);
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (profileSettingSecurityPinSwitch.isChecked()) {
                                profileSettingSecurityPinSwitch.setChecked(false);
                            } else {
                                profileSettingSecurityPinSwitch.setChecked(true);
                            }

                        }
                    });
                    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pinCode = input.getText().toString();
                            if(pinCode.length()!=4){
                                Toast.makeText(getActivity(), "Pin code should be 4 numbers.", Toast.LENGTH_SHORT).show();
                                if (profileSettingSecurityPinSwitch.isChecked()) {
                                    profileSettingSecurityPinSwitch.setChecked(false);
                                } else {
                                    profileSettingSecurityPinSwitch.setChecked(true);
                                }
                            }else {
                                updateSecurityFlag("1");
                            }
                        }
                    });
                    builder.show();


                } else if (Controller.user.getHiddenFiles().equals(1)) {

                    final AlertDialog.Builder builderOff = new AlertDialog.Builder(getActivity());
                    builderOff.setTitle("Pin");

                    final EditText input = new EditText(getActivity());
//                input.setTransformationMethod((TransformationMethod) NumberFormatException.getInstance());
                    input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    builderOff.setView(input);
                    builderOff.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (profileSettingSecurityPinSwitch.isChecked()) {
                                profileSettingSecurityPinSwitch.setChecked(false);
                            } else {
                                profileSettingSecurityPinSwitch.setChecked(true);
                            }

                        }
                    });
                    builderOff.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pinCode = input.getText().toString();
                            if(pinCode.length()!=4){
                                Toast.makeText(getActivity(), "Pin code should be 4 numbers.", Toast.LENGTH_SHORT).show();
                            }else {
                                postVerifySecurityFilePin(PasswordConvertClass.convertedPassword(pinCode));
                            }
                        }
                    });
                    builderOff.show();


                }

            }
        });

        profileSettingFileSecurityBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingFileSecurity f = (ProfileSettingFileSecurity) fm.findFragmentByTag("profileSettingFileSecurityFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });



        return view;
    }

    public void updateSecurityFlag(final String securityFlag){

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostSecureFileFlag(securityFlag));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                if(securityFlag.equals("1")) {
                    postSecurityFilePin(PasswordConvertClass.convertedPassword(pinCode));
                    Controller.user.setHiddenFiles(1);
                }else if(securityFlag.equals("0")){
                    Toast.makeText(getActivity(), "Files no longer secured.", Toast.LENGTH_LONG).show();
                    Controller.user.setHiddenFiles(0);
                    updateView();
                }
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();

                if (profileSettingSecurityPinSwitch.isChecked()) {
                    profileSettingSecurityPinSwitch.setChecked(false);
                } else {
                    profileSettingSecurityPinSwitch.setChecked(true);
                }

            }
        });

    }


    public void postSecurityFilePin(String securityFilePin){

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostTemplatePin(securityFilePin));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                Controller.user.setHiddenFiles(1);
                Toast.makeText(getActivity(), "Your files are now secured.", Toast.LENGTH_LONG).show();
                updateView();
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void postVerifySecurityFilePin(String securityFilePin){

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostVerifyPin(securityFilePin));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                if(response.body().getErrorCode()!=0){
                    Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    updateView();
                }else if(response.body().getErrorCode() == 0) {
                    updateSecurityFlag("0");
                }
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void updateView(){
        if(Controller.user.getHiddenFiles().equals(1)){
            profileSettingSecurityPinSwitch.setChecked(true);
        }else{
            profileSettingSecurityPinSwitch.setChecked(false);
        }

    }

}

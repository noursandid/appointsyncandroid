package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.AcceptAppointment;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.CustomSearchableSpinner;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetThread;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.PostMessage;
import com.appointsync.appointsyncpro.ClientsObjects.ClientRecyclerAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.ThreadAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.RefreshThreadsProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class Messages extends Fragment implements RefreshThreadsProtocol {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<MappedThread> threadList;

    public ButtonSelectedProtocol delegate;
    private SwipeRefreshLayout threadSwipeContainer;
    private TextView availableThreads;
    private RefreshThreadsProtocol miniME;
    ImageView createNewThread;
    private ProgressDialog pd;
    private long mLastClickTime = 0;

    List<String> clients = new ArrayList<String>();
    List<String> selectedClient = new ArrayList<String>();

    ////    ConstraintLayout ClientLayoutItem;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            miniME = this;
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void didFinishAddingMessage() {
        fetchTimelineAsync(0);
    }

    public Messages() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        final View view = inflater.inflate(R.layout.fragment_messages, container, false);

        availableThreads = view.findViewById(R.id.noAvailableThreads);

        createNewThread = view.findViewById(R.id.createNewThread);

        recyclerView = view.findViewById(R.id.thread_recycler_view);
        threadSwipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.threadSwipeContainer);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        threadList = new ArrayList<>();

        if (Controller.threadList == null) {
            Call<AppointSyncResponse<ArrayList<MappedThread>>> call = RetrofitInstance.getRetrofitInstance(new GetThread());

            pd.show();
            call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedThread>>>() {
                @Override
                public void onResponse(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Response<AppointSyncResponse<ArrayList<MappedThread>>> response) {

                    if (response.body().getErrorCode() == 0) {
                        Controller.threadList = response.body().getData();

                        List<MappedThread> mappedThreads = Controller.threadList;

                        int threadListCounter = mappedThreads.size();

                        if (threadListCounter == 0) {

                            availableThreads.setText("You don't have any threads.");
                        }


                        for (int i = 0; i < threadListCounter; i++) {

                            threadList.add(mappedThreads.get(i));

                        }

// w
                        adapter = new ThreadAdapter(threadList, getActivity().getApplicationContext(), delegate, miniME);

                        recyclerView.setAdapter(adapter);
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    }
                    pd.dismiss();

                }

                @Override
                public void onFailure(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }

            });

        } else {
            adapter = new ThreadAdapter(Controller.threadList, getActivity().getApplicationContext(), delegate, miniME);
            recyclerView.setAdapter(adapter);
        }

        createNewThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = inflater.inflate(R.layout.popup_new_thread_view, null);
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                boolean focusable = true;
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
                popupWindow.setTouchable(true);

                clients.clear();
                selectedClient.clear();

                final pl.utkala.searchablespinner.SearchableSpinner searchableSpinner = popupView.findViewById(R.id.searchableSpinner);


//                searchableSpinner.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
//                            return;
//                        }
//                        mLastClickTime = SystemClock.elapsedRealtime();
//
//                    }
//                });

                final Button postNewThread = popupView.findViewById(R.id.postNewThread);
                final Button cancelNewThread = popupView.findViewById(R.id.cancelNewThread);


                final TextInputEditText newThreadTitleText = popupView.findViewById(R.id.newThreadTitleText);
                final EditText newThreadMessage = popupView.findViewById(R.id.newThreadMessage);

                searchableSpinner.setDialogTitle("Select Client");
                searchableSpinner.setDismissText("Cancel");

                if (Controller.clientList == null) {

                    Call<AppointSyncResponse<ArrayList<MappedClient>>> call = RetrofitInstance.getRetrofitInstance(new GetClients());
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClient>>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Response<AppointSyncResponse<ArrayList<MappedClient>>> response) {
                            Controller.clientList = response.body().getData();

                            if (response.body().getErrorCode() == 0) {

                                for (int i = 0; i < Controller.clientList.size(); i++) {

                                    if (Controller.clientList.get(i).getType() == 2 || Controller.clientList.get(i).getType() == 3) {

                                        clients.add(Controller.clientList.get(i).getUsername());
                                        selectedClient.add(Controller.clientList.get(i).getWebID());

                                    } else {

                                    }
                                }

                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, clients);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                searchableSpinner.setAdapter(dataAdapter);
                                pd.dismiss();

                            } else {
                                Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Throwable t) {
                            Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {

                    for (int i = 0; i < Controller.clientList.size(); i++) {

                        if (Controller.clientList.get(i).getType() == 2 || Controller.clientList.get(i).getType() == 3) {

                            clients.add(Controller.clientList.get(i).getUsername());
                            selectedClient.add(Controller.clientList.get(i).getWebID());

                        } else {

                        }

                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, clients);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    searchableSpinner.setAdapter(dataAdapter);

                }

                postNewThread.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();


                        final String newThreadTitleTextPost = newThreadTitleText.getText().toString();
                        String newThreadTitleMessagePost = newThreadMessage.getText().toString();
                        newThreadTitleMessagePost = newThreadTitleMessagePost.replaceAll("\n", "<br>");

                        int selectedSpinnerCLient = searchableSpinner.getSelectedItemPosition();
                        final String newThreadClientWebIdPost = selectedClient.get(selectedSpinnerCLient);

                        if (!newThreadTitleText.getText().toString().equals("") && !newThreadMessage.getText().toString().equals("")) {
                            Call<AppointSyncResponse<MappedThread>> call = RetrofitInstance.getRetrofitInstance(new PostMessage(newThreadTitleTextPost, newThreadTitleMessagePost, newThreadClientWebIdPost));
                            pd.show();
                            call.enqueue(new Callback<AppointSyncResponse<MappedThread>>() {
                                @Override
                                public void onResponse(Call<AppointSyncResponse<MappedThread>> call, Response<AppointSyncResponse<MappedThread>> response) {

                                    if (response.body().getErrorCode() == 0) {
                                        pd.dismiss();

                                        popupWindow.dismiss();

                                        System.out.print("THREAD POST IN SUCCESS");

                                        fetchTimelineAsync(0);

                                    } else {
                                        Toast.makeText(getActivity(), "Something went wrong! Please try again later.", Toast.LENGTH_LONG).show();
                                        pd.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AppointSyncResponse<MappedThread>> call, Throwable t) {


                                    pd.dismiss();

                                    popupWindow.dismiss();

                                    Toast.makeText(getActivity(), "Something went wrong! Please try again later.", Toast.LENGTH_LONG).show();

                                }
                            });

                        } else {
                            Toast.makeText(getActivity(), "Please make sure to fill all the fields.", Toast.LENGTH_LONG).show();
                        }
                    }
                });


                cancelNewThread.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();

                        popupWindow.dismiss();
                    }
                });


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupWindow.setElevation(20);
                }

                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

            }
        });


        threadSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchTimelineAsync(0);

            }
        });
        // Configure the refreshing colors
        threadSwipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        return view;
    }

    //    and this is the function
    public void fetchTimelineAsync(int page) {


        setView();
        threadSwipeContainer.setRefreshing(false);
        adapter.notifyDataSetChanged();

    }

    public void setView() {

        Call<AppointSyncResponse<ArrayList<MappedThread>>> call = RetrofitInstance.getRetrofitInstance(new GetThread());
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedThread>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Response<AppointSyncResponse<ArrayList<MappedThread>>> response) {

                if (response.body().getErrorCode() == 0) {
                    if (Controller.threadList != null) {
                        Controller.threadList.clear();
                    }
                    if (threadList != null) {
                        threadList.clear();
                    }
                    Controller.threadList = response.body().getData();

                    List<MappedThread> mappedThreadsRefreshed = Controller.threadList;

                    int threadListCounterRefreshed = mappedThreadsRefreshed.size();
                    if (threadListCounterRefreshed == 0) {

                        availableThreads.setText("You don't have any threads.");
                    } else {
                        availableThreads.setVisibility(View.GONE);
                    }


                    for (int i = 0; i < threadListCounterRefreshed; i++) {

                        threadList.add(mappedThreadsRefreshed.get(i));

                    }

                    adapter = new ThreadAdapter(threadList, getActivity().getApplicationContext(), delegate, miniME);
                    recyclerView.setAdapter(adapter);
                } else {
                    Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong, please try again.", Toast.LENGTH_SHORT).show();
            }

        });

    }


}

package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.DatabaseAccess;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Calendar.SCalendar;
import com.appointsync.appointsyncpro.Class.Main.AcceptAppointment;
import com.appointsync.appointsyncpro.Class.Main.CancelAmendment;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.DeleteAppointment;
import com.appointsync.appointsyncpro.Class.Main.GetLocation;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedLocation;
import com.appointsync.appointsyncpro.Class.Main.PasswordConvertClass;
import com.appointsync.appointsyncpro.Class.Main.RejectAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.ClientAppointmentAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.DayViewAppointmentAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.NotificationAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarDayView;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppointmentView extends Fragment implements Observer, UpdateCalendarView, UpdateCalendarDayView, UpdateAllObserver {
    ImageView appointmentViewBackArrow;
    private String profilePicture = "";
    ButtonSelectedProtocol delegate;
    String appointmentLocationName = "";
    public static final String APPOINTMENT_OBJECT = "appointmentObject";

    AlertDialog.Builder builder;
    private long mLastClickTime = 0;
    private List<Observer> observers = new ArrayList<>();

    private static final String STATE_TEXTVIEW = "STATE_TEXTVIEW";
    private TextView textView;
    private ProgressDialog pd;

    private MappedAppointment mappedAppointment;

    //    Appointment View
    private ImageView profileImageView, appointmentNotes;
    private TextView appointmentViewProfileName, appointmentViewEmail, appointmentViewProfileGender, appointmentViewPhoneNumber, appointmentViewLocation, appointmentViewDateFrom,
            appointmentViewDateTo, appointmentViewStatus, appointmentRejectedBy, appointmentCommentBox, appointmentAmendedViewDateTo, appointmentAmendedViewDateFrom, appointmentAmendedViewLocation;
    private Button appointmentCancelButton, appointmentAcceptButton, appointmentAmendButton, appointmentRejectButton, appointmentRemoveButton, appointmentCancelAmendButton;
    private CardView appointmentViewProfileCardView;
    private Fragment minime;
    private Integer appointmentStatus = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public AppointmentView() {
        // Required empty public constructor


//        asdaasdadasd
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_appointment_view, container, false);

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        minime = this;
        mappedAppointment = (MappedAppointment) getArguments().getSerializable(ClientAppointmentAdapter.APPOINTMENT_OBJECT);
        if (mappedAppointment == null) {
            mappedAppointment = (MappedAppointment) getArguments().getSerializable(NotificationAdapter.NOTIFICATION_OBJECT);
        }
        if (mappedAppointment == null) {
            mappedAppointment = (MappedAppointment) getArguments().getSerializable(DayViewAppointmentAdapter.DAY_VIEW_APPOINTMENT_OBJECT);
        }

        builder = new AlertDialog.Builder(getActivity());
        appointmentViewBackArrow = view.findViewById(R.id.appointmentViewBackArrow);

        appointmentNotes = view.findViewById(R.id.appointmentNotesButton);
        profileImageView = view.findViewById(R.id.appointmentViewProfileImage);

        appointmentViewProfileName = view.findViewById(R.id.appointmentViewProfileName);
        appointmentViewEmail = view.findViewById(R.id.appointmentViewEmail);
        appointmentViewProfileGender = view.findViewById(R.id.appointmentViewProfileGender);
        appointmentViewPhoneNumber = view.findViewById(R.id.appointmentViewPhoneNumber);
        appointmentViewLocation = view.findViewById(R.id.appointmentViewLocation);
        appointmentViewDateFrom = view.findViewById(R.id.appointmentViewDateFrom);
        appointmentViewDateTo = view.findViewById(R.id.appointmentViewDateTo);
        appointmentViewStatus = view.findViewById(R.id.appointmentViewStatus);
        appointmentRejectedBy = view.findViewById(R.id.appointmentRejectedBy);
        appointmentCommentBox = view.findViewById(R.id.appointmentCommentBox);

        appointmentAmendedViewLocation = view.findViewById(R.id.appointmentAmendedViewLocation);
        appointmentAmendedViewDateFrom = view.findViewById(R.id.appointmentAmendedViewDateFrom);
        appointmentAmendedViewDateTo = view.findViewById(R.id.appointmentAmendedViewDateTo);

        appointmentCancelButton = view.findViewById(R.id.appointmentCancelButton);
        appointmentAcceptButton = view.findViewById(R.id.appointmentAcceptButton);
        appointmentAmendButton = view.findViewById(R.id.appointmentAmendButton);
        appointmentRejectButton = view.findViewById(R.id.appointmentRejectButton);
        appointmentRemoveButton = view.findViewById(R.id.appointmentRemoveButton);
        appointmentNotes = view.findViewById(R.id.appointmentNotesButton);
        appointmentCancelAmendButton = view.findViewById(R.id.appointmentCancelAmendButton);


        appointmentViewProfileCardView = view.findViewById(R.id.appointmentViewProfileCardView);


        updateAppointmentView();

        appointmentViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (minime != null) {
                    ft.remove(minime);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();


            }
        });

        appointmentAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Accept Appointment");
                builder.setMessage("Are you sure you want to Accept the appointment?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pd.show();
                                Call<AppointSyncResponse<MappedAppointment>> call = RetrofitInstance.getRetrofitInstance(new AcceptAppointment(mappedAppointment.getWebID()));
                                call.enqueue(new Callback<AppointSyncResponse<MappedAppointment>>() {
                                    @Override
                                    public void onResponse(Call<AppointSyncResponse<MappedAppointment>> call, Response<AppointSyncResponse<MappedAppointment>> response) {


                                        Controller.user.calendar.removeAppointment(mappedAppointment);
                                        mappedAppointment = response.body().getData();
                                        Controller.user.calendar.addAppointment(mappedAppointment);

                                        Toast.makeText(getActivity(), "Appointment Accepted.", Toast.LENGTH_SHORT).show();
                                        notifyObservers();
                                        pd.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<AppointSyncResponse<MappedAppointment>> call, Throwable t) {
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();

                                    }
                                });

                            }
                        });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        });


        appointmentNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment);
                AppointmentNotes appointmentNotes = new AppointmentNotes();
                appointmentNotes.setArguments(bundle);

                delegate.seeAppointmentNotes(appointmentNotes);
            }

        });


        appointmentAmendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment);
                AmendAppointment amendAppointment = new AmendAppointment();
                amendAppointment.setArguments(bundle);

                delegate.seeAmendAppointment(amendAppointment);


            }
        });


        appointmentRejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Reject reason");

                final EditText input = new EditText(getActivity());
                input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100)});
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                builder.setView(input);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String rejectReason = input.getText().toString();

                        pd.show();
                        Call<AppointSyncResponse<MappedAppointment>> call = RetrofitInstance.getRetrofitInstance(new RejectAppointment(mappedAppointment.getWebID(), rejectReason));
                        call.enqueue(new Callback<AppointSyncResponse<MappedAppointment>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<MappedAppointment>> call, Response<AppointSyncResponse<MappedAppointment>> response) {

                                Controller.user.calendar.removeAppointment(mappedAppointment);
                                mappedAppointment.update(response.body().getData());
                                Controller.user.calendar.addAppointment(mappedAppointment);

                                FragmentManager fm = getFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();

                                if (Controller.isInAppointmentView != null) {
                                    if (Controller.isInAppointmentView.equals("Calendar")) {
                                        if (fm.findFragmentByTag("appointmentView") != null) {
                                            AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                            ft.remove(f);
                                        }
                                        if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                            AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                            ft.remove(fn);
                                        }
                                        if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                            ft.remove(fca);
                                        }
                                        if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                            ft.remove(fca);
                                        }
                                    }
                                    if (Controller.isInAppointmentView.equals("Client")) {
                                        if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                            ft.remove(fca);
                                        }
                                        if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                            AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                            ft.remove(fd);
                                        }
                                        if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                            AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                            ft.remove(fn);
                                        }
                                        if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                            ft.remove(fca);
                                        }
                                    }
                                    if (Controller.isInAppointmentView.equals("Notification")) {
                                        if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                            AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                            ft.remove(fd);
                                        }
                                        if (fm.findFragmentByTag("appointmentView") != null) {
                                            AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                            ft.remove(f);
                                        }
                                        if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                            ft.remove(fca);
                                        }
                                        if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                            AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                            ft.remove(fca);
                                        }

                                    }
                                }

                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                ft.commit();

                                Toast.makeText(getActivity(), "Appointment Rejected.", Toast.LENGTH_SHORT).show();
                                notifyObservers();
                                pd.dismiss();
                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<MappedAppointment>> call, Throwable t) {
                                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                pd.dismiss();

                            }
                        });

                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });


        appointmentCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Cancel Appointment");
                builder.setMessage("Are you sure you want to Cancel the appointment?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pd.show();
                                Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new DeleteAppointment(mappedAppointment.getWebID()));
                                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                                    @Override
                                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {


                                        Controller.user.calendar.removeAppointment(mappedAppointment);

                                        Toast.makeText(getActivity(), "Appointment Canceled.", Toast.LENGTH_SHORT).show();
                                        update();
                                        notifyObservers();

                                        pd.dismiss();

                                        FragmentManager fm = getFragmentManager();
                                        FragmentTransaction ft = fm.beginTransaction();

                                        if (Controller.isInAppointmentView != null) {
                                            if (Controller.isInAppointmentView.equals("Calendar")) {
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Client")) {
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Notification")) {
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }

                                            }
                                        }
                                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                        ft.commit();
                                    }

                                    @Override
                                    public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();

                                    }
                                });

                            }
                        });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        });


        appointmentCancelAmendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Cancel Amendment");
                builder.setMessage("Are you sure you want to cancel the amendment?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pd.show();
                                Call<AppointSyncResponse<MappedAppointment>> call = RetrofitInstance.getRetrofitInstance(new CancelAmendment(mappedAppointment.getWebID()));
                                call.enqueue(new Callback<AppointSyncResponse<MappedAppointment>>() {
                                    @Override
                                    public void onResponse(Call<AppointSyncResponse<MappedAppointment>> call, Response<AppointSyncResponse<MappedAppointment>> response) {


                                        mappedAppointment.update(response.body().getData());
                                        Controller.user.calendar.removeAppointment(mappedAppointment);
                                        Controller.user.calendar.addAppointment(response.body().getData());
//

                                        Toast.makeText(getActivity(), "Amendment Canceled.", Toast.LENGTH_SHORT).show();
                                        notifyObservers();

                                        FragmentManager fm = getFragmentManager();
                                        FragmentTransaction ft = fm.beginTransaction();

                                        if (Controller.isInAppointmentView != null) {
                                            if (Controller.isInAppointmentView.equals("Calendar")) {
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Client")) {
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Notification")) {
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }

                                            }
                                        }

                                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                        ft.commit();

                                        pd.dismiss();
                                    }

                                    @Override
                                    public void onFailure(Call<AppointSyncResponse<MappedAppointment>> call, Throwable t) {
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();

                                    }
                                });

                            }
                        });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        });


        appointmentRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Remove Appointment");
                builder.setMessage("Are you sure you want to Remove the appointment?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                pd.show();
                                Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new DeleteAppointment(mappedAppointment.getWebID()));
                                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                                    @Override
                                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {


                                        Controller.user.calendar.removeAppointment(mappedAppointment);

                                        Toast.makeText(getActivity(), "Appointment Removed.", Toast.LENGTH_SHORT).show();
                                        notifyObservers();

                                        pd.dismiss();

                                        FragmentManager fm = getFragmentManager();
                                        FragmentTransaction ft = fm.beginTransaction();

                                        if (Controller.isInAppointmentView != null) {
                                            if (Controller.isInAppointmentView.equals("Calendar")) {
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Client")) {
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                                                    AppointmentView fn = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                                                    ft.remove(fn);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }
                                            }
                                            if (Controller.isInAppointmentView.equals("Notification")) {
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fd = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fd);
                                                }
                                                if (fm.findFragmentByTag("appointmentView") != null) {
                                                    AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentView");
                                                    ft.remove(f);
                                                }
                                                if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                                                    ft.remove(fca);
                                                }
                                                if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                                                    AppointmentView fca = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                                                    ft.remove(fca);
                                                }

                                            }
                                        }
                                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                        ft.commit();
                                    }

                                    @Override
                                    public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();

                                    }
                                });

                            }
                        });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();

            }
        });
        Controller.registerAsAnObserverToUpdateAll(this);

        return view;
    }

    @Override
    public void updateAll() {

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateAppointmentView();
                }
            });
        }
    }

    @Override
    public void update() {
        updateAppointmentView();
    }

    public void updateAppointmentView() {

        appointmentStatus = mappedAppointment.getStatus();
        String appointmentStatusText = "";
        long unixTime = System.currentTimeMillis() / 1000L;


        if (mappedAppointment.getEndDate() < unixTime) {
            appointmentStatus = 6;
            appointmentStatusText = "Passed";
        }


        appointmentCancelButton.setVisibility(View.GONE);
        appointmentAcceptButton.setVisibility(View.GONE);
        appointmentAmendButton.setVisibility(View.GONE);
        appointmentRejectButton.setVisibility(View.GONE);
        appointmentRemoveButton.setVisibility(View.GONE);
        appointmentCancelAmendButton.setVisibility(View.GONE);

        appointmentAmendedViewDateTo.setVisibility(View.GONE);
        appointmentAmendedViewDateFrom.setVisibility(View.GONE);
        appointmentAmendedViewLocation.setVisibility(View.GONE);

        profilePicture = mappedAppointment.getClient().getProfilePic();
        if (profilePicture.isEmpty()) {
            profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
        }

        Picasso.get().load(profilePicture).fit().centerCrop().into(profileImageView);

        if (mappedAppointment.getClient().getMiddleName().isEmpty()) {
            appointmentViewProfileName.setText("Name: " + mappedAppointment.getClient().getFirstName() + " " + mappedAppointment.getClient().getLastName());
        } else if (!mappedAppointment.getClient().getMiddleName().isEmpty()) {
            appointmentViewProfileName.setText("Name: " + mappedAppointment.getClient().getFirstName() + " " + mappedAppointment.getClient().getMiddleName() + " " + mappedAppointment.getClient().getLastName());
        }

        if (mappedAppointment.getClient().getType().equals(4)) {
            appointmentViewEmail.setVisibility(View.GONE);
        } else {
            appointmentViewEmail.setText("Email: " + mappedAppointment.getClient().getEmail());
        }
        appointmentViewProfileGender.setText("Gender: " + mappedAppointment.getClient().getGender());
        appointmentViewPhoneNumber.setText("Phone Number: " + mappedAppointment.getClient().getPhoneNumber());
        appointmentViewLocation.setText("Location: ");

        if (Controller.locationList == null) {

            Call<AppointSyncResponse<ArrayList<MappedLocation>>> call = RetrofitInstance.getRetrofitInstance(new GetLocation());

            call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedLocation>>>() {
                @Override
                public void onResponse(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Response<AppointSyncResponse<ArrayList<MappedLocation>>> response) {

                    Controller.locationList = response.body().getData();
                    System.out.println("IN RESPONSE LOVATION");

                    if (mappedAppointment.getAmendedAppointment() != null && !mappedAppointment.getAmendedAppointment().getLocationID().equals(mappedAppointment.getLocationID())) {
                        if (mappedAppointment.getAmendedAppointment().getLocationID() != null) {

                            if (appointmentStatus != 6) {
                                appointmentAmendedViewLocation.setVisibility(View.VISIBLE);
                            }
                            List<MappedLocation> mappedLocationInResponse = Controller.getLocation(mappedAppointment.getLocationID());
                            List<MappedLocation> mappedAmendedLocationInResponse = Controller.getLocation(mappedAppointment.getAmendedAppointment().getLocationID());

                            int locationIndexInResponse = mappedLocationInResponse.size();
                            for (int i = 0; i < locationIndexInResponse; i++) {

                                appointmentViewLocation.setText("Location: " + mappedLocationInResponse.get(i).getLocationTitle());

                            }

                            int locationAmendedIndexInResponse = mappedAmendedLocationInResponse.size();
                            for (int i = 0; i < locationAmendedIndexInResponse; i++) {

                                appointmentAmendedViewLocation.setText("Location: " + mappedAmendedLocationInResponse.get(i).getLocationTitle());
                                appointmentLocationName = mappedAmendedLocationInResponse.get(i).getLocationTitle();
                            }
                        }
                    } else {
                        List<MappedLocation> mappedLocationInResponse = Controller.getLocation(mappedAppointment.getLocationID());

                        int locationIndexInResponse = mappedLocationInResponse.size();
                        for (int i = 0; i < locationIndexInResponse; i++) {

                            appointmentViewLocation.setText("Location: " + mappedLocationInResponse.get(i).getLocationTitle());
                            appointmentLocationName = mappedLocationInResponse.get(i).getLocationTitle();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Throwable t) {
                    System.out.println("IN FAIL Location");
                }

            });

        } else {
            if (mappedAppointment.getAmendedAppointment() != null && !mappedAppointment.getAmendedAppointment().getLocationID().equals(mappedAppointment.getLocationID())) {
                if (mappedAppointment.getAmendedAppointment().getLocationID() != null) {
                    if (appointmentStatus != 6) {
                        appointmentAmendedViewLocation.setVisibility(View.VISIBLE);
                    }


                    List<MappedLocation> mappedLocationInResponse = Controller.getLocation(mappedAppointment.getLocationID());
                    List<MappedLocation> mappedAmendedLocationInResponse = Controller.getLocation(mappedAppointment.getAmendedAppointment().getLocationID());


                    int locationIndexInResponse = mappedLocationInResponse.size();
                    for (int i = 0; i < locationIndexInResponse; i++) {

                        appointmentViewLocation.setText("Location: " + mappedLocationInResponse.get(i).getLocationTitle());

                    }

                    int locationAmendedIndexInResponse = mappedAmendedLocationInResponse.size();
                    for (int i = 0; i < locationAmendedIndexInResponse; i++) {

                        appointmentAmendedViewLocation.setText("Location: " + mappedAmendedLocationInResponse.get(i).getLocationTitle());
                        appointmentLocationName = mappedAmendedLocationInResponse.get(i).getLocationTitle();
                    }


                }
            } else {

                List<MappedLocation> mappedLocation = Controller.getLocation(mappedAppointment.getLocationID());
                int locationIndex = mappedLocation.size();
                for (int i = 0; i < locationIndex; i++) {
                    appointmentViewLocation.setText("Location: " + mappedLocation.get(i).getLocationTitle());
                    appointmentLocationName = mappedLocation.get(i).getLocationTitle();

                }
            }
        }


        UnixDateConverter unixDateConverter = new UnixDateConverter();
        final String appointmentStartTime = unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getStartDate());
        final String appointmentEndTime = unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getEndDate());

        appointmentViewDateFrom.setText("From: " + appointmentStartTime);
        appointmentViewDateTo.setText("To: " + appointmentEndTime);
        if (mappedAppointment.getAmendedAppointment() != null) {
            if (mappedAppointment.getAmendedAppointment().getStartDate() != null && !mappedAppointment.getAmendedAppointment().getStartDate().equals(mappedAppointment.getStartDate())) {
                appointmentAmendedViewDateFrom.setVisibility(View.VISIBLE);
                appointmentAmendedViewDateFrom.setText("From: " + unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getAmendedAppointment().getStartDate()));
            }
            if (mappedAppointment.getAmendedAppointment().getEndDate() != null && !mappedAppointment.getAmendedAppointment().getEndDate().equals(mappedAppointment.getEndDate())) {
                appointmentAmendedViewDateTo.setVisibility(View.VISIBLE);
                appointmentAmendedViewDateTo.setText("To: " + unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getAmendedAppointment().getEndDate()));
            }

        }

        if (appointmentStatus.equals(1)) {
            appointmentStatusText = "Requested";
            String requesterPerson = mappedAppointment.getRequesterID();
            String requesterName = "";
            if (requesterPerson.equals(Controller.user.getId())) {
                requesterName = "You";
                appointmentAcceptButton.setVisibility(View.GONE);
                appointmentAmendButton.setVisibility(View.GONE);
                appointmentRejectButton.setVisibility(View.GONE);
                appointmentRemoveButton.setVisibility(View.GONE);
                appointmentCancelAmendButton.setVisibility(View.GONE);
                appointmentCancelButton.setVisibility(View.VISIBLE);
            } else {
                requesterName = mappedAppointment.getClient().getFirstName() + " " + mappedAppointment.getClient().getLastName();
                appointmentCancelButton.setVisibility(View.GONE);
                appointmentRemoveButton.setVisibility(View.GONE);
                appointmentCancelAmendButton.setVisibility(View.GONE);
                appointmentAcceptButton.setVisibility(View.VISIBLE);
                appointmentAmendButton.setVisibility(View.VISIBLE);
                appointmentRejectButton.setVisibility(View.VISIBLE);
            }
            appointmentRejectedBy.setText("Requested by: " + requesterName);
            appointmentCommentBox.setText("Comment: " + mappedAppointment.getRequestComment());
        } else if (appointmentStatus.equals(2)) {
            appointmentStatusText = "Pending";
            String requesterPerson = mappedAppointment.getAmendedAppointment().getAmendingUserID();
            String requesterName = "";
            if (requesterPerson.equals(Controller.user.getId())) {
                requesterName = "You";
                appointmentRejectedBy.setVisibility(View.VISIBLE);
                appointmentAcceptButton.setVisibility(View.GONE);
                appointmentAmendButton.setVisibility(View.GONE);
                appointmentRejectButton.setVisibility(View.GONE);
                appointmentRemoveButton.setVisibility(View.GONE);
                appointmentCancelAmendButton.setVisibility(View.VISIBLE);
                appointmentCancelButton.setVisibility(View.VISIBLE);
            } else {
                requesterName = mappedAppointment.getClient().getFirstName() + " " + mappedAppointment.getClient().getLastName();
                appointmentRejectedBy.setVisibility(View.VISIBLE);
                appointmentAcceptButton.setVisibility(View.VISIBLE);
                appointmentAmendButton.setVisibility(View.VISIBLE);
                appointmentRejectButton.setVisibility(View.GONE);
                appointmentRemoveButton.setVisibility(View.GONE);
                appointmentCancelAmendButton.setVisibility(View.GONE);
                appointmentCancelButton.setVisibility(View.VISIBLE);
            }
            appointmentRejectedBy.setText("Amended by: " + requesterName);
            appointmentCommentBox.setText("Comment: " + mappedAppointment.getRequestComment());
        } else if (appointmentStatus.equals(3)) {
            appointmentStatusText = "Accepted";
            appointmentRejectedBy.setVisibility(View.GONE);
            appointmentCommentBox.setVisibility(View.GONE);
            appointmentAcceptButton.setVisibility(View.GONE);
            appointmentRejectButton.setVisibility(View.GONE);
            appointmentRemoveButton.setVisibility(View.GONE);
            appointmentCancelAmendButton.setVisibility(View.GONE);
            appointmentAmendButton.setVisibility(View.VISIBLE);
            appointmentCancelButton.setVisibility(View.VISIBLE);
        } else if (appointmentStatus.equals(4)) {
            appointmentStatusText = "Rejected";
            String rejecterPerson = mappedAppointment.getRejecterID();
            String rejecterName = "";
            if (rejecterPerson.equals(Controller.user.getId())) {
                rejecterName = "You";
            } else {
                rejecterName = mappedAppointment.getClient().getFirstName() + " " + mappedAppointment.getClient().getLastName();
            }
            appointmentRejectedBy.setText("Rejected by: " + rejecterName);
            appointmentCommentBox.setText("Reject reason: " + mappedAppointment.getReasonOfRejection());
            appointmentCancelButton.setVisibility(View.VISIBLE);
            appointmentAcceptButton.setVisibility(View.GONE);
            appointmentAmendButton.setVisibility(View.VISIBLE);
            appointmentRejectButton.setVisibility(View.GONE);
            appointmentRemoveButton.setVisibility(View.GONE);
            appointmentCancelAmendButton.setVisibility(View.GONE);


        } else if (appointmentStatus.equals(5)) {
            appointmentStatusText = "Deleted";
            appointmentCancelButton.setVisibility(View.GONE);
            appointmentAcceptButton.setVisibility(View.GONE);
            appointmentAmendButton.setVisibility(View.GONE);
            appointmentRejectButton.setVisibility(View.GONE);
            appointmentRemoveButton.setVisibility(View.GONE);
            appointmentCancelAmendButton.setVisibility(View.GONE);
            appointmentAmendedViewDateTo.setVisibility(View.GONE);
            appointmentAmendedViewDateFrom.setVisibility(View.GONE);
            appointmentAmendedViewLocation.setVisibility(View.GONE);
        } else if (appointmentStatus.equals(6)) {
            appointmentCommentBox.setText("Comment: Appointment date has passed, you can not edit this appointment.");
            appointmentRejectedBy.setVisibility(View.GONE);
            appointmentCancelButton.setVisibility(View.GONE);
            appointmentAcceptButton.setVisibility(View.GONE);
            appointmentAmendButton.setVisibility(View.GONE);
            appointmentRejectButton.setVisibility(View.GONE);
            appointmentCancelAmendButton.setVisibility(View.GONE);
            appointmentRemoveButton.setVisibility(View.VISIBLE);
            appointmentAmendedViewDateTo.setVisibility(View.GONE);
            appointmentAmendedViewDateFrom.setVisibility(View.GONE);
            appointmentAmendedViewLocation.setVisibility(View.GONE);
        }

        appointmentViewStatus.setText("Status: " + appointmentStatusText);

    }

    @Override
    public void updateCalendarView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void updateCalendarDayView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void notifyObservers() {
        Controller.notifyAllObserversToUpdateAll();
        for (Observer observer : observers) {
            observer.update();
        }
    }

}

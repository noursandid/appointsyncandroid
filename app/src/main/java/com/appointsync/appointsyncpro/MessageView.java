package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetMessage;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedMessage;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.PostMessage;
import com.appointsync.appointsyncpro.Class.Main.PostMessageReply;
import com.appointsync.appointsyncpro.ClientsObjects.MessageViewAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.NotificationAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.ThreadAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.RefreshThreadsProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageView extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<MappedMessage> messageList;
    public RefreshThreadsProtocol delegate;
    private TextView usernameMessager;
    private ImageView messageViewBackArrow;
    private String toUserWebID, clientUserName;
    private MappedThread mappedThread;
    private long mLastClickTime = 0;
    private Fragment minime;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
//            delegate = (RefreshThreadsProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public MessageView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_message_view, container, false);
        minime = this;

        usernameMessager = view.findViewById(R.id.userMessage);

        final ImageView messageReplyImage = view.findViewById(R.id.messageReplyImage);
        messageViewBackArrow = view.findViewById(R.id.messageViewBackArrow);

        mappedThread = (MappedThread) getArguments().getSerializable(ThreadAdapter.THREAD_OBJECT);
        if (mappedThread == null) {
        mappedThread = (MappedThread) getArguments().getSerializable(NotificationAdapter.THREAD_OBJECT);
        }

        usernameMessager.setText(mappedThread.getTitle());

        toUserWebID = mappedThread.getToWebID();
        if (toUserWebID.equals(Controller.user.getWebID())) {
            toUserWebID = mappedThread.getFromWebID();
        }

        clientUserName = mappedThread.getFromUsername();
        if (clientUserName.equals(Controller.user.getUsername())) {
            clientUserName = mappedThread.getToUsername();
        }

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        recyclerView = view.findViewById(R.id.message_view_recycler);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        messageList = new ArrayList<>();


        Call<AppointSyncResponse<ArrayList<MappedMessage>>> call = RetrofitInstance.getRetrofitInstance(new GetMessage(mappedThread.getWebID()));

        pd.show();

        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedMessage>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedMessage>>> call, Response<AppointSyncResponse<ArrayList<MappedMessage>>> response) {

                if(response.body().getErrorCode() == 0) {

                    Controller.messageList = response.body().getData();

                    List<MappedMessage> mappedMessages = Controller.messageList;

                    int mappedMessageCounter = mappedMessages.size();
                    for (int i = 0; i < mappedMessageCounter; i++) {

                        messageList.add(mappedMessages.get(i));

                    }


                    adapter = new MessageViewAdapter(messageList, getActivity().getApplicationContext());


                    recyclerView.setAdapter(adapter);
                    pd.dismiss();

                }else{
                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedMessage>>> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
            }

        });


        messageReplyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = inflater.inflate(R.layout.popup_reply_to_message, null);
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                boolean focusable = true;
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
                popupWindow.setTouchable(true);

                final Button postReplyToMessage = popupView.findViewById(R.id.postReplyToThread);
                final Button cancelReplyToMessage = popupView.findViewById(R.id.cancelReplyToThread);

                final TextView threadTitle = popupView.findViewById(R.id.replyToMessageThread);
                final TextView replyToMessageUser = popupView.findViewById(R.id.replyToMessageUser);

                threadTitle.setText(mappedThread.getTitle());
                replyToMessageUser.setText("User: " + clientUserName);

                final EditText replyToThreadMessage = popupView.findViewById(R.id.replyToThreadMessage);


                postReplyToMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(replyToThreadMessage.getText()!=null && !replyToThreadMessage.getText().toString().equals("")) {

                            final String postReplyToMessagePost = replyToThreadMessage.getText().toString();
//                         newThreadTitleMessagePost = newThreadTitleMessagePost.replaceAll("\n", "&lt;br&gt;");

                            Call<AppointSyncResponse<MappedMessage>> call = RetrofitInstance.getRetrofitInstance(new PostMessageReply(mappedThread.getWebID(), postReplyToMessagePost, toUserWebID));
                            pd.show();
                            call.enqueue(new Callback<AppointSyncResponse<MappedMessage>>() {
                                @Override
                                public void onResponse(Call<AppointSyncResponse<MappedMessage>> call, Response<AppointSyncResponse<MappedMessage>> response) {

                                    messageList.add(0, response.body().getData());
                                    Controller.messageList.add(response.body().getData());

                                    adapter.notifyDataSetChanged();
                                    pd.dismiss();
                                    if(delegate!=null){
                                       delegate.didFinishAddingMessage();
                                    }
                                    popupWindow.dismiss();


                                }

                                @Override
                                public void onFailure(Call<AppointSyncResponse<MappedMessage>> call, Throwable t) {


                                    pd.dismiss();

                                    popupWindow.dismiss();

                                    Toast.makeText(getActivity(), "Something went wrong! Please try again later.", Toast.LENGTH_LONG).show();

                                }
                            });

                        }else{
                            Toast.makeText(getActivity(), "Please type a message.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                cancelReplyToMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();


                        popupWindow.dismiss();
                    }
                });

//                popupView.setOnTouchListener(new View.OnTouchListener() {
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
////                        popupWindow.dismiss();
//                        return true;
//                    }
//                });

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    popupWindow.setElevation(20);
                }

                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

            }
        });


        messageViewBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (minime != null) {
                    ft.remove(minime);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });


        return view;
    }

}

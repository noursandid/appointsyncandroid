package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.DateFullAppointmentDateToUnix;
import com.appointsync.appointsyncpro.Class.Main.GetClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.PostPaymentDue;
import com.appointsync.appointsyncpro.Class.Main.PostPaymentResponse;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileBalanceAdapter;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.RefreshClientBalance;
import com.appointsync.appointsyncpro.Interface.RefreshThreadsProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientFileAddPaymentDue extends Fragment implements RefreshClientBalance{
    private TextInputEditText paymentDueNoteInputLayoutText, paymentDueInputLayoutText;
    private Button buttonAddPaymentDueDate, paymentDueNoteSubmit, paymentDueNoteCancel;
    private ImageView addPaymentDueBackArrow;
    private long mLastClickTime = 0;
    private MappedClient mappedClient;
    private ProgressDialog pd;
    private List<Observer> observers = new ArrayList<>();

    private static final String TAG = "Sample";
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private SwitchDateTimeDialogFragment dateTimeFragment;

    public ClientFileAddPaymentDue() {

        observers = new ArrayList<>();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_file_add_payment_due, container, false);

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        addPaymentDueBackArrow = view.findViewById(R.id.addPaymentDueBackArrow);

        paymentDueNoteInputLayoutText = view.findViewById(R.id.paymentDueNoteInputLayoutText);
        paymentDueInputLayoutText = view.findViewById(R.id.paymentDueInputLayoutText);

        buttonAddPaymentDueDate = view.findViewById(R.id.buttonAddPaymentDueDate);
        paymentDueNoteSubmit = view.findViewById(R.id.paymentDueNoteSubmit);
        paymentDueNoteCancel = view.findViewById(R.id.paymentDueNoteCancel);


        Long timeFrom = System.currentTimeMillis() / 1000;

        final int appointmentStartTimeYear = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeYear(timeFrom.intValue()));
        final int appointmentStartTimeMonth = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMonth(timeFrom.intValue())) - 1;
        final int appointmentStartTimeDay = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeDay(timeFrom.intValue()));
        final int appointmentStartTimeHour = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeHour(timeFrom.intValue()));
        final int appointmentStartTimeMinute = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMinute(timeFrom.intValue()));

        buttonAddPaymentDueDate.setText(UnixDateConverter.UnixDateConverterAppointmentTime(timeFrom.intValue()));

        dateTimeFragment = (SwitchDateTimeDialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("EEEE, MMM d, yyyy h:mm a", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(false);
        dateTimeFragment.setHighlightAMPMSelection(true);

        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                buttonAddPaymentDueDate.setText(myDateFormat.format(date));

            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
            }
        });


        buttonAddPaymentDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentStartTimeYear, appointmentStartTimeMonth, appointmentStartTimeDay, appointmentStartTimeHour, appointmentStartTimeMinute).getTime());
                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);


            }
        });

        paymentDueNoteSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                if (!paymentDueNoteInputLayoutText.getText().toString().equals("") && !paymentDueInputLayoutText.getText().toString().equals("")) {

                    DateFullAppointmentDateToUnix paymentDueDateFrom = new DateFullAppointmentDateToUnix(buttonAddPaymentDueDate.getText().toString());

                    String paymentDueDateFromUnixMS = paymentDueDateFrom.getUnix();
                    String paymentDueDateFromUnix = paymentDueDateFromUnixMS.substring(0, paymentDueDateFromUnixMS.length() - 3);

                    pd.show();
                    Call<AppointSyncResponse<PostPaymentResponse>> call = RetrofitInstance.getRetrofitInstance(new PostPaymentDue(paymentDueInputLayoutText.getText().toString(),mappedClient.getWebID(),paymentDueNoteInputLayoutText.getText().toString(),paymentDueDateFromUnix));
                    call.enqueue(new Callback<AppointSyncResponse<PostPaymentResponse>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<PostPaymentResponse>> call, Response<AppointSyncResponse<PostPaymentResponse>> response) {

                            if(response.body().getErrorCode()!=0){
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                            }else{
                                notifyObservers();
                                HideKeyboardFunction.hideKeyboard(getActivity());
                                closeFragment();
                            }
                            pd.dismiss();

                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<PostPaymentResponse>> call, Throwable t) {

                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }

                    });


                } else {
                    Toast.makeText(getActivity(), "Please make sure to fill all the fields.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        paymentDueNoteCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                HideKeyboardFunction.hideKeyboard(getActivity());
                closeFragment();

            }
        });


        addPaymentDueBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                HideKeyboardFunction.hideKeyboard(getActivity());
                closeFragment();

            }
        });

        return view;
    }

    public void closeFragment(){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ClientFileAddPaymentDue f = (ClientFileAddPaymentDue) fm.findFragmentByTag("seeClientFileBalanceAddPaymentDueFragment");
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();
    }

    @Override
    public void didFinishAddingPaymentMessage(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }


}

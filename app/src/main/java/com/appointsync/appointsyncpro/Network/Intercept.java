package com.appointsync.appointsyncpro.Network;

/**
 * Created by cgsawma on 11/1/18.
 */
//
//public class Intercept implements Interceptor {
//
//    private static final String BASE_URL = "https://appointsync.com/apiDev/mobile/host/";
//
//    public Retrofit intercept (String userID, String body) {
//        final String uID = userID;
//        final String token = body;
//        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
//        okHttpBuilder.addInterceptor(new Interceptor() {
//            @Override
//            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
//                Request original = chain.request();
//
//                // Request customization: add request headers
//                Request.Builder requestBuilder = original.newBuilder()
//                        .header("USERID", uID)
//                        .header("TOKEN", token);
//
//                Request request = requestBuilder.build();
//                return chain.proceed(request);
//            }
//        });
//
//
//        Retrofit.Builder builder = new Retrofit.Builder()
//                .baseUrl(BASE_URL)
//                .client(okHttpBuilder.build())
//                .addConverterFactory(GsonConverterFactory.create());
//
//        Retrofit retrofit = builder.build();
//
//        return retrofit;
//
//    }
//}

package com.appointsync.appointsyncpro.Network;

import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Interface.API;
import com.appointsync.appointsyncpro.R;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cgsawma on 10/19/18.
 */

public class RetrofitInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://appointsync.com/apiDev/mobile/host/";
    private static String jsonBody;
    private static String batata;
    public static String userID = "";

    /**
     * Create an instance of Retrofit object to send a JSON request
     */
    public static <T, R> Call<AppointSyncResponse<R>> getRetrofitInstance(T request) {

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        jsonBody = gson.toJson(request) + userID;
        String jsonBodyEncrypted = "";

//        To add encryption here

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] data = md.digest(jsonBody.getBytes());
            StringBuilder encryptedBody = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                encryptedBody.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
            }
            jsonBodyEncrypted = encryptedBody.toString();
            System.out.println(encryptedBody);
            System.out.println(jsonBodyEncrypted.toString());
        } catch (Exception e) {
            System.out.println(e);
        }

        batata = jsonBodyEncrypted.toString();

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("USERID", userID)
                        .header("TOKEN", batata.toString());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpBuilder.build())
                    .build()
            ;
        }
        try {
            return (Call<AppointSyncResponse<R>>) retrofit.create(API.class).getClass().getMethod("post" + request.getClass().getSimpleName(), request.getClass()).invoke(retrofit.create(API.class), request);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create an instance of Retrofit object to send a JSON request
     * *///one sec ta chouf 3nde chou l code deal 100200 FUCK ;)
    //mich hon l mechkle l mechkle enno you have no access 3al response b ma7al wa7ad take me 3al login request


}

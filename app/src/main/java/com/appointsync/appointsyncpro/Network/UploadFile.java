package com.appointsync.appointsyncpro.Network;

import com.appointsync.appointsyncpro.Interface.API;

import java.io.IOException;
import java.security.MessageDigest;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cgsawma on 1/22/19.
 */

public class UploadFile {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://appointsync.com/apiDev/mobile/host/";
    private static String jsonBody;
    private static String batata;
    public static String userID = "";



    public static API getImageRetrofitInstance() {
        jsonBody = userID;
        String jsonBodyEncrypted = "";

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] data = md.digest(jsonBody.getBytes());
            StringBuilder encryptedBody = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                encryptedBody.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
            }
            jsonBodyEncrypted = encryptedBody.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        batata = jsonBodyEncrypted.toString();

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("USERID", userID)
                        .header("TOKEN", batata.toString());

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpBuilder.build())
                    .build()
            ;
        }
        try {
            return retrofit.create(API.class);
        }
        catch (Exception e ){
            e.printStackTrace();
            return null;
        }

    }
}

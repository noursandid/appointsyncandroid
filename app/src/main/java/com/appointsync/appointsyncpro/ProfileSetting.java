package com.appointsync.appointsyncpro;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.DatabaseAccess;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedProfile;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationRemoveToken;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationToken;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSetting extends Fragment {
    ButtonSelectedProtocol delegate;
    private long mLastClickTime = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public ProfileSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting, container, false);

        final ImageView profileSettingBackArrow = view.findViewById(R.id.profileSettingBackArrow);

        final TextView settingsSubscription = view.findViewById(R.id.settingsSubscription);
        final TextView settingsEditProfile = view.findViewById(R.id.settingsEditProfile);
        final TextView settingsChangePassword = view.findViewById(R.id.settingsChangePassword);
        final TextView settingsClientPermission = view.findViewById(R.id.settingsClientPermission);
        final TextView settingFileSecurity = view.findViewById(R.id.settingFileSecurity);
        final TextView settingsNotification = view.findViewById(R.id.settingsNotification);
        final TextView settingsContactUs = view.findViewById(R.id.settingsContactUs);
//        final TextView settingsHelp = view.findViewById(R.id.settingsHelp);
        final TextView settingsAbout = view.findViewById(R.id.settingsAbout);
        final TextView settingsPrivacyPolicy = view.findViewById(R.id.settingsPrivacyPolicy);
        final TextView settingsTermsAndConditions = view.findViewById(R.id.settingsTermsAndConditions);
        final TextView settingsLogout = view.findViewById(R.id.settingsLogout);


        profileSettingBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSetting f = (ProfileSetting) fm.findFragmentByTag("profileSettingFragment");

                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();


            }
        });


        settingsSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                delegate.seeProfileSettingSubscription(settingsSubscription);
            }
        });

        settingsEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingEditProfile(settingsEditProfile);
            }
        });

        settingsChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                delegate.seeProfileSettingChangePassword(settingsChangePassword);
            }
        });


        settingsClientPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (Controller.user.getSubscriptionType().equals(1)) {
                    delegate.seeNotPremiumUser(settingsClientPermission);
                } else if (Controller.user.getSubscriptionType().equals(2)) {
                    delegate.seeProfileSettingClientPermission(settingsClientPermission);
                }
            }
        });


        settingFileSecurity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (Controller.user.getSubscriptionType().equals(1)) {
                    delegate.seeNotPremiumUser(settingFileSecurity);
                } else if (Controller.user.getSubscriptionType().equals(2)) {
                    delegate.seeProfileSettingFileSecurity(settingFileSecurity);
                }
            }
        });

        settingsNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingNotification(settingsNotification);
            }
        });

        settingsContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingContactUs(settingsContactUs);
            }
        });

//        settingsHelp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
//                    return;
//                }
//                mLastClickTime = SystemClock.elapsedRealtime();
//                delegate.seeProfileSettingHelp(settingsHelp);
//            }
//        });

        settingsAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingAbout(settingsAbout);
            }
        });

        settingsPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingPrivacyPolicy(settingsPrivacyPolicy);
            }
        });

        settingsTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeProfileSettingTermsAndCondition(settingsTermsAndConditions);
            }
        });


        settingsLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Logout");
                builder.setMessage("Are you sure you want to log out?");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finishActivities();
                                if (Controller.appointmentList != null) {
                                    Controller.appointmentList.clear();
                                }
                                if (Controller.clientList != null) {
                                    Controller.clientList.clear();
                                }
                                if (Controller.threadList != null) {
                                    Controller.threadList.clear();
                                }
                                if (Controller.locationList != null) {
                                    Controller.locationList.clear();
                                }
                                if (Controller.messageList != null) {
                                    Controller.messageList.clear();
                                }


                                Call<AppointSyncResponse<MappedProfile>> call = RetrofitInstance.getRetrofitInstance(new PostNotificationRemoveToken(Controller.loggedInuserToken));
                                call.enqueue(new Callback<AppointSyncResponse<MappedProfile>>() {
                                    @Override
                                    public void onResponse(Call<AppointSyncResponse<MappedProfile>> call, Response<AppointSyncResponse<MappedProfile>> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<AppointSyncResponse<MappedProfile>> call, Throwable t) {


                                    }
                                });


                                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
                                databaseAccess.open();
                                databaseAccess.deleteLogin();
                                databaseAccess.close();
                                Intent intent = new Intent(view.getContext(), Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });


        return view;
    }

    public static List<Context> listContext = new ArrayList<Context>();


    public static void finishActivities() {

        for (Context context : listContext) {
            if (context != null)
                ((Activity) context).finish();
        }
        listContext.clear();
    }

}

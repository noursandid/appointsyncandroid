package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.PostAddClient;
import com.appointsync.appointsyncpro.Class.Main.PostLinkOfflineClient;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddOnlineClientTab extends Fragment implements Subject {
    private IntentIntegrator qrScan;
    private Button addOnlineClientOpenQrScanner, addOnlineClientUsernamePost;
    private TextInputEditText addOnlineClientUsernameText;
    private long mLastClickTime = 0;
    private List<Observer> observers = new ArrayList<>();

    public AddOnlineClientTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_online_client_tab, container, false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        addOnlineClientOpenQrScanner = view.findViewById(R.id.addOnlineClientOpenQrScanner);
        addOnlineClientUsernamePost = view.findViewById(R.id.addOnlineClientUsernamePost);
        addOnlineClientUsernameText = view.findViewById(R.id.addOnlineClientUsernameText);

        qrScan = new IntentIntegrator(getActivity());


        addOnlineClientOpenQrScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                IntentIntegrator.forSupportFragment(AddOnlineClientTab.this).setPrompt("Scan user QR").setOrientationLocked(true).initiateScan();
            }
        });


        addOnlineClientUsernamePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (addOnlineClientUsernameText.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Username can not be empty.", Toast.LENGTH_SHORT).show();
                }else if (addOnlineClientUsernameText.getText().toString().equals(Controller.user.getUsername())) {
                    Toast.makeText(getActivity(), "Are you seriously trying to add yourself?", Toast.LENGTH_SHORT).show();
                } else {


                    Call<AppointSyncResponse<MappedClient>> call = RetrofitInstance.getRetrofitInstance(new PostAddClient(addOnlineClientUsernameText.getText().toString()));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<MappedClient>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedClient>> call, Response<AppointSyncResponse<MappedClient>> response) {

                            if (response.body().getErrorCode() == 0) {

                                Controller.clientList.add(response.body().getData());
                                notifyObservers();
                                Toast.makeText(getActivity(), "Client added successfully.", Toast.LENGTH_SHORT).show();
                                pd.dismiss();

                                FragmentManager fm = getFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                AddClientFragment f = (AddClientFragment) fm.findFragmentByTag("addNewClientFragment");
                                ft.remove(f);
                                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                                ft.commit();

                            } else {
                                pd.dismiss();
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure
                                (Call<AppointSyncResponse<MappedClient>> call, Throwable t) {

                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                        }
                    });

                }

            }
        });


        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Log.d("MainActivity", "Cancelled");
//                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();

            } else {
                Log.d("MainActivity", "Scanned");

                Call<AppointSyncResponse<MappedClientWithQRCode>> call = RetrofitInstance.getRetrofitInstance(new GetClientWithQRCode(intentResult.getContents()));
                call.enqueue(new Callback<AppointSyncResponse<MappedClientWithQRCode>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedClientWithQRCode>> call, Response<AppointSyncResponse<MappedClientWithQRCode>> response) {

                        if (response.body().getErrorCode() == 190700) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Warning");
                            builder.setMessage("No client is available for this QR.");
                            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();
                        } else if (response.body().getErrorCode() == 0) {

                            addOnlineClientUsernameText.setText(response.body().getData().getUsername());

                        }

                    }

                    @Override
                    public void onFailure
                            (Call<AppointSyncResponse<MappedClientWithQRCode>> call, Throwable t) {

                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        }
    }

    @Override
    public void register(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(final Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }


}

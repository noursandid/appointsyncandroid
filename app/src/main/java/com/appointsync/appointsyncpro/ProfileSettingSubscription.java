package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetPrices;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedPlan;
import com.appointsync.appointsyncpro.Class.Main.MappedPrices;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.PostCancelSubscription;
import com.appointsync.appointsyncpro.Class.Main.PostChangePlan;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.RenewPlanPaymentDelegate;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import org.joda.time.Days;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingSubscription extends Fragment implements RenewPlanPaymentDelegate {
    private TextView tvSettingPlanTitle, tvSettingSubscriptionDateFrom, tvSettingSubscriptionDateTo, tvSettingSubscriptionDateTill;
    private Button btnSettingChangePlan, btnSettingRenewSubscription, btnSettingCancelSubscription;
    private long mLastClickTime = 0;
    ButtonSelectedProtocol delegate;
    private static ProfileSettingSubscription instance = null;
    RenewPlanPaymentDelegate minime;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public ProfileSettingSubscription() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting_subscription, container, false);
        instance = this;

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        final ImageView profileSettingSubscriptionBackArrow = view.findViewById(R.id.profileSettingSubscriptionBackArrow);

        tvSettingPlanTitle = view.findViewById(R.id.tvSettingPlanTitle);
        tvSettingSubscriptionDateFrom = view.findViewById(R.id.tvSettingSubscriptionDateFrom);
        tvSettingSubscriptionDateTo = view.findViewById(R.id.tvSettingSubscriptionDateTo);
        tvSettingSubscriptionDateTill = view.findViewById(R.id.tvSettingSubscriptionDateTill);

        btnSettingChangePlan = view.findViewById(R.id.btnSettingChangePlan);
        btnSettingRenewSubscription = view.findViewById(R.id.btnSettingRenewSubscription);
        btnSettingCancelSubscription = view.findViewById(R.id.btnSettingCancelSubscription);
        minime = this;
        refreshView();

        btnSettingChangePlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }


                if (Controller.user.getSubscriptionType().equals(2)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Downgrade");
                    builder.setMessage("If you downgrade, you wont be able to use client files, secure your clients files with a pin, and manage what your clients can do with appointments!!\n" +
                            "\n" +
                            "\n" +
                            "You wont get any refund if you downgrade, and you'll be able to add days after you downgrade.");
                    builder.setPositiveButton("Downgrade",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    //call to change plan

                                    Call<AppointSyncResponse<MappedPlan>> call = RetrofitInstance.getRetrofitInstance(new PostChangePlan(1));
                                    pd.show();
                                    call.enqueue(new Callback<AppointSyncResponse<MappedPlan>>() {
                                        @Override
                                        public void onResponse(Call<AppointSyncResponse<MappedPlan>> call, Response<AppointSyncResponse<MappedPlan>> response) {

                                            Controller.user.setSubscriptionType(1);
                                            refreshView();
                                            pd.dismiss();

                                        }

                                        @Override
                                        public void onFailure(Call<AppointSyncResponse<MappedPlan>> call, Throwable t) {
                                        }
                                    });


                                }
                            });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                } else if (Controller.user.getSubscriptionType().equals(1)) {

                    Call<AppointSyncResponse<MappedPrices>> call = RetrofitInstance.getRetrofitInstance(new GetPrices(5));
                    call.enqueue(new Callback<AppointSyncResponse<MappedPrices>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedPrices>> call, Response<AppointSyncResponse<MappedPrices>> response) {

                            if(response.body().getErrorCode()==0){

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Upgrade to Premium");
                                builder.setMessage("Upgrade to premium for an additional $9.99/month to be able to use client files, secure your clients files with a pin, and manage what your clients can do with appointments!!\n" +
                                        "\n" +
                                        "\n" +
                                        "Your payment to upgrade based on your current plan and remaining time is USD "+response.body().getData().getPrice()+", and you'll be able to add days after you upgrade.");
                                builder.setPositiveButton("Upgrade",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                delegate.seeRenewPlanPayment(true, 0, "M", minime);

                                            }
                                        });
                                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                builder.setCancelable(false);
                                builder.show();


                            }else{
                                Toast.makeText(getActivity(), "You can't downgrade at the moment, please try again later.", Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<MappedPrices>> call, Throwable t) {

                            Toast.makeText(getActivity(), "You can't downgrade at the moment, please try again later.", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });


        btnSettingRenewSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.user.getSubscriptionType().equals(1)) {
                    delegate.seeRenewProAccount(btnSettingRenewSubscription);

                } else if (Controller.user.getSubscriptionType().equals(2)) {
                    delegate.seeRenewPremiumAccount(btnSettingRenewSubscription);
                }


            }
        });


        btnSettingCancelSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if(Controller.user.getAutoRenew()==1){


                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Cancel Subscription");
                    builder.setMessage("Cancel subscription will terminate your account when you reach your last day of subscription.\nYou won't be able to use the application or access any of your content until you subscribe again.");
                    builder.setPositiveButton("Cancel Subscription",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    pd.show();
                                    Call<AppointSyncResponse<MappedPrices>> call = RetrofitInstance.getRetrofitInstance(new PostCancelSubscription("0"));
                                    call.enqueue(new Callback<AppointSyncResponse<MappedPrices>>() {
                                        @Override
                                        public void onResponse(Call<AppointSyncResponse<MappedPrices>> call, Response<AppointSyncResponse<MappedPrices>> response) {

                                            if(response.body().getErrorCode()==0){

                                                Controller.user.setAutoRenew(0);
                                                refreshView();
                                            }else{
                                                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                            }

                                            pd.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<AppointSyncResponse<MappedPrices>> call, Throwable t) {

                                            pd.dismiss();
                                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                }
                            });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                }else if(Controller.user.getAutoRenew()==0){

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Activate Subscription");
                    builder.setMessage("Are you sure you want to reactivate your account? \nYou will be charged by the end of your subscription based on the package selected.");
                    builder.setPositiveButton("Subscribe",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    pd.show();
                                    Call<AppointSyncResponse<MappedPrices>> call = RetrofitInstance.getRetrofitInstance(new PostCancelSubscription("1"));
                                    call.enqueue(new Callback<AppointSyncResponse<MappedPrices>>() {
                                        @Override
                                        public void onResponse(Call<AppointSyncResponse<MappedPrices>> call, Response<AppointSyncResponse<MappedPrices>> response) {

                                            if(response.body().getErrorCode()==0){

                                                Controller.user.setAutoRenew(1);
                                                refreshView();
                                            }else{
                                                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                            }

                                            pd.dismiss();
                                        }

                                        @Override
                                        public void onFailure(Call<AppointSyncResponse<MappedPrices>> call, Throwable t) {

                                            pd.dismiss();
                                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                                        }
                                    });

                                }
                            });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();

                }

            }
        });


        profileSettingSubscriptionBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingSubscription f = (ProfileSettingSubscription) fm.findFragmentByTag("profileSettingSubscriptionFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });

        return view;
    }


    public static ProfileSettingSubscription getInstance() {
        return instance;
    }

    public void refreshView() {

        if (Controller.user.getSubscriptionType().equals(1)) {
            tvSettingPlanTitle.setText("AppointSync Pro");
            btnSettingChangePlan.setText("Upgrade");
        } else if (Controller.user.getSubscriptionType().equals(2)) {
            tvSettingPlanTitle.setText("AppointSync Premium");
            btnSettingChangePlan.setText("Downgrade");
        }

        if(Controller.user.getAutoRenew()==1){

            btnSettingCancelSubscription.setText("Cancel Subscription");

        }else if(Controller.user.getAutoRenew()==0){

            btnSettingCancelSubscription.setText("Subscribe");

        }

        tvSettingSubscriptionDateFrom.setText("Subscription Start Date: " + UnixDateConverter.UnixDateConverter(Controller.user.getSubscriptionDate()));
        tvSettingSubscriptionDateTo.setText("Subscription End Date: " + UnixDateConverter.UnixDateConverter(Controller.user.getTillDate()));


        String dayDifference = "";

        try {
            //Dates to compare
            Long tsLong = System.currentTimeMillis() / 1000;
//        String ts = tsLong.toString();
            String CurrentDate = UnixDateConverter.UnixDateConverterDateGetNumberOfDays(Integer.parseInt(tsLong.toString()));
            String FinalDate = UnixDateConverter.UnixDateConverterDateGetNumberOfDays(Controller.user.getTillDate());

            Date date1;
            Date date2;

            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");

            //Setting dates
            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            //Comparing dates
            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            //Convert long to String
            dayDifference = Long.toString(differenceDates);

            Log.e("HERE", "HERE: " + dayDifference);

        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }


        tvSettingSubscriptionDateTill.setText("You still have " + dayDifference + " days in order to renew.");

    }

    @Override
    public void didFinishPaying() {
        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        Call<AppointSyncResponse<MappedPlan>> call = RetrofitInstance.getRetrofitInstance(new PostChangePlan(2));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<MappedPlan>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedPlan>> call, Response<AppointSyncResponse<MappedPlan>> response) {

                Controller.user.setSubscriptionType(2);
                refreshView();
                pd.dismiss();

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<MappedPlan>> call, Throwable t) {
            }
        });
    }
}

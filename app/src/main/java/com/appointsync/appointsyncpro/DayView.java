package com.appointsync.appointsyncpro;


import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Calendar.Day;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.ClientsObjects.DayViewAppointmentListAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class DayView extends Fragment implements Observer {
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView dayViewBackButton, dayViewAddNewAppointment;
    public ButtonSelectedProtocol delegate;
    private long mLastClickTime = 0;

    public DayView() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_day_view, container, false);

        dayViewBackButton = view.findViewById(R.id.dayViewBackButton);
        dayViewAddNewAppointment = view.findViewById(R.id.dayViewAddNewAppointment);
        mRecyclerView = view.findViewById(R.id.dayViewAppointmentList);

        updateView();

        dayViewAddNewAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.dayViewAddNewAppointment();


            }
        });


        dayViewBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Controller.selectedYearMonthDayController != null) {
                    Controller.selectedYearMonthDayController = null;
                }

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                DayView f = (DayView) fm.findFragmentByTag("seeDayView");

                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();


            }
        });

//        Controller.registerAsAnObserverToUpdateAll(this);
        return view;
    }

    public void updateAll() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    updateView();
                }
            });
        }
    }

    public void update() {
        updateView();
    }

    public void updateView() {

        int[] selectedYearMonthDay = (int[]) getArguments().getSerializable(Dashboard.SELECTED_YEAR_DAY_VIEW);

        ArrayList<Day> days = Controller.user.calendar.getDaysWithAppointmentsForYearMonthDay(selectedYearMonthDay);


        final DayViewAppointmentListAdapter dayViewAppointmentListAdapter = new DayViewAppointmentListAdapter(days, getContext());
        dayViewAppointmentListAdapter.delegateDayAppointmentView = delegate;

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        Day selectedDay = Controller.user.calendar.getDayFromYearMonthDay(selectedYearMonthDay);
        if (selectedDay != null) {
            int indexOfSelectedDay = days.indexOf(selectedDay);
            mLinearLayoutManager.scrollToPositionWithOffset(indexOfSelectedDay, 0);
        }

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(dayViewAppointmentListAdapter);


    }

}

package com.appointsync.appointsyncpro;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationAppointSyncPro extends Fragment {
    private Button buttonRegistrationPro;
    private TextView registrationAppointSyncProPrice;
    private long mLastClickTime = 0;

    public RegistrationAppointSyncPro() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration_appoint_sync_pro, container, false);

        buttonRegistrationPro = view.findViewById(R.id.buttonRegistrationPro);
        registrationAppointSyncProPrice = view.findViewById(R.id.registrationAppointSyncProPrice);

        registrationAppointSyncProPrice.setText(
                Html.fromHtml("$<Font color=\'" + "#3e92cf"
                        + "'\" >" + "<strong><big><big><big><big><big>14</big></big></big></big></big></strong>"
                        + "</Font>.99/Month"), TextView.BufferType.SPANNABLE);

        if( Controller.noActiveSubscription != null &&  Controller.noActiveSubscription.equals(true)){
            buttonRegistrationPro.setText("Select");
        }

        buttonRegistrationPro.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if( Controller.noActiveSubscription != null &&  Controller.noActiveSubscription.equals(true)){
//                   delegate.seeRenewProAccount(buttonRegistrationPro);

                    RenewProAccount nextFrag= new RenewProAccount();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.registrationPackagesConstraintLayout, nextFrag, "RenewProAccountNoSubscription")
                            .addToBackStack(null)
                            .commit();

                }else {

                    Intent intent = new Intent(getActivity(), RegisterHostPro.class);
                    startActivity(intent);
                }
            }

        });



        return view;
    }

}

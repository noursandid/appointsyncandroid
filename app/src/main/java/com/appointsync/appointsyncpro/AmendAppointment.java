package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.DateFullAppointmentDateToUnix;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedLocation;
import com.appointsync.appointsyncpro.Class.Main.PostAmendAppointment;
import com.appointsync.appointsyncpro.Class.Main.PostNewAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.ClientsObjects.ClientAppointmentAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.DayViewAppointmentAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.NotificationAdapter;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarDayView;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class AmendAppointment extends Fragment implements UpdateAllObserver {

    private Button buttonAppointmentTimeFrom,buttonAppointmentTimeTo;

    private MappedAppointment mappedAppointment;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private static final String TAG = "Sample";
    int calendarFlag = 0;
    String appointmentLocationName = "";
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private Fragment minime;
    private long mLastClickTime = 0;

    private List<Observer> observers = new ArrayList<>();

    public AmendAppointment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_amend_appointment, container, false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        minime = this;
        mappedAppointment = (MappedAppointment) getArguments().getSerializable(AppointmentView.APPOINTMENT_OBJECT);

        List<MappedLocation> mappedLocation = Controller.getLocation(mappedAppointment.getLocationID());
        int locationIndex = mappedLocation.size();
        for (int i = 0; i < locationIndex; i++) {
            appointmentLocationName = mappedLocation.get(i).getLocationTitle();

        }

        UnixDateConverter unixDateConverter = new UnixDateConverter();
        final String appointmentStartTime = unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getStartDate());
        final String appointmentEndTime = unixDateConverter.UnixDateConverterAppointmentTime(mappedAppointment.getEndDate());

//        create datetimepicker

        // Construct SwitchDateTimePicker
        dateTimeFragment = (SwitchDateTimeDialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
//                    getString(R.string.clean) // Optional
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("EEEE, MMM d, yyyy h:mm a", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(false);
        dateTimeFragment.setHighlightAMPMSelection(true);
//        dateTimeFragment.setMinimumDateTime(new GregorianCalendar(2015, Calendar.JANUARY, 1).getTime());
//        dateTimeFragment.setMaximumDateTime(new GregorianCalendar(2025, Calendar.DECEMBER, 31).getTime());

        // Define new day and month format
        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        // Set listener for date
        // Or use dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                if (calendarFlag == 1) {
                    buttonAppointmentTimeFrom.setText(myDateFormat.format(date));
                } else if (calendarFlag == 2) {
                    buttonAppointmentTimeTo.setText(myDateFormat.format(date));
                }
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                // Optional if neutral button does'nt exists
//                textView.setText("");
            }
        });

        final int appointmentStartTimeYear = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeYear(mappedAppointment.getStartDate()));
        final int appointmentStartTimeMonth = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeMonth(mappedAppointment.getStartDate())) - 1;
        final int appointmentStartTimeDay = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeDay(mappedAppointment.getStartDate()));
        final int appointmentStartTimeHour = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeHour(mappedAppointment.getStartDate()));
        final int appointmentStartTimeMinute = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeMinute(mappedAppointment.getStartDate()));

        final int appointmentEndTimeYear = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeYear(mappedAppointment.getEndDate()));
        final int appointmentEndTimeMonth = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeMonth(mappedAppointment.getEndDate())) - 1;
        final int appointmentEndTimeDay = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeDay(mappedAppointment.getEndDate()));
        final int appointmentEndTimeHour = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeHour(mappedAppointment.getEndDate()));
        final int appointmentEndTimeMinute = Integer.parseInt(unixDateConverter.UnixDateConverterAppointmentTimeMinute(mappedAppointment.getEndDate()));


        final Button appointmentAcceptAmend = view.findViewById(R.id.appointmentAcceptAmend);
        final Button appointmentCancelAmend = view.findViewById(R.id.appointmentCancelAmend);
        buttonAppointmentTimeFrom = view.findViewById(R.id.buttonAppointmentTimeFrom);
        buttonAppointmentTimeTo = view.findViewById(R.id.buttonAppointmentTimeTo);
        final Spinner spinner = (Spinner) view.findViewById(R.id.spinnerAmendLocation);

        List<String> categories = new ArrayList<String>();

        buttonAppointmentTimeFrom.setText(appointmentStartTime);
        buttonAppointmentTimeTo.setText(appointmentEndTime);

        Controller.locationList.size();
        for (int i = 0; i < Controller.locationList.size(); i++) {
            categories.add(Controller.locationList.get(i).getLocationTitle());
        }

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


//                spinner.setSelection(getIndex(spinner, appointmentLocationName));
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        spinner.setSelection(dataAdapter.getPosition(appointmentLocationName));


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                // On selecting a spinner item
                String item = parent.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        buttonAppointmentTimeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calendarFlag = 1;
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentStartTimeYear, appointmentStartTimeMonth, appointmentStartTimeDay, appointmentStartTimeHour, appointmentStartTimeMinute).getTime());
                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

            }
        });

        buttonAppointmentTimeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calendarFlag = 2;
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentEndTimeYear, appointmentEndTimeMonth, appointmentEndTimeDay, appointmentEndTimeHour, appointmentEndTimeMinute).getTime());
                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

            }
        });

        appointmentAcceptAmend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                dateTimeFragment.startAtCalendarView();
//                System.out.println("Month: " + appointmentStartTimeMonth + "Day: " + appointmentStartTimeDay);
//                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentStartTimeYear, appointmentStartTimeMonth, appointmentStartTimeDay, appointmentStartTimeHour, appointmentStartTimeMinute).getTime());
//                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);


                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                DateFullAppointmentDateToUnix appointsyncDateFrom = new DateFullAppointmentDateToUnix(buttonAppointmentTimeFrom.getText().toString());
                DateFullAppointmentDateToUnix appointsyncDateTo = new DateFullAppointmentDateToUnix(buttonAppointmentTimeTo.getText().toString());

                String appointsyncDateFromUnixMS = appointsyncDateFrom.getUnix();
                String appointsyncDateFromUnix = appointsyncDateFromUnixMS.substring(0, appointsyncDateFromUnixMS.length() - 3);

                String appointsyncDateToUnixMS = appointsyncDateTo.getUnix();
                String appointsyncDateToUnix = appointsyncDateToUnixMS.substring(0, appointsyncDateFromUnixMS.length() - 3);

                String LocationWebID = Controller.locationList.get(spinner.getSelectedItemPosition()).getLocationWebID();

                Call<AppointSyncResponse<MappedAppointment>> call = RetrofitInstance.getRetrofitInstance(new PostAmendAppointment(mappedAppointment.getWebID().toString(),appointsyncDateFromUnix.toString(), appointsyncDateToUnix.toString(), LocationWebID.toString()));
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<MappedAppointment>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedAppointment>> call, Response<AppointSyncResponse<MappedAppointment>> response) {

                        HideKeyboardFunction.hideKeyboard(getActivity());
                        pd.dismiss();
                        if(response.body().getErrorCode() == 0) {
                            mappedAppointment.update(response.body().getData());
                            Controller.user.calendar.removeAppointment(mappedAppointment);
                            Controller.user.calendar.addAppointment(response.body().getData());
//                                        mappedAppointment = response.body().getData();
                            Toast.makeText(getActivity(), "Amendment Created.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                        notifyObservers();


                        FragmentManager fm = getFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        if (fm.findFragmentByTag("seeCalendarAmendAppointment") != null) {
                            AmendAppointment f = (AmendAppointment) fm.findFragmentByTag("seeCalendarAmendAppointment");
                            ft.remove(f);
                        }
                        if (fm.findFragmentByTag("seeClientAmendAppointment") != null) {
                            AmendAppointment fn = (AmendAppointment) fm.findFragmentByTag("seeClientAmendAppointment");
                            ft.remove(fn);
                        }
                        if (fm.findFragmentByTag("seeNotificationAmendAppointment") != null) {
                            AmendAppointment fd = (AmendAppointment) fm.findFragmentByTag("seeNotificationAmendAppointment");
                            ft.remove(fd);
                        }
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft.commit();


                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<MappedAppointment>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        pd.dismiss();
                    }
                });


            }
        });

        appointmentCancelAmend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (minime != null) {
                    ft.remove(minime);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });


        return view;
    }

    public void updateAll() {


    }

    public void notifyObservers() {
        Controller.notifyAllObserversToUpdateAll();
        for (Observer observer : observers) {
            observer.update();
        }
    }

}

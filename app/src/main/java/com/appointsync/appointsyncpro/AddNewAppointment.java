package com.appointsync.appointsyncpro;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.DateFullAppointmentDateToUnix;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.CustomSearchableSpinner;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetLocation;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedLocation;
import com.appointsync.appointsyncpro.Class.Main.PostNewAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarDayView;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddNewAppointment extends Fragment implements UpdateCalendarView, UpdateCalendarDayView {
    private Button buttonAddAppointmentTimeFrom, buttonAddAppointmentTimeTo, appointmentAcceptAddAppointment, appointmentCancelAddAppointment;
    private pl.utkala.searchablespinner.SearchableSpinner addAppointmentSpinnerLocation;
    private EditText newAppointmentMessage;
    private pl.utkala.searchablespinner.SearchableSpinner spinnerAddAppointmentClient;
    private long mLastClickTime = 0;
    MappedClient mappedClient;
    private Boolean isFromClient = true;

    private List<MappedClient> developersLists;
    private List<String> clientIdentifier;

    private String selectedUser = "";
    private String selectedLocation;

    private static final String TAG = "Sample";
    int calendarFlag = 0;
    private static final String TAG_DATETIME_FRAGMENT = "TAG_DATETIME_FRAGMENT";
    private static final String STATE_TEXTVIEW = "STATE_TEXTVIEW";
    private TextView textView;
    private SwitchDateTimeDialogFragment dateTimeFragment;
    private List<Observer> observers = new ArrayList<>();
    private String selectedClientWebID = null;
    private Date selectedDate;
    private int appointmentStartTimeYear, appointmentStartTimeMonth, appointmentStartTimeDay, appointmentEndTimeYear, appointmentEndTimeMonth, appointmentEndTimeDay;

    private Fragment minime;

    public AddNewAppointment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_appointment, container, false);

        minime = this;
        spinnerAddAppointmentClient = view.findViewById(R.id.spinnerAddAppointmentClient);
        addAppointmentSpinnerLocation = view.findViewById(R.id.addAppointmentSpinnerLocation);
        buttonAddAppointmentTimeFrom = view.findViewById(R.id.buttonAddAppointmentTimeFrom);
        buttonAddAppointmentTimeTo = view.findViewById(R.id.buttonAddAppointmentTimeTo);
        newAppointmentMessage = view.findViewById(R.id.newAppointmentMessage);
        appointmentAcceptAddAppointment = view.findViewById(R.id.appointmentAcceptAddAppointment);
        appointmentCancelAddAppointment = view.findViewById(R.id.appointmentCancelAddAppointment);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        addAppointmentSpinnerLocation.setDialogTitle("Select Location");
        addAppointmentSpinnerLocation.setDismissText("Cancel");
        spinnerAddAppointmentClient.setDialogTitle("Select Client");
        addAppointmentSpinnerLocation.setDismissText("Cancel");

        if (getArguments() != null) {
            mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);
        }
        if (mappedClient == null) {

            developersLists = new ArrayList<>();
            clientIdentifier = new ArrayList<>();

            if (Controller.clientList != null) {


                for (int i = 0; i < Controller.clientList.size(); i++) {

                    developersLists.add(Controller.client = Controller.clientList.get(i));
                    clientIdentifier.add(Controller.clientList.get(i).getIdentifier());
                    Collections.sort(developersLists, MappedClient.StuNameComparator);


                }

                ArrayAdapter<String> clientDataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, clientIdentifier);
                clientDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinnerAddAppointmentClient.setAdapter(clientDataAdapter);

            } else {

                Call<AppointSyncResponse<ArrayList<MappedClient>>> call = RetrofitInstance.getRetrofitInstance(new GetClients());
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClient>>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Response<AppointSyncResponse<ArrayList<MappedClient>>> response) {

                        if (response.body().getErrorCode() == 0) {

                            if (Controller.clientList != null) {
                                Controller.clientList.clear();
                            }
                            if (developersLists != null) {
                                developersLists.clear();
                            }
                            if (clientIdentifier != null) {
                                clientIdentifier.clear();
                            }

                            Controller.clientList = response.body().getData();

                            for (Iterator<MappedClient> i = response.body().getData().iterator(); i.hasNext(); ) {
                                MappedClient mappedClient = i.next();

                                developersLists.add(Controller.client = mappedClient);
                                Collections.sort(developersLists, MappedClient.StuNameComparator);
                                clientIdentifier.add(mappedClient.getIdentifier());

                                System.out.println("FML " + mappedClient.getFirstName());

                            }

                            ArrayAdapter<String> clientDataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, clientIdentifier);
                            clientDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            spinnerAddAppointmentClient.setAdapter(clientDataAdapter);
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Throwable t) {

                        pd.dismiss();
                    }

                });

            }


        } else {
            spinnerAddAppointmentClient.setClickable(false);
            spinnerAddAppointmentClient.setEnabled(false);
            selectedClientWebID = mappedClient.getWebID();
            List<String> client = new ArrayList<String>();
            client.add(mappedClient.getIdentifier());
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, client);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerAddAppointmentClient.setAdapter(dataAdapter);
            selectedUser = mappedClient.getWebID();
        }


        if (Controller.locationList == null) {

            Call<AppointSyncResponse<ArrayList<MappedLocation>>> call = RetrofitInstance.getRetrofitInstance(new GetLocation());

            call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedLocation>>>() {
                @Override
                public void onResponse(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Response<AppointSyncResponse<ArrayList<MappedLocation>>> response) {

                    if (response.body().getErrorCode() == 0) {
                        Controller.locationList = response.body().getData();

                        List<String> locations = new ArrayList<String>();

                        Controller.locationList.size();
                        for (int i = 0; i < Controller.locationList.size(); i++) {
                            locations.add(Controller.locationList.get(i).getLocationTitle());
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, locations);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        addAppointmentSpinnerLocation.setAdapter(dataAdapter);
                        checkNewUser();
                    }

                }

                @Override
                public void onFailure(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Throwable t) {
                    System.out.println("IN FAIL LOVATION");
                    checkNewUser();
                }
            });
        } else {
            List<String> locations = new ArrayList<String>();

            Controller.locationList.size();
            for (int i = 0; i < Controller.locationList.size(); i++) {
                locations.add(Controller.locationList.get(i).getLocationTitle());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, locations);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            addAppointmentSpinnerLocation.setAdapter(dataAdapter);
            checkNewUser();
        }

        spinnerAddAppointmentClient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                selectedUser = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        addAppointmentSpinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                selectedLocation = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Long timeFrom = System.currentTimeMillis() / 1000;
        Long timeTo = System.currentTimeMillis() / 1000;
        Date date = new java.util.Date(timeTo * 1000L + TimeUnit.MINUTES.toMillis(30));
        Long timeToAdded = date.getTime() / 1000;


        if (Controller.selectedYearMonthDayController != null) {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            try {
                selectedDate = (Date) formatter.parse(Controller.selectedYearMonthDayController + " " + UnixDateConverter.UnixDateConverterAppointmentTimeHour(timeFrom.intValue()).toString() + ":" + UnixDateConverter.UnixDateConverterAppointmentTimeMinute(timeFrom.intValue()).toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Long output = selectedDate.getTime() / 1000L;
            String str = Long.toString(output);
            long timestamp = Long.parseLong(str) * 1000;

            System.out.println("Today is " + selectedDate.getTime());
            System.out.println("Today is timestamp" + timestamp);

            appointmentStartTimeYear = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeYear(output.intValue()));
            appointmentStartTimeMonth = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMonth(output.intValue())) - 1;
            appointmentStartTimeDay = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeDay(output.intValue()));

            appointmentEndTimeYear = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeYear(output.intValue()));
            appointmentEndTimeMonth = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMonth(output.intValue())) - 1;
            appointmentEndTimeDay = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeDay(output.intValue()));

            buttonAddAppointmentTimeFrom.setText(UnixDateConverter.UnixDateConverterAppointmentTime(output.intValue()));
            buttonAddAppointmentTimeTo.setText(UnixDateConverter.UnixDateConverterAppointmentTimePlusThirtyMinuets(output.intValue()));

        } else {
            appointmentStartTimeYear = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeYear(timeFrom.intValue()));
            appointmentStartTimeMonth = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMonth(timeFrom.intValue())) - 1;
            appointmentStartTimeDay = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeDay(timeFrom.intValue()));

            appointmentEndTimeYear = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeYear(timeToAdded.intValue()));
            appointmentEndTimeMonth = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMonth(timeToAdded.intValue())) - 1;
            appointmentEndTimeDay = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeDay(timeToAdded.intValue()));

            buttonAddAppointmentTimeFrom.setText(UnixDateConverter.UnixDateConverterAppointmentTime(timeFrom.intValue()));
            buttonAddAppointmentTimeTo.setText(UnixDateConverter.UnixDateConverterAppointmentTimePlusThirtyMinuets(timeTo.intValue()));
        }


        final int appointmentStartTimeHour = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeHour(timeFrom.intValue()));
        final int appointmentStartTimeMinute = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMinute(timeFrom.intValue()));

        final int appointmentEndTimeHour = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeHour(timeToAdded.intValue()));
        final int appointmentEndTimeMinute = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMinute(timeToAdded.intValue()));

//DateTime Picker


        dateTimeFragment = (SwitchDateTimeDialogFragment) getActivity().getSupportFragmentManager().findFragmentByTag(TAG_DATETIME_FRAGMENT);
        if (dateTimeFragment == null) {
            dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                    getString(R.string.label_datetime_dialog),
                    getString(android.R.string.ok),
                    getString(android.R.string.cancel)
            );
        }

        // Optionally define a timezone
        dateTimeFragment.setTimeZone(TimeZone.getDefault());

        // Init format
        final SimpleDateFormat myDateFormat = new SimpleDateFormat("EEEE, MMM d, yyyy h:mm a", java.util.Locale.getDefault());
        // Assign unmodifiable values
        dateTimeFragment.set24HoursMode(false);
        dateTimeFragment.setHighlightAMPMSelection(true);

        try {
            dateTimeFragment.setSimpleDateMonthAndDayFormat(new SimpleDateFormat("MMMM dd", Locale.getDefault()));
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {
            Log.e(TAG, e.getMessage());
        }

        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener() {
            @Override
            public void onPositiveButtonClick(Date date) {
                if (calendarFlag == 1) {
                    buttonAddAppointmentTimeFrom.setText(myDateFormat.format(date));
                } else if (calendarFlag == 2) {
                    buttonAddAppointmentTimeTo.setText(myDateFormat.format(date));
                }
            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Do nothing
            }

            @Override
            public void onNeutralButtonClick(Date date) {
                textView.setText("");
            }
        });


        buttonAddAppointmentTimeFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                calendarFlag = 1;
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentStartTimeYear, appointmentStartTimeMonth, appointmentStartTimeDay, appointmentStartTimeHour, appointmentStartTimeMinute).getTime());
                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

            }
        });

        buttonAddAppointmentTimeTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                calendarFlag = 2;
                dateTimeFragment.startAtCalendarView();
                dateTimeFragment.setDefaultDateTime(new GregorianCalendar(appointmentEndTimeYear, appointmentEndTimeMonth, appointmentEndTimeDay, appointmentEndTimeHour, appointmentEndTimeMinute).getTime());
                dateTimeFragment.show(getActivity().getSupportFragmentManager(), TAG_DATETIME_FRAGMENT);

            }
        });

        appointmentAcceptAddAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                try {

                    DateFullAppointmentDateToUnix appointsyncDateFrom = new DateFullAppointmentDateToUnix(buttonAddAppointmentTimeFrom.getText().toString());
                    DateFullAppointmentDateToUnix appointsyncDateTo = new DateFullAppointmentDateToUnix(buttonAddAppointmentTimeTo.getText().toString());

                    String appointsyncDateFromUnixMS = appointsyncDateFrom.getUnix();
                    String appointsyncDateFromUnix = appointsyncDateFromUnixMS.substring(0, appointsyncDateFromUnixMS.length() - 3);

                    String appointsyncDateToUnixMS = appointsyncDateTo.getUnix();
                    String appointsyncDateToUnix = appointsyncDateToUnixMS.substring(0, appointsyncDateFromUnixMS.length() - 3);

                    String LocationWebID = Controller.locationList.get(addAppointmentSpinnerLocation.getSelectedItemPosition()).getLocationWebID();

                    if (selectedClientWebID == null) {
                        String selectedUserWebId = Controller.clientList.get(spinnerAddAppointmentClient.getSelectedItemPosition()).getWebID();
                        selectedClientWebID = selectedUserWebId;
                    }

                    if (buttonAddAppointmentTimeFrom.getText().toString().isEmpty() || buttonAddAppointmentTimeTo.getText().toString().isEmpty() || LocationWebID.isEmpty() || newAppointmentMessage.getText().toString().equals("")) {
                        Toast.makeText(getActivity(), "Please make sure to fill all the required fields.", Toast.LENGTH_LONG).show();
                    } else if (Integer.parseInt(appointsyncDateFromUnix) > Integer.parseInt(appointsyncDateToUnix)) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Warning");
                        builder.setMessage("Please note that Appointment end date can not be before its start date.");
                        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builder.setCancelable(false);
                        builder.show();

                    } else {

                        Call<AppointSyncResponse<MappedAppointment>> call = RetrofitInstance.getRetrofitInstance(new PostNewAppointment(appointsyncDateFromUnix, selectedClientWebID, newAppointmentMessage.getText().toString(), LocationWebID, appointsyncDateToUnix));
                        pd.show();
                        call.enqueue(new Callback<AppointSyncResponse<MappedAppointment>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<MappedAppointment>> call, Response<AppointSyncResponse<MappedAppointment>> response) {

                                if (response.body().getErrorCode() == 0) {
                                    Toast.makeText(getActivity(), "Appointment created.", Toast.LENGTH_SHORT).show();
                                    HideKeyboardFunction.hideKeyboard(getActivity());
                                    pd.dismiss();
                                    Controller.appointmentList.add(response.body().getData());
                                    Controller.user.calendar.addAppointment(response.body().getData());
                                    notifyObservers();
                                    closeView();
                                } else {
                                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<MappedAppointment>> call, Throwable t) {
                                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                pd.dismiss();
                            }
                        });

                        if (Controller.selectedYearMonthDayController != null) {
                            Controller.selectedYearMonthDayController = null;
                        }

                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please make sure to fill all the fields.", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }
            }
        });


        appointmentCancelAddAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (Controller.selectedYearMonthDayController != null) {
                    Controller.selectedYearMonthDayController = null;
                }
                closeView();
            }
        });

        return view;

    }

    public void checkNewUser() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (Controller.locationList == null || Controller.clientList == null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Notice");
            builder.setMessage(Html.fromHtml("Please note that you need to have at least one location/client in order to add an appointment.<br/><br/> <strong>To add a location:</strong> Go to the bottom of your profile.<br/><strong>To add a client:</strong> Go to client tab and click add on the top right."));
            builder.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    closeView();
                }
            });
            builder.setCancelable(false);
            builder.show();

        } else if (Controller.locationList != null && Controller.locationList.size() == 0 || Controller.clientList != null && Controller.clientList.size() == 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Notice");
            builder.setMessage(Html.fromHtml("Please note that you need to have at least one location/client in order to add an appointment.<br/><br/> <strong>To add a location:</strong> Go to the bottom of your profile.<br/><strong>To add a client:</strong> Go to client tab and click add on the top right."));
            builder.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    closeView();
                }
            });
            builder.setCancelable(false);
            builder.show();
        }

    }

    public void closeView() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (minime != null) {
            ft.remove(minime);
        }
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

        ft.commit();
    }

    @Override
    public void updateCalendarView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void updateCalendarDayView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}

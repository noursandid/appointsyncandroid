package com.appointsync.appointsyncpro;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetNotification;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.MappedUser;
import com.appointsync.appointsyncpro.Class.Main.PostReadNotifications;
import com.appointsync.appointsyncpro.Class.MyFirebaseMessagingService;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.NoSubscriptionObserver;
import com.appointsync.appointsyncpro.Interface.NotificationObserverDelegate;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.RefreshClientBalance;
import com.appointsync.appointsyncpro.Interface.RenewPlanPaymentDelegate;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarDayView;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarView;
import com.appointsync.appointsyncpro.Interface.UpdateFilePageView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity implements ButtonSelectedProtocol, NoSubscriptionObserver, NotificationObserverDelegate, UpdateAllObserver {


    private BottomNavigationView navigation;

    //let me research how to access a fragment from activit deal  ana 7a oum dakhen cig
    final Fragment calendarFragment = new MainCalendar();
    Fragment clientFragment = null;
    //    final Fragment fragment2 = new Clients();
    Fragment notificationFragment = null;
    //    final Fragment fragment3 = new Notifications();
    Fragment messagesFragment = null;
    //    final Fragment fragment4 = new Messages();
    Fragment profileFragment = null;
    Fragment locationFragment = null;
    Fragment clientProfileDash = null;
    Fragment clientAppointment = null;
    Fragment appointmentViewFragment = null;
    Fragment appointmentDayViewFragment = null;
    Fragment appointmentNotificationViewFragment = null;
    Fragment messageViewFragment = null;
    Fragment profileSettingFragment = null;
    Fragment profileLocationAddLocationFragment = null;
    Fragment addNewClientFragment = null;

    Fragment endOfSubscriptionFragment = null;

    //    settings fragments
    Fragment profileSettingSubscriptionFragment = null;
    Fragment profileSettingEditProfileFragment = null;
    Fragment profileSettingChangePasswordFragment = null;
    Fragment profileSettingClientPermissionFragment = null;
    Fragment profileSettingFileSecurityFragment = null;
    Fragment profileSettingNotificationFragment = null;
    Fragment profileSettingContactUsFragment = null;
    Fragment profileSettingHelpFragment = null;
    Fragment profileSettingAboutFragment = null;
    Fragment profileSettingPrivacyPolicyFragment = null;
    Fragment profileSettingTermsAndConditionFragment = null;
    Fragment notPremiumUserFragment = null;
    Fragment profileSettingLogoutFragment = null;
    Fragment renewPremiumAccountFragment = null;
    Fragment renewProAccountFragment = null;
    Fragment seeRenewPlanPaymentFragment = null;

//    client Page

    Fragment addNewAppointmentFragment = null;
    Fragment seeClientSettingsPermissionFragment = null;
    Fragment seeClientSendMessageFragment = null;
    Fragment seeClientFileFragment = null;
    Fragment seeClientFileFragmentNotification = null;
    Fragment seeClientAddFilePageFragment = null;
    Fragment seeClientAddFilePageFragmentNotification = null;
    Fragment seeClientLinkToOnlineAccountFragment = null;
    Fragment seeClientFileBalanceFragment = null;
    Fragment seeClientFileBalanceAddPaymentDueFragment = null;
    Fragment seeClientFileBalanceAddPaymentReceivedFragment = null;
    Fragment appointmentClientViewFragment = null;

//    Calendar

    Fragment seeDayView = null;

//    Appointments

    Fragment seeAmendAppointment = null;
    Fragment seeAppointmentNotes = null;

    //    final Fragment fragment5 = new Profile();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = calendarFragment;


    private View vBadge;
    private TextView mtxtnotificationsbadge;
    private BottomNavigationItemView itemView;

    Fragment messageViewFragmentNotification = null;

    //   Create other fragments as null and in the case set if fragment = null, then make it new class.
    public MappedClient selectedClient;
    public static final String SELECTED_CLIENT_ADD_APPOINTMENT = "selectedClient";
    public static final String SELECTED_YEAR_DAY_VIEW = "selectedYearDayView";
    public static final String CLIENT_OBJECT = "clientObject";
    public static final String THREAD_OBJECT = "threadObject";
    public static final String NOTIFICATION_OBJECT = "notificationObject";
    public static final String APPOINTMENT_OBJECT = "appointmentObject";

    public void seeLocationsSelected(Button button) {

        if (locationFragment == null) {
            locationFragment = new LocationView();
            fm.beginTransaction().add(R.id.loggedInUserProfileLayout, locationFragment, "6").show(locationFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.loggedInUserProfileLayout, locationFragment, "6").show(locationFragment).commit();
        }

    }//find in all project kif ?


    public void seeAddNewLocation(ImageView imageView) {

        if (profileLocationAddLocationFragment == null) {
            profileLocationAddLocationFragment = new LocationAddLocation();
            fm.beginTransaction().add(R.id.loggedInUserLocations, profileLocationAddLocationFragment, "profileLocationAddLocationFragment").show(profileLocationAddLocationFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.loggedInUserLocations, profileLocationAddLocationFragment, "profileLocationAddLocationFragment").show(profileLocationAddLocationFragment).commit();
        }

    }

    public void seeClientProfile(ClientProfile clientProfile) {

        clientProfileDash = clientProfile;

        if (clientProfileDash == null) {
            clientProfileDash = new ClientProfile();
            fm.beginTransaction().add(R.id.fragmentClientList, clientProfileDash, "8").show(clientProfileDash).commit();
        } else {
            fm.beginTransaction().add(R.id.fragmentClientList, clientProfileDash, "8").show(clientProfileDash).commit();
        }
//        fm.beginTransaction().hide(active).show(locationFragment).commit();
//        active = locationFragment;

    }

    public void seeAddNewClient() {

        addNewClientFragment = new AddClientFragment();
        ((Subject) addNewClientFragment).register((Observer) clientFragment);
        fm.beginTransaction().add(R.id.fragmentClientList, addNewClientFragment, "addNewClientFragment").show(addNewClientFragment).commit();
    }

    public void seeClientProfileNotification(ClientProfile clientProfile) {

        clientProfileDash = clientProfile;

        if (clientProfileDash == null) {
            clientProfileDash = new ClientProfile();
            fm.beginTransaction().add(R.id.notificationFragmentView, clientProfileDash, "seeClientProfileNotification").show(clientProfileDash).commit();
        } else {
            fm.beginTransaction().add(R.id.notificationFragmentView, clientProfileDash, "seeClientProfileNotification").show(clientProfileDash).commit();
        }
    }

    public void seeClientAppointment(Button button, MappedClient client) {

        clientAppointment = new ClientAppointment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("client", client);
        clientAppointment.setArguments(bundle);
        fm.beginTransaction().add(R.id.clientProfilePageFragment, clientAppointment, "9").show(clientAppointment).commit();

    }


    public void seeAppointment(AppointmentView appointmentView) {

//Removed add to back stack

        appointmentViewFragment = appointmentView;

        if (appointmentViewFragment == null) {
            appointmentViewFragment = new AppointmentView();
            fm.beginTransaction().add(R.id.clientAppointmentsConstraintView, appointmentViewFragment, "appointmentView").show(appointmentViewFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.clientAppointmentsConstraintView, appointmentViewFragment, "appointmentView").show(appointmentViewFragment).commit();
        }

    }


    public void seeAppointmentFromClient(AppointmentView appointmentView) {


        appointmentClientViewFragment = appointmentView;

        if (appointmentClientViewFragment == null) {
            appointmentClientViewFragment = new AppointmentView();
            fm.beginTransaction().add(R.id.clientAppointmentsConstraintView, appointmentClientViewFragment, "appointmentClientViewFragment").show(appointmentClientViewFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.clientAppointmentsConstraintView, appointmentClientViewFragment, "appointmentClientViewFragment").show(appointmentClientViewFragment).commit();
        }

    }

    public void seeAppointmentFromNotification(AppointmentView appointmentView) {


        appointmentNotificationViewFragment = appointmentView;

        if (appointmentNotificationViewFragment == null) {
            appointmentNotificationViewFragment = new AppointmentView();

            fm.beginTransaction().add(R.id.notificationFragmentView, appointmentNotificationViewFragment, "appointmentNotificationViewFragment").show(appointmentNotificationViewFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.notificationFragmentView, appointmentNotificationViewFragment, "appointmentNotificationViewFragment").show(appointmentNotificationViewFragment).commit();
        }

    }


    public void seeThreadMessageFromNotification(MessageView messageView) {


        messageViewFragmentNotification = messageView;

        if (messageViewFragmentNotification == null) {
            messageViewFragmentNotification = new MessageView();

            fm.beginTransaction().add(R.id.notificationFragmentView, messageViewFragmentNotification, "messageViewFragmentNotification").show(messageViewFragmentNotification).commit();
        } else {
            fm.beginTransaction().add(R.id.notificationFragmentView, messageViewFragmentNotification, "messageViewFragmentNotification").show(messageViewFragmentNotification).commit();
//
        }

    }

    public void seeAppointmentFromDayView(AppointmentView appointmentView) {


        appointmentDayViewFragment = appointmentView;

        if (appointmentDayViewFragment == null) {
            appointmentDayViewFragment = new AppointmentView();
            fm.beginTransaction().add(R.id.day_view, appointmentDayViewFragment, "appointmentDayViewFragment").show(appointmentDayViewFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.day_view, appointmentDayViewFragment, "appointmentDayViewFragment").show(appointmentDayViewFragment).commit();
        }

    }


    public void seeThreadMessage(MessageView messageView) {


        messageViewFragment = messageView;

        if (messageViewFragment == null) {
            messageViewFragment = new MessageView();

            fm.beginTransaction().add(R.id.threadListConstraintView, messageViewFragment, "messageViewFragment").show(messageViewFragment).commit();
            Controller.thread = messageViewFragment;
        } else {
            fm.beginTransaction().add(R.id.threadListConstraintView, messageViewFragment, "messageViewFragment").show(messageViewFragment).commit();
            Controller.thread = messageViewFragment;
        }

    }

    public void seeProfileSettings(ImageView imageView) {

        if (profileSettingFragment == null) {
            profileSettingFragment = new ProfileSetting();

            fm.beginTransaction().add(R.id.loggedInUserProfileLayout, profileSettingFragment, "profileSettingFragment").show(profileSettingFragment).commit();
        } else {
            fm.beginTransaction().add(R.id.loggedInUserProfileLayout, profileSettingFragment, "profileSettingFragment").show(profileSettingFragment).commit();
        }

    }


//profile settings

    public void seeProfileSettingSubscription(TextView textView) {

        if (profileSettingSubscriptionFragment == null) {
            profileSettingSubscriptionFragment = new ProfileSettingSubscription();
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingSubscriptionFragment, "profileSettingSubscriptionFragment").show(profileSettingSubscriptionFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingSubscriptionFragment, "profileSettingSubscriptionFragment").show(profileSettingSubscriptionFragment).commit();
        }

    }


    public void seeProfileSettingEditProfile(TextView textView) {

        if (profileSettingEditProfileFragment == null) {
            profileSettingEditProfileFragment = new ProfileSettingEditProfile();
            ((Subject) profileSettingEditProfileFragment).unregister((Observer) profileFragment);

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingEditProfileFragment, "profileSettingEditProfileFragment").show(profileSettingEditProfileFragment).commit();
        } else {
            ((Subject) profileSettingEditProfileFragment).unregister((Observer) profileFragment);
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingEditProfileFragment, "profileSettingEditProfileFragment").show(profileSettingEditProfileFragment).commit();
        }

    }


    public void seeProfileSettingChangePassword(TextView textView) {

        if (profileSettingChangePasswordFragment == null) {
            profileSettingChangePasswordFragment = new ProfileSettingChangePassword();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingChangePasswordFragment, "profileSettingChangePasswordFragment").show(profileSettingChangePasswordFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingChangePasswordFragment, "profileSettingChangePasswordFragment").show(profileSettingChangePasswordFragment).commit();
        }

    }


    public void seeProfileSettingClientPermission(TextView textView) {

        if (profileSettingClientPermissionFragment == null) {
            profileSettingClientPermissionFragment = new ProfileSettingClientPermission();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingClientPermissionFragment, "profileSettingClientPermissionFragment").show(profileSettingClientPermissionFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingClientPermissionFragment, "profileSettingClientPermissionFragment").show(profileSettingClientPermissionFragment).commit();
        }

    }


    public void seeProfileSettingFileSecurity(TextView textView) {

        if (profileSettingFileSecurityFragment == null) {
            profileSettingFileSecurityFragment = new ProfileSettingFileSecurity();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingFileSecurityFragment, "profileSettingFileSecurityFragment").show(profileSettingFileSecurityFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingFileSecurityFragment, "profileSettingFileSecurityFragment").show(profileSettingFileSecurityFragment).commit();
        }

    }


    public void seeProfileSettingNotification(TextView textView) {

        if (profileSettingNotificationFragment == null) {
            profileSettingNotificationFragment = new ProfileSettingNotification();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingNotificationFragment, "profileSettingNotificationFragment").show(profileSettingNotificationFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingNotificationFragment, "profileSettingNotificationFragment").show(profileSettingNotificationFragment).commit();
        }

    }


    public void seeProfileSettingContactUs(TextView textView) {

        if (profileSettingContactUsFragment == null) {
            profileSettingContactUsFragment = new ProfileSettingContactUs();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingContactUsFragment, "profileSettingContactUsFragment").show(profileSettingContactUsFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingContactUsFragment, "profileSettingContactUsFragment").show(profileSettingContactUsFragment).commit();
        }

    }


    public void seeProfileSettingHelp(TextView textView) {

        if (profileSettingHelpFragment == null) {
            profileSettingHelpFragment = new ProfileSettingHelp();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingHelpFragment, "profileSettingHelpFragment").show(profileSettingHelpFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingHelpFragment, "profileSettingHelpFragment").show(profileSettingHelpFragment).commit();
        }

    }


    public void seeProfileSettingAbout(TextView textView) {

        if (profileSettingAboutFragment == null) {
            profileSettingAboutFragment = new ProfileSettingAbout();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingAboutFragment, "profileSettingAboutFragment").show(profileSettingAboutFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingAboutFragment, "profileSettingAboutFragment").show(profileSettingAboutFragment).commit();
        }

    }


    public void seeProfileSettingPrivacyPolicy(TextView textView) {

        if (profileSettingPrivacyPolicyFragment == null) {
            profileSettingPrivacyPolicyFragment = new ProfileSettingPrivacyPolicy();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingPrivacyPolicyFragment, "profileSettingPrivacyPolicyFragment").show(profileSettingPrivacyPolicyFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingPrivacyPolicyFragment, "profileSettingPrivacyPolicyFragment").show(profileSettingPrivacyPolicyFragment).commit();
        }

    }


    public void seeProfileSettingTermsAndCondition(TextView textView) {

        if (profileSettingTermsAndConditionFragment == null) {
            profileSettingTermsAndConditionFragment = new ProfileSettingTermsAndCondition();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingTermsAndConditionFragment, "profileSettingTermsAndConditionFragment").show(profileSettingTermsAndConditionFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, profileSettingTermsAndConditionFragment, "profileSettingTermsAndConditionFragment").show(profileSettingTermsAndConditionFragment).commit();
        }

    }


    public void seeNotPremiumUser(TextView textView) {

        if (notPremiumUserFragment == null) {
            notPremiumUserFragment = new ProfileSettingNotPremiumUser();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, notPremiumUserFragment, "ProfileSettingNotPremiumUser").show(notPremiumUserFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingConstraintView, notPremiumUserFragment, "ProfileSettingNotPremiumUser").show(notPremiumUserFragment).commit();
        }

    }

    public void seeNotPremiumUserClientProfile() {

        if (notPremiumUserFragment == null) {
            notPremiumUserFragment = new ProfileSettingNotPremiumUser();

            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, notPremiumUserFragment, "notPremiumUserClientFragment").show(notPremiumUserFragment).commit();
        } else {
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, notPremiumUserFragment, "notPremiumUserClientFragment").show(notPremiumUserFragment).commit();
        }

    }


    public void seeRenewPremiumAccount(Button button) {

        renewPremiumAccountFragment = new RenewPremium();
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingSubscriptionView, renewPremiumAccountFragment, "renewPremiumAccountFragment").show(renewPremiumAccountFragment).commit();
    }


    public void seeRenewProAccount(Button button) {

        renewProAccountFragment = new RenewProAccount();
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingSubscriptionView, renewProAccountFragment, "renewProAccountFragment").show(renewProAccountFragment).commit();

    }

    public void seeRenewPlanPayment(boolean isUpgrade, int type, String subscriptionPackage, RenewPlanPaymentDelegate delegate) {

        seeRenewPlanPaymentFragment = new RenewPlanPayment();
        ((RenewPlanPayment) seeRenewPlanPaymentFragment).isUpgrade = isUpgrade;
        ((RenewPlanPayment) seeRenewPlanPaymentFragment).type = type;
        ((RenewPlanPayment) seeRenewPlanPaymentFragment).delegate = delegate;
        ((RenewPlanPayment) seeRenewPlanPaymentFragment).subscriptionPackage = subscriptionPackage;
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.profileSettingSubscriptionView, seeRenewPlanPaymentFragment, "seeRenewPlanPaymentFragment").show(seeRenewPlanPaymentFragment).commit();

    }

//    Client File


    public void seeAddNewAppointment(Button button, MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        addNewAppointmentFragment = new AddNewAppointment();
        ((UpdateCalendarDayView) addNewAppointmentFragment).updateCalendarDayView((Observer) seeDayView);
        ((UpdateCalendarView) addNewAppointmentFragment).updateCalendarView((Observer) calendarFragment);
        addNewAppointmentFragment.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, addNewAppointmentFragment, "addNewAppointmentFromClientFragment").commit();


    }

    public void seeClientSettingsPermission(Button button, MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientSettingsPermissionFragment = new ClientPermissionSettings();
        seeClientSettingsPermissionFragment.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientSettingsPermissionFragment, "seeClientSettingsPermissionFragment").show(seeClientSettingsPermissionFragment).commit();


    }

    public void seeClientSendMessage(Button button, MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientSendMessageFragment = new ClientSendMessage();
        seeClientSendMessageFragment.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientSendMessageFragment, "seeClientSendMessageFragment").show(seeClientSendMessageFragment).commit();


    }

    public void seeClientFile(MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);

        if (active == clientFragment) {
            if (seeClientFileFragment == null) {
                seeClientFileFragment = new ClientFile();
                seeClientFileFragment.setArguments(bundle);
                fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientFileFragment, "seeClientFileFragment").show(seeClientFileFragment).commit();


            } else {
                seeClientFileFragment.setArguments(bundle);
                fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientFileFragment, "seeClientFileFragment").show(seeClientFileFragment).commit();
            }

        }
    }

    public void seeClientAddFilePage(MappedClient client, UpdateFilePageView observer) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);


        if (active == clientFragment) {
            if (seeClientAddFilePageFragment == null) {
                seeClientAddFilePageFragment = new NewFilePage();
                ((NewFilePage) seeClientAddFilePageFragment).addObserver(observer);
                seeClientAddFilePageFragment.setArguments(bundle);
                fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientAddFilePageFragment, "seeClientAddFilePageFragment").show(seeClientAddFilePageFragment).commit();


            } else {
                seeClientAddFilePageFragment.setArguments(bundle);
                fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientAddFilePageFragment, "seeClientAddFilePageFragment").show(seeClientAddFilePageFragment).commit();
            }

        }

    }

    public void seeClientFileBalance(MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientFileBalanceFragment = new ClientFileBalance();
        seeClientFileBalanceFragment.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientFileConstraintLayout, seeClientFileBalanceFragment, "seeClientFileBalanceFragment").show(seeClientFileBalanceFragment).commit();


    }

    public void seeClientLinkToOnlineAccount(Button button, MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientLinkToOnlineAccountFragment = new LinkOfflineClient();
        ((Subject) seeClientLinkToOnlineAccountFragment).register((Observer) clientFragment);
        seeClientLinkToOnlineAccountFragment.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeClientLinkToOnlineAccountFragment, "seeClientLinkToOnlineAccountFragment").show(seeClientLinkToOnlineAccountFragment).commit();


    }

    public void seeClientBalanceAddPaymentDue(MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientFileBalanceAddPaymentDueFragment = new ClientFileAddPaymentDue();
        seeClientFileBalanceAddPaymentDueFragment.setArguments(bundle);
        ((RefreshClientBalance) seeClientFileBalanceAddPaymentDueFragment).didFinishAddingPaymentMessage((Observer) seeClientFileBalanceFragment);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientFileBalanceConstraintLayout, seeClientFileBalanceAddPaymentDueFragment, "seeClientFileBalanceAddPaymentDueFragment").show(seeClientFileBalanceAddPaymentDueFragment).commit();


    }

    public void seeClientBalanceAddPaymentReceived(MappedClient client) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_CLIENT_ADD_APPOINTMENT, client);
        seeClientFileBalanceAddPaymentReceivedFragment = new ClientFileAddPaymentReceived();
        seeClientFileBalanceAddPaymentReceivedFragment.setArguments(bundle);
        ((RefreshClientBalance) seeClientFileBalanceAddPaymentReceivedFragment).didFinishAddingPaymentMessage((Observer) seeClientFileBalanceFragment);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientFileBalanceConstraintLayout, seeClientFileBalanceAddPaymentReceivedFragment, "seeClientFileBalanceAddPaymentReceivedFragment").show(seeClientFileBalanceAddPaymentReceivedFragment).commit();


    }

//    Calendar View

    public void seeDayView(int[] yeaDayView) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(SELECTED_YEAR_DAY_VIEW, yeaDayView);
        seeDayView = new DayView();
        seeDayView.setArguments(bundle);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.main_calendar_fragment, seeDayView, "seeDayView").show(seeDayView).commit();


    }

    public void calendarAddNewAppointment() {

        addNewAppointmentFragment = new AddNewAppointment();
        ((UpdateCalendarView) addNewAppointmentFragment).updateCalendarView((Observer) calendarFragment);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.main_calendar_fragment, addNewAppointmentFragment, "addNewAppointmentCalendarFragment").show(addNewAppointmentFragment).commit();


    }

    public void dayViewAddNewAppointment() {

        addNewAppointmentFragment = new AddNewAppointment();
        ((UpdateCalendarDayView) addNewAppointmentFragment).updateCalendarDayView((Observer) seeDayView);
        ((UpdateCalendarView) addNewAppointmentFragment).updateCalendarView((Observer) calendarFragment);
        fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.main_calendar_fragment, addNewAppointmentFragment, "addNewAppointmentDayViewFragment").show(addNewAppointmentFragment).commit();


    }

//    Appointments

    public void seeAmendAppointment(AmendAppointment amendAppointment) {
        if (active.equals(calendarFragment)) {
            seeAmendAppointment = amendAppointment;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.main_calendar_fragment, seeAmendAppointment, "seeCalendarAmendAppointment").show(seeAmendAppointment).commit();
        }
        if (active.equals(clientFragment)) {
            seeAmendAppointment = amendAppointment;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeAmendAppointment, "seeClientAmendAppointment").show(seeAmendAppointment).commit();
        }
        if (active.equals(notificationFragment)) {
            seeAmendAppointment = amendAppointment;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.notificationFragmentView, seeAmendAppointment, "seeNotificationAmendAppointment").show(seeAmendAppointment).commit();
        }
    }


    public void seeAppointmentNotes(AppointmentNotes appointmentNotes) {
        if (active.equals(calendarFragment)) {
            seeAppointmentNotes = appointmentNotes;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.main_calendar_fragment, seeAppointmentNotes, "seeCalendarAppointmentNotes").show(seeAppointmentNotes).commit();
        }
        if (active.equals(clientFragment)) {
            seeAppointmentNotes = appointmentNotes;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.clientProfilePageFragment, seeAppointmentNotes, "seeClientAppointmentNotes").show(seeAppointmentNotes).commit();
        }
        if (active.equals(notificationFragment)) {
            seeAppointmentNotes = appointmentNotes;
            fm.beginTransaction().setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit).add(R.id.notificationFragmentView, seeAppointmentNotes, "seeNotificationAppointmentNotes").show(seeAppointmentNotes).commit();
        }
    }


    public void openClientsPage(MappedClient client) {

        if (clientFragment == null) {
            clientFragment = new Clients();

            fm.beginTransaction().add(R.id.main_container, clientFragment, "2").hide(clientFragment).commit();
        }
        fm.beginTransaction().hide(active).show(clientFragment).commit();
        Controller.isInAppointmentView = "Client";
        active = clientFragment;
        navigation.setSelectedItemId(R.id.navigation_dashboard);

        if (fm.findFragmentByTag("seeClientFileBalanceAddPaymentReceivedFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ClientFileAddPaymentReceived f = (ClientFileAddPaymentReceived) fm.findFragmentByTag("seeClientFileBalanceAddPaymentReceivedFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        }if (fm.findFragmentByTag("seeClientFileBalanceAddPaymentDueFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ClientFileAddPaymentDue f = (ClientFileAddPaymentDue) fm.findFragmentByTag("seeClientFileBalanceAddPaymentDueFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        } if (fm.findFragmentByTag("seeClientFileBalanceFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ClientFileBalance f = (ClientFileBalance) fm.findFragmentByTag("seeClientFileBalanceFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        } if (fm.findFragmentByTag("seeClientAddFilePageFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            NewFilePage f = (NewFilePage) fm.findFragmentByTag("seeClientAddFilePageFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        } if (fm.findFragmentByTag("seeNotificationAddFilePageFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            NewFilePage f = (NewFilePage) fm.findFragmentByTag("seeNotificationAddFilePageFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        } if (fm.findFragmentByTag("seeClientFileFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ClientFile f = (ClientFile) fm.findFragmentByTag("seeClientFileFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        } if (fm.findFragmentByTag("8") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            ClientProfile f = (ClientProfile) fm.findFragmentByTag("8");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(CLIENT_OBJECT, client);
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setArguments(bundle);
        seeClientProfile(clientProfile);

    }


    public void openMessagePage(MappedThread mappedThread) {


//        fm.beginTransaction().add(R.id.threadListConstraintView, messageViewFragment, "messagesFragment").show(messageViewFragment).commit();


        if (messagesFragment == null) {
            messagesFragment = new Messages();
            fm.beginTransaction().add(R.id.main_container, messagesFragment, "4").hide(messagesFragment).commit();
        }

        fm.beginTransaction().hide(active).show(messagesFragment).commit();
        active = messagesFragment;
        navigation.setSelectedItemId(R.id.testNew);

        if (fm.findFragmentByTag("messageViewFragment") != null) {
            FragmentTransaction ft = fm.beginTransaction();
            MessageView f = (MessageView) fm.findFragmentByTag("messageViewFragment");
            ft.remove(f);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();
        }


        Bundle bundle = new Bundle();
        bundle.putSerializable(THREAD_OBJECT, mappedThread);
        MessageView messageViewNotification = new MessageView();
        messageViewNotification.setArguments(bundle);
        seeThreadMessage(messageViewNotification);

    }


    public void openAppointmentNote(AppointmentView appointmentView, MappedAppointment mappedAppointment) {



        appointmentNotificationViewFragment = appointmentView;

        if (appointmentNotificationViewFragment == null) {
            appointmentNotificationViewFragment = new AppointmentView();

            fm.beginTransaction().add(R.id.notificationFragmentView, appointmentNotificationViewFragment, "appointmentNotificationViewFragment").show(appointmentNotificationViewFragment).commit();

            Bundle bundle = new Bundle();
            bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment);
            AppointmentNotes appointmentNotes = new AppointmentNotes();
            appointmentNotes.setArguments(bundle);
            seeAppointmentNotes(appointmentNotes);
        } else {
            fm.beginTransaction().add(R.id.notificationFragmentView, appointmentNotificationViewFragment, "appointmentNotificationViewFragment").show(appointmentNotificationViewFragment).commit();

            Bundle bundle = new Bundle();
            bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment);
            AppointmentNotes appointmentNotes = new AppointmentNotes();
            appointmentNotes.setArguments(bundle);
            seeAppointmentNotes(appointmentNotes);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        Controller.noSubscriptionObserver = this;
        setContentView(R.layout.activity_dashboard);

        Controller.isInAppointmentView = "Calendar";
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        getNotifications();
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        fm.beginTransaction().add(R.id.main_container, fragment5, "5").hide(fragment5).commit();
//        fm.beginTransaction().add(R.id.main_container, messagesFragment, "4").hide(fragment4).commit();
//        fm.beginTransaction().add(R.id.main_container, notificationFragment, "3").hide(fragment3).commit();
//        fm.beginTransaction().add(R.id.main_container, clientFragment, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.main_container, calendarFragment, "1").commit();

        Controller.registerAsAnObserverToUpdateAll(this);

    }

    @Override
    public void updateAll() {
        getNotifications();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fm.beginTransaction().hide(active).show(calendarFragment).commit();
                    Controller.isInAppointmentView = "Calendar";
                    active = calendarFragment;
                    return true;
                case R.id.navigation_dashboard:
                    if (clientFragment == null) {
                        clientFragment = new Clients();

                        fm.beginTransaction().add(R.id.main_container, clientFragment, "2").hide(clientFragment).commit();
                    }
                    fm.beginTransaction().hide(active).show(clientFragment).commit();
                    Controller.isInAppointmentView = "Client";
                    active = clientFragment;

                    return true;

                case R.id.navigation_notifications:
                    if (notificationFragment == null) {
                        notificationFragment = new Notifications();
                        fm.beginTransaction().add(R.id.main_container, notificationFragment, "3").hide(notificationFragment).commit();
                        if (mtxtnotificationsbadge != null) {
                            mtxtnotificationsbadge.setText("");
                            mtxtnotificationsbadge.setBackgroundResource(0);
                            itemView.removeView(vBadge);
                        }
                    }

                    fm.beginTransaction().hide(active).show(notificationFragment).commit();
                    Controller.isInAppointmentView = "Notification";
                    active = notificationFragment;
                    if (mtxtnotificationsbadge != null) {
                        mtxtnotificationsbadge.setText("");
                        mtxtnotificationsbadge.setBackgroundResource(0);
                        itemView.removeView(vBadge);
                    }
                    return true;

                case R.id.testNew:
                    if (messagesFragment == null) {
                        messagesFragment = new Messages();
                        fm.beginTransaction().add(R.id.main_container, messagesFragment, "4").hide(messagesFragment).commit();
                    }

                    fm.beginTransaction().hide(active).show(messagesFragment).commit();
                    active = messagesFragment;
                    return true;

                case R.id.profileNav:
                    if (profileFragment == null) {
                        profileFragment = new Profile();
                        fm.beginTransaction().add(R.id.main_container, profileFragment, "5").hide(profileFragment).commit();
                    }

                    fm.beginTransaction().hide(active).show(profileFragment).commit();
                    active = profileFragment;

                    return true;
            }

            return false;
        }
    };


    @Override
    public void notifyDayViewToUpdateAppointmentList() {
        if (seeDayView != null) {
            ((DayView) seeDayView).updateAll();
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();

        System.out.println("CURRENTLY ACTIVE FRAGMENT: " + active.toString());
        if (active.equals(calendarFragment)) {

            if (fm.findFragmentByTag("seeCalendarAppointmentNotes") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentNotes f = (AppointmentNotes) fm.findFragmentByTag("seeCalendarAppointmentNotes");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeCalendarAmendAppointment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AmendAppointment f = (AmendAppointment) fm.findFragmentByTag("seeCalendarAmendAppointment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("appointmentDayViewFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentDayViewFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("addNewAppointmentDayViewFragment") != null) {
                if(Controller.selectedYearMonthDayController!=null) {
                    Controller.selectedYearMonthDayController = null;
                }
                FragmentTransaction ft = fm.beginTransaction();
                AddNewAppointment f = (AddNewAppointment) fm.findFragmentByTag("addNewAppointmentDayViewFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeDayView") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                DayView f = (DayView) fm.findFragmentByTag("seeDayView");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("addNewAppointmentCalendarFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AddNewAppointment f = (AddNewAppointment) fm.findFragmentByTag("addNewAppointmentCalendarFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else {
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
            }
        } else if (active.equals(profileFragment)) {
            if (fm.findFragmentByTag("renewProAccountFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                RenewProAccount f = (RenewProAccount) fm.findFragmentByTag("renewProAccountFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("renewPremiumAccountFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                RenewPremium f = (RenewPremium) fm.findFragmentByTag("renewPremiumAccountFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingSubscriptionFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingSubscription f = (ProfileSettingSubscription) fm.findFragmentByTag("profileSettingSubscriptionFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingEditProfileFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingEditProfile f = (ProfileSettingEditProfile) fm.findFragmentByTag("profileSettingEditProfileFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingChangePasswordFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingChangePassword f = (ProfileSettingChangePassword) fm.findFragmentByTag("profileSettingChangePasswordFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingClientPermissionFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingClientPermission f = (ProfileSettingClientPermission) fm.findFragmentByTag("profileSettingClientPermissionFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingFileSecurityFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingFileSecurity f = (ProfileSettingFileSecurity) fm.findFragmentByTag("profileSettingFileSecurityFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingNotificationFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingNotification f = (ProfileSettingNotification) fm.findFragmentByTag("profileSettingNotificationFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingContactUsFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingContactUs f = (ProfileSettingContactUs) fm.findFragmentByTag("profileSettingContactUsFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingHelpFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingHelp f = (ProfileSettingHelp) fm.findFragmentByTag("profileSettingHelpFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingAboutFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingAbout f = (ProfileSettingAbout) fm.findFragmentByTag("profileSettingAboutFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingPrivacyPolicyFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingPrivacyPolicy f = (ProfileSettingPrivacyPolicy) fm.findFragmentByTag("profileSettingPrivacyPolicyFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingTermsAndConditionFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingTermsAndCondition f = (ProfileSettingTermsAndCondition) fm.findFragmentByTag("profileSettingTermsAndConditionFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("ProfileSettingNotPremiumUser") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingNotPremiumUser f = (ProfileSettingNotPremiumUser) fm.findFragmentByTag("ProfileSettingNotPremiumUser");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("profileSettingFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSetting f = (ProfileSetting) fm.findFragmentByTag("profileSettingFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeRenewPlanPaymentFragment") != null) {
//                FragmentTransaction ft = fm.beginTransaction();
//                RenewPlanPayment f = (RenewPlanPayment) fm.findFragmentByTag("seeRenewPlanPaymentFragment");
//                ft.remove(f);
//                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
//                ft.commit();
            } else if (fm.findFragmentByTag("profileLocationAddLocationFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                LocationAddLocation f = (LocationAddLocation) fm.findFragmentByTag("profileLocationAddLocationFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("6") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                LocationView f = (LocationView) fm.findFragmentByTag("6");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else {

                System.out.println("CURRENTLY ACTIVE FRAGMENT IN IF: " + active.toString());
                fm.beginTransaction().hide(active).show(calendarFragment).commit();
                active = calendarFragment;
                navigation.setSelectedItemId(R.id.navigation_home);
            }
        } else if (active.equals(messagesFragment)) {

            if (fm.findFragmentByTag("messageViewFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                MessageView f = (MessageView) fm.findFragmentByTag("messageViewFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else {

                System.out.println("CURRENTLY ACTIVE FRAGMENT IN IF: " + active.toString());
                fm.beginTransaction().hide(active).show(calendarFragment).commit();
                active = calendarFragment;
                navigation.setSelectedItemId(R.id.navigation_home);
            }

        } else if (active.equals(clientFragment)) {

            System.out.println("THIS IS ZOUB");
            if (fm.findFragmentByTag("seeClientLinkToOnlineAccountFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                LinkOfflineClient f = (LinkOfflineClient) fm.findFragmentByTag("seeClientLinkToOnlineAccountFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientSendMessageFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientSendMessage f = (ClientSendMessage) fm.findFragmentByTag("seeClientSendMessageFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("addNewAppointmentFromClientFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AddNewAppointment f = (AddNewAppointment) fm.findFragmentByTag("addNewAppointmentFromClientFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientSettingsPermissionFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientPermissionSettings f = (ClientPermissionSettings) fm.findFragmentByTag("seeClientSettingsPermissionFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("notPremiumUserClientFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingNotPremiumUser f = (ProfileSettingNotPremiumUser) fm.findFragmentByTag("notPremiumUserClientFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientAppointmentNotes") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentNotes f = (AppointmentNotes) fm.findFragmentByTag("seeClientAppointmentNotes");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientAmendAppointment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AmendAppointment f = (AmendAppointment) fm.findFragmentByTag("seeClientAmendAppointment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("appointmentClientViewFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentClientViewFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            }else if (fm.findFragmentByTag("seeClientFileBalanceAddPaymentReceivedFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientFileAddPaymentReceived f = (ClientFileAddPaymentReceived) fm.findFragmentByTag("seeClientFileBalanceAddPaymentReceivedFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientFileBalanceAddPaymentDueFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientFileAddPaymentDue f = (ClientFileAddPaymentDue) fm.findFragmentByTag("seeClientFileBalanceAddPaymentDueFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientFileBalanceFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientFileBalance f = (ClientFileBalance) fm.findFragmentByTag("seeClientFileBalanceFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientAddFilePageFragment") != null) {
//                FragmentTransaction ft = fm.beginTransaction();
//                NewFilePage f = (NewFilePage) fm.findFragmentByTag("seeClientAddFilePageFragment");
//                ft.remove(f);
//                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
//                ft.commit();
                if (Controller.clientFilePage.equals(false)) {
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                    NewFilePage ff = (NewFilePage) fm.findFragmentByTag("seeClientAddFilePageFragment");
                    ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    ft.remove(ff);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                    ClientFile f = (ClientFile) fm.findFragmentByTag("seeClientFileFragment");
                    ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    ft.remove(f);
                    ft.commit();
                }else{
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                    NewFilePage f = (NewFilePage) fm.findFragmentByTag("seeClientAddFilePageFragment");
                    ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    ft.remove(f);
                    ft.commit();
                }
            } else if (fm.findFragmentByTag("seeClientFileFragment") != null) {
                if(((ClientFile)seeClientFileFragment).adapter!=null){
                    ((ClientFile)seeClientFileFragment).adapter.stopScheduler();
                }
                FragmentTransaction ft = fm.beginTransaction();
                ClientFile f = (ClientFile) fm.findFragmentByTag("seeClientFileFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            }  else if (fm.findFragmentByTag("9") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientAppointment f = (ClientAppointment) fm.findFragmentByTag("9");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("8") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientProfile f = (ClientProfile) fm.findFragmentByTag("8");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("addNewClientFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AddClientFragment f = (AddClientFragment) fm.findFragmentByTag("addNewClientFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else {

                fm.beginTransaction().hide(active).show(calendarFragment).commit();
                active = calendarFragment;
                navigation.setSelectedItemId(R.id.navigation_home);
            }

        } else if (active == notificationFragment) {


            if (fm.findFragmentByTag("seeNotificationAppointmentNotes") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentNotes f = (AppointmentNotes) fm.findFragmentByTag("seeNotificationAppointmentNotes");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientProfileNotification") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientProfile f = (ClientProfile) fm.findFragmentByTag("seeClientProfileNotification");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientSendMessageFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientSendMessage f = (ClientSendMessage) fm.findFragmentByTag("seeClientSendMessageFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("addNewAppointmentFromClientFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AddNewAppointment f = (AddNewAppointment) fm.findFragmentByTag("addNewAppointmentFromClientFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientSettingsPermissionFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ClientPermissionSettings f = (ClientPermissionSettings) fm.findFragmentByTag("seeClientSettingsPermissionFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("notPremiumUserClientFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingNotPremiumUser f = (ProfileSettingNotPremiumUser) fm.findFragmentByTag("notPremiumUserClientFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeClientAppointmentNotes") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentNotes f = (AppointmentNotes) fm.findFragmentByTag("seeClientAppointmentNotes");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("seeNotificationAmendAppointment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AmendAppointment f = (AmendAppointment) fm.findFragmentByTag("seeNotificationAmendAppointment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("messageViewFragmentNotification") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                MessageView f = (MessageView) fm.findFragmentByTag("messageViewFragmentNotification");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else if (fm.findFragmentByTag("appointmentNotificationViewFragment") != null) {
                FragmentTransaction ft = fm.beginTransaction();
                AppointmentView f = (AppointmentView) fm.findFragmentByTag("appointmentNotificationViewFragment");
                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
            } else {

                fm.beginTransaction().hide(active).show(calendarFragment).commit();
                active = calendarFragment;
                navigation.setSelectedItemId(R.id.navigation_home);
            }

        } else if (active.equals(clientFragment) || active.equals(profileFragment) || active.equals(messagesFragment) || active.equals(notificationFragment)) {
            System.out.println("CURRENTLY ACTIVE FRAGMENT IN IF: " + active.toString());
            fm.beginTransaction().hide(active).show(calendarFragment).commit();
            active = calendarFragment;
            navigation.setSelectedItemId(R.id.navigation_home);
        }
    }

    private void getNotifications() {
        Controller.user.getNotifications(this);

        System.out.println("THE NUMBER OF ZOUBI IS" + String.valueOf(Controller.newNotificationCounter));
//        SetUpBottomNavigationCounter();

    }

    public void notifyEndOfSubscription() {

        Intent intent = new Intent(Dashboard.this, RegistrationPackages.class);
        startActivity(intent);

    }

    public void didUpdateNotifications(List<MappedNotification> mappedNotifications) {
        List<MappedNotification> notificationListNew;
        notificationListNew = new ArrayList<>();

        for (int i = 0; i < mappedNotifications.size(); i++) {

            if (mappedNotifications.get(i).getStatus().equals("0")) {
                notificationListNew.add(mappedNotifications.get(i));
            }
        }

        if (notificationListNew.size() != 0) {
            BottomNavigationView bottomNavigation = (BottomNavigationView) findViewById(R.id.navigation);
            BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bottomNavigation.getChildAt(0);
            View vBotNavView = bottomNavigationMenuView.getChildAt(2); //replace 3 with the index of the menu item that you want to add the badge to.
            itemView = (BottomNavigationItemView) vBotNavView;
            vBadge = LayoutInflater.from(this).inflate(R.layout.notificiation_badge, bottomNavigationMenuView, false);
            itemView.addView(vBadge);
            mtxtnotificationsbadge = (TextView) findViewById(R.id.txtnotificationsbadge);
            mtxtnotificationsbadge.setText(String.valueOf(notificationListNew.size()));
            if (mtxtnotificationsbadge.getText().length() == 1) {
                //if the counter is between 0-9 then set the textview background shape to circle
                mtxtnotificationsbadge.setBackgroundResource(R.drawable.custom_circle_shape);
            } else {
                //if the counter is greater than 9 then set the textview background shape to circle
                mtxtnotificationsbadge.setBackgroundResource(R.drawable.custom_rectangle_shape);
            }
        } else {

        }
    }


}

package com.appointsync.appointsyncpro;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedPlan;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePackage;
import com.appointsync.appointsyncpro.Interface.RenewPlanPaymentDelegate;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A simple {@link Fragment} subclass.
 */
public class RenewPlanPayment extends Fragment {

    private WebView paymentWebView;
    public boolean isUpgrade = false;
    public int type = 0;
    public String subscriptionPackage = "";
    public RenewPlanPayment minime;
    public RenewPlanPaymentDelegate delegate;
    public RenewPlanPayment() {
        // Required empty public constructor
    }


    public WebViewClient wvc =  new WebViewClient() {

        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            try {
                System.out.println("REQUEST OVERRIDE " + url);
                if (url.endsWith("/Payment.php")){
                    System.out.println("REQUEST OVERRIDE PAYMENT");
                    OkHttpClient okHttpClient = new OkHttpClient();
                    Long date = System.currentTimeMillis() / 1000;
                    Request request = new Request.Builder().url(url).addHeader("UPGRADING", isUpgrade ? "Y" : "N")
                            .addHeader("TYPE", String.valueOf(type))
                            .addHeader("PACKAGE", subscriptionPackage)
                            .addHeader("ORDERID", Controller.user.getWebID() + date.toString())
                            .addHeader("USERID", Controller.user.getId()).build();

                    Response response = okHttpClient.newCall(request).execute();


                    return new WebResourceResponse("text/html", // You can set something other as default content-type
                            "text/html",  // Again, you can set another encoding as default
                            response.body().byteStream());
                }
                else{
                    return super.shouldInterceptRequest(view,url);
//                    try {
//                        URL u = new URL(url);
//                        URLConnection connection = u.openConnection();
//                        return new WebResourceResponse(connection.getContentType(), connection.getHeaderField("encoding"), connection.getInputStream());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        return null;
//                    }
                }




            } catch (Exception e) {
                //return null to tell WebView we failed to sfetch it WebView should try again.
                e.printStackTrace();
                return null;

            }
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            // do your stuff here

            if (url.endsWith("/Payment.php")) {
                System.out.println("SENDING SHOW PAYMENT PAGE");
                view.loadUrl("javascript:showPaymentPage()");
            }
            if (url.contains("Payment.php")){
                System.out.println("ADDING APP");
                view.addJavascriptInterface(minime, "app");
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            System.out.println("onReceivedError :" + error);
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            System.out.println("onReceivedError HTTP :" + errorResponse);
        }
    };



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_renew_plan_payment, container, false);
        paymentWebView = view.findViewById(R.id.renewPlanWebView);
        paymentWebView.setWebViewClient(wvc);
        paymentWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                System.out.println("onReceivedError CHROME " + consoleMessage.message());
                return true;
            }

        });
        minime = this;

        paymentWebView.setInitialScale(1);
        paymentWebView.getSettings().setLoadWithOverviewMode(true);
        paymentWebView.getSettings().setUseWideViewPort(true);
        paymentWebView.getSettings().setDefaultFontSize(25);
        paymentWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        paymentWebView.setScrollbarFadingEnabled(false);
        paymentWebView.getSettings().setJavaScriptEnabled(true);
        paymentWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        paymentWebView.getSettings().setDomStorageEnabled(true);
        paymentWebView.getSettings().setAllowContentAccess(true);
        paymentWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        paymentWebView.addJavascriptInterface(this, "app");
        paymentWebView.loadUrl("https://appointsync.com/payment/Payment.php");



        return view;
    }
    private void closeCurrentView(){

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        RenewPlanPayment f = (RenewPlanPayment) fm.findFragmentByTag("seeRenewPlanPaymentFragment");
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();
    }
    @JavascriptInterface
    public void completeCallback(String message){
        System.out.println("THIS IS SPARTAAAAAAAAAAAA");
        if (message.equalsIgnoreCase("success")) {
            if (Controller.noActiveSubscription!=null&&Controller.noActiveSubscription.equals(true)) {
                Call<AppointSyncResponse<MappedPlan>> call = RetrofitInstance.getRetrofitInstance(new PostUpdatePackage(subscriptionPackage, type));
                call.enqueue(new Callback<AppointSyncResponse<MappedPlan>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedPlan>> call, retrofit2.Response<AppointSyncResponse<MappedPlan>> response) {

                        if(response.body().getErrorCode()==0) {
                            if (Controller.noActiveSubscription != null && Controller.noActiveSubscription.equals(true)) {

                                Controller.noActiveSubscription = false;
                                Intent intent = new Intent(getActivity(), Initial.class);
                                startActivity(intent);

                            }
                        }else{
                            Toast.makeText(getActivity(), "Something went wrong!! please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<MappedPlan>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong!! please try again later.", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                delegate.didFinishPaying();
                closeCurrentView();
            }
        }
        else if (message.equalsIgnoreCase("cancel")){
            closeCurrentView();
        }
    }
}

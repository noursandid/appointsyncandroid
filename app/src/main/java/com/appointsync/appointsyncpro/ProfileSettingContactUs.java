package com.appointsync.appointsyncpro;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.PostChangePassword;
import com.appointsync.appointsyncpro.Class.Main.PostContactUs;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingContactUs extends Fragment {
    private TextInputEditText editProfileContactUsSubjectText;
    private EditText editProfileContactUsMessage;
    private Button btnSettingSubmitContactForm;
    private Spinner editProfileContactUsSpinner;
    private String selectedContactType;
    private long mLastClickTime = 0;

    public ProfileSettingContactUs() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting_contact_us, container, false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        final ImageView profileSettingContactUsBackArrow = view.findViewById(R.id.profileSettingContactUsBackArrow);

        editProfileContactUsSpinner = view.findViewById(R.id.editProfileContactUsSpinner);
        btnSettingSubmitContactForm = view.findViewById(R.id.btnSettingSubmitContactForm);
        editProfileContactUsMessage = view.findViewById(R.id.editProfileContactUsMessage);
        editProfileContactUsSubjectText = view.findViewById(R.id.editProfileContactUsSubjectText);

        List<String> categories = new ArrayList<String>();
        categories.add("Contact Us");
        categories.add("Report An Issue");
        categories.add("Suggestion");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        editProfileContactUsSpinner.setAdapter(dataAdapter);
        editProfileContactUsSpinner.setSelection(0);

        editProfileContactUsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                selectedContactType = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btnSettingSubmitContactForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                if (editProfileContactUsSubjectText.getText().toString().length() == 0 || editProfileContactUsMessage.getText().length() == 0) {
                    Toast.makeText(getActivity(), "Please fill the form before submit!", Toast.LENGTH_LONG).show();
                } else {
                    Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostContactUs(Controller.user.getEmail(), String.valueOf(Build.VERSION.SDK_INT), editProfileContactUsMessage.getText().toString(), editProfileContactUsSubjectText.getText().toString(), String.valueOf(Controller.user.getType()), selectedContactType));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                            if (response.body().getErrorCode() != 0) {
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_LONG).show();
                            } else if (response.body().getErrorCode() == 0) {
                                Toast.makeText(getActivity(), "Thank you for your message. We will get back to you shortly.", Toast.LENGTH_LONG).show();
                                editProfileContactUsMessage.setText("");
                                editProfileContactUsSubjectText.setText("");
                                editProfileContactUsSpinner.setSelection(0);
                            }
                            pd.dismiss();

                            HideKeyboardFunction.hideKeyboard(getActivity());
                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        });


        profileSettingContactUsBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingContactUs f = (ProfileSettingContactUs) fm.findFragmentByTag("profileSettingContactUsFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });


        return view;
    }
}

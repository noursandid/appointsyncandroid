package com.appointsync.appointsyncpro;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetFilesPages;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileAddAttachment;
import com.appointsync.appointsyncpro.Class.Main.MappedFile;
import com.appointsync.appointsyncpro.Class.Main.MappedFileAttachment;
import com.appointsync.appointsyncpro.Class.Main.PostClientFileUpdate;
import com.appointsync.appointsyncpro.Class.Main.PostClientFileUpdateNote;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileAttachmentsAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileViewAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateFilePageView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.Network.UploadFile;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientFile extends Fragment implements Observer, UpdateFilePageView {
    private MappedClient mappedClient;
    private TextView clientFileClientName, clientFileClientFullName, clientFileClientDOB, clientFileClientLastAppointmentDate, clientFileClientFileDate, clientFileClientGender,
            clientFilePhoneNumber, clientFilePageNumber, clientFileClientNumber, clientFileAttachmentTV;
    private Button clientFileShowBalance, clientFileShowPages, clientFileAttachFile;
    private EditText clientFileClientNote;
    private ImageView clientFileBackArrow;
    private View clientFileLine;
    private ScrollView clientFileScrollView;
    private String filePinCode = "";
    private long mLastClickTime = 0;
    private InputStream inputStreamImg;
    private String filePickerLocation = "";
    private File fileToDelete;

    private Boolean isRefreshing = false;

    private MappedFile selectedMappedFile;

    long delay = 1000; // 1 seconds after user stops typing
    long last_text_edit = 0;
    Handler handler = new Handler();

    private RecyclerView recyclerView;
    private RecyclerView clientFileAttachmentRecyclerView;

    private ArrayList<MappedFileAttachment> list_data;
    UpdateFilePageView minime;
    public ClientFileViewAdapter adapter;
    private ClientFileAttachmentsAdapter clientFileAttachmentAdapter;
    public ButtonSelectedProtocol delegate;

    private Fragment clientFileFragment;

    int gridViewHeight = 0;
    int gridElements = 0;

    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2, PICK_PDF_FILE = 3;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {

            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    public ClientFile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_client_file, container, false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);
        clientFileFragment = this;
        minime = this;
        clientFileClientName = view.findViewById(R.id.clientFileClientName);
        clientFileClientFullName = view.findViewById(R.id.clientFileClientFullName);
        clientFileClientDOB = view.findViewById(R.id.clientFileClientDOB);
        clientFileClientLastAppointmentDate = view.findViewById(R.id.clientFileClientLastAppointmentDate);
        clientFileClientFileDate = view.findViewById(R.id.clientFileClientFileDate);
        clientFileClientGender = view.findViewById(R.id.clientFileClientGender);
        clientFilePhoneNumber = view.findViewById(R.id.clientFilePhoneNumber);
        clientFilePageNumber = view.findViewById(R.id.clientFilePageNumber);
        clientFileClientNumber = view.findViewById(R.id.clientFileClientNumber);
        clientFileClientNote = view.findViewById(R.id.clientFileClientNote);
        clientFileBackArrow = view.findViewById(R.id.clientFileBackArrow);
        clientFileLine = view.findViewById(R.id.clientFileLine);
        clientFileAttachmentTV = view.findViewById(R.id.clientFileAttachmentTV);
        clientFileScrollView = view.findViewById(R.id.clientFileScrollView);

        clientFileShowBalance = view.findViewById(R.id.clientFileShowBalance);
        clientFileAttachFile = view.findViewById(R.id.clientFileAttachFile);
        clientFileShowPages = view.findViewById(R.id.clientFileShowPages);

        clientFileShowBalance.setVisibility(View.INVISIBLE);
        clientFileAttachFile.setVisibility(View.INVISIBLE);
        clientFileShowBalance.setVisibility(View.INVISIBLE);
        clientFileClientNote.setVisibility(View.INVISIBLE);
        clientFileShowPages.setVisibility(View.INVISIBLE);
        clientFileLine.setVisibility(View.INVISIBLE);
        clientFileAttachmentTV.setVisibility(View.INVISIBLE);


        list_data = new ArrayList<>();

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        clientFileAttachmentRecyclerView = view.findViewById(R.id.clientFileAttachmentRecyclerView);
        clientFileAttachmentRecyclerView.setHasFixedSize(true);
        clientFileAttachmentRecyclerView.setNestedScrollingEnabled(false);
        clientFileAttachmentRecyclerView.setItemViewCacheSize(20);
        clientFileAttachmentRecyclerView.setDrawingCacheEnabled(true);
        clientFileAttachmentRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        recyclerView = view.findViewById(R.id.clientFileRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        clientFileClientNote.addTextChangedListener(new TextWatcher() {
                                                        @Override
                                                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                                                      int after) {
                                                        }

                                                        @Override
                                                        public void onTextChanged(final CharSequence s, int start, int before,
                                                                                  int count) {
                                                            //You need to remove this to run only once
                                                            handler.removeCallbacks(input_finish_checker);

                                                        }

                                                        @Override
                                                        public void afterTextChanged(final Editable s) {
                                                            //avoid triggering event when text is empty
                                                            if (s.length() > 0) {
                                                                last_text_edit = System.currentTimeMillis();
                                                                handler.postDelayed(input_finish_checker, delay);
                                                            } else {

                                                            }
                                                        }
                                                    }

        );


        if (Controller.user.getHiddenFiles().equals(1)) {
            filePinCode = Controller.userPinCode;
        }

        Call<AppointSyncResponse<ArrayList<MappedFile>>> call = RetrofitInstance.getRetrofitInstance(new GetFilesPages(filePinCode, mappedClient.getWebID()));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedFile>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedFile>>> call, Response<AppointSyncResponse<ArrayList<MappedFile>>> response) {

                if (response.body().getErrorCode() == 0) {

                    if (Controller.selectedClientFilePage != null) {
                        Controller.selectedClientFilePage = null;
                    }

                    Controller.clientFilePage = true;

                    mappedClient.mappedFiles = response.body().getData();
                    Controller.selectedClientFilePage = mappedClient.mappedFiles.size() - 1;
                    if (mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getAttachments().size() >= 1) {
                        gridViewHeight = 200;
                    }
                    for (int i = 0; i < mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getAttachments().size(); i++) {

                        list_data.add(mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getAttachments().get(i));
                        gridElements++;
                        if (gridElements > 3) {
                            gridViewHeight += 200;
                            gridElements = 0;

                        }

                    }
                    makeView(Controller.selectedClientFilePage);
                    selectedMappedFile = mappedClient.mappedFiles.get(Controller.selectedClientFilePage);
                    adapter = new ClientFileViewAdapter(selectedMappedFile, mappedClient, getActivity().getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    pd.dismiss();


                } else if (response.body().getErrorCode() == 353700) {

                    pd.dismiss();
                    Controller.clientFilePage = false;
                    delegate.seeClientAddFilePage(mappedClient, minime);

                } else {
                    pd.dismiss();
                    Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                }


                int numberOfColumns = 3;
                clientFileAttachmentRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
                clientFileAttachmentAdapter = new ClientFileAttachmentsAdapter(list_data, getActivity().getApplicationContext());

                clientFileAttachmentRecyclerView.setAdapter(clientFileAttachmentAdapter);


            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<ArrayList<MappedFile>>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                System.out.print("WHY THE ERROR: " + t.toString());
                t.printStackTrace();

            }


        });


        clientFileAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                selectFile();


            }
        });


        clientFileShowBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (adapter != null) {
                    adapter.stopScheduler();
                }
                delegate.seeClientFileBalance(mappedClient);
            }
        });// go to dashboard


        clientFileShowPages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (adapter != null) {
                    adapter.stopScheduler();
                }   if (adapter != null) {
                    adapter.stopScheduler();
                }
                delegate.seeClientAddFilePage(mappedClient, minime);
//                kif mapped client hal bahloul
            }
        });


        clientFileBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (adapter != null) {
                    adapter.stopScheduler();
                }
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (clientFileFragment != null) {
                    ft.remove(clientFileFragment);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });

        return view;
    }
    public void didSelectPage(int pageNumber) {
        update();
    }

    @Override
    public void onResume() {
        super.onResume();
        HideKeyboardFunction.hideKeyboard(getActivity());
    }

    @Override
    public void update() {

        updateFileView(Controller.selectedClientFilePage);
    }

    public void makeView(int mappedFilePage) {

        System.out.println("Zoubi page number: " + mappedFilePage);


        clientFileShowBalance.setVisibility(View.VISIBLE);
        clientFileAttachFile.setVisibility(View.VISIBLE);
        clientFileShowBalance.setVisibility(View.VISIBLE);
        clientFileClientNote.setVisibility(View.VISIBLE);
        clientFileClientNote.clearFocus();
        clientFileShowPages.setVisibility(View.VISIBLE);
        clientFileLine.setVisibility(View.VISIBLE);
        clientFileAttachmentTV.setVisibility(View.VISIBLE);

        int locationIndex = mappedFilePage;
        clientFileClientName.setText(mappedClient.mappedFiles.get(locationIndex).getName());
        clientFileClientFullName.setText(Html.fromHtml("<b>Name: </b>" + mappedClient.mappedFiles.get(locationIndex).getName()));
        clientFileClientDOB.setText(Html.fromHtml("<b>Date of Birth: </b>" + UnixDateConverter.UnixDateConverter(mappedClient.mappedFiles.get(locationIndex).getDob())));
        if (mappedClient.mappedFiles.get(locationIndex).getLastAppointmentDate() == null) {
            clientFileClientLastAppointmentDate.setText(Html.fromHtml("<b>Last Appointment Date: </b>No previous appointments"));
        } else {
            clientFileClientLastAppointmentDate.setText(Html.fromHtml("<b>Last Appointment Date: </b>" + UnixDateConverter.UnixDateConverterAppointmentTime(mappedClient.mappedFiles.get(locationIndex).getLastAppointmentDate())));
        }
        clientFileClientFileDate.setText(Html.fromHtml("<b>File Date: </b>" + UnixDateConverter.UnixDateConverterAppointmentTime(mappedClient.mappedFiles.get(locationIndex).getDate())));
        clientFileClientGender.setText(Html.fromHtml("<b>Gender: </b>" + mappedClient.mappedFiles.get(locationIndex).getGender()));
        clientFilePhoneNumber.setText(Html.fromHtml("<b>Phone Number: </b>" + mappedClient.mappedFiles.get(locationIndex).getPhoneNumber()));
        clientFilePageNumber.setText(Html.fromHtml("<b>Page Number: </b>" + mappedClient.mappedFiles.get(locationIndex).getNumber()));
        clientFileClientNumber.setText(Html.fromHtml("<b>Client Number: </b>" + mappedClient.mappedFiles.get(locationIndex).getClientNumber()));
        String fileClientNote = Html.fromHtml(mappedClient.mappedFiles.get(locationIndex).getNote()).toString();
        fileClientNote = Html.fromHtml(fileClientNote).toString();
        clientFileClientNote.setText(fileClientNote);
        HideKeyboardFunction.hideKeyboard(getActivity());
        clientFileScrollView.scrollTo(0, 0);


    }

    public void updateFileView(int mappedFilePage) {

        makeView(mappedFilePage);

        adapter = new ClientFileViewAdapter(mappedClient.mappedFiles.get(mappedFilePage), mappedClient, getActivity().getApplicationContext());
        recyclerView.setAdapter(adapter);

        list_data.clear();

        for (int i = 0; i < mappedClient.mappedFiles.get(mappedFilePage).getAttachments().size(); i++) {

//                        String fileFromat = mappedClient.mappedFiles.get(selectedFilePageIndex).getAttachments().get(i).substring(mappedClient.mappedFiles.get(selectedFilePageIndex).getAttachments().lastIndexOf('.') + 1).trim();

            list_data.add(mappedClient.mappedFiles.get(mappedFilePage).getAttachments().get(i));
            gridElements++;
            if (gridElements > 3) {
                gridViewHeight += 200;
                gridElements = 0;

            }

        }

        int numberOfColumns = 3;
//        clientFileAttachmentRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
//        clientFileAttachmentAdapter = new ClientFileAttachmentsAdapter(list_data, getActivity().getApplicationContext());
//
//        clientFileAttachmentRecyclerView.setAdapter(clientFileAttachmentAdapter);

        clientFileAttachmentAdapter.notifyDataSetChanged();


    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

//    @Override
//    public void onBackPressed(){
//        if(active == profileFragment || active == messagesFragment || active == notificationFragment || active == clientFragment){
//            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            fm.beginTransaction().hide(active).show(calendarFragment).commit();
//            active = calendarFragment;
//        }
//    }

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {

                saveFileNote();
            }
        }
    };

    public void saveFileNote() {

        if (mappedClient.mappedFiles != null) {
            if (mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getNote() != null) {
                final String clientFileNote = clientFileClientNote.getText().toString().replaceAll("\n", "<br>");
                String fileClientNote = Html.fromHtml(mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getNote()).toString();

                if (!fileClientNote.equals(clientFileNote)) {

                    Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostClientFileUpdateNote(mappedClient.getWebID(), mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getWebID(), clientFileNote));
                    call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                            mappedClient.mappedFiles.get(Controller.selectedClientFilePage).setNote(clientFileNote);

                        }

                        @Override
                        public void onFailure
                                (Call<AppointSyncResponse<Success>> call, Throwable t) {

                        }
                    });

                }
            }
        }

    }

    public void selectFile() {

        try {
            PackageManager pm = getActivity().getPackageManager();
            final int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Choose From Files", "Cancel"};
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                            dialog.dismiss();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {
                                    Log.v(TAG, "Permission is granted");

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                                } else {

                                    Log.v(TAG, "Permission is revoked");
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                    Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                                }
                            } else { //permission is automatically granted on sdk<23 upon installation
                                Log.v(TAG, "Permission is granted");

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);

                            }

                        } else {

                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_REQUEST_CODE);
//                                Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                        }
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG, "Permission is granted");

                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                            } else {

                                Log.v(TAG, "Permission is revoked");
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                            }
                        } else { //permission is automatically granted on sdk<23 upon installation
                            Log.v(TAG, "Permission is granted");

                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                        }
                    } else if (options[item].equals("Choose From Files")) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG, "Permission is granted");

                                Intent intent = new Intent();
                                intent.setType("application/pdf");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_FILE);

                            } else {

                                Log.v(TAG, "Permission is revoked");
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                            }
                        } else { //permission is automatically granted on sdk<23 upon installation
                            Log.v(TAG, "Permission is granted");

                            Intent intent = new Intent();
                            intent.setType("application/pdf");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_FILE);

                        }
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            filePickerLocation = "PICK_IMAGE_CAMERA";
            if (data != null) {
                try {


                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
//
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");

                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    fileToDelete = file;
                    Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

                    uploadFile(uri);


//                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
////
//                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
//                    File destination = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
//
//                    FileOutputStream fo;
//                    try {
//                        destination.createNewFile();
//                        fo = new FileOutputStream(destination);
//                        fo.write(bytes.toByteArray());
//                        fo.close();
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                    fileToDelete = file;
//                    Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);
//
//                    uploadFile(uri);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            filePickerLocation = "PICK_IMAGE_GALLERY";
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    uploadFile(selectedImage);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            }
        } else if (requestCode == PICK_PDF_FILE) {
            filePickerLocation = "PICK_PDF_FILE";
            if (data != null) {
                File file = new File(getRealPathFromURI(getContext(), data.getData()));
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getAbsoluteFile());

                MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                Toast.makeText(getActivity(), "Uploading...", Toast.LENGTH_LONG).show();
                RequestBody clientWebID = RequestBody.create(MediaType.parse("text/plain"), mappedClient.getWebID().toString());
                RequestBody pageWebID = RequestBody.create(MediaType.parse("text/plain"), mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getWebID().toString());

                Call<AppointSyncResponse<MappedFileAttachment>> call = UploadFile.getImageRetrofitInstance().uploadClientFileAttachment(clientWebID, pageWebID, multipartBody);

                call.enqueue(new Callback<AppointSyncResponse<MappedFileAttachment>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedFileAttachment>> call, Response<AppointSyncResponse<MappedFileAttachment>> response) {
                        System.out.println("SUCCEEDED");
                        if (response.body().getErrorCode() == 0) {
                            Toast.makeText(getActivity(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                            int mappedFileIndex = Controller.selectedClientFilePage;
                            mappedClient.mappedFiles.get(mappedFileIndex).getAttachments().add(response.body().getData());
                            updateFileView(Controller.selectedClientFilePage);
                        } else if(response.body().getErrorCode() == 120) {
                            Toast.makeText(getActivity(), "File size must be less than 2MB", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<MappedFileAttachment>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                        t.printStackTrace();
                    }
                });
            }
        }
    }

    public String getRealPathFromURI(final Context context, final Uri uri) {
        if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
            System.out.println(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + uri.getLastPathSegment().toString());
            return Environment.getExternalStorageDirectory().getPath().toString() + File.separator + uri.getLastPathSegment().toString();
        } else {
            //check for KITKAT or above
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {
                    return getDownloadFilePath(context, uri);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }
                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };
                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
            return null;
        }

    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getDownloadFilePath(Context context, Uri uri) {
        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };
        try {
            cursor = context.getContentResolver().query(uri, projection, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                String fileName = cursor.getString(0);
                String path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                if (!TextUtils.isEmpty(path)) {
                    return path;
                }
            }
        } finally {
            cursor.close();
        }
        String id = DocumentsContract.getDocumentId(uri);
        if (id.startsWith("raw:")) {
            return id.replaceFirst("raw:", "");
        }
        Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads"), java.lang.Long.valueOf(id));
        return getDataColumn(context, contentUri, null, null);
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Log.v(TAG, "Permission is granted");

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                    } else {

                        Log.v(TAG, "Permission is revoked");
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Log.v(TAG, "Permission is granted");

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                }

            } else {

                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
    }


    private void uploadFile(Uri fileUri) {

        //creating a file
        File file = new File(getRealPathFromURI(getContext(), fileUri));

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        RequestBody clientWebID = RequestBody.create(MediaType.parse("text/plain"), mappedClient.getWebID().toString());
        RequestBody pageWebID = RequestBody.create(MediaType.parse("text/plain"), mappedClient.mappedFiles.get(Controller.selectedClientFilePage).getWebID().toString());
        Toast.makeText(getActivity(), "Uploading...", Toast.LENGTH_LONG).show();
        Call<AppointSyncResponse<MappedFileAttachment>> call = UploadFile.getImageRetrofitInstance().uploadClientFileAttachment(clientWebID, pageWebID, filePart);
        //finally performing the call
        call.enqueue(new Callback<AppointSyncResponse<MappedFileAttachment>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedFileAttachment>> call, Response<AppointSyncResponse<MappedFileAttachment>> response) {
                System.out.println("SUCCEEDED");
                if (response.body().getErrorCode() == 0) {
                    Toast.makeText(getActivity(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                    int mappedFileIndex = Controller.selectedClientFilePage;
                    mappedClient.mappedFiles.get(mappedFileIndex).getAttachments().add(response.body().getData());
                    updateFileView(Controller.selectedClientFilePage);
                } else if(response.body().getErrorCode() == 120) {
                    Toast.makeText(getActivity(), "File size must be less than 2MB", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                }
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<MappedFileAttachment>> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_LONG).show();
                t.printStackTrace();
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }
        });
    }

}

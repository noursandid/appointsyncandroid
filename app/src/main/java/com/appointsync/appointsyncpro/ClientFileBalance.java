package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Currency;
import com.appointsync.appointsyncpro.Class.CurrencyList;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.PostCurrency;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileBalanceAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.gson.Gson;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientFileBalance extends Fragment implements Observer {
    private MappedClient mappedClient;
    private TextView ClientFileBalanceClientName;
    private ImageView ClientFileBalanceBackArrow;
    private ProgressDialog pd;
    private String filePin = "";
    private List<MappedClientFileBalance> mappedClientFileBalances;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private long mLastClickTime = 0;
    private ButtonSelectedProtocol delegate;

    private Button balanceAddPaymentReceived, balanceAddPaymentDue;
    public ClientFileBalance() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_file_balance, container, false);

        if(Controller.userPinCode !=null){
            filePin = Controller.userPinCode;
        }

        pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        ClientFileBalanceClientName = view.findViewById(R.id.ClientFileBalanceClientName);
        balanceAddPaymentDue = view.findViewById(R.id.balanceAddPaymentDue);
        balanceAddPaymentReceived = view.findViewById(R.id.balanceAddPaymentReceived);
        ClientFileBalanceBackArrow = view.findViewById(R.id.ClientFileBalanceBackArrow);
        recyclerView = view.findViewById(R.id.clientFileBalanceRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        ClientFileBalanceClientName.setText(mappedClient.getIdentifier());

        mappedClientFileBalances = new ArrayList<>();

        pd.show();
        Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call = RetrofitInstance.getRetrofitInstance(new GetClientFileBalance(mappedClient.getWebID(),filePin));
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClientFileBalance>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call, Response<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> response) {

                if(response.body().getErrorCode() == 0) {
                    if (mappedClientFileBalances != null) {
                        mappedClientFileBalances.clear();
                    }
                    List<MappedClientFileBalance> mappedClientFileBalances1 = response.body().getData();

                    int mappedClientFileBalancesCounter = mappedClientFileBalances1.size();
                    for (int i = 0; i < mappedClientFileBalancesCounter; i++) {

                        mappedClientFileBalances.add(mappedClientFileBalances1.get(i));

                    }

                    adapter = new ClientFileBalanceAdapter(mappedClientFileBalances, getActivity().getApplicationContext());
                    recyclerView.setAdapter(adapter);

                }else if(response.body().getErrorCode() == 363800){


                    InputStream inputStream = getActivity().getResources().openRawResource(R.raw.country_currency_codes);
                    String jsonString = readJsonFile(inputStream);

                    Gson countryGson = new Gson();
                    final CurrencyList currencyList = countryGson.fromJson(jsonString, CurrencyList.class);

                    final ArrayList<Currency> allCurrencyList = currencyList.currencies;

                    List<String> list = new ArrayList<String>();
                    for (Currency currency : allCurrencyList) {

                        list.add(currency.name.toString()+" - "+currency.code.toString());



                    }



                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Set Currency");

                    final Spinner spinner = new Spinner(getActivity());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    spinner.setLayoutParams(lp);
                    builder.setView(spinner);
                    builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            Integer countryCodeSpinnerSelect = spinner.getSelectedItemPosition();
                            String selectedCountryCodeSpinnerSelectedID = allCurrencyList.get(countryCodeSpinnerSelect).code.toString();

                            Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostCurrency(selectedCountryCodeSpinnerSelectedID));
                            call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                                @Override
                                public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                                    if(response.body().getErrorCode() == 0){
                                        updateView();
                                    }else{
                                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                        closeView();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                                    Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                                    closeView();
                                }

                            });

                        }
                    });
                    builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    closeView();

                                }
                            });
                    builder.setCancelable(false);
                    builder.show();

                    ArrayAdapter<String> dataAdapterPhone = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
                    dataAdapterPhone.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(dataAdapterPhone);


                }
                        pd.dismiss();

                    }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call, Throwable t) {


                pd.dismiss();
            }

        });

        balanceAddPaymentDue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                delegate.seeClientBalanceAddPaymentDue(mappedClient);
            }

        });

        balanceAddPaymentReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                delegate.seeClientBalanceAddPaymentReceived(mappedClient);

            }

        });



        ClientFileBalanceBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

             closeView();

            }

        });


                return view;
    }

    @Override
    public void update() {

        updateView();
    }

    public void updateView(){

        Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call = RetrofitInstance.getRetrofitInstance(new GetClientFileBalance(mappedClient.getWebID(),filePin));
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClientFileBalance>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call, Response<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> response) {
                if (mappedClientFileBalances != null) {
                    mappedClientFileBalances.clear();
                }
                List<MappedClientFileBalance> mappedClientFileBalances1 = response.body().getData();

                int mappedClientFileBalancesCounter = mappedClientFileBalances1.size();
                for (int i = 0; i < mappedClientFileBalancesCounter; i++) {

                    mappedClientFileBalances.add(mappedClientFileBalances1.get(i));

                }

                adapter = new ClientFileBalanceAdapter(mappedClientFileBalances, getActivity().getApplicationContext());
                recyclerView.setAdapter(adapter);
                pd.dismiss();

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> call, Throwable t) {

            }

        });

    }


    private String readJsonFile(InputStream inputStream) {
// TODO Auto-generated method stub
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte bufferByte[] = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(bufferByte)) != -1) {
                outputStream.write(bufferByte, 0, length);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {

        }
        return outputStream.toString();
    }

    public void closeView(){

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ClientFileBalance f = (ClientFileBalance) fm.findFragmentByTag("seeClientFileBalanceFragment");
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();
    }

}

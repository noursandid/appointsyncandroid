package com.appointsync.appointsyncpro;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.TemplateAdapter;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.Country;
import com.appointsync.appointsyncpro.Class.CountryList;
import com.appointsync.appointsyncpro.Class.GetTemplateClass;
import com.appointsync.appointsyncpro.Class.HostRegistrationClass;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Class.Template;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterHostPremium extends AppCompatActivity {


    private TemplateAdapter adapter;
    EditText date, eText, workingHourTo;
    DatePickerDialog datePickerDialog;
    TimePickerDialog picker;
    ArrayList<Template> templates;
    Integer templateWebIDLoop = 0;
    Spinner countryCodeSpinner;
    String encryptedPasswordString, selectedDateOfBirth, workingHoursToDate, workingHoursFromDate;
    private TextView registerHostPremiumTermsAndConditions;
    private long mLastClickTime = 0;

    private Button registerHost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_host_premium);


        final Spinner spinner = (Spinner) findViewById(R.id.spinnerClientFile);
        final ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        eText = (EditText) findViewById(R.id.workingHourFromRegistration);
        eText.setInputType(InputType.TYPE_NULL);

        registerHost = findViewById(R.id.registerButton);
        registerHostPremiumTermsAndConditions = findViewById(R.id.registerHostPremiumTermsAndConditions);

        String linkText = "By registering, it means that you have read and accepted our <a href='https://www.appointsync.com/TermsAndConditions.html'>terms and conditions</a>.";
        registerHostPremiumTermsAndConditions.setText(Html.fromHtml(linkText));
        registerHostPremiumTermsAndConditions.setMovementMethod(LinkMovementMethod.getInstance());

        workingHourTo = (EditText) findViewById(R.id.workingHourToRegistration);
        workingHourTo.setInputType(InputType.TYPE_NULL);

        final ImageView cancelRegister = findViewById(R.id.backSignArrow);
        cancelRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

//                Intent intent = new Intent(RegisterHostPro.this, Login.class);
//                startActivity(intent);

                finish();

            }

        });

        date = (EditText) findViewById(R.id.dateOfBirthRegistration);
        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(RegisterHostPremium.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        eText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);
                // time picker dialog
                picker = new TimePickerDialog(RegisterHostPremium.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                eText.setText(String.format("%02d:%02d", sHour, sMinute));
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });

        workingHourTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);
                // time picker dialog
                picker = new TimePickerDialog(RegisterHostPremium.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                                workingHourTo.setText(String.format("%02d:%02d", sHour, sMinute));
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });


        final Spinner spinnerGender = findViewById(R.id.spinnerGender);

        // Initializing a String Array
        String[] plants = new String[]{
                "Select a gender...",
                "Male",
                "Female",
                "Other"
        };

        final List<String> plantsList = new ArrayList<>(Arrays.asList(plants));

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, plantsList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerGender.setAdapter(spinnerArrayAdapter);

        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
//                    Toast.makeText
//                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
//                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


//countryCodeSpinner

        countryCodeSpinner = (Spinner) findViewById(R.id.spinnerCountryCode);
        InputStream inputStream = this.getResources().openRawResource(R.raw.countrycodes);
        String jsonString = readJsonFile(inputStream);

        Gson countryGson = new Gson();
        final CountryList countryList = countryGson.fromJson(jsonString, CountryList.class);

        final ArrayList<Country> allCountryList = countryList.countries;

        List<String> list = new ArrayList<String>();
        for (Country country : allCountryList) {

            list.add(country.name.toString() + " (+" + country.dial_code.toString() + ")");

        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCodeSpinner.setAdapter(dataAdapter);


//        Client File Spinner


        final Spinner clientFileSpinner = findViewById(R.id.spinnerClientFile);

        final List<String> clientFilesList = new ArrayList<>();

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> clientFileSpinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, clientFilesList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);

                }
                return view;
            }
        };
        clientFileSpinnerArrayAdapter.add("Select a template");
        clientFileSpinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        clientFileSpinner.setAdapter(clientFileSpinnerArrayAdapter);

        clientFileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
//                    Toast.makeText
//                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
//                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });


//        spinner.setAdapter(spinnerAdapter);
//        spinnerAdapter.add("Select a template");

        /** Create handle for the RetrofitInstance interface*/

        /** Call the method with parameter in the interface to get the notice data*/
        Call<AppointSyncResponse<ArrayList<Template>>> call = RetrofitInstance.getRetrofitInstance(new GetTemplateClass());


        call.enqueue(new Callback<AppointSyncResponse<ArrayList<Template>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<Template>>> call, Response<AppointSyncResponse<ArrayList<Template>>> response) {

                for (Iterator<Template> i = response.body().getData().iterator(); i.hasNext(); ) {
                    Template template = i.next();

                    clientFileSpinnerArrayAdapter.add(template.getName());
                    clientFileSpinnerArrayAdapter.notifyDataSetChanged();


                }

                templates = response.body().getData();

            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<Template>>> call, Throwable t) {
                Toast.makeText(RegisterHostPremium.this, "Something went wrong...Error message: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }

        });


        registerHost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                registerHost.setEnabled(false);

                try {


                    Integer countryCodeSpinnerSelect = countryCodeSpinner.getSelectedItemPosition();
                    String selectedCountryCodeSpinnerSelectedID = allCountryList.get(countryCodeSpinnerSelect).dial_code.toString();


                    final TextInputEditText firstNameRegistration = findViewById(R.id.firstNameRegistrationText);
                    final String firstNameRegistrationString = firstNameRegistration.getText().toString();


                    final TextInputEditText lastNameRegistration = findViewById(R.id.lastNameRegistrationText);
                    final String lastNameRegistrationString = lastNameRegistration.getText().toString();

                    final String spinnerGenderString = spinnerGender.getItemAtPosition(spinnerGender.getSelectedItemPosition()).toString();

                    final EditText dateOfBirthRegistration = findViewById(R.id.dateOfBirthRegistration);
                    final String dateOfBirthRegistrationInput = dateOfBirthRegistration.getText().toString();
                    selectedDateOfBirth = dateOfBirthRegistrationInput;

                    AppointsyncDate date = new AppointsyncDate(selectedDateOfBirth + " 14:00");
                    String dateOfBirthUnixMS = date.getUnix();
                    String dateOfBirthUnix = dateOfBirthUnixMS.substring(0, dateOfBirthUnixMS.length() - 3);


                    final TextInputEditText usernameRegistration = findViewById(R.id.usernameRegistrationText);
                    final String usernameRegistrationString = usernameRegistration.getText().toString();


                    final TextInputEditText passwordRegistration = findViewById(R.id.passwordRegistrationText);
                    final String passwordRegistrationString = passwordRegistration.getText().toString();

                    try {
                        MessageDigest md = MessageDigest.getInstance("SHA-512");
                        byte[] data = md.digest(passwordRegistrationString.getBytes());
                        StringBuilder encryptedPassword = new StringBuilder();
                        for (int i = 0; i < data.length; i++) {
                            encryptedPassword.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
                        }
                        encryptedPasswordString = encryptedPassword.toString();
                        System.out.println(encryptedPassword);
                        System.out.println(passwordRegistrationString.toString());
                    } catch (Exception e) {
                        System.out.println(e);
                    }

                    final TextInputEditText confirmPasswordRegistration = findViewById(R.id.confirmPasswordRegistrationText);
                    final String confirmPasswordRegistrationString = confirmPasswordRegistration.getText().toString();

                    final String spinnerTemplateString = spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();

                    Integer templateSpinnerSelect = spinner.getSelectedItemPosition();
                    Integer templateSpinnerSelected = templateSpinnerSelect - 1;
                    String selectedTemplateWebID = templates.get(templateSpinnerSelected).getWebID();


                    final TextInputEditText emailRegistration = findViewById(R.id.emailRegistrationText);
                    final String emailRegistrationString = emailRegistration.getText().toString();

                    final TextInputEditText professionRegistration = findViewById(R.id.professionRegistrationText);
                    final String professionRegistrationString = professionRegistration.getText().toString();

//
                    final EditText numberRegistration = findViewById(R.id.numberRegistration);
                    final String numberRegistrationString = numberRegistration.getText().toString();
//
                    final EditText workingHourFromRegistration = findViewById(R.id.workingHourFromRegistration);
                    final String workingHourFromRegistrationString = workingHourFromRegistration.getText().toString();
                    workingHoursFromDate = workingHourFromRegistrationString;

                    AppointsyncDate passWorkingHourFrom = new AppointsyncDate("12/12/1975 " + workingHoursFromDate);
                    String workingHourFromUnixMS = passWorkingHourFrom.getUnix();
                    String workingHourFromUnix = workingHourFromUnixMS.substring(0, workingHourFromUnixMS.length() - 3);

                    final EditText workingHourToRegistration = findViewById(R.id.workingHourToRegistration);
                    final String workingHourToRegistrationString = workingHourToRegistration.getText().toString();
                    workingHoursToDate = workingHourToRegistrationString;

                    AppointsyncDate passWorkingHourto = new AppointsyncDate("12/12/1975 " + workingHoursToDate);
                    String workingHourtoUnixMS = passWorkingHourto.getUnix();
                    String workingHourtoUnix = workingHourtoUnixMS.substring(0, workingHourtoUnixMS.length() - 3);

                    if (!passwordRegistrationString.equalsIgnoreCase(confirmPasswordRegistrationString) || confirmPasswordRegistrationString.equalsIgnoreCase("")) {
                        System.out.println("Inside if 1 ");
                        Toast.makeText(RegisterHostPremium.this, "Passwords do not match!! ", Toast.LENGTH_SHORT).show();
                        registerHost.setEnabled(true);

                    } else if (firstNameRegistrationString == "" || lastNameRegistrationString == "" || spinnerGenderString == "" || dateOfBirthUnix == "" || usernameRegistrationString == "" || passwordRegistrationString == "" || confirmPasswordRegistrationString == "" || spinnerTemplateString == "" || numberRegistrationString == "" || workingHourFromRegistrationString == "" || workingHourToRegistrationString == "") {
                        System.out.println("Inside if 2 ");
                        Toast.makeText(RegisterHostPremium.this, "Some fields are missing!! ", Toast.LENGTH_SHORT).show();
                        registerHost.setEnabled(true);

                    } else if (firstNameRegistrationString != "" || lastNameRegistrationString != "" || spinnerGenderString != "" || dateOfBirthUnix != "" || usernameRegistrationString != "" || passwordRegistrationString != "" || confirmPasswordRegistrationString != "" || spinnerTemplateString != "" || numberRegistrationString != "" || workingHourFromRegistrationString != "" || workingHourToRegistrationString != "") {

                        Pattern pattern1 = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");

                        Matcher matcher1 = pattern1.matcher(emailRegistrationString);

                        if (!matcher1.matches()) {
                            Toast.makeText(RegisterHostPremium.this, "Invalid email format.", Toast.LENGTH_SHORT).show();
                            registerHost.setEnabled(true);
                        } else {

                            /** Call the method with parameter in the interface to get the notice data*/
                            Call<AppointSyncResponse<Success>> registerHostCall = RetrofitInstance.getRetrofitInstance(new HostRegistrationClass(usernameRegistrationString, encryptedPasswordString, firstNameRegistrationString, lastNameRegistrationString, professionRegistrationString, selectedTemplateWebID, emailRegistrationString, spinnerGenderString, numberRegistrationString, selectedCountryCodeSpinnerSelectedID, workingHourFromUnix, workingHourtoUnix, dateOfBirthUnix, "T", "2"));

                            System.out.println("after call");
                            registerHostCall.enqueue(new Callback<AppointSyncResponse<Success>>() {
                                @Override
                                public void onResponse(Call<AppointSyncResponse<Success>> registerHostCall, Response<AppointSyncResponse<Success>> response) {

                                    if (response.body().isSuccessful()) {
                                        System.out.println("successful");
                                        Toast.makeText(RegisterHostPremium.this, "Registration Successful, please check your email!!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegisterHostPremium.this, Login.class);
                                        startActivity(intent);
                                    } else {
                                        Integer errorCodeResponse = response.body().getErrorCode();
                                        if (errorCodeResponse == 486600) {
                                            Toast.makeText(RegisterHostPremium.this, "Username or Email already exists!!", Toast.LENGTH_SHORT).show();
                                            registerHost.setEnabled(true);
                                        } else {
                                            Toast.makeText(RegisterHostPremium.this, "Something went wrong... Please try again!!", Toast.LENGTH_LONG).show();
                                            registerHost.setEnabled(true);
                                        }
                                    }

//                            Toast.makeText(RegisterHostPro.this, "Email Sent! Please check your inbox.", Toast.LENGTH_SHORT).show();


                                }

                                @Override
                                public void onFailure(Call<AppointSyncResponse<Success>> registerHostCall, Throwable t) {
                                    Toast.makeText(RegisterHostPremium.this, "Something went wrong...Error message: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                    registerHost.setEnabled(true);
                                }
                            });
                        }
                        }
                    } catch(Exception e){
                        Toast.makeText(RegisterHostPremium.this, "Please make sure to fill all the fields.", Toast.LENGTH_SHORT).show();
                        registerHost.setEnabled(true);
                    }
            }


        });


//        final Spinner spinnerClientFile = findViewById(R.id.spinnerClientFile);
//
//        // Initializing a String Array
//        String[] clientFiles = new String[]{
//                "Select a file...",
//                "zoubi",
//                "Female",
//                "Other"
//        };

//        final List<String> clientFilesList = new ArrayList<>(Arrays.asList(clientFiles));

        // Initializing an ArrayAdapter
//        final ArrayAdapter<String> clientFileArrayAdapter = new ArrayAdapter<String>(
//                this,R.layout.spinner_item,clientFilesList){
//            @Override
//            public boolean isEnabled(int position){
//                if(position == 0)
//                {
//                    // Disable the first item from Spinner
//                    // First item will be use for hint
//                    return false;
//                }
//                else
//                {
//                    return true;
//                }
//            }
//            @Override
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View view = super.getDropDownView(position, convertView, parent);
//                TextView tv = (TextView) view;
//                if(position == 0){
//                    // Set the hint text color gray
//                    tv.setTextColor(Color.GRAY);
//                }
//                else {
//                    tv.setTextColor(Color.BLACK);
//                }
//                return view;
//            }
//        };
//        clientFileArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
//        spinnerClientFile.setAdapter(clientFileArrayAdapter);

//        spinnerClientFile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String selectedItemText = (String) parent.getItemAtPosition(position);
//                // If user change the default selection
//                // First item is disable and it is used for hint
//                if (position > 0) {
//                    // Notify the selected item text
////                    Toast.makeText
////                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
////                            .show();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//
//        });

    }

    private String readJsonFile(InputStream inputStream) {
// TODO Auto-generated method stub
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte bufferByte[] = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(bufferByte)) != -1) {
                outputStream.write(bufferByte, 0, length);
            }
            outputStream.close();
            inputStream.close();
        } catch (IOException e) {

        }
        return outputStream.toString();
    }
}

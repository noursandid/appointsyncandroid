package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.AppointsyncDateAM;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.PostEditProfile;
import com.appointsync.appointsyncpro.Class.Main.PostVerifyPin;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingEditProfile extends Fragment implements Subject {
    int hourFrom, minuteFrom, hourTo,minuteTo;
    private TextInputEditText editProfileFirstName,editProfileLastName,editProfileProfession;
    private Button buttonWorkingHourFrom,buttonWorkingHourTo,btnSettingSaveEditProfile;
    ProgressDialog pd;
    String workingHoursFromDate, workingHoursToDate;
    private List<Observer> observers = new ArrayList<>();
    private long mLastClickTime = 0;

    public ProfileSettingEditProfile() {
        // Required empty public constructor
        observers = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_profile_setting_edit_profile, container, false);


        pd = new ProgressDialog(getContext());

        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        final ImageView profileSettingEditProfileBackArrow = view.findViewById(R.id.profileSettingEditProfileBackArrow);

        editProfileFirstName = view.findViewById(R.id.editProfileFirstNameText);
        editProfileLastName = view.findViewById(R.id.editProfileLastNameText);
        editProfileProfession = view.findViewById(R.id.editProfileProfessionText);

        buttonWorkingHourFrom = view.findViewById(R.id.btnSettingEditProfileWorkingHourFrom);
        buttonWorkingHourTo = view.findViewById(R.id.btnSettingEditProfileWorkingHourTo);
        btnSettingSaveEditProfile = view.findViewById(R.id.btnSettingSaveEditProfile);

        updateView();

        buttonWorkingHourFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override

                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        String completeMinute = String.valueOf(minute);
                        if (completeMinute.length() == 1){
                            completeMinute = "0"+completeMinute;
                        }
                        String am_pm = "";

                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hour);
                        datetime.set(Calendar.MINUTE, minute);

                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";

                        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";

                        buttonWorkingHourFrom.setText( strHrsToShow+":"+completeMinute+" "+am_pm );

                        hourFrom = hour;
                        minuteFrom = minute;

                    }
                };


                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hourFrom, minuteFrom,false);

//                timePickerDialog.setIcon(R.drawable.if_snowman);
                timePickerDialog.setTitle("Please select time.");

                timePickerDialog.show();
            }
        });


        buttonWorkingHourTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                // Create a new OnTimeSetListener instance. This listener will be invoked when user click ok button in TimePickerDialog.
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        String completeMinute = String.valueOf(minute);
                        if (completeMinute.length() == 1){
                            completeMinute = "0"+completeMinute;
                        }
                        String am_pm = "";

                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hour);
                        datetime.set(Calendar.MINUTE, minute);

                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";

                        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ?"12":datetime.get(Calendar.HOUR)+"";

                        buttonWorkingHourTo.setText( strHrsToShow+":"+completeMinute+" "+am_pm );

                        hourTo = hour;
                        minuteTo = minute;
                    }
                };


                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, onTimeSetListener, hourTo, minuteTo,false);

//                timePickerDialog.setIcon(R.drawable.if_snowman);
                timePickerDialog.setTitle("Please select time.");

                timePickerDialog.show();
            }
        });



        btnSettingSaveEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                final String workingHourFromRegistrationString = buttonWorkingHourFrom.getText().toString();
                workingHoursFromDate = workingHourFromRegistrationString;

                AppointsyncDateAM passWorkingHourFrom = new AppointsyncDateAM("12/12/1975 " + workingHoursFromDate);
                String workingHourFromUnixMS = passWorkingHourFrom.getUnix();
                final String workingHourFromUnix = workingHourFromUnixMS.substring(0, workingHourFromUnixMS.length() - 3);

                final String workingHourToRegistrationString = buttonWorkingHourTo.getText().toString();
                workingHoursToDate = workingHourToRegistrationString;

                AppointsyncDateAM passWorkingHourto = new AppointsyncDateAM("12/12/1975 " + workingHoursToDate);
                String workingHourtoUnixMS = passWorkingHourto.getUnix();
                final String workingHourtoUnix = workingHourtoUnixMS.substring(0, workingHourtoUnixMS.length() - 3);

                if(buttonWorkingHourFrom.getText().toString().length()==0||buttonWorkingHourTo.getText().toString().length()==0||editProfileLastName.getText().toString().length()==0||editProfileProfession.getText().toString().length()==0||editProfileFirstName.getText().toString().length()==0){
                    Toast.makeText(getActivity(), "Please make sure to fill all the fields!!", Toast.LENGTH_SHORT).show();
                }else if(workingHourFromUnix.equals(Controller.user.getWorkingHourFrom())&&workingHourtoUnix.equals(Controller.user.getWorkingHourTo())&&editProfileLastName.getText().toString().equals(Controller.user.getLastName())&&editProfileProfession.getText().toString().equals(Controller.user.getProfession())&&editProfileFirstName.getText().toString().equals(Controller.user.getFirstName())){
                    Toast.makeText(getActivity(), "You have no changes in your profile.", Toast.LENGTH_SHORT).show();
                }else {

                    Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostEditProfile(editProfileLastName.getText().toString(), workingHourFromUnix, workingHourtoUnix, editProfileProfession.getText().toString(), editProfileFirstName.getText().toString()));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                            if (response.body().getErrorCode() != 0) {
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                                updateView();
                            } else if (response.body().getErrorCode() == 0) {
                                Controller.user.setWorkingHourFrom(Integer.parseInt(workingHourFromUnix));
                                Controller.user.setWorkingHourTo(Integer.parseInt(workingHourtoUnix));
                                Controller.user.setLastName(editProfileLastName.getText().toString());
                                Controller.user.setFirstName(editProfileFirstName.getText().toString());
                                Controller.user.setProfession(editProfileProfession.getText().toString());
                                updateView();
                            }

                            pd.dismiss();


                        }


                        @Override
                        public void onFailure
                                (Call<AppointSyncResponse<Success>> call, Throwable t) {

                            Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        });



        profileSettingEditProfileBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingEditProfile f = (ProfileSettingEditProfile) fm.findFragmentByTag("profileSettingEditProfileFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });


        return view;
    }

    public void updateView(){

        hourFrom = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeHour(Controller.user.getWorkingHourFrom()));
        minuteFrom = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMinute(Controller.user.getWorkingHourFrom()));

        hourTo = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeHour(Controller.user.getWorkingHourTo()));
        minuteTo = Integer.parseInt(UnixDateConverter.UnixDateConverterAppointmentTimeMinute(Controller.user.getWorkingHourTo()));

        editProfileFirstName.setText(Controller.user.getFirstName());
        editProfileLastName.setText(Controller.user.getLastName());
        editProfileProfession.setText(Controller.user.getProfession());

        buttonWorkingHourFrom.setText(UnixDateConverter.UnixDateConverterTime(Controller.user.getWorkingHourFrom()));
        buttonWorkingHourTo.setText(UnixDateConverter.UnixDateConverterTime(Controller.user.getWorkingHourTo()));

        notifyObservers();
    }

    @Override
    public void register(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

}

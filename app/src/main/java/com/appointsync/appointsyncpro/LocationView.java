package com.appointsync.appointsyncpro;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetLocation;
import com.appointsync.appointsyncpro.Class.Main.MappedLocation;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationView extends Fragment {
    ButtonSelectedProtocol delegate;
    private RecyclerView mRecyclerView;
    ImageView profileLocationBackArrow, profileLocationAddLocation;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private static LocationView instance = null;
    private long mLastClickTime = 0;
    private TextView textViewNoLocation;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public LocationView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        instance = this;


        profileLocationBackArrow = view.findViewById(R.id.profileLocationBackArrow);
        profileLocationAddLocation = view.findViewById(R.id.profileLocationAddLocation);
        textViewNoLocation = view.findViewById(R.id.textViewNoLocation);
        mGridLayoutManager = new GridLayoutManager(getActivity(), 2);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        final MapAdapter mapAdapter = new MapAdapter(LIST_LOCATIONS);
        // Set up the RecyclerView
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mapAdapter);
        mRecyclerView.setRecyclerListener(mRecycleListener);


        Call<AppointSyncResponse<ArrayList<MappedLocation>>> call = RetrofitInstance.getRetrofitInstance(new GetLocation());


        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedLocation>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Response<AppointSyncResponse<ArrayList<MappedLocation>>> response) {

                if (response.body().getErrorCode() == 0) {
                    NamedLocation[] namedLocations = new NamedLocation[response.body().getData().size()];
                    int index = 0;

                    Controller.locationList = response.body().getData();

                    for (Iterator<MappedLocation> i = response.body().getData().iterator(); i.hasNext(); ) {
                        MappedLocation mappedLocation = i.next();
                        Controller.location = Controller.locationList.get(index);
                        NamedLocation namedLocation = new NamedLocation(Controller.location.getLocationTitle(), new LatLng(Double.parseDouble(Controller.location.getLocationLatitude()), Double.parseDouble(Controller.location.getLocationLongitude())));
                        namedLocations[index] = namedLocation;
                        index++;
                    }
                    LIST_LOCATIONS = namedLocations;
                    MapAdapter mapAdapter = new MapAdapter(LIST_LOCATIONS);
                    mRecyclerView.setAdapter(mapAdapter);

                    mapAdapter.notifyDataSetChanged();

                    if (Controller.locationList == null || Controller.locationList.size() == 0) {
                        textViewNoLocation.setText("You don't have any locations.");
                    }
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Throwable t) {
            }

        });


        profileLocationAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                checkLocationPermission();

            }
        });


        profileLocationBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                closeView();


            }
        });

        return view;


    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getActivity())
                        .setTitle("Location Permission Needed")
                        .setMessage("We need the location permission to set your location, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);

//                                delegate.seeAddNewLocation(profileLocationAddLocation);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        } else {
            delegate.seeAddNewLocation(profileLocationAddLocation);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    delegate.seeAddNewLocation(profileLocationAddLocation);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Need to add location permission in order to add a new location.", Toast.LENGTH_LONG).show();
                    closeView();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void closeView() {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        LocationView f = (LocationView) fm.findFragmentByTag("6");

        ft.remove(f);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

        ft.commit();

    }
    /** Create a menu to switch between Linear and Grid LayoutManager. */
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.lite_list_menu, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.layout_linear:
//                mRecyclerView.setLayoutManager(mLinearLayoutManager);
//                break;
//            case R.id.layout_grid:
//                mRecyclerView.setLayoutManager(mGridLayoutManager);
//                break;
//        }
//        return true;
//    }

    /**
     * Adapter that displays a title and {@link com.google.android.gms.maps.MapView} for each item.
     * The layout is defined in <code>lite_list_demo_row.xml</code>. It contains a MapView
     * that is programatically initialised in
     * {@link #(int, android.view.View, android.view.ViewGroup)}
     */
    private class MapAdapter extends RecyclerView.Adapter<MapAdapter.ViewHolder> {

        private NamedLocation[] namedLocations;

        private MapAdapter(NamedLocation[] locations) {
            super();
            namedLocations = locations;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_location_item, parent, false));
        }

        /**
         * This function is called when the user scrolls through the screen and a new item needs
         * to be shown. So we will need to bind the holder with the details of the next item.
         */
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (holder == null) {
                return;
            }
            holder.bindView(position);
        }

        @Override
        public int getItemCount() {
            return namedLocations.length;
        }

        /**
         * Holder for Views used in the {@link LocationView.MapAdapter}.
         * Once the  the <code>map</code> field is set, otherwise it is null.
         * When the {@link #onMapReady(com.google.android.gms.maps.GoogleMap)} callback is received and
         * the {@link com.google.android.gms.maps.GoogleMap} is ready, it stored in the {@link #map}
         * field. The map is then initialised with the NamedLocation that is stored as the tag of the
         * MapView. This ensures that the map is initialised with the latest data that it should
         * display.
         */
        class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

            MapView mapView;
            TextView title;
            GoogleMap map;
            View layout;

            private ViewHolder(View itemView) {
                super(itemView);
                layout = itemView;
                mapView = layout.findViewById(R.id.lite_listrow_map);
                title = layout.findViewById(R.id.lite_listrow_text);
                if (mapView != null) {
                    // Initialise the MapView
                    mapView.onCreate(null);
                    // Set the map ready callback to receive the GoogleMap object
                    mapView.getMapAsync(this);
                }
            }

            @Override
            public void onMapReady(GoogleMap googleMap) {
                MapsInitializer.initialize(getContext());
                map = googleMap;
                setMapLocation();
            }

            /**
             * Displays a {@link LocationView.NamedLocation} on a
             * {@link com.google.android.gms.maps.GoogleMap}.
             * Adds a marker and centers the camera on the NamedLocation with the normal map type.
             */
            private void setMapLocation() {
                if (map == null) return;

                NamedLocation data = (NamedLocation) mapView.getTag();
                if (data == null) return;

                // Add a marker for this item and set the camera
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(data.location, 13f));
                map.addMarker(new MarkerOptions().position(data.location));

                // Set the map type back to normal.
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }

            private void bindView(int pos) {
                NamedLocation item = namedLocations[pos];
                // Store a reference of the ViewHolder object in the layout.
                layout.setTag(this);
                // Store a reference to the item in the mapView's tag. We use it to get the
                // coordinate of a location, when setting the map location.
                mapView.setTag(item);
                setMapLocation();
                title.setText(item.name);
            }
        }
    }

    /**
     * RecycleListener that completely clears the {@link com.google.android.gms.maps.GoogleMap}
     * attached to a row in the RecyclerView.
     * Sets the map type to {@link com.google.android.gms.maps.GoogleMap#MAP_TYPE_NONE} and clears
     * the map.
     */
    private RecyclerView.RecyclerListener mRecycleListener = new RecyclerView.RecyclerListener() {

        @Override
        public void onViewRecycled(RecyclerView.ViewHolder holder) {
            MapAdapter.ViewHolder mapHolder = (MapAdapter.ViewHolder) holder;
            if (mapHolder != null && mapHolder.map != null) {
                // Clear the map and free up resources by changing the map type to none.
                // Also reset the map when it gets reattached to layout, so the previous map would
                // not be displayed.
                mapHolder.map.clear();
                mapHolder.map.setMapType(GoogleMap.MAP_TYPE_NONE);
            }
        }
    };

    /**
     * Location represented by a position ({@link com.google.android.gms.maps.model.LatLng} and a
     * name ({@link java.lang.String}).
     */
    private static class NamedLocation {

        public final String name;
        public final LatLng location;

        NamedLocation(String name, LatLng location) {
            this.name = name;
            this.location = location;
        }
    }

    /**
     * A list of locations to show in this ListView.
     */

    private static NamedLocation[] LIST_LOCATIONS = new NamedLocation[]{};


//         private static NamedLocation[] LIST_LOCATIONS = new NamedLocation[]   {
//            new NamedLocation("Cape Town", new LatLng(-33.920455, 18.466941)),
//            new NamedLocation("Beijing", new LatLng(39.937795, 116.387224)),
//            new NamedLocation("Bern", new LatLng(46.948020, 7.448206)),
//            new NamedLocation("Breda", new LatLng(51.589256, 4.774396)),
//            new NamedLocation("Brussels", new LatLng(50.854509, 4.376678)),
//            new NamedLocation("Copenhagen", new LatLng(55.679423, 12.577114)),
//            new NamedLocation("Hannover", new LatLng(52.372026, 9.735672)),
//            new NamedLocation("Helsinki", new LatLng(60.169653, 24.939480)),
//            new NamedLocation("Hong Kong", new LatLng(22.325862, 114.165532)),
//            new NamedLocation("Istanbul", new LatLng(41.034435, 28.977556)),
//            new NamedLocation("Johannesburg", new LatLng(-26.202886, 28.039753)),
//            new NamedLocation("Lisbon", new LatLng(38.707163, -9.135517)),
//            new NamedLocation("London", new LatLng(51.500208, -0.126729)),
//            new NamedLocation("Madrid", new LatLng(40.420006, -3.709924)),
//            new NamedLocation("Mexico City", new LatLng(19.427050, -99.127571)),
//            new NamedLocation("Moscow", new LatLng(55.750449, 37.621136)),
//            new NamedLocation("New York", new LatLng(40.750580, -73.993584)),
//            new NamedLocation("Oslo", new LatLng(59.910761, 10.749092)),
//            new NamedLocation("Paris", new LatLng(48.859972, 2.340260)),
//            new NamedLocation("Prague", new LatLng(50.087811, 14.420460)),
//            new NamedLocation("Rio de Janeiro", new LatLng(-22.90187, -43.232437)),
//            new NamedLocation("Rome", new LatLng(41.889998, 12.500162)),
//            new NamedLocation("Sao Paolo", new LatLng(-22.863878, -43.244097)),
//            new NamedLocation("Seoul", new LatLng(37.560908, 126.987705)),
//            new NamedLocation("Stockholm", new LatLng(59.330650, 18.067360)),
//            new NamedLocation("Sydney", new LatLng(-33.873651, 151.2068896)),
//            new NamedLocation("Taipei", new LatLng(25.022112, 121.478019)),
//            new NamedLocation("Tokyo", new LatLng(35.670267, 139.769955)),
//            new NamedLocation("Tulsa Oklahoma", new LatLng(36.149777, -95.993398)),
//            new NamedLocation("Vaduz", new LatLng(47.141076, 9.521482)),
//            new NamedLocation("Vienna", new LatLng(48.209206, 16.372778)),
//            new NamedLocation("Warsaw", new LatLng(52.235474, 21.004057)),
//            new NamedLocation("Wellington", new LatLng(-41.286480, 174.776217)),
//            new NamedLocation("Winnipeg", new LatLng(49.875832, -97.150726))
//    };

//    @Override
//    public void onResume() {
//        super.onResume();
//        mMapView.onResume();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        mMapView.onPause();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mMapView.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mMapView.onLowMemory();
//    }


    public static LocationView getInstance() {
        return instance;
    }

    public void refreshList() {
        Call<AppointSyncResponse<ArrayList<MappedLocation>>> call = RetrofitInstance.getRetrofitInstance(new GetLocation());


        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedLocation>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Response<AppointSyncResponse<ArrayList<MappedLocation>>> response) {

                if (response.body().getErrorCode() == 0) {

                    NamedLocation[] namedLocations = new NamedLocation[response.body().getData().size()];
                    int index = 0;

                    Controller.locationList = response.body().getData();

                    for (Iterator<MappedLocation> i = response.body().getData().iterator(); i.hasNext(); ) {
                        MappedLocation mappedLocation = i.next();
                        Controller.location = Controller.locationList.get(index);
                        NamedLocation namedLocation = new NamedLocation(Controller.location.getLocationTitle(), new LatLng(Double.parseDouble(Controller.location.getLocationLatitude()), Double.parseDouble(Controller.location.getLocationLongitude())));
                        namedLocations[index] = namedLocation;
                        index++;
                    }
                    LIST_LOCATIONS = namedLocations;
                    MapAdapter mapAdapter = new MapAdapter(LIST_LOCATIONS);
                    mRecyclerView.setAdapter(mapAdapter);

                    mapAdapter.notifyDataSetChanged();


                    if (Controller.locationList == null || Controller.locationList.size() == 0) {
                        textViewNoLocation.setText("You don't have any locations.");
                    }else{
                        textViewNoLocation.setVisibility(View.GONE);
                    }

                }

            }
            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedLocation>>> call, Throwable t) {
            }

        });

        if (Controller.locationList != null && Controller.locationList.size() > 0) {
            textViewNoLocation.setVisibility(View.GONE);
        }
    }
}
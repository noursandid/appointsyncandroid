package com.appointsync.appointsyncpro;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.DatabaseAccess;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedPlan;
import com.appointsync.appointsyncpro.Class.Main.PostChangePassword;
import com.appointsync.appointsyncpro.Class.Main.PostChangePlan;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.security.MessageDigest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileSettingChangePassword extends Fragment {
    String inputPasswordStringEncrypt, oldInputPasswordStringEncrypt;
    private TextInputEditText oldPasswordText,newPasswordText,newPasswordConfirmText;
    private Button btnSettingSaveChangePassword;
    private long mLastClickTime = 0;

    public ProfileSettingChangePassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting_change_password, container, false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        final ImageView profileSettingChangePasswordBackArrow = view.findViewById(R.id.profileSettingChangePasswordBackArrow);
        btnSettingSaveChangePassword = view.findViewById(R.id.btnSettingSaveChangePassword);


         oldPasswordText = view.findViewById(R.id.oldPasswordText);
         newPasswordText = view.findViewById(R.id.newPasswordText);
         newPasswordConfirmText = view.findViewById(R.id.newPasswordConfirmText);



        btnSettingSaveChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

//                Toast.makeText(getActivity(), "mjkhvjgchfxdzsxtyfjtukgihkgjvch", Toast.LENGTH_LONG).show();

//                System.out.print("adsfedfsrefvetgvvdfvdfvdvdfvdfv");
//
//                System.out.print("Zoubi one:    "+oldPasswordTextInput.getText().toString() + " Zoubi Two:      "+newPasswordTextInput.getText().toString()+" Zoubi Three:      "+newPasswordConfirmTextInput.getText().toString());

        if (oldPasswordText.getText().length() == 0 || newPasswordText.getText().length() == 0 || newPasswordConfirmText.getText().length() == 0) {
            Toast.makeText(getActivity(), "Make sure to fill all fields!!", Toast.LENGTH_LONG).show();
        }
 else {
            if (!newPasswordText.getText().toString().equals(newPasswordConfirmText.getText().toString())) {
                Toast.makeText(getActivity(), "Passwords do not match.", Toast.LENGTH_LONG).show();
            } else if (newPasswordConfirmText.getText().toString().equals(newPasswordText.getText().toString())) {

                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] data = md.digest(newPasswordConfirmText.getText().toString().getBytes());
                    StringBuilder encryptedPassword = new StringBuilder();
                    for (int i = 0; i < data.length; i++) {
                        encryptedPassword.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    inputPasswordStringEncrypt = encryptedPassword.toString();
                } catch (Exception e) {
                    System.out.println(e);
                }

                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] data = md.digest(oldPasswordText.getText().toString().getBytes());
                    StringBuilder encryptedPassword = new StringBuilder();
                    for (int i = 0; i < data.length; i++) {
                        encryptedPassword.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
                    }
                    oldInputPasswordStringEncrypt = encryptedPassword.toString();
                } catch (Exception e) {
                    System.out.println(e);
                }

                Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostChangePassword(oldInputPasswordStringEncrypt,inputPasswordStringEncrypt));
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                        if(response.body().getErrorCode() != 0){
                            Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_LONG).show();
                        }else if(response.body().getErrorCode() == 0){
                            Toast.makeText(getActivity(), "Password successfully updated.", Toast.LENGTH_LONG).show();
                            saveToLoginTable(Controller.user.getUsername(),inputPasswordStringEncrypt);
                            oldPasswordText.setText("");
                            newPasswordConfirmText.setText("");
                            newPasswordText.setText("");
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        }
            }
        });

        profileSettingChangePasswordBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingChangePassword f = (ProfileSettingChangePassword) fm.findFragmentByTag("profileSettingChangePasswordFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });


        return view;
    }

    public void saveToLoginTable(String username, String password){
        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
        databaseAccess.insertLogin(username, password);
        databaseAccess.close();
    }


}
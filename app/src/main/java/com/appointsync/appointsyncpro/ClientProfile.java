package com.appointsync.appointsyncpro;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.PasswordConvertClass;
import com.appointsync.appointsyncpro.Class.Main.PostVerifyPin;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientsObjects.ClientRecyclerAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.NotificationAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientProfile extends Fragment {

    ImageView clientProfileBackArrow;
    private String profilePicture = "";
    ButtonSelectedProtocol delegate;
    MappedClient mappedClient;
    private long mLastClickTime = 0;
    private String pinCode;
    private String selectedUser = "";
    private ProgressDialog pd;
    private Fragment minime;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    public ClientProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_client_profile, container, false);


        minime= this;
        pd = new ProgressDialog(getContext());

        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        hideKeyboard(getActivity());

        clientProfileBackArrow = view.findViewById(R.id.clientProfileBackArrow);

        final ImageView profileImageView = view.findViewById(R.id.clientProfileImage);

        final TextView clientName = view.findViewById(R.id.clientProfileFirstName);
        final TextView clientNumber = view.findViewById(R.id.clientProfileNumber);
        final TextView clientUsername = view.findViewById(R.id.clientProfileUsername);
        final TextView clientEmail = view.findViewById(R.id.clientProfileEmail);
        final TextView clientDOB = view.findViewById(R.id.clientProfileDOB);
        final TextView clientGender = view.findViewById(R.id.clientProfileGender);
        final TextView clientPhoneNumber = view.findViewById(R.id.clientProfilePhoneNumber);

        final ImageView clientProfileBackArrow = view.findViewById(R.id.clientProfileBackArrow);

        final CardView clientProfileCardView = view.findViewById(R.id.clientProfileCardView);


        final Button addAppointment = view.findViewById(R.id.clientProfileAddAppointment);
        final Button clientFile = view.findViewById(R.id.clientProfileClientFile);
        final Button sendMessage = view.findViewById(R.id.clientProfileSendMessage);
        final Button settingsButton = view.findViewById(R.id.clientProfileSettings);
        final Button showAppointment = view.findViewById(R.id.clientProfileShowAppointments);
        final Button linkToOnlineAccount = view.findViewById(R.id.clientProfileLinkToOnlineAccount);

        profileImageView.setVisibility(View.INVISIBLE);
        clientName.setVisibility(View.INVISIBLE);


        mappedClient = (MappedClient) getArguments().getSerializable(ClientRecyclerAdapter.CLIENT_OBJECT);
        if (mappedClient == null) {
            mappedClient = (MappedClient) getArguments().getSerializable(NotificationAdapter.NOTIFICATION_OBJECT);
        }if (mappedClient == null) {
            mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.CLIENT_OBJECT);
        }

        int clientType = mappedClient.getType();

        if (clientType == 4) {
            profileImageView.setBackgroundColor(Color.parseColor("#D94432"));
        } else if (clientType == 2) {
            profileImageView.setBackgroundColor(Color.parseColor("#5391CA"));
        }

        if (mappedClient.getMiddleName().isEmpty()) {
            clientName.setText("Name: " + mappedClient.getFirstName() + " " + mappedClient.getLastName());
        } else {
            clientName.setText("Name: " + mappedClient.getFirstName() + " " + mappedClient.getMiddleName() + " " + mappedClient.getLastName());
        }

        profilePicture = mappedClient.getProfilePic();
        if (profilePicture.isEmpty()) {
            profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
        }

        Picasso.get().load(profilePicture).fit().centerCrop().into(profileImageView);

        clientNumber.setText("Client Number: " + mappedClient.getClientNumber().toString());

        if (clientType == 2) {
            clientUsername.setText("Username: " + mappedClient.getUsername());
            clientEmail.setText("Email: " + mappedClient.getEmail());
            linkToOnlineAccount.setVisibility(View.GONE);
        } else if (clientType == 3) {
            clientUsername.setText("Username: " + mappedClient.getUsername());
            clientEmail.setText("Email: " + mappedClient.getEmail());
            linkToOnlineAccount.setVisibility(View.GONE);
        }else if (clientType == 4) {
            clientUsername.setVisibility(View.GONE);
            clientEmail.setVisibility(View.GONE);
            settingsButton.setVisibility(View.GONE);
            sendMessage.setVisibility(View.GONE);
        }

        UnixDateConverter unixDateConverter = new UnixDateConverter();
        String formattedDate = unixDateConverter.UnixDateConverter(mappedClient.getDob());


        clientDOB.setText("Date of Birth: " + formattedDate);
        clientGender.setText("Gender: " + mappedClient.getGender());
        clientPhoneNumber.setText("Phone Number: +" + mappedClient.getCountryCode().toString()+mappedClient.getPhoneNumber().toString());

        profileImageView.setVisibility(View.VISIBLE);
        clientName.setVisibility(View.VISIBLE);


        addAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeAddNewAppointment(addAppointment, mappedClient);

            }
        });


        showAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeClientAppointment(showAppointment, mappedClient);

            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if(Controller.user.getSubscriptionType().equals(1)) {
                    delegate.seeNotPremiumUserClientProfile();
                }else if(Controller.user.getSubscriptionType().equals(2)) {
                    delegate.seeClientSettingsPermission(settingsButton, mappedClient);
                }
            }
        });


        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeClientSendMessage(sendMessage, mappedClient);

            }
        });

        linkToOnlineAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.seeClientLinkToOnlineAccount(linkToOnlineAccount, mappedClient);

            }
        });

        clientFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (Controller.user.getSubscriptionType().equals(1)) {
                    delegate.seeNotPremiumUserClientProfile();
                } else if (Controller.user.getSubscriptionType().equals(2)) {


                if(Controller.user.getHiddenFiles() == 0) {
                    delegate.seeClientFile(mappedClient);
                }else if(Controller.user.getHiddenFiles() == 1) {


                    final AlertDialog.Builder builderOff = new AlertDialog.Builder(getActivity());
                    builderOff.setTitle("Pin");

                    final EditText input = new EditText(getActivity());
//                input.setTransformationMethod((TransformationMethod) NumberFormatException.getInstance());
                    input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                    input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    builderOff.setView(input);
                    builderOff.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builderOff.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pinCode = input.getText().toString();
                            postVerifySecurityFilePin(PasswordConvertClass.convertedPassword(pinCode));
                        }
                    });
                    builderOff.show();

                }
                }
            }
        });

        clientProfileBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                if (minime != null) {
                    ft.remove(minime);
                }
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        HideKeyboardFunction.hideKeyboard(getActivity());
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void postVerifySecurityFilePin(final String securityFilePin){

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostVerifyPin(securityFilePin));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                if(response.body().getErrorCode()!=0){
                    Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                    HideKeyboardFunction.hideKeyboard(getActivity());

                }else if(response.body().getErrorCode() == 0) {
                   Controller.userPinCode = securityFilePin;
                    delegate.seeClientFile(mappedClient);
                }
                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        });

    }

}

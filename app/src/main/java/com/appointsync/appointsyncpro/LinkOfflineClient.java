package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.PostLinkOfflineClient;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.Subject;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarDayView;
import com.appointsync.appointsyncpro.Interface.UpdateCalendarView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LinkOfflineClient extends Fragment implements Observer, UpdateCalendarView, UpdateCalendarDayView, UpdateAllObserver, Subject {
    private ImageView clientLinkOfflineClientBackArrow;
    private IntentIntegrator qrScan;
    private Button openQrScanner, clientLinkOfflineClientUsernameSubmit;
    private TextInputEditText clientLinkOfflineClientUsernameText;
    private MappedClient mappedClient;
    private long mLastClickTime = 0;
    private List<Observer> observers = new ArrayList<>();

    public LinkOfflineClient() {
        // Required empty public constructor

        observers = new ArrayList<>();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_link_offline_client, container, false);


        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        openQrScanner = view.findViewById(R.id.openQrScanner);
        clientLinkOfflineClientBackArrow = view.findViewById(R.id.clientLinkOfflineClientBackArrow);
        clientLinkOfflineClientUsernameText = view.findViewById(R.id.clientLinkOfflineClientUsernameText);
        clientLinkOfflineClientUsernameSubmit = view.findViewById(R.id.clientLinkOfflineClientUsernameSubmit);
        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        qrScan = new IntentIntegrator(getActivity());


        openQrScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                IntentIntegrator.forSupportFragment(LinkOfflineClient.this).setPrompt("Scan user QR").setOrientationLocked(true).initiateScan();
            }
        });


        clientLinkOfflineClientUsernameSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                if (clientLinkOfflineClientUsernameText.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Username can not be empty.", Toast.LENGTH_SHORT).show();
                } else {


                    Call<AppointSyncResponse<MappedClient>> call = RetrofitInstance.getRetrofitInstance(new PostLinkOfflineClient(mappedClient.getWebID(), clientLinkOfflineClientUsernameText.getText().toString()));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<MappedClient>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedClient>> call, Response<AppointSyncResponse<MappedClient>> response) {

                            if (response.body().getErrorCode() == 0) {

                                Controller.clientList.add(response.body().getData());
                                Toast.makeText(getActivity(), "Client linked successfully.", Toast.LENGTH_SHORT).show();
                                notifyObservers();
                                pd.dismiss();
                                FragmentManager fm = getFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                ClientProfile f = (ClientProfile) fm.findFragmentByTag("8");
                                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                ft.remove(f);
                                ft.commit();

                            } else {
                                pd.dismiss();
                                Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure
                                (Call<AppointSyncResponse<MappedClient>> call, Throwable t) {
                            pd.dismiss();
                            Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                        }
                    });

                }

            }
        });


        clientLinkOfflineClientBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                LinkOfflineClient f = (LinkOfflineClient) fm.findFragmentByTag("seeClientLinkToOnlineAccountFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();

            }
        });


        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Log.d("MainActivity", "Cancelled");
//                Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_LONG).show();

            } else {
                Log.d("MainActivity", "Scanned");

                Call<AppointSyncResponse<MappedClientWithQRCode>> call = RetrofitInstance.getRetrofitInstance(new GetClientWithQRCode(intentResult.getContents()));
                call.enqueue(new Callback<AppointSyncResponse<MappedClientWithQRCode>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedClientWithQRCode>> call, Response<AppointSyncResponse<MappedClientWithQRCode>> response) {

                        if (response.body().getErrorCode() == 190700) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Warning");
                            builder.setMessage("No client is available for this QR.");
                            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();
                        } else if (response.body().getErrorCode() == 0) {

                            clientLinkOfflineClientUsernameText.setText(response.body().getData().getUsername());

                        }

                    }

                    @Override
                    public void onFailure
                            (Call<AppointSyncResponse<MappedClientWithQRCode>> call, Throwable t) {

                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        }
    }

    @Override
    public void register(final Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregister(final Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void updateAll() {

        if (getActivity() != null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    }

    @Override
    public void update() {

    }


    @Override
    public void updateCalendarView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void updateCalendarDayView(final Observer observer) {
        if (observer != null && !observers.contains(observer)) {
            observers.add(observer);
        }
    }


    @Override
    public void notifyObservers() {
        Controller.notifyAllObserversToUpdateAll();
        for (Observer observer : observers) {
            observer.update();
        }
    }

}
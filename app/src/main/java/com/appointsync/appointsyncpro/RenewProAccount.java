package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedPlan;
import com.appointsync.appointsyncpro.Class.Main.PostChangePlan;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePackage;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.RenewPlanPaymentDelegate;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class RenewProAccount extends Fragment implements RenewPlanPaymentDelegate {
    private Button proMonthPackage, proYearPackage, renewProPackage, cancelProPackage;
    private Boolean isMonthSelected = false, isYearSelected = false;
    private long mLastClickTime = 0;
    String packageSelected = "";
    Integer packageTypeSelected = 0;
    ButtonSelectedProtocol delegate;
    RenewPlanPaymentDelegate minime;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (Controller.noActiveSubscription != null && Controller.noActiveSubscription.equals(false)) {
            // This makes sure that the container activity has implemented
            // the callback interface. If not, it throws an exception
            try {
                delegate = (ButtonSelectedProtocol) context;
            } catch (ClassCastException e) {
                throw new ClassCastException(context.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
        }
    }

    public RenewProAccount() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_renew_pro_account, container, false);


        proMonthPackage = view.findViewById(R.id.btnRenewProMonthPackage);
        proYearPackage = view.findViewById(R.id.btnRenewYearPackage);
        renewProPackage = view.findViewById(R.id.btnRenewProPackage);
        cancelProPackage = view.findViewById(R.id.btnCancelProPackage);

        minime = this;
        proMonthPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMonthSelected.equals(false) && isYearSelected.equals(true)) {
                    isMonthSelected = true;
                    isYearSelected = false;
                    proMonthPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_full_blue));
                    proYearPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_dark_grey));
                    packageSelected = "M";
                    packageTypeSelected = 1;
                } else if (isMonthSelected.equals(false) && isYearSelected.equals(false)) {
                    isMonthSelected = true;
                    proMonthPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_full_blue));
                    packageSelected = "M";
                    packageTypeSelected = 1;
                }
            }
        });


        proYearPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isMonthSelected.equals(true) && isYearSelected.equals(false)) {
                    isMonthSelected = false;
                    isYearSelected = true;
                    proYearPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_full_blue));
                    proMonthPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_dark_grey));
                    packageSelected = "Y";
                    packageTypeSelected = 1;
                } else if (isMonthSelected.equals(false) && isYearSelected.equals(false)) {
                    isYearSelected = true;
                    proYearPackage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_full_blue));
                    packageSelected = "Y";
                    packageTypeSelected = 1;
                }
            }
        });


        renewProPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                if (isMonthSelected.equals(false) && isYearSelected.equals(false)) {
                    Toast.makeText(getActivity(), "Please make sure to select a package.", Toast.LENGTH_SHORT).show();
                } else {
                    if (Controller.noActiveSubscription != null && Controller.noActiveSubscription.equals(false)) {
                        System.out.println("I SHOULDNT BE HERE");
                        delegate.seeRenewPlanPayment(false, packageTypeSelected, packageSelected, minime);
                    } else {
                        System.out.println("WHY AM I NOT OPENING");
                            RenewPlanPayment renewPlanPayment = new RenewPlanPayment();
                            ((RenewPlanPayment) renewPlanPayment).isUpgrade = false;
                            ((RenewPlanPayment) renewPlanPayment).type = packageTypeSelected;
                            ((RenewPlanPayment) renewPlanPayment).subscriptionPackage = packageSelected;
                            ((RenewPlanPayment) renewPlanPayment).delegate = minime;
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragmentRenewProMainConstraint, renewPlanPayment, "seeRenewPlanPaymentFragment")
                                    .addToBackStack(null)
                                    .commit();
                    }
                }
            }
        });


        cancelProPackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }

                closeCurrentView();

            }
        });

        return view;
    }

    private void closeCurrentView() {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        RenewProAccount f;
        if (fm.findFragmentByTag("renewProAccountFragment") == null) {
            f = (RenewProAccount) fm.findFragmentByTag("RenewProAccountNoSubscription");
        } else {
            f = (RenewProAccount) fm.findFragmentByTag("renewProAccountFragment");
        }
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();
    }

    @Override
    public void didFinishPaying() {
        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);
        Call<AppointSyncResponse<MappedPlan>> call = RetrofitInstance.getRetrofitInstance(new PostUpdatePackage(packageSelected, packageTypeSelected));
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<MappedPlan>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedPlan>> call, Response<AppointSyncResponse<MappedPlan>> response) {

                Controller.user.setTillDate(response.body().getData().getTillDate());
                pd.dismiss();
                if (Controller.noActiveSubscription != null && Controller.noActiveSubscription.equals(true)) {

                    Controller.noActiveSubscription = false;
                    Intent intent = new Intent(getActivity(), Initial.class);
                    startActivity(intent);

                } else {
                    ProfileSettingSubscription.getInstance().refreshView();
                    closeCurrentView();
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<MappedPlan>> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong!! please try again later.", Toast.LENGTH_SHORT).show();
            }
        });

    }
}

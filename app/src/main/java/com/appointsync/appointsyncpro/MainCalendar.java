package com.appointsync.appointsyncpro;


import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetAppointments;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.ClientsObjects.MainCalendarAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.MonthDaysRecyclerAdapterDelegate;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainCalendar extends Fragment implements MonthDaysRecyclerAdapterDelegate, UpdateAllObserver, Observer {
    MainCalendarAdapter adapter;
    ButtonSelectedProtocol delegate;
    private long mLastClickTime = 0;
    Button calendarTodayButton;
    private RecyclerView recyclerView;
    private ImageView mainCalendarAddAppointment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    public MainCalendar() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_calendar, container, false);

        // set up the RecyclerView
        recyclerView = view.findViewById(R.id.rvNumbers);
        calendarTodayButton = view.findViewById(R.id.calendarTodayButton);
        mainCalendarAddAppointment = view.findViewById(R.id.mainCalendarAddAppointment);


            buildView();
            updateView();
        mainCalendarAddAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                delegate.calendarAddNewAppointment();
            }
        });

        Controller.registerAsAnObserverToUpdateAll(this);


        return view;
    }

//    @Override
//    public void onItemClick(View view, int position) {
//        Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);
//    }

    @Override
    public void didSelectDay(int[] selectedYearMonthDay) {

        delegate.seeDayView(selectedYearMonthDay);
    }

    public void update(){
        updateView();
    }

    public void updateView() {



            Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call = RetrofitInstance.getRetrofitInstance(new GetAppointments());


            call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointment>>>() {
                @Override
                public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call, Response<AppointSyncResponse<ArrayList<MappedAppointment>>> response) {

                    if (response.body().getErrorCode() == 0) {
                        Controller.user.calendar.addAppointments(response.body().getData());
                        adapter.notifyDataSetChanged();
                        delegate.notifyDayViewToUpdateAppointmentList();
                    }

                }

                @Override
                public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call, Throwable t) {

                    t.printStackTrace();
                }

            });





    }

    public void updateAll() {

System.out.println("THIS IS UPDATE ALL IN MAIN CALENDAR");
        if(getActivity() != null) {
            System.out.println("THIS IS UPDATE ALL IN MAIN CALENDAR1");
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    updateView();
                }
            });

        }

    }



    public void buildView() {

        int numberOfColumns = 1;
        final GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), numberOfColumns);

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        linearLayoutManager.scrollToPositionWithOffset((year * 12) + month, 0);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new MainCalendarAdapter(getActivity());
        adapter.delegate = this;
//        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        calendarTodayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());

                linearLayoutManager.scrollToPositionWithOffset((cal.get(Calendar.YEAR) * 12) + cal.get(Calendar.MONTH), 0);
            }
        });

    }

}

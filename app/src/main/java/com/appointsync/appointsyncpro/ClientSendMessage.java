package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.HideKeyboardFunction;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.PostMessage;
import com.appointsync.appointsyncpro.Interface.RefreshThreadsProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientSendMessage extends Fragment {
    private Button clientSendMessageName, clientSendMessagePostNewThread, clientSendMessageCancelNewThread;
    private TextInputEditText clientSendMessageNewThreadTitleText;
    private EditText clientSendMessageNewThreadMessage;
    MappedClient mappedClient;
    private long mLastClickTime = 0;

    public ClientSendMessage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_send_message, container, false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        clientSendMessageName = view.findViewById(R.id.clientSendMessageName);
        clientSendMessagePostNewThread = view.findViewById(R.id.clientSendMessagePostNewThread);
        clientSendMessageCancelNewThread = view.findViewById(R.id.clientSendMessageCancelNewThread);
        clientSendMessageNewThreadTitleText = view.findViewById(R.id.clientSendMessageNewThreadTitleText);
        clientSendMessageNewThreadMessage = view.findViewById(R.id.clientSendMessageNewThreadMessage);

        clientSendMessageName.setEnabled(false);
        clientSendMessageName.setText(mappedClient.getUsername());

        clientSendMessagePostNewThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (clientSendMessageName.getText().toString().isEmpty() || clientSendMessageNewThreadMessage.getText().toString().isEmpty() || clientSendMessageNewThreadTitleText.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please make sure to fill all fields.", Toast.LENGTH_LONG).show();
                } else {

                    Call<AppointSyncResponse<MappedThread>> call = RetrofitInstance.getRetrofitInstance(new PostMessage(clientSendMessageNewThreadTitleText.getText().toString(), clientSendMessageNewThreadMessage.getText().toString(), mappedClient.getWebID()));
                    pd.show();
                    call.enqueue(new Callback<AppointSyncResponse<MappedThread>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedThread>> call, Response<AppointSyncResponse<MappedThread>> response) {

                            pd.dismiss();
                            closeWindow();
                            Toast.makeText(getActivity(), "Message sent.", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<MappedThread>> call, Throwable t) {

                            pd.dismiss();
                            Toast.makeText(getActivity(), "Something went wrong! Please try again later.", Toast.LENGTH_SHORT).show();

                        }
                    });

                }

            }
        });


        clientSendMessageCancelNewThread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                closeWindow();

            }
        });


        return view;
    }

    public void closeWindow() {

        HideKeyboardFunction.hideKeyboard(getActivity());
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ClientSendMessage f = (ClientSendMessage) fm.findFragmentByTag("seeClientSendMessageFragment");
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();

    }

}

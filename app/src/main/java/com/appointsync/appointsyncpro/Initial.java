package com.appointsync.appointsyncpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Adapters.DatabaseAccess;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.LoginRequest;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedProfile;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationToken;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.Network.UploadFile;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Initial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);


        DatabaseAccess databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        List<String> loginData = databaseAccess.getLogin();
        databaseAccess.close();

        try {
            System.out.println(loginData.get(1));
            if (loginData.get(1) != "") {

                try {
                    Call<AppointSyncResponse<MappedProfile>> call = RetrofitInstance.getRetrofitInstance(new LoginRequest(loginData.get(1), loginData.get(2)));
                    call.enqueue(new Callback<AppointSyncResponse<MappedProfile>>() {
                        @Override
                        public void onResponse(Call<AppointSyncResponse<MappedProfile>> call, Response<AppointSyncResponse<MappedProfile>> response) {

                            if (response.body().getErrorCode() == 0) {

                                Controller.noActiveSubscription = false;
                            FirebaseInstanceId.getInstance().getInstanceId()
                                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                            if (!task.isSuccessful()) {
                                                return;
                                            }

                                            String token = task.getResult().getToken();

                                            Controller.loggedInuserToken = token;
                                            Call<AppointSyncResponse<MappedProfile>> call = RetrofitInstance.getRetrofitInstance(new PostNotificationToken(token));
                                            call.enqueue(new Callback<AppointSyncResponse<MappedProfile>>() {
                                                @Override
                                                public void onResponse(Call<AppointSyncResponse<MappedProfile>> call, Response<AppointSyncResponse<MappedProfile>> response) {

                                                    if(response.body().getErrorCode()==0){

                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<AppointSyncResponse<MappedProfile>> call, Throwable t) {


                                                }
                                            });

                                        }
                                    });

                                Controller.user = response.body().getData();

                                RetrofitInstance retrofitInstance = new RetrofitInstance();
                                retrofitInstance.userID = Controller.user.getId();
                                UploadFile.userID = Controller.user.getId();


                                Intent intent = new Intent(Initial.this, Dashboard.class);

                                startActivity(intent);

                            } else if (response.body().getErrorCode() == 476300) {
                                Toast.makeText(Initial.this, "Invalid username or password!", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Initial.this, Login.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(Initial.this, "Something went wrong! ", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Initial.this, Login.class);
                                startActivity(intent);
                            }
                        }

                        @Override
                        public void onFailure(Call<AppointSyncResponse<MappedProfile>> call, Throwable t) {

                            System.out.println("FAILLL");

                        }
                    });
                } catch (Exception e) {
//                    Toast.makeText(Login.this, "Fill all fields! ", Toast.LENGTH_SHORT).show();
                }
            } else {

                Intent intent = new Intent(Initial.this, Login.class);
                startActivity(intent);

            }
        } catch (Exception e) {

            Intent intent = new Intent(Initial.this, Login.class);
            startActivity(intent);

            System.out.println(e);
        }
    }
}

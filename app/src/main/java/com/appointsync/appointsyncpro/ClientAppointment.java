package com.appointsync.appointsyncpro;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetAppointments;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.ClientsObjects.ClientAppointmentAdapter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientAppointment extends Fragment implements UpdateAllObserver {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private List<MappedAppointment> appointmentListDev;
    //    String profilePicture, userName = "";
    public MappedClient client;
    public ButtonSelectedProtocol delegate;
    ImageView clientAppointmentBackArrow;
    TextView noAvailableAppointments;
    private SwipeRefreshLayout swipeContainer;


    ////    ConstraintLayout ClientLayoutItem;
//
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public ClientAppointment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_client_appointment, container, false);

        clientAppointmentBackArrow = view.findViewById(R.id.clientAppointmentBackArrow);

        client = (MappedClient) getArguments().getSerializable("client");
        recyclerView = view.findViewById(R.id.clientAppointmentsList);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        appointmentListDev = new ArrayList<>();


        List<MappedAppointment> clientAppointment = Controller.getAppointmentForClient(client.getWebID());

        int Zoubi = clientAppointment.size();

        if (Zoubi == 0) {
            noAvailableAppointments = view.findViewById(R.id.noAvailableAppointments);
            noAvailableAppointments.setText("You don't have any Appointments with this user.");
        }


        for (int i = 0; i < Zoubi; i++) {

            appointmentListDev.add(clientAppointment.get(i));

        }

        Collections.sort(appointmentListDev, MappedAppointment.StuRollno);

        adapter = new ClientAppointmentAdapter(appointmentListDev, getActivity().getApplicationContext(), delegate);


        recyclerView.setAdapter(adapter);


        clientAppointmentBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ClientAppointment f = (ClientAppointment) fm.findFragmentByTag("9");

                ft.remove(f);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                ft.commit();


            }
        });


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchTimelineAsync(0);

            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        Controller.registerAsAnObserverToUpdateAll(this);
        return view;
    }

    @Override
    public void updateAll() {


        if(getActivity()!=null) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                    // ...the data has come back, a
                    // Now we call setRefreshing(false) to signal refresh has finished
                    swipeContainer.setRefreshing(false);
                }
            });

        }
    }

    public void fetchTimelineAsync(int page) {
        // Send the network request to fetch the updated data
        // `client` here is an instance of Android Async HTTP
        // getHomeTimeline is an example endpoint.
        Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call = RetrofitInstance.getRetrofitInstance(new GetAppointments());


        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedAppointment>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call, Response<AppointSyncResponse<ArrayList<MappedAppointment>>> response) {

//                Controller.appointmentList.clear();
//                Controller.appointmentList = response.body().getData();
                Controller.user.calendar.addAppointments(response.body().getData());
                List<MappedAppointment> clientAppointmentRefreshed = Controller.getAppointmentForClient(client.getWebID());
                appointmentListDev.clear();
                Integer clientAppointmentSize = clientAppointmentRefreshed.size();
                System.out.println("Main Controller User      " + Controller.user);

                for (int i = 0; i < clientAppointmentSize; i++) {

                    appointmentListDev.add(clientAppointmentRefreshed.get(i));

                }

                Collections.sort(appointmentListDev, MappedAppointment.StuRollno);

                System.out.println("Controller.user.calendar " + Controller.user.calendar);
                System.out.println("Controller.appointmentList " + Controller.appointmentList);
                Controller.user.calendar.addAppointments(Controller.appointmentList);

                System.out.println("Main Cal Zoubi      " + Controller.user.calendar);

                adapter.notifyDataSetChanged();
                // ...the data has come back, a
                // Now we call setRefreshing(false) to signal refresh has finished
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedAppointment>>> call, Throwable t) {

            }

            public void onFailure(Throwable e) {
                Log.d("DEBUG", "Fetch timeline error: " + e.toString());
            }
        });
    }
}

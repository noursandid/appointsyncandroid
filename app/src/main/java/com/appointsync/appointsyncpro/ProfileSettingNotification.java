package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetNotificationFlag;
import com.appointsync.appointsyncpro.Class.Main.GetPermissions;
import com.appointsync.appointsyncpro.Class.Main.MappedNotificationFlag;
import com.appointsync.appointsyncpro.Class.Main.MappedPermission;
import com.appointsync.appointsyncpro.Class.Main.PostEmailFlag;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationFlag;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingNotification extends Fragment {
    Switch profileSettingEmailNotificationSwitch,profileSettingPushNotificationSwitch;
    private long mLastClickTime = 0;

    public ProfileSettingNotification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_profile_setting_notification, container, false);

        final ImageView profileSettingNotificationBackArrow = view.findViewById(R.id.profileSettingNotificationBackArrow);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);


        profileSettingEmailNotificationSwitch = view.findViewById(R.id.profileSettingEmailNotificationSwitch);
        profileSettingPushNotificationSwitch = view.findViewById(R.id.profileSettingPushNotificationSwitch);


        Call<AppointSyncResponse<MappedNotificationFlag>> call = RetrofitInstance.getRetrofitInstance(new GetNotificationFlag());
        pd.show();
        call.enqueue(new Callback<AppointSyncResponse<MappedNotificationFlag>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<MappedNotificationFlag>> call, Response<AppointSyncResponse<MappedNotificationFlag>> response) {

                Controller.notificationFlag = response.body().getData();

                updateView();

                pd.dismiss();

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<MappedNotificationFlag>> call, Throwable t) {

                Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();

            }
        });

        profileSettingEmailNotificationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                final String emailFlag;
                if(Controller.notificationFlag.getEmail().equals(0)){
                    emailFlag = "1";
                }else{
                    emailFlag = "0";
                }

                Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostEmailFlag(emailFlag));
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                        if(response.body().getErrorCode() == 0) {
                            Controller.notificationFlag.setEmail(Integer.parseInt(emailFlag));
                            if (emailFlag.equals("0")) {
                                Toast.makeText(getActivity(), "Email notifications disabled.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "Email notifications enabled.", Toast.LENGTH_SHORT).show();
                            }

                        }  else{
                            Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                        }
                        updateView();
                        pd.dismiss();
                    }

                    @Override
                    public void onFailure
                            (Call<AppointSyncResponse<Success>> call, Throwable t) {

                        Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });


        profileSettingPushNotificationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                final String notificationFlag;
                if(Controller.notificationFlag.getNotification().equals(0)){
                    notificationFlag = "1";
                }else{
                    notificationFlag = "0";
                }

                Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostNotificationFlag(notificationFlag));
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

                        if (response.body().getErrorCode() == 0) {
                            Controller.notificationFlag.setNotification(Integer.parseInt(notificationFlag));
                            if (notificationFlag.equals("0")) {
                                Toast.makeText(getActivity(), "Push notifications disabled.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "Push notifications enabled.", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                        }

                        updateView();
                        pd.dismiss();

                    }
                    @Override
                    public void onFailure
                            (Call<AppointSyncResponse<Success>> call, Throwable t) {

                        Toast.makeText(getActivity(), "Something went wrong! please try again later.", Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });


        profileSettingNotificationBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingNotification f = (ProfileSettingNotification) fm.findFragmentByTag("profileSettingNotificationFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });



        return view;
    }


    public void updateView(){
        if (Controller.notificationFlag.getEmail().equals(1)) {
            profileSettingEmailNotificationSwitch.setChecked(true);
        } else if (Controller.notificationFlag.getEmail().equals(0)) {
            profileSettingEmailNotificationSwitch.setChecked(false);
        }

        if (Controller.notificationFlag.getNotification().equals(1)) {
            profileSettingPushNotificationSwitch.setChecked(true);
        } else if (Controller.notificationFlag.getNotification().equals(0)) {
            profileSettingPushNotificationSwitch.setChecked(false);
        }
    }

}

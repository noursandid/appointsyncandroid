package com.appointsync.appointsyncpro;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.ForgotPasswordClass;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    private long mLastClickTime = 0;
    private Button forgotPassword;
    private EditText forgotPassswordEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);


        forgotPassword = findViewById(R.id.submitForgotPassword);
        forgotPassswordEmail = findViewById(R.id.inputEmail);

        final ImageView closeButton = findViewById(R.id.closeButtonX);
        closeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
//                Intent intent = new Intent(ForgotPassword.this, Login.class);
//                startActivity(intent);

                finish();

            }

        });


        forgotPassword.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                forgotPassword.setEnabled(false);

                if (forgotPassswordEmail.getText() == null) {

                    Toast.makeText(getBaseContext(), "Please fill your email.", Toast.LENGTH_LONG).show();
                    forgotPassword.setEnabled(true);
                } else {

                    Pattern pattern1 = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");

                    Matcher matcher1 = pattern1.matcher(forgotPassswordEmail.getText());

                    if (!matcher1.matches()) {
                        Toast.makeText(ForgotPassword.this, "Invalid email format.", Toast.LENGTH_SHORT).show();
                        forgotPassword.setEnabled(true);
                    } else {

                        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new ForgotPasswordClass(forgotPassswordEmail.getText().toString()));
                        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {
                                Toast.makeText(ForgotPassword.this, "Email Sent! Please check your inbox.", Toast.LENGTH_SHORT).show();
                                forgotPassswordEmail.setText("");
                                System.out.println("SUCCESSSSSS");
                                forgotPassword.setEnabled(true);
                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                                Toast.makeText(ForgotPassword.this, "Something went wrong...Error message: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                System.out.println("FAILEDDDD" + t.getMessage());
                                forgotPassword.setEnabled(true);
                            }
                        });

                    }
                }
            }
        });


    }


}

package com.appointsync.appointsyncpro;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Network.UploadFile;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment implements Observer {
    Dashboard dashboard;
    String userProfilePicture;
    ImageView profilePicture, profileSettings,loggedUserQRCode;
    CardView profilePictureCard;
    Button seeLocations, editProfile, profileSetting;
    TextView userFullName, userProfession, userGender, userWorkingHours, userEmail, userPhoneNumber, userUsername;
    ScrollView scrollView;
    ButtonSelectedProtocol delegate;
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private File fileToDelete;
    private String filePickerLocation = "";
    private long mLastClickTime = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            delegate = (ButtonSelectedProtocol) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    public Profile() {
        // Required empty public constructor
    }

//ipad


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        profilePicture = view.findViewById(R.id.loggedUserProfileImage);
        profilePictureCard = view.findViewById(R.id.loggedUserProfileCardView);
        profilePicture.setVisibility(View.INVISIBLE);
        profilePictureCard.setVisibility(View.INVISIBLE);
        userFullName = view.findViewById(R.id.loggedInUserName);
        userProfession = view.findViewById(R.id.loggedInUserProfession);
        userGender = view.findViewById(R.id.loggedInUserGender);
        userWorkingHours = view.findViewById(R.id.loggedInUserWorkingHours);
        userEmail = view.findViewById(R.id.loggedInUserEmail);
        userPhoneNumber = view.findViewById(R.id.loggedInUserPhoneNumber);
        userUsername = view.findViewById(R.id.loggedInUserUsername);
        seeLocations = view.findViewById(R.id.userProfileSeeLocation);
        seeLocations.setVisibility(View.INVISIBLE);
//        editProfile = view.findViewById(R.id.userProfileEditProfile);
//        editProfile.setVisibility(View.INVISIBLE);
//        profileSetting = view.findViewById(R.id.userProfileSeeLocation);
//        profileSetting.setVisibility(View.INVISIBLE);
        scrollView = (ScrollView) view.findViewById(R.id.scrollZoubi);
        loggedUserQRCode = view.findViewById(R.id.loggedUserQRCode);

        profileSettings = (ImageView) view.findViewById(R.id.profileOpenSettings);


        updateView();

//                editProfile.setVisibility(View.VISIBLE);
//                profileSetting.setVisibility(View.VISIBLE);


        seeLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                scrollView.fullScroll(ScrollView.FOCUS_UP);

                delegate.seeLocationsSelected(seeLocations);
            }
        });


        profileSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();


                delegate.seeProfileSettings(profileSettings);
            }
        });


        profilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                selectImage();

            }
        });


        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(Controller.user.getQrCode().toString(), BarcodeFormat.QR_CODE, 200, 200);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            loggedUserQRCode.setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            final int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                            dialog.dismiss();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {
                                    Log.v(TAG,"Permission is granted");

                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);

                                } else {

                                    Log.v(TAG,"Permission is revoked");
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                    Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else { //permission is automatically granted on sdk<23 upon installation
                                Log.v(TAG,"Permission is granted");

                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);

                            }

                        } else {

                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_REQUEST_CODE);
//                                Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                        }
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                Log.v(TAG,"Permission is granted");

                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                            } else {

                                Log.v(TAG,"Permission is revoked");
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else { //permission is automatically granted on sdk<23 upon installation
                            Log.v(TAG,"Permission is granted");

                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

                        }

                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();
        } catch (Exception e) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_REQUEST_CODE);
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            filePickerLocation = "PICK_IMAGE_CAMERA";
            if (data != null) {
                try {

                    File file = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");
//
                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                    File destination = new File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg");

                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    Toast.makeText(getActivity(), "THIS IS SPARRTA" + TAG, Toast.LENGTH_LONG).show();

                    fileToDelete = file;
                    Uri uri = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", file);

                    uploadFile(uri);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            filePickerLocation = "PICK_IMAGE_GALLERY";
            if (data != null) {
                Uri selectedImage = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                    Log.e("Activity", "Pick from Gallery::>>> ");

                    imgPath = getRealPathFromURI(selectedImage);
                    destination = new File(imgPath.toString());
//                    Toast.makeText(getActivity(), "THIS IS SPARRTA" + TAG, Toast.LENGTH_LONG).show();


                    File file = new File(selectedImage.getPath());
                    MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
//                RequestBody filePart = RequestBody.create(MediaType.parse("image/*"), file);

//                calling the upload file method after choosing the file
                    uploadFile(selectedImage);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
            System.out.println(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + contentUri.getLastPathSegment().toString());
            return Environment.getExternalStorageDirectory().getPath().toString() + File.separator + contentUri.getLastPathSegment().toString();
        } else {
            String[] proj = {MediaStore.Audio.Media.DATA};
            Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
    }


    @Override
    public void update() {
        updateView();
    }

    public void updateView() {

        userProfilePicture = Controller.user.getProfilePic();
        if (userProfilePicture.isEmpty()) {
            userProfilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
        }

        Picasso.get().load(userProfilePicture)
                .fit()
                .centerCrop()
                .into(profilePicture);

        profilePictureCard.setVisibility(View.VISIBLE);
        profilePicture.setVisibility(View.VISIBLE);
        userFullName.setText("Name: " + Controller.user.getFirstName() + " " + Controller.user.getLastName());
        userProfession.setText("Profession: " + Controller.user.getProfession());
        userGender.setText("Gender: " + Controller.user.getGender());

        UnixDateConverter unixDateConverter = new UnixDateConverter();
        String workingHourFrom = unixDateConverter.UnixDateConverterTime(Controller.user.getWorkingHourFrom());
        String workingHourTo = unixDateConverter.UnixDateConverterTime(Controller.user.getWorkingHourTo());

        userWorkingHours.setText("Working Hours: From " + workingHourFrom + " To " + workingHourTo);
        userEmail.setText("Email: " + Controller.user.getEmail());
        userPhoneNumber.setText("Phone number: " + Controller.user.getPhoneNumber());
        userUsername.setText("Username: " + Controller.user.getUsername());

        seeLocations.setVisibility(View.VISIBLE);

    }


    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                == PackageManager.PERMISSION_GRANTED) {
                            Log.v(TAG,"Permission is granted");

                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);

                        } else {

                            Log.v(TAG,"Permission is revoked");
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                            Toast.makeText(getActivity(), "Need to grant storage permission inorder to continue.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else { //permission is automatically granted on sdk<23 upon installation
                        Log.v(TAG,"Permission is granted");

                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                }

            } else {

                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();

            }

        }
    }//end onRequestPermissionsResult


    private void uploadFile(Uri fileUri) {

        //creating a file
        File file = new File(getRealPathFromURI(fileUri));

        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        Call<AppointSyncResponse<Success>> call = UploadFile.getImageRetrofitInstance().uploadAttachment(filePart);

//        Toast.makeText(getActivity(), "File  ZOUBI...", Toast.LENGTH_LONG).show();
        //finally performing the call
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {
                if (response.body().getErrorCode() == 0) {
                    Toast.makeText(getActivity(), "File Uploaded Successfully...", Toast.LENGTH_LONG).show();
                    updateView();
                } else if(response.body().getErrorCode() == 120) {
                    Toast.makeText(getActivity(), "File size must be less than 2MB", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getActivity(), "Some error occurred...", Toast.LENGTH_LONG).show();
                }
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<Success>> call, Throwable t) {
                Toast.makeText(getActivity(), "in Fail...", Toast.LENGTH_LONG).show();
                Log.e("Activity", "REQUEST IMAGE FAILED ");
                t.printStackTrace();
                if (filePickerLocation.equals("PICK_IMAGE_CAMERA")) {
                    fileToDelete.delete();
                }
            }
        });
    }


}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/19/19.
 */

public class PostContactUs {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("browserType")
    @Expose
    private String browserType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("userType")
    @Expose
    private String userType;
    @SerializedName("supportType")
    @Expose
    private String supportType;

    public PostContactUs(String email, String browserType, String message, String subject, String userType, String supportType) {
        this.email = email;
        this.browserType = browserType;
        this.message = message;
        this.subject = subject;
        this.userType = userType;
        this.supportType = supportType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBrowserType() {
        return browserType;
    }

    public void setBrowserType(String browserType) {
        this.browserType = browserType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSupportType() {
        return supportType;
    }

    public void setSupportType(String supportType) {
        this.supportType = supportType;
    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/26/19.
 */

public class PostNewAppointment {

    @SerializedName("appointmentStartDate")
    @Expose
    private String appointmentStartDate;
    @SerializedName("appointmentClientWebID")
    @Expose
    private String appointmentClientWebID;
    @SerializedName("appointmentComment")
    @Expose
    private String appointmentComment;
    @SerializedName("appointmentLocationWebID")
    @Expose
    private String appointmentLocationWebID;
    @SerializedName("appointmentEndDate")
    @Expose
    private String appointmentEndDate;

    public PostNewAppointment(String appointmentStartDate, String appointmentClientWebID, String appointmentComment, String appointmentLocationWebID, String appointmentEndDate) {
        this.appointmentStartDate = appointmentStartDate;
        this.appointmentClientWebID = appointmentClientWebID;
        this.appointmentComment = appointmentComment;
        this.appointmentLocationWebID = appointmentLocationWebID;
        this.appointmentEndDate = appointmentEndDate;
    }

    public String getAppointmentStartDate() {
        return appointmentStartDate;
    }

    public void setAppointmentStartDate(String appointmentStartDate) {
        this.appointmentStartDate = appointmentStartDate;
    }

    public String getAppointmentClientWebID() {
        return appointmentClientWebID;
    }

    public void setAppointmentClientWebID(String appointmentClientWebID) {
        this.appointmentClientWebID = appointmentClientWebID;
    }

    public String getAppointmentComment() {
        return appointmentComment;
    }

    public void setAppointmentComment(String appointmentComment) {
        this.appointmentComment = appointmentComment;
    }

    public String getAppointmentLocationWebID() {
        return appointmentLocationWebID;
    }

    public void setAppointmentLocationWebID(String appointmentLocationWebID) {
        this.appointmentLocationWebID = appointmentLocationWebID;
    }

    public String getAppointmentEndDate() {
        return appointmentEndDate;
    }

    public void setAppointmentEndDate(String appointmentEndDate) {
        this.appointmentEndDate = appointmentEndDate;
    }

}

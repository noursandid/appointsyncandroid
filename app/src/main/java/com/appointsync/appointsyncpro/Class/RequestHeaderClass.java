package com.appointsync.appointsyncpro.Class;

/**
 * Created by cgsawma on 11/1/18.
 */

public class RequestHeaderClass {

    public String userID;
    public String token;

    public RequestHeaderClass(String userID, String token) {
        this.userID = userID;
        this.token = token;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

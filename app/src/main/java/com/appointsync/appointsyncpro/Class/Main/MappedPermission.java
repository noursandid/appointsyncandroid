package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/16/19.
 */

public class MappedPermission {

    @SerializedName("canCreateAppointment")
    @Expose
    private Integer canCreateAppointment;
    @SerializedName("canAcceptAppointment")
    @Expose
    private Integer canAcceptAppointment;
    @SerializedName("canAmendAppointment")
    @Expose
    private Integer canAmendAppointment;
    @SerializedName("canCancelAppointment")
    @Expose
    private Integer canCancelAppointment;

    public Integer getCanCreateAppointment() {
        return canCreateAppointment;
    }

    public void setCanCreateAppointment(Integer canCreateAppointment) {
        this.canCreateAppointment = canCreateAppointment;
    }

    public Integer getCanAcceptAppointment() {
        return canAcceptAppointment;
    }

    public void setCanAcceptAppointment(Integer canAcceptAppointment) {
        this.canAcceptAppointment = canAcceptAppointment;
    }

    public Integer getCanAmendAppointment() {
        return canAmendAppointment;
    }

    public void setCanAmendAppointment(Integer canAmendAppointment) {
        this.canAmendAppointment = canAmendAppointment;
    }

    public Integer getCanCancelAppointment() {
        return canCancelAppointment;
    }

    public void setCanCancelAppointment(Integer canCancelAppointment) {
        this.canCancelAppointment = canCancelAppointment;
    }

}

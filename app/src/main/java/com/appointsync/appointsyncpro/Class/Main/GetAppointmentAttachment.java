package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/11/19.
 */

public class GetAppointmentAttachment {

    @SerializedName("appointmentWebID")
    @Expose
    private String appointmentWebID;

    public GetAppointmentAttachment(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

}

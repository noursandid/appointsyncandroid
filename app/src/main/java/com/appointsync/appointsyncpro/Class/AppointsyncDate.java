package com.appointsync.appointsyncpro.Class;

import java.util.Date;

/**
 * Created by cgsawma on 10/27/18.
 */

public class AppointsyncDate {

    Date date;

    private long output;
    public AppointsyncDate(String date){
        try {
            this.date = DateFormatters.formatters.ddmmyyyyhhmm.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AppointsyncDate(Date date){
        try {
            this.date = date;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public long getIntUnix(){
        this.output = date.getTime()/1000L;
        return Long.parseLong(Long.toString(output));
    }

    public String getUnix(){
        this.output = date.getTime()/1000L;
        return Long.toString(Long.parseLong(Long.toString(output)) * 1000);
    }

}

package com.appointsync.appointsyncpro.Class;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 10/19/18.
 */

public class AppointSyncResponse<T> {

    @SerializedName("errorCode")
    private int errorCode;
    @SerializedName("errorDescription")
    private String errorDescription;
    @SerializedName("data")
    private T data;

    public AppointSyncResponse(int errorCode, String errorDescription, T data) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.data = data;
    }



    public int getErrorCode() {

        if (this.errorCode == 100200){
            Controller.notifyNoSubscription();
            Controller.noActiveSubscription = true;
        }

        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

   public Boolean isSuccessful(){
        return this.errorCode == 0;

   }

}

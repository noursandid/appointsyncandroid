package com.appointsync.appointsyncpro.Class;

/**
 * Created by cgsawma on 10/20/18.
 */

public class ForgotPasswordClass {

    private String email;
    public ForgotPasswordClass(){

    }
    public ForgotPasswordClass(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}

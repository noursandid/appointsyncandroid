package com.appointsync.appointsyncpro.Class;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 10/19/18.
 */

public class Template {

        @SerializedName("webID")
        @Expose
        private String webID;
        @SerializedName("name")
        @Expose
        private String name;


        public Template(String webID, String name) {
            this.webID = webID;
            this.name = name;
        }



        public String getWebID() {
            return webID;
        }

        public void setWebID(String webID) {
            this.webID = webID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }



}

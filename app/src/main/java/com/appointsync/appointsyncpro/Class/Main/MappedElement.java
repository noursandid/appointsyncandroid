package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cgsawma on 1/29/19.
 */

public class MappedElement implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("index")
    @Expose
    private Integer index;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("typeWebID")
    @Expose
    private String typeWebID;
    @SerializedName("dataKeyValueList")
    @Expose
    private List<MappedElementKeyValue> dataKeyValueList = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeWebID() {
        return typeWebID;
    }

    public void setTypeWebID(String typeWebID) {
        this.typeWebID = typeWebID;
    }

    public List<MappedElementKeyValue> getDataKeyValueList() {
        return dataKeyValueList;
    }

    public void setDataKeyValueList(List<MappedElementKeyValue> dataKeyValueList) {
        this.dataKeyValueList = dataKeyValueList;
    }

}

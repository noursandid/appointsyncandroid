package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/16/19.
 */

public class PostAddFile {

    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;

    public PostAddFile(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }
}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/19/19.
 */

public class MappedPlan {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("tillDate")
    @Expose
    private Integer tillDate;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getTillDate() {
        return tillDate;
    }

    public void setTillDate(Integer tillDate) {
        this.tillDate = tillDate;
    }

}

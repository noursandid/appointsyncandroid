package com.appointsync.appointsyncpro.Class;

import java.util.Date;

/**
 * Created by cgsawma on 1/26/19.
 */

public class DateFullAppointmentDateToUnix {

    Date date;

    private long output;
    public DateFullAppointmentDateToUnix(String date){
        try {
            this.date = DateFormatters.formatters.fullAppointmentDate.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUnix(){
        this.output = date.getTime()/1000L;
        return Long.toString(Long.parseLong(Long.toString(output)) * 1000);
    }

}

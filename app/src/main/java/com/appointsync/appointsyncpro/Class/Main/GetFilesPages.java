package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/29/19.
 */

public class GetFilesPages {


    @SerializedName("pinCode")
    @Expose
    private String pinCode;
    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;

    public GetFilesPages(String pinCode, String clientWebID) {
        this.pinCode = pinCode;
        this.clientWebID = clientWebID;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

}

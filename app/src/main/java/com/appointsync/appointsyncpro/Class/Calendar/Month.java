package com.appointsync.appointsyncpro.Class.Calendar;

import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.DateHandler;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by cgsawma on 1/7/19.
 */

public class Month {

    private ArrayList<Day> days = new ArrayList<>();
    public String value;
    public void addAppointment(MappedAppointment appointment){
        for (Date appointmentDate : DateHandler.daysBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())) {
            String month = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix()) + " " + UnixDateConverter.UnixDateConverterAppointmentTimeMonth((new AppointsyncDate(appointmentDate)).getIntUnix());
            if (month.equalsIgnoreCase(this.value)){
                String appointmentDay = month + " " + UnixDateConverter.UnixDateConverterAppointmentTimeDay((new AppointsyncDate(appointmentDate)).getIntUnix());

                Boolean found = false;
                Day foundDay = null;
                for (Day day : this.days) {
                    if (day.value.equals(appointmentDay)) {
                        found = true;
                        foundDay = day;
                    }
                }

                if (found && foundDay != null) {
                    foundDay.addAppointment(appointment);
                } else {
                    Day day = new Day();
                    day.value = appointmentDay;
                    day.addAppointment(appointment);
                    this.days.add(day);
                }
            }
        }
    }


    public void removeAppointment(MappedAppointment appointment){

        for (Date appointmentDate : DateHandler.daysBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())) {
            String month = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix()) + " " + UnixDateConverter.UnixDateConverterAppointmentTimeMonth((new AppointsyncDate(appointmentDate)).getIntUnix());
                String appointmentDay = month + " " + UnixDateConverter.UnixDateConverterAppointmentTimeDay((new AppointsyncDate(appointmentDate)).getIntUnix());
            for (Day day : this.days) {
                if (day.value.equals(appointmentDay)) {
                    day.removeAppointment(appointment);
                }
            }
        }
    }


    public ArrayList<MappedAppointment> getAppointmentsForYearMonthDay(int[] yearMonthDay){

        ArrayList<MappedAppointment> appointments = new ArrayList<>();

        for (Day day : this.days){

            DecimalFormat df = new DecimalFormat("00");
            if (day.value.equals(yearMonthDay[0]+" "+df.format(yearMonthDay[1])+" "+df.format(yearMonthDay[2]))){
                return day.getAppointments();
            }
        }
        return appointments;
    }

    public ArrayList<Day> getDaysWithAppointmentsForYearMonthDay(){

        ArrayList<Day> days = new ArrayList<>();

        for (Day day : this.days){
            if (day.getAppointments().size() > 0 ){
                days.add(day);
            }
        }

        Collections.sort(days, new DayComparator());

        return days;
    }

    public class DayComparator implements Comparator<Day>
    {
        public int compare(Day left, Day right) {
            return left.value.compareTo(right.value);
        }
    }

    public Day getDayFromYearMonthDay(int[] yearMonthDay){
        for (Day day : this.days){
            DecimalFormat df = new DecimalFormat("00");
            if (day.value.equals(yearMonthDay[0]+" "+df.format(yearMonthDay[1])+" "+df.format(yearMonthDay[2]))){
                return day;
            }
        }

        return null;
    }
}

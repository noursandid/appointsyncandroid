package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cgsawma on 1/29/19.
 */

public class MappedFile implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dob")
    @Expose
    private Integer dob;
    @SerializedName("lastAppointmentDate")
    @Expose
    private Integer lastAppointmentDate;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("clientNumber")
    @Expose
    private Integer clientNumber;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("attachments")
    @Expose
    private List<MappedFileAttachment> attachments = null;
    @SerializedName("elements")
    @Expose
    private List<MappedElement> elements = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDob() {
        return dob;
    }

    public void setDob(Integer dob) {
        this.dob = dob;
    }

    public Integer getLastAppointmentDate() {
        return lastAppointmentDate;
    }

    public void setLastAppointmentDate(Integer lastAppointmentDate) {
        this.lastAppointmentDate = lastAppointmentDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<MappedFileAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<MappedFileAttachment> attachments) {
        this.attachments = attachments;
    }

    public List<MappedElement> getElements() {
        return elements;
    }

    public void setElements(List<MappedElement> elements) {
        this.elements = elements;
    }

}

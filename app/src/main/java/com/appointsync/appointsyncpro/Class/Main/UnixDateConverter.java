package com.appointsync.appointsyncpro.Class.Main;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by cgsawma on 12/28/18.
 */

public class UnixDateConverter {

//    private String date;

    public static String UnixDateConverter(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("EEE, d MMM yyyy");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterTime(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("h:mm a");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }


    public static String UnixDateConverterAppointmentTime(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("EEEE, MMM d, yyyy h:mm a");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterAppointmentTimePlusThirtyMinuets(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L + TimeUnit.MINUTES.toMillis(30));
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("EEEE, MMM d, yyyy h:mm a");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }


    public static String UnixDateConverterAppointmentTimeYear(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }



    public static String UnixDateConverterAppointmentTimeMonth(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterAppointmentTimeYear(long unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }



    public static String UnixDateConverterAppointmentTimeMonth(long unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterAppointmentTimeMonthText(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMM");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }



    public static String UnixDateConverterAppointmentTimeDay(long unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }



    public static String UnixDateConverterAppointmentTimeHour(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("H");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }



    public static String UnixDateConverterAppointmentTimeMinute(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("m");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterDateDayCompare(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterDateMonthCompare(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterDateGetNumberOfDays(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM/dd/yyyy");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

    public static String UnixDateConverterDateGetBalanceDate(int unixDate) {
        Date date = new java.util.Date(unixDate * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("MMM d, yyyy");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        String formattedDate = sdf.format(date);

        return formattedDate;
    }

}

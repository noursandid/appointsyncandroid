package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 2/9/19.
 */

public class RejectAppointment {

    String appointmentWebID;
    String rejectReason;

    public RejectAppointment(String appointmentWebID, String rejectReason) {
        this.appointmentWebID = appointmentWebID;
        this.rejectReason = rejectReason;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}

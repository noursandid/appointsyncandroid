package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/11/19.
 */

public class MappedAppointmentAttachments {

    @SerializedName("attachmentID")
    @Expose
    private String attachmentID;
    @SerializedName("attachmentAppointmentID")
    @Expose
    private String attachmentAppointmentID;
    @SerializedName("attachmentUserID")
    @Expose
    private String attachmentUserID;
    @SerializedName("attachment")
    @Expose
    private String attachment;
    @SerializedName("isDeleted")
    @Expose
    private Integer isDeleted;

    public MappedAppointmentAttachments(String attachmentID, String attachmentAppointmentID, String attachmentUserID, String attachment, Integer isDeleted) {
        this.attachmentID = attachmentID;
        this.attachmentAppointmentID = attachmentAppointmentID;
        this.attachmentUserID = attachmentUserID;
        this.attachment = attachment;
        this.isDeleted = isDeleted;
    }

    public String getAttachmentID() {
        return attachmentID;
    }

    public void setAttachmentID(String attachmentID) {
        this.attachmentID = attachmentID;
    }

    public String getAttachmentAppointmentID() {
        return attachmentAppointmentID;
    }

    public void setAttachmentAppointmentID(String attachmentAppointmentID) {
        this.attachmentAppointmentID = attachmentAppointmentID;
    }

    public String getAttachmentUserID() {
        return attachmentUserID;
    }

    public void setAttachmentUserID(String attachmentUserID) {
        this.attachmentUserID = attachmentUserID;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

}

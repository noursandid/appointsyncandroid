package com.appointsync.appointsyncpro.Class.Main;

import android.util.Log;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Calendar.SCalendar;
import com.appointsync.appointsyncpro.Interface.NotificationObserverDelegate;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cgsawma on 12/28/18.
 */

public class MappedProfile {

    @SerializedName("workingHourFrom")
    @Expose
    private Integer workingHourFrom;
    @SerializedName("workingHourTo")
    @Expose
    private Integer workingHourTo;
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("subscriptionDate")
    @Expose
    private Integer subscriptionDate;
    @SerializedName("tillDate")
    @Expose
    private Integer tillDate;
    @SerializedName("hiddenFiles")
    @Expose
    private Integer hiddenFiles;
    @SerializedName("locations")
    @Expose
    private Object locations;
    @SerializedName("loggedInAs")
    @Expose
    private Integer loggedInAs;
    @SerializedName("subscriptionType")
    @Expose
    private Integer subscriptionType;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("renewal")
    @Expose
    private String renewal;
    @SerializedName("autoRenew")
    @Expose
    private Integer autoRenew;
    @SerializedName("fileTemplate")
    @Expose
    private String fileTemplate;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("dob")
    @Expose
    private Integer dob;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("qrCode")
    @Expose
    private String qrCode;

    public SCalendar calendar = new SCalendar();

//    public MappedProfile(Integer workingHourFrom, Integer workingHourTo, String profession, Integer subscriptionDate, Integer tillDate, Integer hiddenFiles, Object locations, Integer loggedInAs, Integer subscriptionType, Double amount, String renewal, Integer autoRenew, String fileTemplate, String id, String webID, String username, String firstName, String middleName, String lastName, Integer dob, String countryCode, String phoneNumber, String gender, String email, String profilePic, Integer type, String createdDate, String qrCode) {
//        this.workingHourFrom = workingHourFrom;
//        this.workingHourTo = workingHourTo;
//        this.profession = profession;
//        this.subscriptionDate = subscriptionDate;
//        this.tillDate = tillDate;
//        this.hiddenFiles = hiddenFiles;
//        this.locations = locations;
//        this.loggedInAs = loggedInAs;
//        this.subscriptionType = subscriptionType;
//        this.amount = amount;
//        this.renewal = renewal;
//        this.autoRenew = autoRenew;
//        this.fileTemplate = fileTemplate;
//        this.id = id;
//        this.webID = webID;
//        this.username = username;
//        this.firstName = firstName;
//        this.middleName = middleName;
//        this.lastName = lastName;
//        this.dob = dob;
//        this.countryCode = countryCode;
//        this.phoneNumber = phoneNumber;
//        this.gender = gender;
//        this.email = email;
//        this.profilePic = profilePic;
//        this.type = type;
//        this.createdDate = createdDate;
//        this.qrCode = qrCode;
//    }

    public Integer getWorkingHourFrom() {
        return workingHourFrom;
    }

    public void setWorkingHourFrom(Integer workingHourFrom) {
        this.workingHourFrom = workingHourFrom;
    }

    public Integer getWorkingHourTo() {
        return workingHourTo;
    }

    public void setWorkingHourTo(Integer workingHourTo) {
        this.workingHourTo = workingHourTo;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Integer getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Integer subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public Integer getTillDate() {
        return tillDate;
    }

    public void setTillDate(Integer tillDate) {
        this.tillDate = tillDate;
    }

    public Integer getHiddenFiles() {
        return hiddenFiles;
    }

    public void setHiddenFiles(Integer hiddenFiles) {
        this.hiddenFiles = hiddenFiles;
    }

    public Object getLocations() {
        return locations;
    }

    public void setLocations(Object locations) {
        this.locations = locations;
    }

    public Integer getLoggedInAs() {
        return loggedInAs;
    }

    public void setLoggedInAs(Integer loggedInAs) {
        this.loggedInAs = loggedInAs;
    }

    public Integer getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(Integer subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRenewal() {
        return renewal;
    }

    public void setRenewal(String renewal) {
        this.renewal = renewal;
    }

    public Integer getAutoRenew() {
        return autoRenew;
    }

    public void setAutoRenew(Integer autoRenew) {
        this.autoRenew = autoRenew;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public void setFileTemplate(String fileTemplate) {
        this.fileTemplate = fileTemplate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getDob() {
        return dob;
    }

    public void setDob(Integer dob) {
        this.dob = dob;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }


    public void getNotifications(final NotificationObserverDelegate notificationObserver) {


        Call<AppointSyncResponse<ArrayList<MappedNotification>>> call = RetrofitInstance.getRetrofitInstance(new GetNotification());
        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedNotification>>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Response<AppointSyncResponse<ArrayList<MappedNotification>>> response) {

                if (response.body().getErrorCode() == 0) {

                    List<MappedNotification> mappedNotifications = response.body().getData();
//
                    Controller.notificationList = response.body().getData();
                    notificationObserver.didUpdateNotifications(mappedNotifications);
//                Controller.newNotificationCounter = notificationListNew.size();

//                notificationListNew.size();

//                notificationListNew.clear();
//                notificationListToday.clear();
//                notificationListThisMonth.clear();
//                notificationListOlder.clear();
//                notificationList.clear();

//                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//                Date date = new Date();
//                String dateToday = dateFormat.format(date);
//
//
//                DateFormat dateFormatMonth = new SimpleDateFormat("yyyy/MM");
//                Date dateMonth = new Date();
//                String dateStringMonth = dateFormatMonth.format(dateMonth);
//
//                List<MappedNotification> mappedNotifications = response.body().getData();
//
//                int mappedNotificationsCounter = mappedNotifications.size();
//                for (int i = 0; i < mappedNotificationsCounter; i++) {
//
//                    notificationList.add(mappedNotifications.get(i));
//
//                    if (mappedNotifications.get(i).getStatus().equals("0")){
//                        notificationListNew.add(mappedNotifications.get(i));
//                    }
//                    else if ((UnixDateConverter.UnixDateConverterDateDayCompare(mappedNotifications.get(i).getDate())).equals(dateToday)) {
//                        notificationListToday.add(mappedNotifications.get(i));
//                    }
//                    else if ((UnixDateConverter.UnixDateConverterDateMonthCompare(mappedNotifications.get(i).getDate())).equals(dateStringMonth)) {
//                        notificationListThisMonth.add(mappedNotifications.get(i));
//                    }else{
//                        notificationListOlder.add(mappedNotifications.get(i));
//                    }
//
////                    notificationList.add(mappedNotifications.get(i));
////                    System.out.println("Fragment NOTIFICATION->" + mappedNotifications.get(i).getDescription());
//
//                }
//
//                adapter.notifyDataSetChanged();
//                // ...the data has come back, a
//                // Now we call setRefreshing(false) to signal refresh has finished
//                swipeContainer.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedNotification>>> call, Throwable t) {

            }

            public void onFailure(Throwable e) {
                Log.d("DEBUG", "Fetch timeline error: " + e.toString());
            }
        });
    }
}


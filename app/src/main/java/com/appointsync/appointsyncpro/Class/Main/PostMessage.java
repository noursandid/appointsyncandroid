package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 1/13/19.
 */

public class PostMessage {

    private String threadTitle;
    private String message;
    private String toWebID;


    public PostMessage(String threadTitle, String message, String toWebID) {
        this.threadTitle = threadTitle;
        this.message = message;
        this.toWebID = toWebID;
    }


    public String getThreadTitle() {
        return threadTitle;
    }

    public void setThreadTitle(String threadTitle) {
        this.threadTitle = threadTitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToWebID() {
        return toWebID;
    }

    public void setToWebID(String toWebID) {
        this.toWebID = toWebID;
    }
}

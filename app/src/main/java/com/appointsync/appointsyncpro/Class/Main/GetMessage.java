package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 1/11/19.
 */

public class GetMessage {

    private String threadWebID;


    public GetMessage(String threadWebID) {
        this.threadWebID = threadWebID;
    }

    public String getThreadWebID() {
        return threadWebID;
    }

    public void setThreadWebID(String threadWebID) {
        this.threadWebID = threadWebID;
    }
}

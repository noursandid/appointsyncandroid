package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/19/19.
 */

public class PostEditProfile {

    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("workingHourFrom")
    @Expose
    private String workingHourFrom;
    @SerializedName("workingHourTo")
    @Expose
    private String workingHourTo;
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("firstName")
    @Expose
    private String firstName;

    public PostEditProfile(String lastName, String workingHourFrom, String workingHourTo, String profession, String firstName) {
        this.lastName = lastName;
        this.workingHourFrom = workingHourFrom;
        this.workingHourTo = workingHourTo;
        this.profession = profession;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkingHourFrom() {
        return workingHourFrom;
    }

    public void setWorkingHourFrom(String workingHourFrom) {
        this.workingHourFrom = workingHourFrom;
    }

    public String getWorkingHourTo() {
        return workingHourTo;
    }

    public void setWorkingHourTo(String workingHourTo) {
        this.workingHourTo = workingHourTo;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

}

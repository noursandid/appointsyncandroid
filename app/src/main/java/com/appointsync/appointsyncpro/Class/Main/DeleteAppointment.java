package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 2/9/19.
 */

public class DeleteAppointment {

    String appointmentWebID;

    public DeleteAppointment(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/12/19.
 */

public class GetClientFileBalance {

    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;
    @SerializedName("pinCode")
    @Expose
    private Object pinCode;

    public GetClientFileBalance(String clientWebID, Object pinCode) {
        this.clientWebID = clientWebID;
        this.pinCode = pinCode;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public Object getPinCode() {
        return pinCode;
    }

    public void setPinCode(Object pinCode) {
        this.pinCode = pinCode;
    }


}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/13/19.
 */

public class PostPaymentDue {

    @SerializedName("paymentDue")
    @Expose
    private String paymentDue;
    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("date")
    @Expose
    private String date;

    public PostPaymentDue(String paymentDue, String clientWebID, String note, String date) {
        this.paymentDue = paymentDue;
        this.clientWebID = clientWebID;
        this.note = note;
        this.date = date;
    }

    public String getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(String paymentDue) {
        this.paymentDue = paymentDue;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

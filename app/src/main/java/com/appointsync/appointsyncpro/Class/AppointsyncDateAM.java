package com.appointsync.appointsyncpro.Class;

import java.util.Date;

/**
 * Created by cgsawma on 1/19/19.
 */

public class AppointsyncDateAM {

    Date date;

    private long output;
    public AppointsyncDateAM(String date){
        try {
            this.date = DateFormatters.formatters.ddmmyyyyhmma.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUnix(){
        this.output = date.getTime()/1000L;
        return Long.toString(Long.parseLong(Long.toString(output)) * 1000);
    }
}

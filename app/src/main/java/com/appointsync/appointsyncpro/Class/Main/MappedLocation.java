package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 12/28/18.
 */

public class MappedLocation {

    @SerializedName("locationID")
    @Expose
    private String locationID;
    @SerializedName("locationTitle")
    @Expose
    private String locationTitle;
    @SerializedName("locationLongitude")
    @Expose
    private String locationLongitude;
    @SerializedName("locationLatitude")
    @Expose
    private String locationLatitude;
    @SerializedName("locationWebID")
    @Expose
    private String locationWebID;
    @SerializedName("locationIsDeleted")
    @Expose
    private Integer locationIsDeleted;


    public MappedLocation(String locationID, String locationTitle, String locationLongitude, String locationLatitude, String locationWebID, Integer locationIsDeleted) {
        this.locationID = locationID;
        this.locationTitle = locationTitle;
        this.locationLongitude = locationLongitude;
        this.locationLatitude = locationLatitude;
        this.locationWebID = locationWebID;
        this.locationIsDeleted = locationIsDeleted;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(String locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(String locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public String getLocationWebID() {
        return locationWebID;
    }

    public void setLocationWebID(String locationWebID) {
        this.locationWebID = locationWebID;
    }

    public Integer getLocationIsDeleted() {
        return locationIsDeleted;
    }

    public void setLocationIsDeleted(Integer locationIsDeleted) {
        this.locationIsDeleted = locationIsDeleted;
    }

}

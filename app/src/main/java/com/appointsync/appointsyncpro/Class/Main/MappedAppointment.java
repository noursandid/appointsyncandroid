package com.appointsync.appointsyncpro.Class.Main;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import com.appointsync.appointsyncpro.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by cgsawma on 1/4/19.
 */

public class MappedAppointment implements Serializable {

    @SerializedName("appointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("appointmentHostID")
    @Expose
    private String appointmentHostID;
    @SerializedName("appointmentClientID")
    @Expose
    private String appointmentClientID;
    @SerializedName("requesterID")
    @Expose
    private String requesterID;
    @SerializedName("startDate")
    @Expose
    private Integer startDate;
    @SerializedName("endDate")
    @Expose
    private Integer endDate;
    @SerializedName("locationID")
    @Expose
    private String locationID;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("requestComment")
    @Expose
    private String requestComment;
    @SerializedName("reasonOfRejection")
    @Expose
    private String reasonOfRejection;
    @SerializedName("rejecterID")
    @Expose
    private String rejecterID;
    @SerializedName("hasAttachment")
    @Expose
    private Integer hasAttachment;
    @SerializedName("isOffline")
    @Expose
    private Integer isOffline;
    @SerializedName("isDeleted")
    @Expose
    private Integer isDeleted;
    @SerializedName("client")
    @Expose
    private MappedClient client;
    @SerializedName("host")
    @Expose
    private Object host;
    @SerializedName("amendedAppointment")
    @Expose
    private MappedAmendedAppointment amendedAppointment;


    public MappedAppointment(String appointmentID, String webID, String appointmentHostID, String appointmentClientID, String requesterID, Integer startDate, Integer endDate, String locationID, Integer status, String requestComment, String reasonOfRejection, String rejecterID, Integer hasAttachment, Integer isOffline, MappedClient client, Object host, MappedAmendedAppointment amendedAppointment) {
        this.appointmentID = appointmentID;
        this.webID = webID;
        this.appointmentHostID = appointmentHostID;
        this.appointmentClientID = appointmentClientID;
        this.requesterID = requesterID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.locationID = locationID;
        this.status = status;
        this.requestComment = requestComment;
        this.reasonOfRejection = reasonOfRejection;
        this.rejecterID = rejecterID;
        this.hasAttachment = hasAttachment;
        this.isOffline = isOffline;
        this.client = client;
        this.host = host;
        this.amendedAppointment = amendedAppointment;
    }


    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getAppointmentHostID() {
        return appointmentHostID;
    }

    public void setAppointmentHostID(String appointmentHostID) {
        this.appointmentHostID = appointmentHostID;
    }

    public String getAppointmentClientID() {
        return appointmentClientID;
    }

    public void setAppointmentClientID(String appointmentClientID) {
        this.appointmentClientID = appointmentClientID;
    }

    public String getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(String requesterID) {
        this.requesterID = requesterID;
    }

    public Integer getStartDate() {
        return startDate;
    }

    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRequestComment() {
        return requestComment;
    }

    public void setRequestComment(String requestComment) {
        this.requestComment = requestComment;
    }

    public String getReasonOfRejection() {
        return reasonOfRejection;
    }

    public void setReasonOfRejection(String reasonOfRejection) {
        this.reasonOfRejection = reasonOfRejection;
    }

    public String getRejecterID() {
        return rejecterID;
    }

    public void setRejecterID(String rejecterID) {
        this.rejecterID = rejecterID;
    }

    public Integer getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(Integer hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public Integer getIsOffline() {
        return isOffline;
    }

    public void setIsOffline(Integer isOffline) {
        this.isOffline = isOffline;
    }

    public MappedClient getClient() {
        return client;
    }

    public void setClient(MappedClient client) {
        this.client = client;
    }

    public Object getHost() {
        return host;
    }

    public void setHost(Object host) {
        this.host = host;
    }

    public MappedAmendedAppointment getAmendedAppointment() {
        return amendedAppointment;
    }

    public void setAmendedAppointment(MappedAmendedAppointment amendedAppointment) {
        this.amendedAppointment = amendedAppointment;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getFinalStartDate() {

        if (this.amendedAppointment != null && this.amendedAppointment.getAmendingUserID() == Controller.user.getId()) {
            return this.amendedAppointment.getStartDate();
        } else {
            return this.getStartDate();
        }
    }

    public Integer getFinalEndDate() {

        if (this.amendedAppointment != null && this.amendedAppointment.getAmendingUserID() == Controller.user.getId()) {
            return this.amendedAppointment.getEndDate();
        } else {
            return this.getEndDate();
        }
    }

    public void update(MappedAppointment appointment) {
        this.amendedAppointment = appointment.amendedAppointment;
        this.status = appointment.status;
        this.startDate = appointment.startDate;
        this.endDate = appointment.endDate;
        this.locationID = appointment.locationID;
        this.client = appointment.client;
        this.reasonOfRejection = appointment.reasonOfRejection;
        this.rejecterID = appointment.rejecterID;
        this.hasAttachment = appointment.hasAttachment;
        this.client.update(appointment.client);
    }



    public static Comparator<MappedAppointment> StuRollno = new Comparator<MappedAppointment>() {

        public int compare(MappedAppointment s1, MappedAppointment s2) {

            int rollno1 = s1.getStartDate();
            int rollno2 = s2.getStartDate();

	   /*For ascending order*/
//            return rollno1-rollno2;

	   /*For descending order*/
           return rollno2-rollno1;
        }};

    public int getAppointmentColor(){

        Integer appointmentStatus = this.getStatus();
        int resourceID = 0;
        long unixTime = System.currentTimeMillis() / 1000L;


        if (this.getEndDate() < unixTime){

            appointmentStatus = 6;
            resourceID= R.drawable.appointment_passed_background;
        }
        if (appointmentStatus.equals(1)) {
            resourceID= R.drawable.appointment_requested_background;
        } else if (appointmentStatus.equals(2)) {
            resourceID= R.drawable.appointment_amended_background;
        } else if (appointmentStatus.equals(3)) {
            resourceID= R.drawable.appointment_accepted_background;
        } else if (appointmentStatus.equals(4)) {
            resourceID= R.drawable.appointment_rejected_background;
        } else if (appointmentStatus.equals(5)) {
            resourceID= R.drawable.appointment_rejected_background;
        }

        return resourceID;

    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/16/19.
 */

public class MappedClientFileAddAttachment {


    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("attachment")
    @Expose
    private String attachment;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("index")
    @Expose
    private Integer index;

    public MappedClientFileAddAttachment(String id, String webID, String attachment, Integer date, Integer index) {
        this.id = id;
        this.webID = webID;
        this.attachment = attachment;
        this.date = date;
        this.index = index;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/8/19.
 */

public class MappedNotification {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("fromID")
    @Expose
    private String fromID;
    @SerializedName("fromWebID")
    @Expose
    private String fromWebID;
    @SerializedName("toID")
    @Expose
    private String toID;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("post")
    @Expose
    private Integer post;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("correspondentWebID")
    @Expose
    private Object correspondentWebID;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromID() {
        return fromID;
    }

    public void setFromID(String fromID) {
        this.fromID = fromID;
    }

    public String getFromWebID() {
        return fromWebID;
    }

    public void setFromWebID(String fromWebID) {
        this.fromWebID = fromWebID;
    }

    public String getToID() {
        return toID;
    }

    public void setToID(String toID) {
        this.toID = toID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPost() {
        return post;
    }

    public void setPost(Integer post) {
        this.post = post;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Object getCorrespondentWebID() {
        return correspondentWebID;
    }

    public void setCorrespondentWebID(Object correspondentWebID) {
        this.correspondentWebID = correspondentWebID;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }


}

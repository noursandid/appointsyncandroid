package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/19/19.
 */

public class PostSecureFileFlag {

    @SerializedName("secureFile")
    @Expose
    private String secureFile;


    public PostSecureFileFlag(String secureFile) {
        this.secureFile = secureFile;
    }

    public String getSecureFile() {
        return secureFile;
    }

    public void setSecureFile(String secureFile) {
        this.secureFile = secureFile;
    }

}

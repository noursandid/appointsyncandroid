package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cgsawma on 1/10/19.
 */

public class MappedThread implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("webID")
    @Expose
    private String webID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("fromWebID")
    @Expose
    private String fromWebID;
    @SerializedName("fromUsername")
    @Expose
    private String fromUsername;
    @SerializedName("toWebID")
    @Expose
    private String toWebID;
    @SerializedName("toUsername")
    @Expose
    private String toUsername;
    @SerializedName("lastMessage")
    @Expose
    private String lastMessage;
    @SerializedName("lastUpdatedDate")
    @Expose
    private Integer lastUpdatedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebID() {
        return webID;
    }

    public void setWebID(String webID) {
        this.webID = webID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFromWebID() {
        return fromWebID;
    }

    public void setFromWebID(String fromWebID) {
        this.fromWebID = fromWebID;
    }

    public String getFromUsername() {
        return fromUsername;
    }

    public void setFromUsername(String fromUsername) {
        this.fromUsername = fromUsername;
    }

    public String getToWebID() {
        return toWebID;
    }

    public void setToWebID(String toWebID) {
        this.toWebID = toWebID;
    }

    public String getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Integer getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Integer lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

}

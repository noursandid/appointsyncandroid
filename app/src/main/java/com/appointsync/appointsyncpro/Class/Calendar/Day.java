package com.appointsync.appointsyncpro.Class.Calendar;

import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;

import java.util.ArrayList;

/**
 * Created by cgsawma on 1/7/19.
 */

public class Day {

    private ArrayList<MappedAppointment> appointments = new ArrayList<>();
    public String value;
    public void addAppointment(MappedAppointment appointment){
        Boolean found = false;
            System.out.println("ADDING APPOINTMENT " + this.value);
        for (MappedAppointment appo : this.appointments){
            if (appo.getAppointmentID().equals(appointment.getAppointmentID())){
                found = true;

                appo.update(appointment);

            }
        }

        if (!found) {
            this.appointments.add(appointment);
        }
    }


    public void removeAppointment(MappedAppointment appointment) {

        Boolean found = false;
        MappedAppointment foundAppointment = null;
        for (MappedAppointment appo : this.appointments) {
            if (appo.getAppointmentID().equals(appointment.getAppointmentID())) {
                found = true;
                foundAppointment = appo;
            }
        }

            if (found.equals(true)){
                this.appointments.remove(foundAppointment);
        }


    }


    public ArrayList<MappedAppointment> getAppointments(){
       return this.appointments;
    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/16/19.
 */

public class PostClientFileUpdateNote {

    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;

    @SerializedName("pageWebID")
    @Expose
    private String pageWebID;

    @SerializedName("note")
    @Expose
    private String note;

    public PostClientFileUpdateNote(String clientWebID, String pageWebID, String note) {
        this.clientWebID = clientWebID;
        this.pageWebID = pageWebID;
        this.note = note;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getPageWebID() {
        return pageWebID;
    }

    public void setPageWebID(String pageWebID) {
        this.pageWebID = pageWebID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

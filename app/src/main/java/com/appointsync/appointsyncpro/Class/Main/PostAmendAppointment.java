package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 2/11/19.
 */

public class PostAmendAppointment {

    String appointmentWebID;
    String appointmentStartDate;
    String appointmentEndDate;
    String appointmentLocationWebID;

    public PostAmendAppointment(String appointmentWebID, String appointmentStartDate, String appointmentEndDate, String appointmentLocationWebID) {
        this.appointmentWebID = appointmentWebID;
        this.appointmentStartDate = appointmentStartDate;
        this.appointmentEndDate = appointmentEndDate;
        this.appointmentLocationWebID = appointmentLocationWebID;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getAppointmentStartDate() {
        return appointmentStartDate;
    }

    public void setAppointmentStartDate(String appointmentStartDate) {
        this.appointmentStartDate = appointmentStartDate;
    }

    public String getAppointmentEndDate() {
        return appointmentEndDate;
    }

    public void setAppointmentEndDate(String appointmentEndDate) {
        this.appointmentEndDate = appointmentEndDate;
    }

    public String getAppointmentLocationWebID() {
        return appointmentLocationWebID;
    }

    public void setAppointmentLocationWebID(String appointmentLocationWebID) {
        this.appointmentLocationWebID = appointmentLocationWebID;
    }
}

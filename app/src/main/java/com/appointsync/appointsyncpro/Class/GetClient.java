package com.appointsync.appointsyncpro.Class;

/**
 * Created by cgsawma on 12/25/18.
 */

public class GetClient {

    private String clientWebID;

    public GetClient(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }
}

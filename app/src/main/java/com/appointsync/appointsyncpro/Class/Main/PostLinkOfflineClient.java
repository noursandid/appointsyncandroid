package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/28/19.
 */

public class PostLinkOfflineClient {

    @SerializedName("offlineWebID")
    @Expose
    private String offlineWebID;
    @SerializedName("username")
    @Expose
    private String username;

    public PostLinkOfflineClient(String offlineWebID, String username) {
        this.offlineWebID = offlineWebID;
        this.username = username;
    }

    public String getOfflineWebID() {
        return offlineWebID;
    }

    public void setOfflineWebID(String offlineWebID) {
        this.offlineWebID = offlineWebID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}

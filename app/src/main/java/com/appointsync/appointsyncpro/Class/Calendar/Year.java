package com.appointsync.appointsyncpro.Class.Calendar;

import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.DateHandler;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by cgsawma on 1/7/19.
 */

public class Year {

    private ArrayList<Month> months = new ArrayList<>();
    public String value;


    public void addAppointment(MappedAppointment appointment){
        for (Date appointmentDate : DateHandler.monthsBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())) {

            String year = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix());
            if (year.equalsIgnoreCase(this.value)){
                String appointmentMonth = year + " " + UnixDateConverter.UnixDateConverterAppointmentTimeMonth((new AppointsyncDate(appointmentDate)).getIntUnix());

                Boolean found = false;
                Month foundMonth = null;
                for (Month month : this.months) {
                    if (month.value.equals(appointmentMonth)) {
                        found = true;
                        foundMonth = month;
                    }
                }

                if (found && foundMonth != null) {
                    foundMonth.addAppointment(appointment);
                } else {
                    Month month = new Month();
                    month.value = appointmentMonth;
                    month.addAppointment(appointment);
                    this.months.add(month);
                }
            }
        }
        System.out.println(";");
    }



    public void removeAppointment(MappedAppointment appointment){
        for (Date appointmentDate : DateHandler.monthsBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())) {

            String year = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix());
            if (year.equalsIgnoreCase(this.value)) {
                String appointmentMonth = year + " " + UnixDateConverter.UnixDateConverterAppointmentTimeMonth((new AppointsyncDate(appointmentDate)).getIntUnix());
                for (Month month : this.months) {
                    if (month.value.equals(appointmentMonth)) {
                        month.removeAppointment(appointment);
                    }
                }
            }
        }
    }


    public ArrayList<MappedAppointment> getAppointmentsForYearMonthDay(int[] yearMonthDay){

        ArrayList<MappedAppointment> appointments = new ArrayList<>();

        for (Month month : this.months){

            DecimalFormat df = new DecimalFormat("00");
            if (month.value.equals(yearMonthDay[0]+" "+df.format(yearMonthDay[1]))){
                return month.getAppointmentsForYearMonthDay(yearMonthDay);
            }
        }
        return appointments;
    }

    public ArrayList<Day> getDaysWithAppointmentsForYearMonthDay(int[] yearMonthDay){

        ArrayList<Day> days = new ArrayList<>();

        for (Month month : this.months){

            DecimalFormat df = new DecimalFormat("00");
            if (month.value.equals(yearMonthDay[0]+" "+df.format(yearMonthDay[1]))){
                return month.getDaysWithAppointmentsForYearMonthDay();
            }
        }
        return days;
    }

    public Day getDayFromYearMonthDay(int[] yearMonthDay){
        for (Month month : this.months){
            DecimalFormat df = new DecimalFormat("00");
            if (month.value.equals(yearMonthDay[0]+" "+df.format(yearMonthDay[1]))){
                return month.getDayFromYearMonthDay(yearMonthDay);
            }
        }

        return null;
    }

}

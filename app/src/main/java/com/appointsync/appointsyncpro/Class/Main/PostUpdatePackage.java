package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/21/19.
 */

public class PostUpdatePackage {
    @SerializedName("package")
    @Expose
    private String _package;
    @SerializedName("type")
    @Expose
    private Integer type;

    public PostUpdatePackage(String _package, Integer type) {
        this._package = _package;
        this.type = type;
    }

    public String getPackage() {
        return _package;
    }

    public void setPackage(String _package) {
        this._package = _package;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}

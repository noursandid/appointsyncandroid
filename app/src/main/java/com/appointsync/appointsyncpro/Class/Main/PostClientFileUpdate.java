package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/16/19.
 */

public class PostClientFileUpdate {

    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;

    @SerializedName("pageWebID")
    @Expose
    private String pageWebID;

    @SerializedName("elementWebID")
    @Expose
    private String elementWebID;

    @SerializedName("keyWebID")
    @Expose
    private String keyWebID;

    @SerializedName("value")
    @Expose
    private String value;

    public PostClientFileUpdate(String clientWebID, String pageWebID, String elementWebID, String keyWebID, String value) {
        this.clientWebID = clientWebID;
        this.pageWebID = pageWebID;
        this.elementWebID = elementWebID;
        this.keyWebID = keyWebID;
        this.value = value;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getPageWebID() {
        return pageWebID;
    }

    public void setPageWebID(String pageWebID) {
        this.pageWebID = pageWebID;
    }

    public String getElementWebID() {
        return elementWebID;
    }

    public void setElementWebID(String elementWebID) {
        this.elementWebID = elementWebID;
    }

    public String getKeyWebID() {
        return keyWebID;
    }

    public void setKeyWebID(String keyWebID) {
        this.keyWebID = keyWebID;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

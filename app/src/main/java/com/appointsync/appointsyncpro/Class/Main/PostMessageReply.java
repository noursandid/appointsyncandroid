package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 1/13/19.
 */

public class PostMessageReply {


    private String threadWebID;
    private String message;
    private String toWebID;


    public PostMessageReply(String threadWebID, String message, String toWebID) {
        this.threadWebID = threadWebID;
        this.message = message;
        this.toWebID = toWebID;
    }


    public String getThreadTitle() {
        return threadWebID;
    }

    public void setThreadTitle(String threadTitle) {
        this.threadWebID = threadTitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToWebID() {
        return toWebID;
    }

    public void setToWebID(String toWebID) {
        this.toWebID = toWebID;
    }

}

package com.appointsync.appointsyncpro.Class;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by cgsawma on 2/22/19.
 */

public class DateHandler {

    public static ArrayList<Date> yearsBetween(int dateInt1,int dateInt2){
        Date date1 = new Date((long)dateInt1*1000);
        Date date2 = new Date((long)dateInt2*1000);


        System.out.println("DATE1 " + date1.toString() + "with int " + dateInt1);
        System.out.println("DATE2 " + date2.toString() + "with int " + dateInt2);
        ArrayList<Date> dates = new ArrayList<>();
        Date currentDate = date1;

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(currentDate);
        cal2.setTime(date2);


        while (cal1.get(Calendar.YEAR) <= cal2.get(Calendar.YEAR)){

            dates.add(currentDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.YEAR,1);
            currentDate = cal.getTime();
            cal1.setTime(currentDate);
        }
        System.out.println(dates.get(0).toString());
        return dates;
    }

    public static ArrayList<Date> monthsBetween(int dateInt1,int dateInt2){
        Date date1 = new Date((long)dateInt1*1000);
        Date date2 = new Date((long)dateInt2*1000);


        ArrayList<Date> dates = new ArrayList<>();
        Date currentDate = date1;

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(currentDate);
        cal2.setTime(date2);

        while (cal1.get(Calendar.YEAR)+cal1.get(Calendar.MONTH) <= cal2.get(Calendar.YEAR)+cal2.get(Calendar.MONTH)){
            dates.add(currentDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.MONTH,1);
            currentDate = cal.getTime();
            cal1.setTime(currentDate);
        }
        System.out.println(dates.get(0).toString());
        return dates;
    }

    public static ArrayList<Date> daysBetween(int dateInt1,int dateInt2){
        Date date1 = new Date((long)dateInt1*1000);
        Date date2 = new Date((long)dateInt2*1000);
        ArrayList<Date> dates = new ArrayList<>();
        Date currentDate = date1;
        while (currentDate.compareTo(date2) <= 0){
            dates.add(currentDate);
            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.DAY_OF_MONTH,1);
            currentDate = cal.getTime();
        }
        return dates;
    }
}

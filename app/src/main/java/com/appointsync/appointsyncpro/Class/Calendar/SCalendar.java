package com.appointsync.appointsyncpro.Class.Calendar;

import com.appointsync.appointsyncpro.Class.AppointsyncDate;
import com.appointsync.appointsyncpro.Class.DateFullAppointmentDateToUnix;
import com.appointsync.appointsyncpro.Class.DateHandler;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by cgsawma on 1/7/19.
 */

public class SCalendar {


    private ArrayList<Year> years = new ArrayList<>();

    public void addAppointments(ArrayList<MappedAppointment> appointments) {
        System.out.println("THIS IS SPARTA");
        ArrayList<MappedAppointment> appointmentsToRemove = new ArrayList<>();
        for (MappedAppointment appointment : appointments) {
            if (appointment.getIsDeleted() == 0) {
            for (Date appointmentDate : DateHandler.yearsBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())){
                String appointmentYear = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix());
                Boolean found = false;
                Year foundYear = null;
                for (Year year : this.years) {

                    if (year.value.equals(appointmentYear)) {
                        found = true;
                        foundYear = year;
                    }
                }

                MappedAppointment appo = getAppointmentFromAllAppointments(appointment);
                if (appo != null) {
                    appo.update(appointment);

                } else {
                    Controller.appointmentList.add(appointment);
                }//already handled to update in all appointments
                if (found && foundYear != null) {
                    foundYear.addAppointment(appointment);
                } else {
                    Year year = new Year();
                    year.value = appointmentYear;
                    year.addAppointment(appointment);
                    this.years.add(year);
                }
            }
            }
            else{
                appointmentsToRemove.add(appointment);
            }
        }
        if (appointmentsToRemove.size() > 0) {
            removeAppointments(appointmentsToRemove);
        }
    }

    public void addAppointment(MappedAppointment appointment) {
        ArrayList<MappedAppointment> appointments = new ArrayList<>();
        appointments.add(appointment);
        addAppointments(appointments);
    }


//    Removing appointment


    public void removeAppointments(ArrayList<MappedAppointment> appointments) {
        for (MappedAppointment appointment : appointments) {
            for (Date appointmentDate : DateHandler.yearsBetween(appointment.getFinalStartDate(),appointment.getFinalEndDate())) {
                String appointmentYear = UnixDateConverter.UnixDateConverterAppointmentTimeYear((new AppointsyncDate(appointmentDate)).getIntUnix());
                Boolean found = false;
                for (Year year : this.years) {
                    if (year.value.equals(appointmentYear)) {
                        found = true;
                        year.removeAppointment(appointment);
                    }
                }
                if (found.equals(true)) {

                    Controller.appointmentList.remove(appointment);
                }
            }
        }

    }

    public void removeAppointment(MappedAppointment appointment) {

        ArrayList<MappedAppointment> appointments = new ArrayList<>();
        appointments.add(appointment);
        removeAppointments(appointments);
    }


    private MappedAppointment getAppointmentFromAllAppointments(MappedAppointment appointment) {


        for (MappedAppointment appo : Controller.appointmentList) {
            if (appointment.getAppointmentID().equals(appo.getAppointmentID())) {
                return appo;
            }
        }
        return null;
    }

    public ArrayList<MappedAppointment> getAppointmentsForYearMonthDay(int[] yearMonthDay) {
        ArrayList<MappedAppointment> appointments = new ArrayList<>();

        for (Year year : this.years) {
            if (year.value.equals("" + yearMonthDay[0])) {
                return year.getAppointmentsForYearMonthDay(yearMonthDay);

            }
        }

        return appointments;
    }

    public ArrayList<Day> getDaysWithAppointmentsForYearMonthDay(int[] yearMonthDay) {

        ArrayList<Day> days = new ArrayList<>();

        for (Year year : this.years) {
            if (year.value.equals("" + yearMonthDay[0])) {
                return year.getDaysWithAppointmentsForYearMonthDay(yearMonthDay);

            }
        }

        return days;
    }


    public int firstDayInWeek(int[] yearMonth) {
        Calendar c = Calendar.getInstance();
        c.set(yearMonth[0], yearMonth[1] - 1, 1);
        return c.get(Calendar.DAY_OF_WEEK) - 1;
    }

    public int numberOfDaysInMonthWithUnusedDays(int[] yearMonth) {
        Calendar mycal = new GregorianCalendar(yearMonth[0], yearMonth[1] - 1, 1);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return this.firstDayInWeek(yearMonth) + daysInMonth - 1;
    }

    public Date getDateFromYearMonthDay(int[] yearMonthDay) {
        Calendar c = Calendar.getInstance();
        c.set(yearMonthDay[0], yearMonthDay[1] - 1, yearMonthDay[2]);
        return c.getTime();
    }

    public Boolean isToday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        long milliDate = cal.getTimeInMillis();

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date());
        cal1.set(Calendar.HOUR_OF_DAY, 0);
        cal1.set(Calendar.MINUTE, 0);
        cal1.set(Calendar.SECOND, 0);
        cal1.set(Calendar.MILLISECOND, 0);

        return cal1.getTimeInMillis() == milliDate;

    }

    public Day getDayFromYearMonthDay(int[] yearMonthDay) {
        for (Year year : this.years) {
            if (year.value.equals(String.valueOf(yearMonthDay[0]))) {
                return year.getDayFromYearMonthDay(yearMonthDay);
            }
        }

        return null;
    }


}

package com.appointsync.appointsyncpro.Class;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cgsawma on 10/19/18.
 */

public class TemplateList {

    @SerializedName("template_list")
    private ArrayList<Template> templateList;

    public ArrayList<Template> getTemplateArrayList() {
        return templateList;
    }

    public void setTemplateArrayList(ArrayList<Template> getTemplateArrayList) {
        this.templateList = getTemplateArrayList;
    }


}

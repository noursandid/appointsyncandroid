package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/21/19.
 */

public class PostLocation {

    @SerializedName("locationLongitude")
    @Expose
    private String locationLongitude;
    @SerializedName("locationLatitude")
    @Expose
    private String locationLatitude;
    @SerializedName("locationName")
    @Expose
    private String locationName;

    public PostLocation(String locationLongitude, String locationLatitude, String locationName) {
        this.locationLongitude = locationLongitude;
        this.locationLatitude = locationLatitude;
        this.locationName = locationName;
    }

    public String getLocationLongitude() {
        return locationLongitude;
    }

    public void setLocationLongitude(String locationLongitude) {
        this.locationLongitude = locationLongitude;
    }

    public String getLocationLatitude() {
        return locationLatitude;
    }

    public void setLocationLatitude(String locationLatitude) {
        this.locationLatitude = locationLatitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }


}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/16/19.
 */

public class MappedNotificationFlag {

    @SerializedName("notification")
    @Expose
    private Integer notification;
    @SerializedName("email")
    @Expose
    private Integer email;

    public Integer getNotification() {
        return notification;
    }

    public void setNotification(Integer notification) {
        this.notification = notification;
    }

    public Integer getEmail() {
        return email;
    }

    public void setEmail(Integer email) {
        this.email = email;
    }

}

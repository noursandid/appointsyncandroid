package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/20/19.
 */

public class PostCancelSubscription {

    @SerializedName("activeSubscription")
    @Expose
    private String activeSubscription;

    public PostCancelSubscription(String activeSubscription) {
        this.activeSubscription = activeSubscription;
    }

    public String getActiveSubscription() {
        return activeSubscription;
    }

    public void setActiveSubscription(String activeSubscription) {
        this.activeSubscription = activeSubscription;
    }
}

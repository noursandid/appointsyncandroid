package com.appointsync.appointsyncpro.Class;

/**
 * Created by cgsawma on 10/20/18.
 */

public class HostRegistrationClass {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String profession;
    private String fileTemplateWebID;
    private String email;
    private String gender;
    private String phoneNumber;
    private String countryCode;
    private String workingHourFrom;
    private String workingHourTo;
    private String dob;
    private String packageValue;
    private String type;

    public HostRegistrationClass(String username, String password, String firstName, String lastName, String profession, String fileTemplateWebID, String email, String gender, String phoneNumber, String countryCode, String workingHourFrom, String workingHourTo, String dob, String packageValue, String type) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profession = profession;
        this.fileTemplateWebID = fileTemplateWebID;
        this.email = email;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.countryCode = countryCode;
        this.workingHourFrom = workingHourFrom;
        this.workingHourTo = workingHourTo;
        this.dob = dob;
        this.packageValue = packageValue;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getFileTemplateWebID() {
        return fileTemplateWebID;
    }

    public void setFileTemplateWebID(String fileTemplateWebID) {
        this.fileTemplateWebID = fileTemplateWebID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getWorkingHourFrom() {
        return workingHourFrom;
    }

    public void setWorkingHourFrom(String workingHourFrom) {
        this.workingHourFrom = workingHourFrom;
    }

    public String getWorkingHourTo() {
        return workingHourTo;
    }

    public void setWorkingHourTo(String workingHourTo) {
        this.workingHourTo = workingHourTo;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(String packageValue) {
        this.packageValue = packageValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

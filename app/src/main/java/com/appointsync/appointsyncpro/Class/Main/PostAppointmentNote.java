package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 2/12/19.
 */

public class PostAppointmentNote {

    private String appointmentWebID;
    private String note;

    public PostAppointmentNote(String appointmentWebID, String note) {
        this.appointmentWebID = appointmentWebID;
        this.note = note;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

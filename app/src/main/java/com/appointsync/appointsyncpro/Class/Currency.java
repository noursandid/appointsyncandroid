package com.appointsync.appointsyncpro.Class;

import java.text.DecimalFormat;

/**
 * Created by cgsawma on 2/16/19.
 */

public class Currency {

    public String symbol;
    public String name;
    public String symbol_native;
    public Double decimal_digits;
    public Double rounding;
    public String code;
    public String name_plural;
}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by cgsawma on 11/25/18.
 */

public class MappedClient extends MappedUser implements Serializable {

    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("relationType")
    @Expose
    private Integer relationType;
    @SerializedName("clientNumber")
    @Expose
    private Integer clientNumber;

    public ArrayList<MappedFile> mappedFiles = new ArrayList<>();

    public MappedClient(String id, String webID, String username, String firstName, String lastName, Integer dob, String countryCode, String phoneNumber, String gender, String email, String profilePic, Integer type, String createdDate, String qrCode, String middleName, Integer relationType, Integer clientNumber) {
        super(id, webID, username, firstName, lastName, dob, countryCode, phoneNumber, gender, email, profilePic, type, createdDate, qrCode);
        this.middleName = middleName;
        this.relationType = relationType;
        this.clientNumber = clientNumber;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Integer getRelationType() {
        return relationType;
    }

    public void setRelationType(Integer relationType) {
        this.relationType = relationType;
    }

    public Integer getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(Integer clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getIdentifier(){
        String identifier = "";
        if (getType().equals(2)){
            identifier = getUsername();

        }else if(getType().equals(3)){
            identifier = getUsername();

        }else if(getType().equals(4)){

            if(getMiddleName().isEmpty()){
                identifier = getFirstName() + " "+getLastName();
            }else{
                identifier = getFirstName() + " " +getMiddleName() + " " +getLastName();
            }

        }

        return identifier;
    }

    public static Comparator<MappedClient> StuNameComparator = new Comparator<MappedClient>() {

        public int compare(MappedClient s1, MappedClient s2) {
            String StudentName1 = s1.getFirstName().toUpperCase();
            String StudentName2 = s2.getFirstName().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };

    public void update(MappedClient client){
        this.setFirstName(client.getFirstName());
        this.setLastName(client.getLastName());
        this.setDob(client.getDob());
        this.setPhoneNumber(client.getPhoneNumber());
        this.setGender(client.getGender());
        this.setEmail(client.getEmail());
        this.setProfilePic(client.getProfilePic());
        this.setType(client.getType());
        this.setCreatedDate(client.getCreatedDate());
        this.setQrCode(client.getQrCode());
        this.middleName = client.middleName;
        this.relationType = client.relationType;
        this.clientNumber = client.clientNumber;
        this.mappedFiles = client.mappedFiles;
    }
}
package com.appointsync.appointsyncpro.Class;

/**
 * Created by cgsawma on 10/20/18.
 */

public class Success {

    private Boolean success;
    private Integer errorCode;
    private String errorDescription;

    public Success(){

    }
    public Success(Boolean success, Integer errorCode, String errorDescription) {
        this.success = success;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/27/19.
 */

public class PostUpdateClientPermissions {

    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;
    @SerializedName("permission")
    @Expose
    private String permission;
    @SerializedName("permissionValue")
    @Expose
    private String permissionValue;

    public String getClientWebID() {
        return clientWebID;
    }

    public PostUpdateClientPermissions(String clientWebID, String permission, String permissionValue) {
        this.clientWebID = clientWebID;
        this.permission = permission;
        this.permissionValue = permissionValue;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

}

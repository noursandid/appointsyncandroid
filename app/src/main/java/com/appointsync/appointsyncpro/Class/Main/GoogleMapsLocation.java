package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 1/21/19.
 */

public class GoogleMapsLocation {

    private String Latitude;
    private String Longitude;

    public GoogleMapsLocation(String latitude, String longitude) {
        Latitude = latitude;
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}

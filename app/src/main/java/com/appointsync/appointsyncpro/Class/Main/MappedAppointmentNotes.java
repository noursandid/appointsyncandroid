package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/11/19.
 */

public class MappedAppointmentNotes {

    @SerializedName("noteID")
    @Expose
    private String noteID;
    @SerializedName("noteAppointmentID")
    @Expose
    private String noteAppointmentID;
    @SerializedName("noteUserID")
    @Expose
    private String noteUserID;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("isDeleted")
    @Expose
    private String isDeleted;

    public MappedAppointmentNotes(String noteID, String noteAppointmentID, String noteUserID, String note, Integer date, String isDeleted) {
        this.noteID = noteID;
        this.noteAppointmentID = noteAppointmentID;
        this.noteUserID = noteUserID;
        this.note = note;
        this.date = date;
        this.isDeleted = isDeleted;
    }

    public String getNoteID() {
        return noteID;
    }

    public void setNoteID(String noteID) {
        this.noteID = noteID;
    }

    public String getNoteAppointmentID() {
        return noteAppointmentID;
    }

    public void setNoteAppointmentID(String noteAppointmentID) {
        this.noteAppointmentID = noteAppointmentID;
    }

    public String getNoteUserID() {
        return noteUserID;
    }

    public void setNoteUserID(String noteUserID) {
        this.noteUserID = noteUserID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

}

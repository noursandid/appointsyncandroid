package com.appointsync.appointsyncpro.Class;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;
import com.appointsync.appointsyncpro.Interface.NotificationObserverDelegate;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

/**
 * Created by cgsawma on 2/21/19.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        System.out.println("RECEIVED NOTI");
        Controller.notifyAllObserversToUpdateAll();
    }

}
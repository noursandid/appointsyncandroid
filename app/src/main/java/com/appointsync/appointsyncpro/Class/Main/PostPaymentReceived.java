package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/13/19.
 */

public class PostPaymentReceived {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("clientWebID")
    @Expose
    private String clientWebID;
    @SerializedName("paymentReceived")
    @Expose
    private String paymentReceived;
    @SerializedName("note")
    @Expose
    private String note;

    public PostPaymentReceived(String date, String clientWebID, String paymentReceived, String note) {
        this.date = date;
        this.clientWebID = clientWebID;
        this.paymentReceived = paymentReceived;
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClientWebID() {
        return clientWebID;
    }

    public void setClientWebID(String clientWebID) {
        this.clientWebID = clientWebID;
    }

    public String getPaymentReceived() {
        return paymentReceived;
    }

    public void setPaymentReceived(String paymentReceived) {
        this.paymentReceived = paymentReceived;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}

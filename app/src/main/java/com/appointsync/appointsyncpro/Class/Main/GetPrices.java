package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 2/20/19.
 */

public class GetPrices {

    @SerializedName("for")
    @Expose
    private Integer forValue;

    public GetPrices(Integer forValue) {
        this.forValue = forValue;
    }

    public Integer getForValue() {
        return forValue;
    }

    public void setForValue(Integer forValue) {
        this.forValue = forValue;
    }
}

package com.appointsync.appointsyncpro.Class.Main;

import android.app.Fragment;

import com.appointsync.appointsyncpro.Dashboard;
import com.appointsync.appointsyncpro.Interface.NoSubscriptionObserver;
import com.appointsync.appointsyncpro.Interface.UpdateAllObserver;

import java.util.ArrayList;

/**
 * Created by cgsawma on 1/4/19.
 */

public class Controller{

    public static MappedProfile user;
    public static String loggedInuserToken;
    public static Boolean clientFilePage;
    public static Boolean noActiveSubscription;
    public static Integer selectedClientFilePage;
    public static MappedPermission permissions;
    public static MappedPermission clientPermission;
    public static MappedClient client;
    public static MappedAppointment appointment;
    public static MappedNotificationFlag notificationFlag;
    public static ArrayList<MappedNotification> notificationList;
    public static ArrayList<MappedAppointment> appointmentList = new ArrayList<>();
    public static ArrayList<MappedClient> clientList;
    public static ArrayList<MappedClient> onlineClientList;
    public static ArrayList<MappedThread> threadList;
    public static ArrayList<MappedLocation> locationList;
    public static ArrayList<MappedMessage> messageList;
    public static ArrayList<MappedFile> mappedFiles;
    public static MappedLocation location;
    public static android.support.v4.app.Fragment thread;
    public static android.support.v4.app.Fragment clientBalance;
    public static String isInAppointmentView;
    public static String userPinCode;
    public static String selectedYearMonthDayController;

    public static int newNotificationCounter = 0;

    private static ArrayList<UpdateAllObserver> updateAllbservers = new ArrayList<>();
    public static NoSubscriptionObserver noSubscriptionObserver;//lol im drunk hahahaha
    public static ArrayList<MappedAppointment> getAppointmentForClient(String webID){

        ArrayList<MappedAppointment> filteredAppointment = new ArrayList<>();

        for (MappedAppointment a: appointmentList){

            if(a.getClient().getWebID().equals(webID)){
                System.out.println("eh eh bass badde l webID " + webID + "ZOUBI L ABYAD " + a.getClient().getWebID());
                filteredAppointment.add(a);
            }
        }
        System.out.println("halla2 haydik akid 7a ykouno metel ba3ed cz fi if statement bass kess ekht lle be2ammin la android w java " + filteredAppointment);
        return filteredAppointment;

    }

    public static ArrayList<MappedLocation> getLocation(String locationID){

        ArrayList<MappedLocation> filteredLocation = new ArrayList<>();

        for (MappedLocation l: locationList){

            if(l.getLocationID().equals(locationID)){

                filteredLocation.add(l);
            }
        }
        return filteredLocation;

    }

    public static void registerAsAnObserverToUpdateAll(UpdateAllObserver observer){
        if (observer != null && !Controller.updateAllbservers.contains(observer)){
            Controller.updateAllbservers.add(observer);
        }
    }

    public static void notifyAllObserversToUpdateAll(){
        for (UpdateAllObserver observer : Controller.updateAllbservers){
            if (observer != null ){
                observer.updateAll();
            }
        }
    }

    public static void notifyNoSubscription(){
    Controller.noSubscriptionObserver.notifyEndOfSubscription();
    }

}



package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by cgsawma on 1/4/19.
 */

public class MappedAmendedAppointment implements Serializable{

    @SerializedName("appointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("appointmentHostID")
    @Expose
    private String appointmentHostID;
    @SerializedName("appointmentClientID")
    @Expose
    private String appointmentClientID;
    @SerializedName("requesterID")
    @Expose
    private String requesterID;
    @SerializedName("startDate")
    @Expose
    private Integer startDate;
    @SerializedName("endDate")
    @Expose
    private Integer endDate;
    @SerializedName("locationID")
    @Expose
    private String locationID;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("amendingUserID")
    @Expose
    private String amendingUserID;

    public MappedAmendedAppointment(String appointmentID, String appointmentHostID, String appointmentClientID, String requesterID, Integer startDate, Integer endDate, String locationID, Integer status, String amendingUserID) {
        this.appointmentID = appointmentID;
        this.appointmentHostID = appointmentHostID;
        this.appointmentClientID = appointmentClientID;
        this.requesterID = requesterID;
        this.startDate = startDate;
        this.endDate = endDate;
        this.locationID = locationID;
        this.status = status;
        this.amendingUserID = amendingUserID;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getAppointmentHostID() {
        return appointmentHostID;
    }

    public void setAppointmentHostID(String appointmentHostID) {
        this.appointmentHostID = appointmentHostID;
    }

    public String getAppointmentClientID() {
        return appointmentClientID;
    }

    public void setAppointmentClientID(String appointmentClientID) {
        this.appointmentClientID = appointmentClientID;
    }

    public String getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(String requesterID) {
        this.requesterID = requesterID;
    }

    public Integer getStartDate() {
        return startDate;
    }

    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    public Integer getEndDate() {
        return endDate;
    }

    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAmendingUserID() {
        return amendingUserID;
    }

    public void setAmendingUserID(String amendingUserID) {
        this.amendingUserID = amendingUserID;
    }

}

package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/11/19.
 */

public class MappedMessage {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("threadID")
    @Expose
    private String threadID;
    @SerializedName("fromWebID")
    @Expose
    private String fromWebID;
    @SerializedName("toWebID")
    @Expose
    private String toWebID;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("isDeleted")
    @Expose
    private Integer isDeleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThreadID() {
        return threadID;
    }

    public void setThreadID(String threadID) {
        this.threadID = threadID;
    }

    public String getFromWebID() {
        return fromWebID;
    }

    public void setFromWebID(String fromWebID) {
        this.fromWebID = fromWebID;
    }

    public String getToWebID() {
        return toWebID;
    }

    public void setToWebID(String toWebID) {
        this.toWebID = toWebID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

}

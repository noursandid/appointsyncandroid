package com.appointsync.appointsyncpro.Class.Main;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cgsawma on 1/19/19.
 */

public class PasswordConvertClass {

    public static String convertedPassword(String Password){
        String oldInputPasswordStringEncrypt = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] data = md.digest(Password.getBytes());
            StringBuilder encryptedPassword = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                encryptedPassword.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
            }
            oldInputPasswordStringEncrypt = encryptedPassword.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
        return oldInputPasswordStringEncrypt;
    }
}
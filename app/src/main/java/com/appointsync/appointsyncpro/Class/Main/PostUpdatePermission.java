package com.appointsync.appointsyncpro.Class.Main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cgsawma on 1/19/19.
 */

public class PostUpdatePermission {

    @SerializedName("permissionValue")
    @Expose
    private String permissionValue;
    @SerializedName("permission")
    @Expose
    private String permission;


    public PostUpdatePermission(String permissionValue, String permission) {
        this.permissionValue = permissionValue;
        this.permission = permission;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}

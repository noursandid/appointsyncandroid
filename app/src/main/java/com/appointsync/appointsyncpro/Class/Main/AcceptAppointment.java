package com.appointsync.appointsyncpro.Class.Main;

/**
 * Created by cgsawma on 1/8/19.
 */

public class AcceptAppointment {

    String appointmentWebID;

    public AcceptAppointment(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }

    public String getAppointmentWebID() {
        return appointmentWebID;
    }

    public void setAppointmentWebID(String appointmentWebID) {
        this.appointmentWebID = appointmentWebID;
    }
}

package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentAttachments;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentNotes;
import com.appointsync.appointsyncpro.Class.Main.MappedMessage;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by cgsawma on 2/11/19.
 */

public class AppointmentNotesAdapter extends RecyclerView.Adapter<AppointmentNotesAdapter.ViewHolder>{

private List<MappedAppointmentNotes> mappedAppointmentNotesList;
private List<MappedAppointmentAttachments> mappedAppointmentAttachmentsList;
private Context context;


public AppointmentNotesAdapter(List<MappedAppointmentNotes> mappedAppointmentNotesList,List<MappedAppointmentAttachments> mappedAppointmentAttachmentsList, Context context) {

        // generate constructors to initialise the List and Context objects

        this.mappedAppointmentNotesList = mappedAppointmentNotesList;
        this.mappedAppointmentAttachmentsList = mappedAppointmentAttachmentsList;
        this.context = context;

        }

@Override
public AppointmentNotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.appointment_note_item, parent, false);


        return new AppointmentNotesAdapter.ViewHolder(v);
        }

@Override
public void onBindViewHolder(final AppointmentNotesAdapter.ViewHolder holder, final int position) {

    holder.setIsRecyclable(false);

    if(position>=mappedAppointmentNotesList.size()) {

        holder.appointment_note_item_other.setVisibility(View.GONE);
        holder.appointment_note_item_other_date.setVisibility(View.GONE);
        holder.appointment_note_item_self.setVisibility(View.GONE);
        holder.appointment_note_item_self_date.setVisibility(View.GONE);
        holder.appointment_attachment_image_view.setVisibility(View.GONE);
        holder.appointment_attachment_pdfView.setVisibility(View.GONE);

        System.out.println("THIS IS SPARTA ");
        System.out.println("THIS IS SPARTA ATTACHMENTS "+mappedAppointmentAttachmentsList.size());
        System.out.println("THIS IS SPARTA NOTES "+mappedAppointmentNotesList.size());
        final MappedAppointmentAttachments mappedAppointmentAttachments = mappedAppointmentAttachmentsList.get(position - mappedAppointmentNotesList.size());
        System.out.println("THIS IS SPARTA Before the last one");
        String fileFromat = mappedAppointmentAttachments.getAttachment().substring(mappedAppointmentAttachments.getAttachment().lastIndexOf('.') + 1).trim();
        System.out.println("FILE FUCKING FORMAT " + fileFromat);
        if (fileFromat.equals("jpg") || fileFromat.equals("png") || fileFromat.equals("gif") || fileFromat.equals("jpeg")) {
            holder.appointment_attachment_image_view.setVisibility(View.VISIBLE);
            holder.appointment_attachment_pdfView.setVisibility(View.GONE);
            Picasso.get()
                    .load(mappedAppointmentAttachments.getAttachment())
                    .fit()
                    .centerCrop()
                    .into(holder.appointment_attachment_image_view);
        }else if (fileFromat.equals("pdf")) {

            holder.appointment_attachment_pdfView.setVisibility(View.VISIBLE);
            holder.appointment_attachment_image_view.setVisibility(View.GONE);

            //Get file name
            String[] splitURL = mappedAppointmentAttachments.getAttachment().split("/");
            String fileName = splitURL[splitURL.length - 1];
            File pdfFile = new File(Environment.getExternalStorageDirectory().getPath(), fileName);

            try {
                System.out.println("Before pdfFile.createNewFile();");
                pdfFile.createNewFile();
                System.out.println("After pdfFile.createNewFile();");
                new RetrieveFeedTask(pdfFile, holder.appointment_attachment_pdfView).execute(mappedAppointmentAttachments.getAttachment());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    else{
        final MappedAppointmentNotes mappedAppointmentNotes = mappedAppointmentNotesList.get(position);
        System.out.println("THIS IS SPARTA NOTES: "+mappedAppointmentNotesList.size());
        System.out.println("THIS IS SPARTA ATTACHMENTS OUT OF IF "+mappedAppointmentAttachmentsList.size());

        holder.appointment_note_item_other.setVisibility(View.GONE);
        holder.appointment_note_item_other_date.setVisibility(View.GONE);
        holder.appointment_note_item_self.setVisibility(View.GONE);
        holder.appointment_note_item_self_date.setVisibility(View.GONE);
        holder.appointment_attachment_image_view.setVisibility(View.GONE);
        holder.appointment_attachment_pdfView.setVisibility(View.GONE);

        String lastMessage = mappedAppointmentNotes.getNote();

        lastMessage = Html.fromHtml(lastMessage).toString();
        lastMessage = Html.fromHtml(lastMessage).toString();


        if(mappedAppointmentNotes.getNoteUserID().equals(Controller.user.getId())){
            holder.appointment_note_item_self.setBackground(ContextCompat.getDrawable(context, R.drawable.background_blue));
            holder.appointment_note_item_self.setVisibility(View.VISIBLE);
            holder.appointment_note_item_self_date.setVisibility(View.VISIBLE);
            holder.appointment_note_item_self.setText(lastMessage);
            holder.appointment_note_item_self_date.setText(UnixDateConverter.UnixDateConverterAppointmentTime(mappedAppointmentNotes.getDate()));
        }else{
            holder.appointment_note_item_other.setBackground(ContextCompat.getDrawable(context, R.drawable.background_grey));
            holder.appointment_note_item_other.setVisibility(View.VISIBLE);
            holder.appointment_note_item_other_date.setVisibility(View.VISIBLE);
            holder.appointment_note_item_other.setText(lastMessage);
            holder.appointment_note_item_other_date.setText(UnixDateConverter.UnixDateConverterAppointmentTime(mappedAppointmentNotes.getDate()));
        }

    }


//    } else if (fileFromat.equals("pdf")) {
//
//        holder.clientFileAttachmentImageView.setVisibility(View.GONE);
//        holder.clientFileAttachmentPdfView.setVisibility(View.VISIBLE);
//
//        //Get file name
//        String[] splitURL = mappedFileAttachment.getAttachment().split("/");
//        String fileName = splitURL[splitURL.length - 1];
//        File pdfFile = new File(Environment.getExternalStorageDirectory().getPath(), fileName);
//
//        try {
//            pdfFile.createNewFile();
//            new RetrieveFeedTask(pdfFile, holder.clientFileAttachmentPdfView).execute(mappedFileAttachment.getAttachment());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

//                MappedMessage mappedThread1 = messageList.get(position);
//
//                Bundle bundle = new Bundle();
//                MessageView messageView = new MessageView();
//                messageView.setArguments(bundle);
//
//                delegateAppointmentView.seeThreadMessage(messageView);

        }
        });

        }

@Override

//return the size of the listItems (appointmentListDev)

public int getItemCount() {
        return mappedAppointmentNotesList.size()+mappedAppointmentAttachmentsList.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {

    // define the View objects

    public TextView appointment_note_item_self,appointment_note_item_self_date, appointment_note_item_other, appointment_note_item_other_date;
    public ConstraintLayout linearLayout;
    public ImageView appointment_attachment_image_view;
    public PDFView appointment_attachment_pdfView;

    public ViewHolder(View itemView) {
        super(itemView);

        appointment_note_item_self = itemView.findViewById(R.id.appointment_note_item_self);
        appointment_note_item_self_date = itemView.findViewById(R.id.appointment_note_item_self_date);
        appointment_note_item_other = itemView.findViewById(R.id.appointment_note_item_other);
        appointment_note_item_other_date = itemView.findViewById(R.id.appointment_note_item_other_date);
        appointment_attachment_image_view = itemView.findViewById(R.id.appointment_attachment_image_view);
        appointment_attachment_pdfView = itemView.findViewById(R.id.appointment_attachment_pdfView);


        linearLayout = itemView.findViewById(R.id.appointment_note_item_view_layout);
    }

}

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, PDFView> {

        private Exception exception;
        public File outputFile;
        public PDFView clientFileAttachmentPdfView;

        public RetrieveFeedTask(File outputFile, PDFView pdfView) {
            System.out.println("IF IM THE FUCKING RetrieveFeedTask");
            this.outputFile = outputFile;
            this.clientFileAttachmentPdfView = pdfView;
        }

        protected PDFView doInBackground(String... urls) {
            try {
                System.out.println("IF IM THE FUCKING DoInBackground");
                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(urls[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return null;
                    }
                    int fileLength = connection.getContentLength();
                    input = connection.getInputStream();
                    output = new FileOutputStream(outputFile);

                    byte data[] = new byte[fileLength];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        output.write(data, 0, count);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        protected void onPostExecute(PDFView feed) {
            System.out.println("FEED " + outputFile);
            clientFileAttachmentPdfView.fromFile(outputFile)
                    .enableSwipe(false) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(false)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .load();


        }
    }

}

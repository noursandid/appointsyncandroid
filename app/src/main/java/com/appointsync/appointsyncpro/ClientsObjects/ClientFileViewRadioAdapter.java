package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.MappedElement;
import com.appointsync.appointsyncpro.Class.Main.MappedElementKeyValue;
import com.appointsync.appointsyncpro.R;

import java.util.List;

/**
 * Created by cgsawma on 1/31/19.
 */

public class ClientFileViewRadioAdapter extends RecyclerView.Adapter<ClientFileViewRadioAdapter.ViewHolder> {


    //    public static final String WEB_ID = "webID";
//    public static final String CLIENT_OBJECT = "clientObject";

    // we define a list from the DevelopersList java class

    private List<MappedElement> mappedFiles;
    private Context context;
    //    public ButtonSelectedProtocol delegateZoubi;
    private ClientFileViewAdapter.ClientFileViewAdapterListener listener;
    int i = 0;

    public ClientFileViewRadioAdapter(List<MappedElement> mappedFiles, Context context) {

        // generate constructors to initialise the List and Context objects

        this.mappedFiles = mappedFiles;
        this.context = context;
//        this.delegateZoubi = delegateZoubi;

    }

    @Override
    public ClientFileViewRadioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_file_radio_button_view, parent, false);


        return new ClientFileViewRadioAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ClientFileViewRadioAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedElement developersList = mappedFiles.get(position);

//        if (developersList.getElements().get(position).getTypeWebID().equals("hBsHxxMsun0GbtZe08083a192e7")) {



//            holder.clientFileInputTextField.setHint(developersList.getElements().get(position).getDataKeyValueList().get(position).getKey());
//            holder.clientFileInputTextFieldText.setText(developersList.getElements().get(position).getDataKeyValueList().get(position).getValue());
//            System.out.println("This is sparta: "+developersList.getElements().get(position).getDataKeyValueList().get(position).getValue());

        holder.clientFileRadioButtonTitle.setText(developersList.getName());
        holder.clientFileRadioButtonOne.setText(developersList.getDataKeyValueList().get(0).getKey());
        holder.clientFileRadioButtonTwo.setText(developersList.getDataKeyValueList().get(1).getKey());
//        System.out.println("This is sparta: "+developersList.getValue());
//        }

//
//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MappedClient mappedClient1 = clientList.get(position);
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(CLIENT_OBJECT, mappedClient1);
//                ClientProfile clientProfile = new ClientProfile();
//                clientProfile.setArguments(bundle);
//
//                delegateZoubi.seeClientProfile(clientProfile);
//
//
//            }
//        });

    }

    @Override

    //return the size of the listItems (developersList)

    public int getItemCount() {
        return mappedFiles.size();
    }

    public interface ClientFileViewAdapterListener {
        void onSelected(String item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public CheckBox clientFileRadioButtonOne,clientFileRadioButtonTwo;
        public TextView clientFileRadioButtonTitle;
        public ConstraintLayout clientFileRadioButtonLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects

            clientFileRadioButtonTitle = itemView.findViewById(R.id.clientFileRadioButtonTitle);
            clientFileRadioButtonOne = itemView.findViewById(R.id.clientFileRadioButtonOne);
            clientFileRadioButtonTwo = itemView.findViewById(R.id.clientFileRadioButtonTwo);

            clientFileRadioButtonLayout = itemView.findViewById(R.id.clientFileRadioButtonLayout);
        }

    }
}

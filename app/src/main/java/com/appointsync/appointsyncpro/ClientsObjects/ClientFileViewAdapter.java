package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.content.SyncStats;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetFilesPages;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedElement;
import com.appointsync.appointsyncpro.Class.Main.MappedElementKeyValue;
import com.appointsync.appointsyncpro.Class.Main.MappedFile;
import com.appointsync.appointsyncpro.Class.Main.PostClientFileUpdate;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.ClientProfile;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.R;
import com.google.android.gms.common.api.Api;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cgsawma on 1/30/19.
 */

public class ClientFileViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    //    public static final String WEB_ID = "webID";
//    public static final String CLIENT_OBJECT = "clientObject";

    // we define a list from the DevelopersList java class

    private MappedFile mappedFile;
    private Context context;
    //    public ButtonSelectedProtocol delegateZoubi;
    private ClientFileViewAdapterListener listener;
    int i = 0;
    CountDownTimer cTimer = null;
    MappedClient client;
    HashMap<MappedElementKeyValue, MappedElement> changes = new HashMap<>();
    boolean isChanged = false;
    private long mLastClickTime = 0;
    private final int TWO_SECONDS = 2000;
    Handler handler = new Handler();
    boolean stopScheduler = false;

    public ClientFileViewAdapter(MappedFile mappedFile, MappedClient client, Context context) {
        this.client = client;
        this.mappedFile = mappedFile;
        this.context = context;

        scheduleSubmitChanges();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created

        switch (viewType) {
            case 0:
                View generalStateView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.client_file_input_text_view, parent, false);
                return new GeneralStateViewHolder(generalStateView);
            case 1:
                View radioButtonView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.client_file_radio_button_view, parent, false);
                return new RadioButtonViewHolder(radioButtonView);
            case 2:
                View teethImageView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.client_file_teath_view, parent, false);
                return new TeethImageViewHolder(teethImageView);
            default:
                View generalStateView1 = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.client_file_input_text_view, parent, false);
                return new GeneralStateViewHolder(generalStateView1);
        }

    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous

        switch (mappedFile.getElements().get(position).getTypeWebID()) {
            case "hBsHxxMsun0GbtZe08083a192e7":
                return 0;
            case "q93ieXxnyI5e92E5dadf9bry5ie":
                return 1;
            case "Kwudnc5Q0h8RkRd550fd61eb695":
                return 2;
            default:
                return 0;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedElement element = mappedFile.getElements().get(position);


        switch (element.getTypeWebID()) {
            case "hBsHxxMsun0GbtZe08083a192e7":
                final GeneralStateViewHolder generalStateViewHolder = (GeneralStateViewHolder) holder;
                generalStateViewHolder.generalStateTitle.setText(element.getName());
                generalStateViewHolder.medicationsTextField.setHint(element.getDataKeyValueList().get(0).getKey());
                generalStateViewHolder.medicationsTextFieldText.setText(element.getDataKeyValueList().get(0).getValue());
                generalStateViewHolder.generalTroubleTextField.setHint(element.getDataKeyValueList().get(1).getKey());
                generalStateViewHolder.generalTroubleTextFieldText.setText(element.getDataKeyValueList().get(1).getValue());
                generalStateViewHolder.allergiesTextField.setHint(element.getDataKeyValueList().get(2).getKey());
                generalStateViewHolder.allergiesTextFieldText.setText(element.getDataKeyValueList().get(2).getValue());
                generalStateViewHolder.otherTextField.setHint(element.getDataKeyValueList().get(3).getKey());
                generalStateViewHolder.otherTextFieldText.setText(element.getDataKeyValueList().get(3).getValue());

                generalStateViewHolder.medicationsTextFieldText.addTextChangedListener(
                        new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            private Timer timer = new Timer();
                            private final long DELAY = 1000; // milliseconds

                            @Override
                            public void afterTextChanged(final Editable s) {
                                timer.cancel();
                                timer = new Timer();
                                timer.schedule(
                                        new TimerTask() {
                                            @Override
                                            public void run() {

                                                element.getDataKeyValueList().get(0).setValue(generalStateViewHolder.medicationsTextFieldText.getText().toString());
                                                changes.put(element.getDataKeyValueList().get(0), element);
                                                isChanged = true;

                                            }
                                        },
                                        DELAY
                                );
                            }
                        }
                );

                generalStateViewHolder.generalTroubleTextFieldText.addTextChangedListener(
                        new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            private Timer timer = new Timer();
                            private final long DELAY = 1000; // milliseconds

                            @Override
                            public void afterTextChanged(final Editable s) {
                                timer.cancel();
                                timer = new Timer();
                                timer.schedule(
                                        new TimerTask() {
                                            @Override
                                            public void run() {

                                                element.getDataKeyValueList().get(1).setValue(generalStateViewHolder.generalTroubleTextFieldText.getText().toString());
                                                changes.put(element.getDataKeyValueList().get(1), element);
                                                isChanged = true;

                                            }
                                        },
                                        DELAY
                                );
                            }
                        }
                );

                generalStateViewHolder.allergiesTextFieldText.addTextChangedListener(
                        new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            private Timer timer = new Timer();
                            private final long DELAY = 1000; // milliseconds

                            @Override
                            public void afterTextChanged(final Editable s) {
                                timer.cancel();
                                timer = new Timer();
                                timer.schedule(
                                        new TimerTask() {
                                            @Override
                                            public void run() {

                                                element.getDataKeyValueList().get(2).setValue(generalStateViewHolder.allergiesTextFieldText.getText().toString());
                                                changes.put(element.getDataKeyValueList().get(2), element);
                                                isChanged = true;

                                            }
                                        },
                                        DELAY
                                );
                            }
                        }
                );

                generalStateViewHolder.otherTextFieldText.addTextChangedListener(
                        new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                            }

                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }

                            private Timer timer = new Timer();
                            private final long DELAY = 1000; // milliseconds

                            @Override
                            public void afterTextChanged(final Editable s) {
                                timer.cancel();
                                timer = new Timer();
                                timer.schedule(
                                        new TimerTask() {
                                            @Override
                                            public void run() {

                                                element.getDataKeyValueList().get(3).setValue(generalStateViewHolder.otherTextFieldText.getText().toString());
                                                changes.put(element.getDataKeyValueList().get(3), element);
                                                isChanged = true;

                                            }
                                        },
                                        DELAY
                                );
                            }
                        }
                );


                break;

            case "q93ieXxnyI5e92E5dadf9bry5ie":
                final RadioButtonViewHolder radioButtonViewHolder = (RadioButtonViewHolder) holder;
                radioButtonViewHolder.clientFileRadioButtonOne.setText(element.getDataKeyValueList().get(0).getKey());
                radioButtonViewHolder.clientFileRadioButtonTwo.setText(element.getDataKeyValueList().get(1).getKey());
                radioButtonViewHolder.clientFileRadioButtonOne.setChecked(element.getDataKeyValueList().get(0).getValue().equals("1"));
                radioButtonViewHolder.clientFileRadioButtonOne.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        element.getDataKeyValueList().get(0).setValue(radioButtonViewHolder.clientFileRadioButtonOne.isChecked() ? "1" : "0");
                        sendAPI(element, element.getDataKeyValueList().get(0));
                    }
                });
                radioButtonViewHolder.clientFileRadioButtonTwo.setChecked(element.getDataKeyValueList().get(1).getValue().equals("1"));
                radioButtonViewHolder.clientFileRadioButtonTwo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        element.getDataKeyValueList().get(1).setValue(radioButtonViewHolder.clientFileRadioButtonTwo.isChecked() ? "1" : "0");
                        sendAPI(element, element.getDataKeyValueList().get(1));
                    }
                });
                radioButtonViewHolder.clientFileRadioButtonTitle.setText(element.getName());
                break;
            case "Kwudnc5Q0h8RkRd550fd61eb695":
                TeethImageViewHolder teethImageViewHolder = (TeethImageViewHolder) holder;
                teethImageViewHolder.clientFileTeethTitle.setText(element.getName());
                for (int i = 0; i < element.getDataKeyValueList().size(); i++) {
                    final int index = i;
                    String toothElemet = element.getDataKeyValueList().get(i).getKey();
                    toothElemet = "tooth" + toothElemet.replaceAll("\\D+", "");
                    int resId = context.getResources().getIdentifier(toothElemet, "id", context.getPackageName());
                    final ImageView toothImageView = holder.itemView.findViewById(resId);
                    if (element.getDataKeyValueList().get(i).getValue().equals("1")) {
                        toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFileRedTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
                    } else if (element.getDataKeyValueList().get(i).getValue().equals("2")) {
                        toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFileGreenTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
                    } else if (element.getDataKeyValueList().get(i).getValue().equals("3")) {
                        toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFilePurpleTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
                    }
                    final ImageView toothImageViewAPI = holder.itemView.findViewById(resId);
                    final MappedElement elementAPI = element;

                    toothImageView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            didClickOnTooth(elementAPI, toothImageViewAPI, index);
                        }
                    });
//                    addClickListenerForTooth(element,toothImageView,i);
                }

                break;
            default:
                GeneralStateViewHolder generalStateViewHolder1 = (GeneralStateViewHolder) holder;
                generalStateViewHolder1.generalStateTitle.setText(element.getName());
                generalStateViewHolder1.medicationsTextField.setHint(element.getDataKeyValueList().get(0).getKey());
                generalStateViewHolder1.medicationsTextFieldText.setText(element.getDataKeyValueList().get(0).getValue());
                generalStateViewHolder1.generalTroubleTextField.setHint(element.getDataKeyValueList().get(1).getKey());
                generalStateViewHolder1.generalTroubleTextFieldText.setText(element.getDataKeyValueList().get(1).getValue());
                generalStateViewHolder1.allergiesTextField.setHint(element.getDataKeyValueList().get(2).getKey());
                generalStateViewHolder1.allergiesTextFieldText.setText(element.getDataKeyValueList().get(2).getValue());
                generalStateViewHolder1.otherTextField.setHint(element.getDataKeyValueList().get(3).getKey());
                generalStateViewHolder1.otherTextFieldText.setText(element.getDataKeyValueList().get(3).getValue());
                break;
        }

//        if (element.getTypeWebID().equals("q93ieXxnyI5e92E5dadf9bry5ie")) {
//
//
//
////            holder.clientFileInputTextField.setHint(developersList.getElements().get(position).getDataKeyValueList().get(position).getKey());
////            holder.clientFileInputTextFieldText.setText(developersList.getElements().get(position).getDataKeyValueList().get(position).getValue());
////            System.out.println("This is sparta: "+developersList.getElements().get(position).getDataKeyValueList().get(position).getValue());
//
//
//
//
//
//
//
//
////            System.out.println("This is sparta: "+developersList.getValue());
//        }
//        else if (element.getTypeWebID().equals("q93ieXxnyI5e92E5dadf9bry5ie")) {
//
//
//
//        }
//
//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MappedClient mappedClient1 = clientList.get(position);
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(CLIENT_OBJECT, mappedClient1);
//                ClientProfile clientProfile = new ClientProfile();
//                clientProfile.setArguments(bundle);
//
//                delegateZoubi.seeClientProfile(clientProfile);
//
//
//            }
//        });

    }

    void addClickListenerForTooth(final MappedElement element, final ImageView toothImageView, final int index) {

    }

    void didClickOnTooth(MappedElement element, ImageView toothImageView, int index) {

        System.out.println("INDEXXX " + index);
        System.out.println("KEYWEBIDDDD " + element.getDataKeyValueList().get(index).getWebID());

        if (element.getDataKeyValueList().get(index).getValue().equals("") || element.getDataKeyValueList().get(index).getValue().equals("0")) {
            element.getDataKeyValueList().get(index).setValue("1");
        } else if (element.getDataKeyValueList().get(index).getValue().equals("1")) {
            element.getDataKeyValueList().get(index).setValue("2");
        } else if (element.getDataKeyValueList().get(index).getValue().equals("2")) {
            element.getDataKeyValueList().get(index).setValue("3");
        } else if (element.getDataKeyValueList().get(index).getValue().equals("3")) {
            element.getDataKeyValueList().get(index).setValue("2");
        }

        if (element.getDataKeyValueList().get(index).getValue().equals("1")) {
            toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFileRedTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else if (element.getDataKeyValueList().get(index).getValue().equals("2")) {
            toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFileGreenTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
        } else if (element.getDataKeyValueList().get(index).getValue().equals("3")) {
            toothImageView.setColorFilter(ContextCompat.getColor(context, R.color.clientFilePurpleTeeth), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

//        element.getDataKeyValueList().get(index).setValue(element.getDataKeyValueList().get(index).getValue());
        sendAPI(element, element.getDataKeyValueList().get(index));
    }

    @Override

    //return the size of the listItems (developersList)

    public int getItemCount() {
        return mappedFile.getElements().size();
    }

    public interface ClientFileViewAdapterListener {
        void onSelected(String item);
    }

    public class GeneralStateViewHolder extends RecyclerView.ViewHolder {

        // define the View objects
        public TextView generalStateTitle;
        public TextInputEditText medicationsTextFieldText, generalTroubleTextFieldText, allergiesTextFieldText, otherTextFieldText;
        public TextInputLayout medicationsTextField, generalTroubleTextField, allergiesTextField, otherTextField;

        public ConstraintLayout clientFileInputTextLayout;

        public GeneralStateViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects

            generalStateTitle = itemView.findViewById(R.id.generalStateTitle);
            medicationsTextFieldText = itemView.findViewById(R.id.medicationsTextFieldText);
            medicationsTextField = itemView.findViewById(R.id.medicationsTextField);
            generalTroubleTextFieldText = itemView.findViewById(R.id.generalTroubleTextFieldText);
            generalTroubleTextField = itemView.findViewById(R.id.generalTroubleTextField);
            allergiesTextFieldText = itemView.findViewById(R.id.allergiesTextFieldText);
            allergiesTextField = itemView.findViewById(R.id.allergiesTextField);
            otherTextFieldText = itemView.findViewById(R.id.otherTextFieldText);
            otherTextField = itemView.findViewById(R.id.otherTextField);

            clientFileInputTextLayout = itemView.findViewById(R.id.clientFileInputTextLayout);
        }
    }

    public class RadioButtonViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public CheckBox clientFileRadioButtonOne, clientFileRadioButtonTwo;
        public TextView clientFileRadioButtonTitle;
        public ConstraintLayout clientFileRadioButtonLayout;

        public RadioButtonViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects

            clientFileRadioButtonTitle = itemView.findViewById(R.id.clientFileRadioButtonTitle);
            clientFileRadioButtonOne = itemView.findViewById(R.id.clientFileRadioButtonOne);
            clientFileRadioButtonTwo = itemView.findViewById(R.id.clientFileRadioButtonTwo);

            clientFileRadioButtonLayout = itemView.findViewById(R.id.clientFileRadioButtonLayout);
        }

    }

    public class TeethImageViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView clientFileTeethTitle;
        public ImageView teeth1, teeth2, teeth3, teeth4, teeth5, teeth6, teeth7, teeth8, teeth9, teeth10, teeth11, teeth12, teeth13, teeth14, teeth15, teeth16, teeth17,
                teeth18, teeth19, teeth20, teeth21, teeth22, teeth23, teeth24, teeth25, teeth26, teeth27, teeth28, teeth29, teeth30, teeth31, teeth32;
        public ConstraintLayout clientFileTeethLayout;

        public TeethImageViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects


            clientFileTeethTitle = itemView.findViewById(R.id.clientFileTeethTitle);
            clientFileTeethLayout = itemView.findViewById(R.id.clientFileTeethLayout);

            teeth1 = itemView.findViewById(R.id.tooth1);
            teeth2 = itemView.findViewById(R.id.tooth2);
            teeth3 = itemView.findViewById(R.id.tooth3);
            teeth4 = itemView.findViewById(R.id.tooth4);
            teeth5 = itemView.findViewById(R.id.tooth5);
            teeth6 = itemView.findViewById(R.id.tooth6);
            teeth7 = itemView.findViewById(R.id.tooth7);
            teeth8 = itemView.findViewById(R.id.tooth8);
            teeth9 = itemView.findViewById(R.id.tooth9);
            teeth10 = itemView.findViewById(R.id.tooth10);
            teeth11 = itemView.findViewById(R.id.tooth11);
            teeth12 = itemView.findViewById(R.id.tooth12);
            teeth13 = itemView.findViewById(R.id.tooth13);
            teeth14 = itemView.findViewById(R.id.tooth14);
            teeth15 = itemView.findViewById(R.id.tooth15);
            teeth16 = itemView.findViewById(R.id.tooth16);
            teeth17 = itemView.findViewById(R.id.tooth17);
            teeth18 = itemView.findViewById(R.id.tooth18);
            teeth19 = itemView.findViewById(R.id.tooth19);
            teeth20 = itemView.findViewById(R.id.tooth20);
            teeth21 = itemView.findViewById(R.id.tooth21);
            teeth22 = itemView.findViewById(R.id.tooth22);
            teeth23 = itemView.findViewById(R.id.tooth23);
            teeth24 = itemView.findViewById(R.id.tooth24);
            teeth25 = itemView.findViewById(R.id.tooth25);
            teeth26 = itemView.findViewById(R.id.tooth26);
            teeth27 = itemView.findViewById(R.id.tooth27);
            teeth28 = itemView.findViewById(R.id.tooth28);
            teeth29 = itemView.findViewById(R.id.tooth29);
            teeth30 = itemView.findViewById(R.id.tooth30);
            teeth31 = itemView.findViewById(R.id.tooth31);
            teeth32 = itemView.findViewById(R.id.tooth32);


        }

    }

    private void submitChanges() {
//        if (isChanged){
        for (MappedElementKeyValue elementKeyValue : changes.keySet()) {

            MappedElement element = changes.get(elementKeyValue);

            changes.remove(elementKeyValue);
            isChanged = false;


            sendAPI(element, elementKeyValue);

//            System.out.println("CLIENT WEB ID: " + clientWebID + " ELEMENT WEB ID: " + elementWebID + " KEY WEB ID: " + keyWebID + " FILE WEB ID: " + pageWebID + " VALUE: " + value);

            //API HERE


        }
//        }
    }

    private void sendAPI(MappedElement element, MappedElementKeyValue elementKeyValue) {
        String clientWebID = this.client.getWebID();
        String pageWebID = mappedFile.getWebID();
        String keyWebID = elementKeyValue.getWebID();
        String value = elementKeyValue.getValue();
        String elementWebID = element.getWebID();

        Call<AppointSyncResponse<Success>> call = RetrofitInstance.getRetrofitInstance(new PostClientFileUpdate(clientWebID, pageWebID, elementWebID, keyWebID, value));
        call.enqueue(new Callback<AppointSyncResponse<Success>>() {
            @Override
            public void onResponse(Call<AppointSyncResponse<Success>> call, Response<AppointSyncResponse<Success>> response) {

            }

            @Override
            public void onFailure
                    (Call<AppointSyncResponse<Success>> call, Throwable t) {

            }
        });

    }


    public void stopScheduler() {
        stopScheduler = true;
        handler = null;
    }

    private void scheduleSubmitChanges() {
        handler.postDelayed(new Runnable() {
            public void run() {
                submitChanges();
                if (!stopScheduler) {
                    if (handler != null) {
                        handler.postDelayed(this, TWO_SECONDS);
                    }
                }
            }
        }, TWO_SECONDS);
    }
}

package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by cgsawma on 2/21/19.
 */

public class ClickThroughRecyclerView extends RecyclerView {


    public ClickThroughRecyclerView(Context context) {
        super(context);
    }

    public ClickThroughRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickThroughRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && this.getScrollState() == RecyclerView.SCROLL_STATE_SETTLING) {
            this.stopScroll();
        }
        return super.onInterceptTouchEvent(event);
    }
}

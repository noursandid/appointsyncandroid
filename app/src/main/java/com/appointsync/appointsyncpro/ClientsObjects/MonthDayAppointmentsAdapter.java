package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.R;

import org.joda.time.DateTimeComparator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by cgsawma on 2/3/19.
 */

public class MonthDayAppointmentsAdapter extends RecyclerView.Adapter<MonthDayAppointmentsAdapter.ViewHolder> {

    private LayoutInflater mInflater;
//    private MonthDayAppointmentsAdapter.ItemClickListener mClickListener;
    private ArrayList<MappedAppointment> appointments;
    // data is passed into the constructor
    public MonthDayAppointmentsAdapter(Context context, ArrayList<MappedAppointment> appointments) {
        this.appointments = appointments;
        this.mInflater = LayoutInflater.from(context);
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public MonthDayAppointmentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.month_day_appointment_view, parent, false);
        return new MonthDayAppointmentsAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull MonthDayAppointmentsAdapter.ViewHolder holder, int position) {
        holder.appointmentTextView.setText(appointments.get(position).getClient().getIdentifier());
        System.out.println("HHHHHHAAAAA" + position);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        System.out.println("THIS IS THE APPOINTMENT SIZE " + appointments.size());
        if (appointments.size() > 5){
            return 5;
        }
        else{
            return appointments.size();
        }
    }
    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView appointmentTextView;

        ViewHolder(View itemView) {
            super(itemView);
            appointmentTextView = itemView.findViewById(R.id.monthdayAppointmentTextView);

//            itemView.setOnClickListener(this);
        }
//        @Override
//        public void onClick(View view) {
//            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
//        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return Controller.appointmentList.get(id).getWebID();
    }

    // allows clicks events to be caught
//    public void setClickListener(MonthDayAppointmentsAdapter.ItemClickListener itemClickListener) {
//        this.mClickListener = itemClickListener;
//    }

    // parent activity will implement this method to respond to click events
//    public interface ItemClickListener {
//        void onItemClick(View view, int position);
//    }
}
package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedMessage;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;

import java.util.List;

/**
 * Created by cgsawma on 1/11/19.
 */

public class MessageViewAdapter extends RecyclerView.Adapter<MessageViewAdapter.ViewHolder>{


    private List<MappedMessage> messageList;
    private Context context;


    public MessageViewAdapter(List<MappedMessage> messageList, Context context) {

        // generate constructors to initialise the List and Context objects

        this.messageList = messageList;
        this.context = context;

    }

    @Override
    public MessageViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);


        return new MessageViewAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MessageViewAdapter.ViewHolder holder, final int position) {


        final MappedMessage mappedMessage = messageList.get(position);

        holder.message_item_other.setVisibility(View.GONE);
        holder.message_item_other_date.setVisibility(View.GONE);
        holder.message_item_self.setVisibility(View.GONE);
        holder.message_item_self_date.setVisibility(View.GONE);

        String lastMessage = mappedMessage.getMessage();
        lastMessage = Html.fromHtml(lastMessage).toString();
        lastMessage = Html.fromHtml(lastMessage).toString();


        if(mappedMessage.getFromWebID().equals(Controller.user.getWebID())){
            holder.message_item_self.setBackground(ContextCompat.getDrawable(context, R.drawable.background_blue));
            holder.message_item_self.setVisibility(View.VISIBLE);
            holder.message_item_self_date.setVisibility(View.VISIBLE);
            holder.message_item_self.setText(lastMessage);
            holder.message_item_self_date.setText(UnixDateConverter.UnixDateConverterAppointmentTime(mappedMessage.getDate()));
        }else{
            holder.message_item_other.setBackground(ContextCompat.getDrawable(context, R.drawable.background_grey));
            holder.message_item_other.setVisibility(View.VISIBLE);
            holder.message_item_other_date.setVisibility(View.VISIBLE);
            holder.message_item_other.setText(lastMessage);
            holder.message_item_other_date.setText(UnixDateConverter.UnixDateConverterAppointmentTime(mappedMessage.getDate()));
        }




        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                MappedMessage mappedThread1 = messageList.get(position);
//
//                Bundle bundle = new Bundle();
//                MessageView messageView = new MessageView();
//                messageView.setArguments(bundle);
//
//                delegateAppointmentView.seeThreadMessage(messageView);

            }
        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return messageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView message_item_self,message_item_self_date, message_item_other, message_item_other_date;
        public ConstraintLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            message_item_self = itemView.findViewById(R.id.message_item_self);
            message_item_self_date = itemView.findViewById(R.id.message_item_self_date);
            message_item_other = itemView.findViewById(R.id.message_item_other);
            message_item_other_date = itemView.findViewById(R.id.message_item_other_date);


            linearLayout = itemView.findViewById(R.id.message_item_view_layout);
        }

    }

}
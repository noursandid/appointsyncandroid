package com.appointsync.appointsyncpro.ClientsObjects;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.ClientProfile;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by cgsawma on 12/8/18.
 */

public class ClientRecyclerAdapter extends RecyclerView.Adapter<ClientRecyclerAdapter.ViewHolder> {


    //    public static final String WEB_ID = "webID";
    public static final String CLIENT_OBJECT = "clientObject";

    // we define a list from the DevelopersList java class

    private List<MappedClient> clientList;
    private List<MappedClient> originalClientList;
    private Context context;
    public ButtonSelectedProtocol delegateZoubi;
    private ClientRecyclerAdapterListener listener;


    public ClientRecyclerAdapter(List<MappedClient> clientList, Context context, ButtonSelectedProtocol delegateZoubi) {

        // generate constructors to initialise the List and Context objects

        this.clientList = clientList;
        this.originalClientList = clientList;
        this.context = context;
        this.delegateZoubi = delegateZoubi;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_item, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedClient developersList = clientList.get(position);

        if (developersList.getType() == 4) {
            holder.clientType.setBackgroundColor(Color.parseColor("#D94432"));
            holder.clientFullName.setVisibility(View.INVISIBLE);
            if (developersList.getMiddleName().isEmpty()) {
                holder.login.setText(developersList.getFirstName() + " " + developersList.getLastName());
            } else {
                holder.login.setText(developersList.getFirstName() + " " + developersList.getMiddleName() + " " + developersList.getLastName());
            }


        } else if (developersList.getType() == 2) {
            holder.clientType.setBackgroundColor(Color.parseColor("#5391CA"));
            holder.login.setText(developersList.getUsername());
            holder.clientFullName.setVisibility(View.VISIBLE);
            if (developersList.getMiddleName().isEmpty()) {
                holder.clientFullName.setText(developersList.getFirstName() + " " + developersList.getLastName());
            } else {
                holder.clientFullName.setText(developersList.getFirstName() + " " + developersList.getMiddleName() + " " + developersList.getLastName());
            }
        } else if (developersList.getType() == 3) {
            holder.clientType.setBackgroundColor(Color.parseColor("#5391CA"));
            holder.login.setText(developersList.getUsername());
            holder.clientFullName.setVisibility(View.VISIBLE);
            if (developersList.getMiddleName().isEmpty()) {
                holder.clientFullName.setText(developersList.getFirstName() + " " + developersList.getLastName());
            } else {
                holder.clientFullName.setText(developersList.getFirstName() + " " + developersList.getMiddleName() + " " + developersList.getLastName());
            }
        }

        String profilePicture = developersList.getProfilePic();
        if (profilePicture.isEmpty()) {
            profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
        }
        Picasso.get().load(profilePicture).fit().centerCrop().into(holder.avatar_url);


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MappedClient mappedClient1 = clientList.get(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable(CLIENT_OBJECT, mappedClient1);
                ClientProfile clientProfile = new ClientProfile();
                clientProfile.setArguments(bundle);

                delegateZoubi.seeClientProfile(clientProfile);


            }
        });

    }

    @Override

    //return the size of the listItems (developersList)

    public int getItemCount() {
        return clientList.size();
    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String query = charSequence.toString();
                List<MappedClient> filtered = new ArrayList<>();

                if (query.isEmpty()) {
                    filtered = originalClientList;
                } else {
                    for (MappedClient mappedClient : originalClientList) {
                        if (mappedClient.getFirstName().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(mappedClient);
                        } else if (mappedClient.getLastName().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(mappedClient);
                        } else if (mappedClient.getUsername().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(mappedClient);
                        } else if (mappedClient.getMiddleName().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(mappedClient);
                        }else if (mappedClient.getIdentifier().toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(mappedClient);
                        }
// else if (mappedClient.getUsername() != null) {
//                            mappedClient.getUsername().toLowerCase().contains(query.toLowerCase());
//                            filtered.add(mappedClient);
//                        }else if (mappedClient.getMiddleName() != null) {
//                            mappedClient.getMiddleName().toLowerCase().contains(query.toLowerCase());
//                            filtered.add(mappedClient);
//                        }
                    }
                }

                FilterResults results = new FilterResults();
                results.count = filtered.size();
                results.values = filtered;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                clientList = (ArrayList<MappedClient>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ClientRecyclerAdapterListener {
        void onSelected(String item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView login, clientFullName, clientType;
        public ImageView avatar_url;
        public TextView html_url;
        public ConstraintLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            // initialize the View objects

            login = itemView.findViewById(R.id.clientUserName);
            clientFullName = itemView.findViewById(R.id.clientName);
            clientType = itemView.findViewById(R.id.clientType);
            avatar_url = itemView.findViewById(R.id.clientItemImageView);

            linearLayout = itemView.findViewById(R.id.zoubiLayout);
        }

    }

}


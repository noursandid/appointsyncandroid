package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Interface.MonthDaysRecyclerAdapterDelegate;
import com.appointsync.appointsyncpro.R;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by cgsawma on 1/9/19.
 */

public class MainCalendarAdapter extends RecyclerView.Adapter<MainCalendarAdapter.ViewHolder> {

    private LayoutInflater mInflater;
//    private ItemClickListener mClickListener;
    MonthDaysRecyclerAdapter daysAdapter;
    private Context context;
    public MonthDaysRecyclerAdapterDelegate delegate;
    // data is passed into the constructor
    public MainCalendarAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.month_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        int[] yearMonth = {position/12,(position%12)+1};
        holder.configureCellWithYearMonth(context,yearMonth);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return 24500;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView monthTitleLabel;
        ViewHolder(View itemView) {
            super(itemView);

        }

        public void configureCellWithYearMonth(Context context,int[] yearMonth){
            this.monthTitleLabel = itemView.findViewById(R.id.monthTitleLabel);
            Calendar calendar = Calendar.getInstance();
            calendar.set(yearMonth[0],yearMonth[1]-1,1);
            this.monthTitleLabel.setText(calendar.getDisplayName(Calendar.MONTH,Calendar.LONG, Locale.getDefault()) + " " + yearMonth[0]);




            final RecyclerView monthDaysRecycler = itemView.findViewById(R.id.MonthDaysRecycler);

            int numberOfColumns = 7;
            monthDaysRecycler.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
            daysAdapter = new MonthDaysRecyclerAdapter(context,yearMonth);
            daysAdapter.delegate = delegate;
            monthDaysRecycler.setAdapter(daysAdapter);
        }



    }



}


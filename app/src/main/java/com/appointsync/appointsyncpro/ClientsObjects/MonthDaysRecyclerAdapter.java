package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.DateFormatters;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.ClientProfile;
import com.appointsync.appointsyncpro.Interface.MonthDaysRecyclerAdapterDelegate;
import com.appointsync.appointsyncpro.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by cgsawma on 2/3/19.
 */

public class MonthDaysRecyclerAdapter extends RecyclerView.Adapter<MonthDaysRecyclerAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private int[] yearMonth;


    private int firstDayInWeek;
    private Context context;
    public MonthDaysRecyclerAdapterDelegate delegate;
    // data is passed into the constructor
    public MonthDaysRecyclerAdapter(Context context,int[] yearMonth) {
        System.out.print("ZOUBIII");
        this.yearMonth = yearMonth;
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        Calendar c = Calendar.getInstance();
        c.set(yearMonth[0],yearMonth[1]-1,1);
        firstDayInWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public MonthDaysRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.month_days_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull MonthDaysRecyclerAdapter.ViewHolder holder,final int position) {
        if (position < this.firstDayInWeek-1){
            holder.myTextView.setText("");
            holder.configureCellWithYearMonthDay(context,null);
        }
        else{
            holder.myTextView.setText(position-this.firstDayInWeek+2+"");
            int[] yearMonthDay = {this.yearMonth[0],this.yearMonth[1],position-this.firstDayInWeek+2};
            holder.configureCellWithYearMonthDay(context,yearMonthDay);

            if (Controller.user.calendar.isToday(Controller.user.calendar.getDateFromYearMonthDay(yearMonthDay))){
                holder.yearMonthDayView.setBackgroundResource(R.drawable.background_blue);
            }


            final int selectedYear = this.yearMonth[0];
            final int selectedMonth = this.yearMonth[1];
            final int firstDayInWeek = this.firstDayInWeek;

            holder.selectedYear = this.yearMonth[0];
            holder.selectedMonth = this.yearMonth[1];
            holder.firstDayInWeek = this.firstDayInWeek;
            holder.position = position;
            holder.yearMonthDayView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int[] selectedYearMonthDay = {selectedYear,selectedMonth,position-firstDayInWeek+2};

                    Controller.selectedYearMonthDayController = String.valueOf(selectedYearMonthDay[2])+"/"+String.valueOf(selectedYearMonthDay[1])+"/"+String.valueOf(selectedYearMonthDay[0]);

                    delegate.didSelectDay(selectedYearMonthDay);
                }
            });

        }




    }

    // total number of cells
    @Override
    public int getItemCount() {
        return Controller.user.calendar.numberOfDaysInMonthWithUnusedDays(this.yearMonth);
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView myTextView;
        ConstraintLayout yearMonthDayView;
        RecyclerView appointmentsRecyclerView;
        int selectedYear = 0;
        int selectedMonth = 0;
        int firstDayInWeek = 0;
        int position = 0;
        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.monthDayTextView);
            yearMonthDayView = itemView.findViewById(R.id.yearMonthDayView);
            appointmentsRecyclerView = itemView.findViewById(R.id.appointmentsRecyclerView);




//            yearMonthDayView.setOnClickListener(this);
        }

        public void configureCellWithYearMonthDay(Context context,int[] yearMonthDay){
            if (yearMonthDay != null){
                ArrayList<MappedAppointment> appointments = Controller.user.calendar.getAppointmentsForYearMonthDay(yearMonthDay);
                final RecyclerView appointmentsRecyclerView = itemView.findViewById(R.id.appointmentsRecyclerView);
                appointmentsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                MonthDayAppointmentsAdapter appointmentsAdapter = new MonthDayAppointmentsAdapter(context,appointments);
                appointmentsRecyclerView.setAdapter(appointmentsAdapter);

                appointmentsRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        if (e.getAction() == MotionEvent.ACTION_UP){
                            int[] selectedYearMonthDay = {selectedYear,selectedMonth,position-firstDayInWeek+2};
                            Controller.selectedYearMonthDayController = String.valueOf(selectedYearMonthDay[2])+"/"+String.valueOf(selectedYearMonthDay[1])+"/"+String.valueOf(selectedYearMonthDay[0]);
                            delegate.didSelectDay(selectedYearMonthDay);
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
                        System.out.println("ZOUBBBBBBB1");

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                        System.out.println("onRequestDisallowInterceptTouchEvent");
                    }
                });

            }
            else{
                ArrayList<MappedAppointment> appointments = new ArrayList<>();
                final RecyclerView appointmentsRecyclerView = itemView.findViewById(R.id.appointmentsRecyclerView);
                appointmentsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                MonthDayAppointmentsAdapter appointmentsAdapter = new MonthDayAppointmentsAdapter(context,appointments);
                appointmentsRecyclerView.setAdapter(appointmentsAdapter);

            }

        }

        @Override
        public void onClick(View view) {
//            if (mClickListener != null) {
//                mClickListener.onItemClick(view, getAdapterPosition());
//            }
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return Controller.appointmentList.get(id).getWebID();
    }

    // allows clicks events to be caught
    public void setClickListener(MonthDaysRecyclerAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}

package com.appointsync.appointsyncpro.ClientsObjects;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedElement;
import com.appointsync.appointsyncpro.Class.Main.MappedFileAttachment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.squareup.picasso.Picasso;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by cgsawma on 2/1/19.
 */

public class ClientFileAttachmentsAdapter extends RecyclerView.Adapter<ClientFileAttachmentsAdapter.ViewHolder>{
    List<MappedFileAttachment> mappedFileAttachments;
    Context context;
    int resource;

    public ClientFileAttachmentsAdapter(List<MappedFileAttachment> mappedFileAttachments, Context context) {

        this.mappedFileAttachments = mappedFileAttachments;
        this.context = context;
        this.resource = resource;
    }


    @Override
    public ClientFileAttachmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_file_attachment_view, parent, false);


        return new ClientFileAttachmentsAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final ClientFileAttachmentsAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedFileAttachment mappedFileAttachment = mappedFileAttachments.get(position);


        System.out.println("POSITIONNN" + position);
        System.out.println("ATTACHMENT SIZE" + mappedFileAttachments.size());

        String fileFromat = mappedFileAttachment.getAttachment().substring(mappedFileAttachment.getAttachment().lastIndexOf('.') + 1).trim();
        System.out.println("FILE FUCKING FORMAT " + fileFromat);
        if (fileFromat.equals("jpg") || fileFromat.equals("png") || fileFromat.equals("gif") || fileFromat.equals("jpeg")) {
            holder.clientFileAttachmentImageView.setVisibility(View.VISIBLE);
            holder.clientFileAttachmentPdfView.setVisibility(View.GONE);
            Picasso.get()
                    .load(mappedFileAttachment.getAttachment())
                    .fit()
                    .centerCrop()
                    .into(holder.clientFileAttachmentImageView);



        } else if (fileFromat.equals("pdf")) {

            holder.clientFileAttachmentImageView.setVisibility(View.GONE);
            holder.clientFileAttachmentPdfView.setVisibility(View.VISIBLE);

            //Get file name
            String[] splitURL = mappedFileAttachment.getAttachment().split("/");
            String fileName = splitURL[splitURL.length - 1];
            File pdfFile = new File(Environment.getExternalStorageDirectory().getPath(), fileName);

            try {
                pdfFile.createNewFile();
                new RetrieveFeedTask(pdfFile, holder.clientFileAttachmentPdfView).execute(mappedFileAttachment.getAttachment());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        holder.clientFileAttachmentImageViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                MappedAppointment mappedAppointment1 = appointmentList.get(position);
//
//                Bundle bundle = new Bundle();
//                bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment1);
//                AppointmentView appointmentView = new AppointmentView();
//
//                appointmentView.setArguments(bundle);
//
//                delegateAppointmentView.seeAppointment(appointmentView);

            }
        });

    }


    @Override
    public int getItemCount() {
        return mappedFileAttachments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects
        ImageView clientFileAttachmentImageView;
        LinearLayout clientFileAttachmentImageViewLayout;
        PDFView clientFileAttachmentPdfView;

        public ViewHolder(View itemView) {
            super(itemView);

            clientFileAttachmentImageView = itemView.findViewById(R.id.clientFileAttachmentImageView);
            clientFileAttachmentPdfView = itemView.findViewById(R.id.clientFileAttachmentPdfView);
            clientFileAttachmentImageViewLayout = itemView.findViewById(R.id.clientFileAttachmentImageViewLayout);
        }

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, PDFView> {

        private Exception exception;
        public File outputFile;
        public PDFView clientFileAttachmentPdfView;

        public RetrieveFeedTask(File outputFile, PDFView pdfView) {
            this.outputFile = outputFile;
            this.clientFileAttachmentPdfView = pdfView;
        }

        protected PDFView doInBackground(String... urls) {
            try {
                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(urls[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return null;
                    }
                    int fileLength = connection.getContentLength();
                    input = connection.getInputStream();
                    output = new FileOutputStream(outputFile);

                    byte data[] = new byte[fileLength];
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        output.write(data, 0, count);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        protected void onPostExecute(PDFView feed) {
            System.out.println("FEED " + outputFile);
            clientFileAttachmentPdfView.fromFile(outputFile)
                    .enableSwipe(false) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(false)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .password(null)
                    .scrollHandle(null)
                    .load();


        }
    }

}

package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;

import java.util.List;

/**
 * Created by cgsawma on 2/12/19.
 */

public class ClientFileBalanceAdapter extends RecyclerView.Adapter<ClientFileBalanceAdapter.ViewHolder> {


    private List<MappedClientFileBalance> mappedClientFileBalanceList;
    private Context context;
    public ButtonSelectedProtocol delegateAppointmentView;
    int locationAt = 0;


    public ClientFileBalanceAdapter(List<MappedClientFileBalance> mappedClientFileBalanceList, Context context) {

        // generate constructors to initialise the List and Context objects

        this.mappedClientFileBalanceList = mappedClientFileBalanceList;
        this.context = context;

    }

    @Override
    public ClientFileBalanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_file_balance_row, parent, false);


        return new ClientFileBalanceAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ClientFileBalanceAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedClientFileBalance mappedClientFileBalance = mappedClientFileBalanceList.get(position);

        if (!mappedClientFileBalance.getDate().equals("Total")) {
            holder.textViewBalanceRowDate.setText(UnixDateConverter.UnixDateConverterDateGetBalanceDate(Integer.parseInt(mappedClientFileBalance.getDate())));
        } else {
            holder.textViewBalanceRowDate.setText(mappedClientFileBalance.getDate().toString());
        }
        if (!mappedClientFileBalance.getPaymentDue().toString().equals("0")) {
            holder.textViewBalanceRowDue.setText(mappedClientFileBalance.getPaymentDue().toString()+mappedClientFileBalance.getCurrency());
        } else {
            holder.textViewBalanceRowDue.setText("-");
        }
        if (!mappedClientFileBalance.getPaymentReceived().toString().equals("0")) {
            holder.textViewBalanceRowReceived.setText(mappedClientFileBalance.getPaymentReceived().toString()+mappedClientFileBalance.getCurrency());
        } else {
            holder.textViewBalanceRowReceived.setText("-");
        }
        if (!mappedClientFileBalance.getRemainingAmount().toString().equals("0")) {
            holder.textViewBalanceRowRemaining.setText(mappedClientFileBalance.getRemainingAmount().toString()+mappedClientFileBalance.getCurrency());
        } else {
            holder.textViewBalanceRowRemaining.setText("-");
        }
        if (!mappedClientFileBalance.getNote().toString().equals("")) {
            holder.textViewBalanceRowNote.setText(mappedClientFileBalance.getNote().toString());
        } else {
            holder.textViewBalanceRowNote.setText("-");
        }

        if (locationAt == 0) {
            locationAt++;
        } else if (locationAt == 1) {
            holder.clientFileBalanceRowConstraintLayout.setBackground(holder.clientFileBalanceBackground);
            locationAt = 0;
        }

        holder.clientFileBalanceRowConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return mappedClientFileBalanceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView textViewBalanceRowDate, textViewBalanceRowDue, textViewBalanceRowReceived, textViewBalanceRowRemaining, textViewBalanceRowNote;
        public ConstraintLayout clientFileBalanceRowConstraintLayout;
        public Drawable clientFileBalanceBackground;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewBalanceRowDate = itemView.findViewById(R.id.textViewBalanceRowDate);
            textViewBalanceRowDue = itemView.findViewById(R.id.textViewBalanceRowDue);
            textViewBalanceRowReceived = itemView.findViewById(R.id.textViewBalanceRowReceived);
            textViewBalanceRowRemaining = itemView.findViewById(R.id.textViewBalanceRowRemaining);
            textViewBalanceRowNote = itemView.findViewById(R.id.textViewBalanceRowNote);

            clientFileBalanceBackground = context.getResources().getDrawable(R.drawable.background_light_grey);

            clientFileBalanceRowConstraintLayout = itemView.findViewById(R.id.clientFileBalanceRowConstraintLayout);
        }

    }


}
package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetThread;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.ClientProfile;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.MessageView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;
import com.appointsync.appointsyncpro.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cgsawma on 1/8/19.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {


    //    public static final String WEB_ID = "webID";
    public static final String NOTIFICATION_OBJECT = "notificationObject";


    // we define a list from the DevelopersList java class

    private List<MappedNotification> notificationListNew;
    private List<MappedNotification> notificationListToday;
    private List<MappedNotification> notificationListThisMonth;
    private List<MappedNotification> notificationList;
    private List<MappedNotification> notificationListOlder;
    private long mLastClickTime = 0;
    private List<String> titlesList;
    public static final String THREAD_OBJECT = "threadObject";
    private Context context;
    public ButtonSelectedProtocol delegateNotificationView;
    private Boolean isNew = false, thisMonth = false, isOlder = false;
    MappedNotification mappedNotification1;


    public NotificationAdapter(List<MappedNotification> notificationList, List<MappedNotification> notificationListNew, List<MappedNotification> notificationListToday,
                               List<MappedNotification> notificationListThisMonth, List<MappedNotification> notificationListOlder, Context context, ButtonSelectedProtocol delegateNotificationView) {

        // generate constructors to initialise the List and Context objects

        this.notificationListNew = notificationListNew;
        this.notificationListToday = notificationListToday;
        this.notificationListThisMonth = notificationListThisMonth;
        this.notificationListOlder = notificationListOlder;
        this.notificationList = notificationList;
        System.out.println("bas ta na3rif enno hay hiyye " + notificationListThisMonth.size());
        this.context = context;
        this.delegateNotificationView = delegateNotificationView;

    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_view, parent, false);


        return new NotificationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        MappedNotification mappedNotification = null;

        System.out.println("POSITION " + position);
        if (position < notificationListNew.size()) {
            mappedNotification = notificationListNew.get(position);
            if (position == 0) {
                holder.notification_bar.setVisibility(View.VISIBLE);
                holder.notification_bar.setText("       New");
            } else {
                holder.notification_bar.setVisibility(View.GONE);
            }
        } else if (position >= notificationListNew.size() && position < (notificationListNew.size() + notificationListToday.size())) {
            mappedNotification = notificationListToday.get(position - notificationListNew.size());
            if (position == notificationListNew.size()) {
                holder.notification_bar.setVisibility(View.VISIBLE);
                holder.notification_bar.setText("       Today");
            } else {
                holder.notification_bar.setVisibility(View.GONE);
            }
        } else if (position >= (notificationListNew.size() + notificationListToday.size()) && position < (notificationListNew.size() + notificationListToday.size() + notificationListThisMonth.size())) {
            mappedNotification = notificationListThisMonth.get(position - notificationListNew.size() - notificationListToday.size());
            if (position == notificationListNew.size() + notificationListToday.size()) {
                holder.notification_bar.setVisibility(View.VISIBLE);
                holder.notification_bar.setText("       This Month");
            } else {
                holder.notification_bar.setVisibility(View.GONE);
            }
        } else if (position >= (notificationListNew.size() + notificationListToday.size() + notificationListThisMonth.size()) && position < (notificationListNew.size() + notificationListToday.size() + notificationListThisMonth.size() + notificationListOlder.size())) {
            mappedNotification = notificationListOlder.get(position - notificationListNew.size() - notificationListToday.size() - notificationListThisMonth.size());
            if (position == notificationListNew.size() + notificationListToday.size() + notificationListThisMonth.size()) {
                holder.notification_bar.setVisibility(View.VISIBLE);
                holder.notification_bar.setText("       Older");
            } else {
                holder.notification_bar.setVisibility(View.GONE);
            }
        }


        System.out.println("Notification at " + position + " is " + mappedNotification);

        String profilePicture = mappedNotification.getProfilePic();
        if (profilePicture.isEmpty()) {
            profilePicture = "https://appointsync.com/dev/webroot/userProfile/default.jpeg";
        }
        Picasso.get().load(profilePicture).into(holder.clientPicture);

        if (mappedNotification.getPost() != null) {
            String postDate = UnixDateConverter.UnixDateConverterAppointmentTime((mappedNotification.getPost()));

            holder.notification_description.setText(mappedNotification.getDescription() + " " + postDate);
        } else {
            holder.notification_description.setText(mappedNotification.getDescription());
        }

        String notificationDate = UnixDateConverter.UnixDateConverterAppointmentTime(mappedNotification.getDate());
        String notificationMonth = UnixDateConverter.UnixDateConverterAppointmentTimeMonth(mappedNotification.getDate());
        holder.notification_date.setText(notificationDate);


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 3500) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                mappedNotification1 = notificationList.get(position);

                if (mappedNotification1.getType().equals(0) || mappedNotification1.getType().equals(1) || mappedNotification1.getType().equals(2) || mappedNotification1.getType().equals(3) || mappedNotification1.getType().equals(4)) {

                    List<MappedAppointment> mappedAppointments = Controller.appointmentList;

                    int mappedAppointmentCounter = mappedAppointments.size();
                    for (int i = 0; i < mappedAppointmentCounter; i++) {

                        if (mappedAppointments.get(i).getWebID().equals(mappedNotification1.getCorrespondentWebID())) {


                            MappedAppointment mappedAppointment = Controller.appointmentList.get(i);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(NOTIFICATION_OBJECT, mappedAppointment);
                            AppointmentView appointmentView = new AppointmentView();
                            appointmentView.setArguments(bundle);

                            delegateNotificationView.seeAppointmentFromNotification(appointmentView);
                        }

                    }
                }else if (mappedNotification1.getType().equals(5)) {

                    List<MappedAppointment> mappedAppointments = Controller.appointmentList;

                    int mappedAppointmentCounter = mappedAppointments.size();
                    for (int i = 0; i < mappedAppointmentCounter; i++) {

                        if (mappedAppointments.get(i).getWebID().equals(mappedNotification1.getCorrespondentWebID())) {


                            MappedAppointment mappedAppointment = Controller.appointmentList.get(i);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(NOTIFICATION_OBJECT, mappedAppointment);
                            AppointmentView appointmentView = new AppointmentView();
                            appointmentView.setArguments(bundle);

                            delegateNotificationView.openAppointmentNote(appointmentView,mappedAppointment);
                        }

                    }
                } else if (mappedNotification1.getType().equals(10)) {

                    if (Controller.clientList == null) {


                        Call<AppointSyncResponse<ArrayList<MappedClient>>> call = RetrofitInstance.getRetrofitInstance(new GetClients());
                        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedClient>>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Response<AppointSyncResponse<ArrayList<MappedClient>>> response) {
                                Controller.clientList = response.body().getData();
                                List<MappedClient> mappedClients = response.body().getData();
                                int mappedClientCounter = mappedClients.size();
                                for (int i = 0; i < mappedClientCounter; i++) {

                                    if (mappedClients.get(i).getWebID().equals(mappedNotification1.getFromWebID())) {


                                        MappedClient mappedClient = Controller.clientList.get(i);
//                                        Bundle bundle = new Bundle();
//                                        bundle.putSerializable(NOTIFICATION_OBJECT, mappedClient);
//                                        ClientProfile clientProfile = new ClientProfile();
//                                        clientProfile.setArguments(bundle);

                                        delegateNotificationView.openClientsPage(mappedClient);
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedClient>>> call, Throwable t) {
                            }
                        });
                    } else {

                        List<MappedClient> mappedClients = Controller.clientList;
                        int mappedClientCounter = mappedClients.size();
                        for (int i = 0; i < mappedClientCounter; i++) {

                            if (mappedClients.get(i).getWebID().equals(mappedNotification1.getFromWebID())) {


                                MappedClient mappedClient = Controller.clientList.get(i);
//                                Bundle bundle = new Bundle();
//                                bundle.putSerializable(NOTIFICATION_OBJECT, mappedClient);
//                                ClientProfile clientProfile = new ClientProfile();
//                                clientProfile.setArguments(bundle);

                                delegateNotificationView.openClientsPage(mappedClient);
                            }

                        }

                    }

                } else if (mappedNotification1.getType().equals(20)) {

                    if (Controller.threadList != null && Controller.threadList.size() > 0) {

                        List<MappedThread> mappedThreads = Controller.threadList;

                        int mappedThreadsCounter = mappedThreads.size();
                        for (int i = 0; i < mappedThreadsCounter; i++) {

                            if (mappedThreads.get(i).getWebID().equals(mappedNotification1.getCorrespondentWebID())) {

                                MappedThread mappedThread = mappedThreads.get(i);

//                                Bundle bundle = new Bundle();
//                                bundle.putSerializable(THREAD_OBJECT, mappedThreads.get(i));
//                                MessageView messageView = new MessageView();
//                                messageView.setArguments(bundle);
                                delegateNotificationView.openMessagePage(mappedThread);
                            }

                        }

                    } else if (Controller.threadList == null) {

                        Call<AppointSyncResponse<ArrayList<MappedThread>>> call = RetrofitInstance.getRetrofitInstance(new GetThread());
                        call.enqueue(new Callback<AppointSyncResponse<ArrayList<MappedThread>>>() {
                            @Override
                            public void onResponse(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Response<AppointSyncResponse<ArrayList<MappedThread>>> response) {

                                if (response.body().getErrorCode() == 0) {
                                    Controller.threadList = response.body().getData();

                                    List<MappedThread> mappedThreads = Controller.threadList;


                                    int mappedThreadCounter = mappedThreads.size();
                                    for (int i = 0; i < mappedThreadCounter; i++) {

                                        if (mappedThreads.get(i).getWebID().equals(mappedNotification1.getCorrespondentWebID())) {

                                            MappedThread mappedThread = mappedThreads.get(i);

//                                Bundle bundle = new Bundle();
//                                bundle.putSerializable(THREAD_OBJECT, mappedThreads.get(i));
//                                MessageView messageView = new MessageView();
//                                messageView.setArguments(bundle);
                                            delegateNotificationView.openMessagePage(mappedThread);
                                        }

                                    }

                                } else {

                                }

                            }

                            @Override
                            public void onFailure(Call<AppointSyncResponse<ArrayList<MappedThread>>> call, Throwable t) {
                            }

                        });
                    }

//                Bundle bundle = new Bundle();
//                bundle.putSerializable(NOTIFICATION_OBJECT, mappedAppointment1);
//                AppointmentView appointmentView = new AppointmentView();
//                appointmentView.setArguments(bundle);
//
//                delegateNotificationView.seeAppointment(appointmentView);

                }

            }

        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return notificationListNew.size() + notificationListThisMonth.size() + notificationListToday.size() + notificationListOlder.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView notification_description, notification_date, notification_bar;
        public ConstraintLayout linearLayout;
        public ImageView clientPicture;

        public ViewHolder(View itemView) {
            super(itemView);

            notification_description = itemView.findViewById(R.id.notification_description);
            clientPicture = itemView.findViewById(R.id.notificationClientItemImageView);
            notification_date = itemView.findViewById(R.id.notification_date);
            notification_bar = itemView.findViewById(R.id.notification_bar);


            notification_bar.setVisibility(View.GONE);

            linearLayout = itemView.findViewById(R.id.notificationLayout);
        }

    }
}

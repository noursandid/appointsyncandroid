package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Calendar.Day;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;

import java.util.List;

/**
 * Created by cgsawma on 2/5/19.
 */

public class DayViewAppointmentAdapter extends RecyclerView.Adapter<DayViewAppointmentAdapter.ViewHolder> {


    //    public static final String WEB_ID = "webID";
    public static final String DAY_VIEW_APPOINTMENT_OBJECT = "dayViewAppointmentObject";


    // we define a list from the DevelopersList java class

    private Day daysAppointment;
    private Context context;
    public ButtonSelectedProtocol delegateDayAppointmentView;
    private Boolean anotherDay = false;


    public DayViewAppointmentAdapter(Day daysAppointment, Context context, ButtonSelectedProtocol delegateDayAppointmentView) {

        // generate constructors to initialise the List and Context objects

        this.daysAppointment = daysAppointment;
        this.context = context;
        this.delegateDayAppointmentView = delegateDayAppointmentView;

    }

    @Override
    public DayViewAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.day_view_appointment_item, parent, false);


        return new DayViewAppointmentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DayViewAppointmentAdapter.ViewHolder holder, final int position) {

        final MappedAppointment appointmentListDev = daysAppointment.getAppointments().get(position);
        holder.dayViewAppointmentIdentifier.setText(appointmentListDev.getClient().getIdentifier());
        holder.dayViewAppointmentTime.setText(UnixDateConverter.UnixDateConverterTime(appointmentListDev.getStartDate())+" - "+UnixDateConverter.UnixDateConverterTime(appointmentListDev.getEndDate()));

        appointmentListDev.getAppointmentColor();

        holder.dayViewAppointmentItemLayout.setBackgroundResource(appointmentListDev.getAppointmentColor());

        holder.dayViewAppointmentItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MappedAppointment mappedAppointment1 = appointmentListDev;

                Bundle bundle = new Bundle();
                bundle.putSerializable(DAY_VIEW_APPOINTMENT_OBJECT, mappedAppointment1);
                AppointmentView appointmentView = new AppointmentView();

                appointmentView.setArguments(bundle);

                delegateDayAppointmentView.seeAppointmentFromDayView(appointmentView);

            }

        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return daysAppointment.getAppointments().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView dayViewAppointmentTime,dayViewAppointmentIdentifier;
        public ConstraintLayout dayViewAppointmentItemLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            dayViewAppointmentTime = itemView.findViewById(R.id.dayViewAppointmentTime);
            dayViewAppointmentIdentifier = itemView.findViewById(R.id.dayViewAppointmentIdentifier);

            dayViewAppointmentItemLayout = itemView.findViewById(R.id.dayViewAppointmentItemLayout);
        }

    }
}
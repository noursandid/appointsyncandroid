package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedFile;
import com.appointsync.appointsyncpro.Class.Main.MappedFileAttachment;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateFilePageView;
import com.appointsync.appointsyncpro.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cgsawma on 2/14/19.
 */

public class FilePagesAdapter extends RecyclerView.Adapter<FilePagesAdapter.ViewHolder>{
    List<MappedFile> mappedFileList;
    Context context;
    UpdateFilePageView observer;

    public FilePagesAdapter(List<MappedFile> mappedFileList, Context context, UpdateFilePageView observer) {

        this.mappedFileList = mappedFileList;
        this.context = context;
        this.observer = observer;
    }


    @Override
    public FilePagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_file_item, parent, false);


        return new FilePagesAdapter.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(final FilePagesAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedFile mappedFile = mappedFileList.get(position);


        Picasso.get().load(R.drawable.file_page).fit().centerCrop().into(holder.fileItemImage);
        holder.fileItemText.setText("File "+mappedFile.getNumber().toString());

//        System.out.println("POSITIONNN" + position);
//        System.out.println("ATTACHMENT SIZE" + mappedFileAttachments.size());
//
//        String fileFromat = mappedFileAttachment.getAttachment().substring(mappedFileAttachment.getAttachment().lastIndexOf('.') + 1).trim();
//        System.out.println("FILE FUCKING FORMAT " + fileFromat);
//        if (fileFromat.equals("jpg") || fileFromat.equals("png") || fileFromat.equals("gif") || fileFromat.equals("jpeg")) {
//            holder.clientFileAttachmentImageView.setVisibility(View.VISIBLE);
//            holder.clientFileAttachmentPdfView.setVisibility(View.GONE);
//            Picasso.get()
//                    .load(mappedFileAttachment.getAttachment())
//                    .fit()
//                    .centerCrop()
//                    .into(holder.clientFileAttachmentImageView);
//
//
//
//        } else if (fileFromat.equals("pdf")) {
//
//            holder.clientFileAttachmentImageView.setVisibility(View.GONE);
//            holder.clientFileAttachmentPdfView.setVisibility(View.VISIBLE);
//
//            //Get file name
//            String[] splitURL = mappedFileAttachment.getAttachment().split("/");
//            String fileName = splitURL[splitURL.length - 1];
//            File pdfFile = new File(Environment.getExternalStorageDirectory().getPath(), fileName);
//
//            try {
//                pdfFile.createNewFile();
//                new FilePagesAdapter.RetrieveFeedTask(pdfFile, holder.clientFileAttachmentPdfView).execute(mappedFileAttachment.getAttachment());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        holder.clientFilePageSelectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Controller.selectedClientFilePage = position;
                observer.didSelectPage(Controller.selectedClientFilePage);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mappedFileList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objectse

        ImageView fileItemImage;
        TextView fileItemText;
        ConstraintLayout clientFilePageSelectLayout;
//        PDFView clientFileAttachmentPdfView;

        public ViewHolder(View itemView) {
            super(itemView);

            fileItemImage = itemView.findViewById(R.id.fileItemImage);
            fileItemText = itemView.findViewById(R.id.fileItemText);
//            clientFileAttachmentPdfView = itemView.findViewById(R.id.clientFileAttachmentPdfView);
            clientFilePageSelectLayout = itemView.findViewById(R.id.clientFilePageSelectLayout);
        }
    }

}

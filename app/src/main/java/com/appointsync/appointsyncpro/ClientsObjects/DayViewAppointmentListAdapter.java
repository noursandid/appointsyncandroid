package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Calendar.Day;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by cgsawma on 2/5/19.
 */

public class DayViewAppointmentListAdapter extends RecyclerView.Adapter<DayViewAppointmentListAdapter.ViewHolder> {


    //    public static final String WEB_ID = "webID";
    public static final String DAY_VIEW_APPOINTMENT_OBJECT = "dayViewAppointmentObject";


    // we define a list from the DevelopersList java class

    private List<Day> availableDaysAppointment;
    private Context context;
    public ButtonSelectedProtocol delegateDayAppointmentView;
    private Boolean anotherDay = false;
    private String dateStr;


    public DayViewAppointmentListAdapter(List<Day> availableDaysAppointment, Context context) {

        // generate constructors to initialise the List and Context objects

        this.availableDaysAppointment = availableDaysAppointment;
        this.context = context;
        this.delegateDayAppointmentView = delegateDayAppointmentView;

    }

    @Override
    public DayViewAppointmentListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.day_view_item, parent, false);


        return new DayViewAppointmentListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DayViewAppointmentListAdapter.ViewHolder holder, final int position) {

        final Day appointmentListDev = availableDaysAppointment.get(position);

        try {

            dateStr = appointmentListDev.value;

            DateFormat srcDf = new SimpleDateFormat("yyyy MM dd");

            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            DateFormat destDf = new SimpleDateFormat("EEEE, MMM d, yyyy");

            // format the date into another format
            dateStr = destDf.format(date);

        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        holder.appointmentDayViewTitle.setText(dateStr);

        final RecyclerView appointmentsRecyclerView = holder.dayViewAppointmentListRecycler;
        appointmentsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        DayViewAppointmentAdapter dayViewAppointmentAdapter = new DayViewAppointmentAdapter(appointmentListDev,context,delegateDayAppointmentView);
        appointmentsRecyclerView.setAdapter(dayViewAppointmentAdapter);


        holder.appointmentDayViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                mappedNotification1 = notificationList.get(position);
//
//                if (mappedNotification1.getType().equals(20)) {
//                    List<MappedThread> mappedThreads = Controller.threadList;
//
//                    int mappedThreadsCounter = mappedThreads.size();
//                    for (int i = 0; i < mappedThreadsCounter; i++) {
//
//                        if (mappedThreads.get(i).getWebID().equals(mappedNotification1)) {
//
//
//                            MappedAppointment mappedAppointment = Controller.appointmentList.get(i);
//                            Bundle bundle = new Bundle();
//                            bundle.putSerializable(NOTIFICATION_OBJECT, mappedAppointment);
//                            AppointmentView appointmentView = new AppointmentView();
//                            appointmentView.setArguments(bundle);
//
//                            delegateNotificationView.seeAppointmentFromNotification(appointmentView);
//                        }
//
//                    }

//                Bundle bundle = new Bundle();
//                bundle.putSerializable(NOTIFICATION_OBJECT, mappedAppointment1);
//                AppointmentView appointmentView = new AppointmentView();
//                appointmentView.setArguments(bundle);
//
//                delegateNotificationView.seeAppointment(appointmentView);

            }

        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return availableDaysAppointment.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView appointmentDayViewTitle;
        public ConstraintLayout appointmentDayViewLayout;
        public RecyclerView dayViewAppointmentListRecycler;

        public ViewHolder(View itemView) {
            super(itemView);

            dayViewAppointmentListRecycler = itemView.findViewById(R.id.dayViewAppointmentListRecycler);
            appointmentDayViewTitle = itemView.findViewById(R.id.appointmentDayViewTitle);
//            notification_bar.setVisibility(View.GONE);

            appointmentDayViewLayout = itemView.findViewById(R.id.appointmentDayViewLayout);
        }

    }
}
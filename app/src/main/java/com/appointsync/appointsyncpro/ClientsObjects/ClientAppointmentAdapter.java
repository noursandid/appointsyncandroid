package com.appointsync.appointsyncpro.ClientsObjects;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.R;

import java.util.List;

/**
 * Created by cgsawma on 1/4/19.
 */

public class ClientAppointmentAdapter extends RecyclerView.Adapter<ClientAppointmentAdapter.ViewHolder>{


    //    public static final String WEB_ID = "webID";
    public static final String APPOINTMENT_OBJECT = "appointmentObject";

    // we define a list from the DevelopersList java class

    private List<MappedAppointment> appointmentList;
    private Context context;
    public ButtonSelectedProtocol delegateAppointmentView;


    public ClientAppointmentAdapter(List<MappedAppointment> appointmentList, Context context, ButtonSelectedProtocol delegateAppointmentView) {

        // generate constructors to initialise the List and Context objects

        this.appointmentList = appointmentList;
        this.context = context;
        this.delegateAppointmentView = delegateAppointmentView;

    }

    @Override
    public ClientAppointmentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.appointment_item, parent, false);


        return new ClientAppointmentAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ClientAppointmentAdapter.ViewHolder holder, final int position) {

        // this method will bind the data to the ViewHolder from whence it'll be shown to other Views

        final MappedAppointment appointmentListDev = appointmentList.get(position);

        UnixDateConverter unixDateConverter = new UnixDateConverter();
        String appointmentStartTime = unixDateConverter.UnixDateConverterAppointmentTime(appointmentListDev.getStartDate());
        String appointmentEndTime = unixDateConverter.UnixDateConverterAppointmentTime(appointmentListDev.getEndDate());
        Integer appointmentStatus = appointmentListDev.getStatus();
        String appointmentStatusText = "";
        GradientDrawable drawable = (GradientDrawable)holder.linearLayout.getBackground();
        long unixTime = System.currentTimeMillis() / 1000L;


        if (appointmentListDev.getEndDate() < unixTime){

            appointmentStatus = 6;
            appointmentStatusText = "Passed";
            drawable.setStroke(4, Color.parseColor("#888888"));
        }

        if (appointmentStatus.equals(1)) {
            appointmentStatusText = "Requested";
            drawable.setStroke(4, Color.parseColor("#4caf50"));
        } else if (appointmentStatus.equals(2)) {
            appointmentStatusText = "Pending";
            drawable.setStroke(4, Color.parseColor("#ffcc33"));
        } else if (appointmentStatus.equals(3)) {
            appointmentStatusText = "Accepted";
            drawable.setStroke(4, Color.parseColor("#1193d4"));
        } else if (appointmentStatus.equals(4)) {
            appointmentStatusText = "Rejected";
            drawable.setStroke(4, Color.parseColor("#f44336"));
        } else if (appointmentStatus.equals(5)) {
            appointmentStatusText = "Deleted";
            drawable.setStroke(4, Color.parseColor("#000000"));
        }

        System.out.println("APPOINTMENT START DATE:     ");
        System.out.println("GENERATED DATE:     "+ unixTime);

        holder.appointmentStartDateTime.setText(appointmentStartTime);
        holder.appointmentEndDateTime.setText(appointmentEndTime);
        holder.appointmentStatus.setText("Status: " + appointmentStatusText);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MappedAppointment mappedAppointment1 = appointmentList.get(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable(APPOINTMENT_OBJECT, mappedAppointment1);
                AppointmentView appointmentView = new AppointmentView();

                appointmentView.setArguments(bundle);

                delegateAppointmentView.seeAppointmentFromClient(appointmentView);

            }
        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return appointmentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView appointmentStartDateTime,appointmentEndDateTime, appointmentStatus,appointmentStatusText;
        public Drawable appointmentBackground;
        public ConstraintLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            appointmentStartDateTime = itemView.findViewById(R.id.appointmentStartDateTime);
            appointmentEndDateTime = itemView.findViewById(R.id.appointmentEndDateTime);
            appointmentStatus = itemView.findViewById(R.id.appointmentStatus);

            appointmentBackground = context.getResources().getDrawable(R.drawable.appointment_background);

            linearLayout = itemView.findViewById(R.id.clientAppointmentLayout);
        }

    }


}

package com.appointsync.appointsyncpro.ClientsObjects;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.UnixDateConverter;
import com.appointsync.appointsyncpro.Interface.ButtonSelectedProtocol;
import com.appointsync.appointsyncpro.Interface.RefreshThreadsProtocol;
import com.appointsync.appointsyncpro.MessageView;
import com.appointsync.appointsyncpro.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by cgsawma on 1/10/19.
 */

public class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder>{


    //    public static final String WEB_ID = "webID";
    public static final String THREAD_OBJECT = "threadObject";

    // we define a list from the DevelopersList java class

    private List<MappedThread> threadList;
    private RefreshThreadsProtocol threads;
    private Context context;
    public ButtonSelectedProtocol delegateAppointmentView;


    public ThreadAdapter(List<MappedThread> threadList, Context context, ButtonSelectedProtocol delegateAppointmentView, RefreshThreadsProtocol threads) {

        // generate constructors to initialise the List and Context objects

        this.threadList = threadList;
        this.context = context;
        this.delegateAppointmentView = delegateAppointmentView;
        this.threads = threads;
    }

    @Override
    public ThreadAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // this method will be called whenever our ViewHolder is created
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.thread_item, parent, false);


        return new ThreadAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ThreadAdapter.ViewHolder holder, final int position) {


        final MappedThread mappedThread = threadList.get(position);


        holder.threadTitle.setText(mappedThread.getTitle());
        holder.threadFrom.setText(mappedThread.getToUsername());

        String lastMessage = mappedThread.getLastMessage();
        lastMessage = Html.fromHtml(lastMessage).toString();
        lastMessage = Html.fromHtml(lastMessage).toString();
        String lastMessageConcat;
        if(lastMessage.length()>30) {
             lastMessageConcat = lastMessage.substring(0, 25) + "...";
        }else{
             lastMessageConcat = lastMessage;
        }
        holder.threadMessage.setText(lastMessageConcat);

        String threadDate = UnixDateConverter.UnixDateConverterDateDayCompare(mappedThread.getLastUpdatedDate());
        String threadDateYear = UnixDateConverter.UnixDateConverterAppointmentTimeYear(mappedThread.getLastUpdatedDate());

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat dateFormatYear = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String dateToday = dateFormat.format(date);
        String dateYear = dateFormatYear.format(date);

        if(threadDate.equals(dateToday)) {
            holder.threadDate.setText(UnixDateConverter.UnixDateConverterTime(mappedThread.getLastUpdatedDate()));
        }else if(dateYear.equals(threadDateYear)){
            holder.threadDate.setText(UnixDateConverter.UnixDateConverterAppointmentTimeDay(mappedThread.getLastUpdatedDate())+" "+UnixDateConverter.UnixDateConverterAppointmentTimeMonthText(mappedThread.getLastUpdatedDate()));
        }else{
            holder.threadDate.setText(UnixDateConverter.UnixDateConverterDateDayCompare(mappedThread.getLastUpdatedDate()));
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MappedThread mappedThread1 = threadList.get(position);

                Bundle bundle = new Bundle();
                bundle.putSerializable(THREAD_OBJECT, mappedThread1);
                MessageView messageView = new MessageView();
                messageView.setArguments(bundle);
                messageView.delegate = threads;
                delegateAppointmentView.seeThreadMessage(messageView);

            }
        });

    }

    @Override

    //return the size of the listItems (appointmentListDev)

    public int getItemCount() {
        return threadList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // define the View objects

        public TextView threadTitle,threadFrom, threadMessage, threadDate;
        public ConstraintLayout linearLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            threadTitle = itemView.findViewById(R.id.thread_title);
            threadFrom = itemView.findViewById(R.id.thread_from);
            threadMessage = itemView.findViewById(R.id.thread_message);
            threadDate = itemView.findViewById(R.id.thread_date);


            linearLayout = itemView.findViewById(R.id.threadLayout);
        }

    }

}

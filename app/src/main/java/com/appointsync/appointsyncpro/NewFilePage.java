package com.appointsync.appointsyncpro;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Class.Main.GetFilesPages;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedFile;
import com.appointsync.appointsyncpro.Class.Main.PostAddFile;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileAttachmentsAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.ClientFileViewAdapter;
import com.appointsync.appointsyncpro.ClientsObjects.FilePagesAdapter;
import com.appointsync.appointsyncpro.Interface.Observer;
import com.appointsync.appointsyncpro.Interface.UpdateFilePageView;
import com.appointsync.appointsyncpro.Network.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewFilePage extends Fragment implements  UpdateFilePageView{
    private TextView newFilePageTitle;
    private ImageView clientAddFileBackArrow, addNewFilePage;
    private MappedClient mappedClient;

    private RecyclerView recyclerView;
    private FilePagesAdapter adapter;
    private long mLastClickTime = 0;

    private List<Observer> observers = new ArrayList<>();
    public UpdateFilePageView updateFileViewObserver;
    private Fragment minime;

   public  NewFilePage(){

   }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_file_page, container, false);

        mappedClient = (MappedClient) getArguments().getSerializable(Dashboard.SELECTED_CLIENT_ADD_APPOINTMENT);

        final ProgressDialog pd = new ProgressDialog(getContext());
        pd.setTitle("Loading...");
        pd.setMessage("Please wait.");
        pd.setCancelable(false);

        minime = this;
        newFilePageTitle = view.findViewById(R.id.newFilePageTitle);
        clientAddFileBackArrow = view.findViewById(R.id.clientAddFileBackArrow);
        addNewFilePage = view.findViewById(R.id.addNewFilePage);

        if (Controller.clientFilePage.equals(false)) {
            clientAddFileBackArrow.setVisibility(View.INVISIBLE);
            newFilePageTitle.setText("No Files.");
        } else {
            newFilePageTitle.setText("Files");


//            newFilePageRecycler

            recyclerView = view.findViewById(R.id.newFilePageRecycler);
//        ClientLayoutItem = view.findViewById(R.id.home_rv);
            int numberOfColumns = 3;
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
            recyclerView.setHasFixedSize(true);


            adapter = new FilePagesAdapter(mappedClient.mappedFiles, getActivity().getApplicationContext(),this);

            recyclerView.setAdapter(adapter);
        }
        addNewFilePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                Call<AppointSyncResponse<MappedFile>> call = RetrofitInstance.getRetrofitInstance(new PostAddFile(mappedClient.getWebID()));
                pd.show();
                call.enqueue(new Callback<AppointSyncResponse<MappedFile>>() {
                    @Override
                    public void onResponse(Call<AppointSyncResponse<MappedFile>> call, Response<AppointSyncResponse<MappedFile>> response) {

                        if (response.body().getErrorCode() == 0) {

                            if(Controller.clientFilePage!=null && Controller.clientFilePage.equals(false)){
                                Controller.clientFilePage = true;
                            }

                            if(mappedClient.mappedFiles == null){
                                mappedClient.mappedFiles = new ArrayList<>();
                            }
//                            Controller.clientFilePage = true;
                            mappedClient.mappedFiles.add(response.body().getData());
                            Controller.selectedClientFilePage = mappedClient.mappedFiles.size()-1;
                            didSelectPage(mappedClient.mappedFiles.size()-1);

                        }else{
                            Toast.makeText(getActivity(), response.body().getErrorDescription(), Toast.LENGTH_SHORT).show();
                        }

                        pd.dismiss();
                    }

                    @Override
                    public void onFailure
                            (Call<AppointSyncResponse<MappedFile>> call, Throwable t) {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later.", Toast.LENGTH_SHORT).show();
                        pd.dismiss();

                    }
                });


            }
        });


        clientAddFileBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                closeView();
            }
        });


        return view;
    }

    @Override
    public void didSelectPage(int pageNumber) {
        updateFileViewObserver.didSelectPage(pageNumber);
        closeView();
//        run ?yes
    }

    public void addObserver(UpdateFilePageView observer){
       this.updateFileViewObserver = observer;
    }

    public void closeView(){

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

        NewFilePage f = (NewFilePage) fm.findFragmentByTag("seeClientAddFilePageFragment");
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.remove(f);
        ft.commit();

    }
}

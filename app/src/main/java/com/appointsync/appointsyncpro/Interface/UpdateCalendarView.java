package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/6/19.
 */

public interface UpdateCalendarView {

    public void updateCalendarView(Observer observer);
    public void notifyObservers();

}

package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/15/19.
 */

//This interface is the type of the delegate
public interface UpdateFilePageView {

//    public void changeFilePageView(Observer observer);
//    public void notifyObservers();

//    This method is the "message" that the "delegate" (or we can call it messenger rasoul allah lol al a3zam) can hold
    public void didSelectPage(int pageNumber);

}


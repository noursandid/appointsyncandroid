package com.appointsync.appointsyncpro.Interface;

import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;

import java.util.List;

/**
 * Created by cgsawma on 2/20/19.
 */

public interface NotificationObserverDelegate {
    void didUpdateNotifications(List<MappedNotification> mappedNotifications);
}

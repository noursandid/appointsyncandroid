package com.appointsync.appointsyncpro.Interface;

import com.appointsync.appointsyncpro.Class.AppointSyncResponse;
import com.appointsync.appointsyncpro.Class.ForgotPasswordClass;
import com.appointsync.appointsyncpro.Class.GetClient;
import com.appointsync.appointsyncpro.Class.GetTemplateClass;
import com.appointsync.appointsyncpro.Class.HostRegistrationClass;
import com.appointsync.appointsyncpro.Class.LoginRequest;
import com.appointsync.appointsyncpro.Class.Main.AcceptAppointment;
import com.appointsync.appointsyncpro.Class.Main.CancelAmendment;
import com.appointsync.appointsyncpro.Class.Main.DeleteAppointment;
import com.appointsync.appointsyncpro.Class.Main.GetAppointmentAttachment;
import com.appointsync.appointsyncpro.Class.Main.GetAppointmentNotes;
import com.appointsync.appointsyncpro.Class.Main.GetAppointments;
import com.appointsync.appointsyncpro.Class.Main.GetClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.GetClientPermissions;
import com.appointsync.appointsyncpro.Class.Main.GetClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.GetClients;
import com.appointsync.appointsyncpro.Class.Main.GetFilesPages;
import com.appointsync.appointsyncpro.Class.Main.GetLocation;
import com.appointsync.appointsyncpro.Class.Main.GetMessage;
import com.appointsync.appointsyncpro.Class.Main.GetNotification;
import com.appointsync.appointsyncpro.Class.Main.GetNotificationFlag;
import com.appointsync.appointsyncpro.Class.Main.GetPermissions;
import com.appointsync.appointsyncpro.Class.Main.GetPrices;
import com.appointsync.appointsyncpro.Class.Main.GetProfile;
import com.appointsync.appointsyncpro.Class.Main.GetThread;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentAttachments;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointmentNotes;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileAddAttachment;
import com.appointsync.appointsyncpro.Class.Main.MappedClientFileBalance;
import com.appointsync.appointsyncpro.Class.Main.MappedClientWithQRCode;
import com.appointsync.appointsyncpro.Class.Main.MappedFile;
import com.appointsync.appointsyncpro.Class.Main.MappedFileAttachment;
import com.appointsync.appointsyncpro.Class.Main.MappedLocation;
import com.appointsync.appointsyncpro.Class.Main.MappedMessage;
import com.appointsync.appointsyncpro.Class.Main.MappedNotification;
import com.appointsync.appointsyncpro.Class.Main.MappedNotificationFlag;
import com.appointsync.appointsyncpro.Class.Main.MappedPermission;
import com.appointsync.appointsyncpro.Class.Main.MappedPlan;
import com.appointsync.appointsyncpro.Class.Main.MappedPrices;
import com.appointsync.appointsyncpro.Class.Main.MappedProfile;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.Class.Main.PostAddClient;
import com.appointsync.appointsyncpro.Class.Main.PostAddFile;
import com.appointsync.appointsyncpro.Class.Main.PostAddOfflineClient;
import com.appointsync.appointsyncpro.Class.Main.PostAmendAppointment;
import com.appointsync.appointsyncpro.Class.Main.PostAppointmentNote;
import com.appointsync.appointsyncpro.Class.Main.PostCancelSubscription;
import com.appointsync.appointsyncpro.Class.Main.PostChangePassword;
import com.appointsync.appointsyncpro.Class.Main.PostChangePlan;
import com.appointsync.appointsyncpro.Class.Main.PostClientFileUpdate;
import com.appointsync.appointsyncpro.Class.Main.PostClientFileUpdateNote;
import com.appointsync.appointsyncpro.Class.Main.PostContactUs;
import com.appointsync.appointsyncpro.Class.Main.PostCurrency;
import com.appointsync.appointsyncpro.Class.Main.PostEditProfile;
import com.appointsync.appointsyncpro.Class.Main.PostEmailFlag;
import com.appointsync.appointsyncpro.Class.Main.PostLinkOfflineClient;
import com.appointsync.appointsyncpro.Class.Main.PostLocation;
import com.appointsync.appointsyncpro.Class.Main.PostMessage;
import com.appointsync.appointsyncpro.Class.Main.PostMessageReply;
import com.appointsync.appointsyncpro.Class.Main.PostNewAppointment;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationFlag;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationRemoveToken;
import com.appointsync.appointsyncpro.Class.Main.PostNotificationToken;
import com.appointsync.appointsyncpro.Class.Main.PostPaymentDue;
import com.appointsync.appointsyncpro.Class.Main.PostPaymentReceived;
import com.appointsync.appointsyncpro.Class.Main.PostPaymentResponse;
import com.appointsync.appointsyncpro.Class.Main.PostReadNotifications;
import com.appointsync.appointsyncpro.Class.Main.PostSecureFileFlag;
import com.appointsync.appointsyncpro.Class.Main.PostTemplatePin;
import com.appointsync.appointsyncpro.Class.Main.PostUpdateClientPermissions;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePackage;
import com.appointsync.appointsyncpro.Class.Main.PostUpdatePermission;
import com.appointsync.appointsyncpro.Class.Main.PostVerifyPin;
import com.appointsync.appointsyncpro.Class.Main.RejectAppointment;
import com.appointsync.appointsyncpro.Class.Success;
import com.appointsync.appointsyncpro.Class.Template;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by cgsawma on 11/3/18.
 */

public interface API {

    //Notification

    @POST("handler/addToken.php")
    Call<AppointSyncResponse<Success>> postPostNotificationToken(@Body PostNotificationToken body);

    @POST("handler/removeToken.php")
    Call<AppointSyncResponse<Success>> postPostNotificationRemoveToken(@Body PostNotificationRemoveToken body);


    //General

    @POST("handler/registerHost.php")
    Call<AppointSyncResponse<Success>> postHostRegistrationClass(@Body HostRegistrationClass body);

    @POST("handler/forgotPassword.php")
    Call<AppointSyncResponse<Success>> postForgotPasswordClass(@Body ForgotPasswordClass body);

    @POST("file/getTemplates.php")
    Call<AppointSyncResponse<ArrayList<Template>>> postGetTemplateClass(@Body GetTemplateClass body);

    @POST("handler/login.php")
    Call<AppointSyncResponse<MappedProfile>> postLoginRequest(@Body LoginRequest body);

    @POST("client/getClients.php")
    Call<AppointSyncResponse<ArrayList<MappedClient>>> postGetClients(@Body GetClients body);

    @POST("client/getClient.php")
    Call<AppointSyncResponse<MappedClient>> postGetClient(@Body GetClient body);

    @POST("profile/getProfile.php")
    Call<AppointSyncResponse<MappedProfile>> postGetProfile(@Body GetProfile body);

    @POST("location/getLocations.php")
    Call<AppointSyncResponse<ArrayList<MappedLocation>>> postGetLocation(@Body GetLocation body);

    @POST("appointment/getAppointments.php")
    Call<AppointSyncResponse<ArrayList<MappedAppointment>>> postGetAppointments(@Body GetAppointments body);

    @POST("notification/getNotifications.php")
    Call<AppointSyncResponse<ArrayList<MappedNotification>>> postGetNotification(@Body GetNotification body);

    @POST("message/getThreads.php")
    Call<AppointSyncResponse<ArrayList<MappedThread>>> postGetThread(@Body GetThread body);

    @POST("message/getMessages.php")
    Call<AppointSyncResponse<ArrayList<MappedMessage>>> postGetMessage(@Body GetMessage body);

    @POST("message/postMessage.php")
    Call<AppointSyncResponse<MappedThread>> postPostMessage(@Body PostMessage body);

    @POST("message/postMessage.php")
    Call<AppointSyncResponse<MappedMessage>> postPostMessageReply(@Body PostMessageReply body);

    @POST("notification/readNotifications.php")
    Call<AppointSyncResponse<Success>> postPostReadNotifications(@Body PostReadNotifications body);


    //API SETTINGS

    @POST("client/getClientsPermissions.php")
    Call<AppointSyncResponse<MappedPermission>> postGetPermissions(@Body GetPermissions body);

    @POST("profile/getNotificationEmailFlags.php")
    Call<AppointSyncResponse<MappedNotificationFlag>> postGetNotificationFlag(@Body GetNotificationFlag body);

    @POST("profile/changePlan.php")
    Call<AppointSyncResponse<MappedPlan>> postPostChangePlan(@Body PostChangePlan body);

    @POST("handler/changePassword.php")
    Call<AppointSyncResponse<Success>> postPostChangePassword(@Body PostChangePassword body);

    @POST("client/updateClientsPermissions.php")
    Call<AppointSyncResponse<Success>> postPostUpdatePermission(@Body PostUpdatePermission body);

    @POST("file/changeSecureFile.php")
    Call<AppointSyncResponse<Success>> postPostSecureFileFlag(@Body PostSecureFileFlag body);

    @POST("file/changeTemplatePin.php")
    Call<AppointSyncResponse<Success>> postPostTemplatePin(@Body PostTemplatePin body);

    @POST("file/verifyPin.php")
    Call<AppointSyncResponse<Success>> postPostVerifyPin(@Body PostVerifyPin body);

    @POST("profile/updateNotificationFlag.php")
    Call<AppointSyncResponse<Success>> postPostNotificationFlag(@Body PostNotificationFlag body);

    @POST("profile/updateEmailFlag.php")
    Call<AppointSyncResponse<Success>> postPostEmailFlag(@Body PostEmailFlag body);

    @POST("handler/contactUs.php")
    Call<AppointSyncResponse<Success>> postPostContactUs(@Body PostContactUs body);

    @POST("profile/editProfile.php")
    Call<AppointSyncResponse<Success>> postPostEditProfile(@Body PostEditProfile body);

    @POST("profile/updateSubscription.php")
    Call<AppointSyncResponse<MappedPlan>> postPostUpdatePackage(@Body PostUpdatePackage body);

    @POST("location/addLocation.php")
    Call<AppointSyncResponse<MappedLocation>> postPostLocation(@Body PostLocation body);

    @POST("payment/GetPrices.php")
    Call<AppointSyncResponse<MappedPrices>> postGetPrices(@Body GetPrices body);

    @POST("profile/cancelSubscription.php")
    Call<AppointSyncResponse<Success>> postPostCancelSubscription(@Body PostCancelSubscription body);

    @Multipart
    @POST("profile/editProfilePic.php")
    Call<AppointSyncResponse<Success>> editUser (@Part("file\"; filename=\"pp.png\" ") RequestBody file);

//    @Multipart
//    @POST("profile/editProfilePic.php")
//    Call<AppointSyncResponse<Success>> uploadAttachment(@Part("file\"; filename=\"pp.png\" ") RequestBody file);

    @Multipart
    @POST("profile/editProfilePic.php")
    Call<AppointSyncResponse<Success>> uploadAttachment(@Part MultipartBody.Part filePart);


    @Multipart
    @POST("profile/editProfilePic.php")
    Call<Success> uploadImage(@Part MultipartBody.Part file);

//    Client's APIs


    @POST("client/updateClientPermissions.php")
    Call<AppointSyncResponse<Success>> postPostUpdateClientPermissions(@Body PostUpdateClientPermissions body);

    @POST("client/getClientPermissions.php")
    Call<AppointSyncResponse<MappedPermission>> postGetClientPermissions(@Body GetClientPermissions body);

    @POST("client/getClientWithQRCode.php")
    Call<AppointSyncResponse<MappedClientWithQRCode>> postGetClientWithQRCode(@Body GetClientWithQRCode body);

    @POST("client/linkClient.php")
    Call<AppointSyncResponse<MappedClient>> postPostLinkOfflineClient(@Body PostLinkOfflineClient body);

    @POST("client/addOfflineClient.php")
    Call<AppointSyncResponse<MappedClient>> postPostAddOfflineClient(@Body PostAddOfflineClient body);

    @POST("client/addClient.php")
    Call<AppointSyncResponse<MappedClient>> postPostAddClient(@Body PostAddClient body);

    @POST("file/getFilesPages.php")
    Call<AppointSyncResponse<ArrayList<MappedFile>>> postGetFilesPages(@Body GetFilesPages body);

    @POST("file/addFile.php")
    Call<AppointSyncResponse<MappedFile>> postPostAddFile(@Body PostAddFile body);

    @POST("file/getPayments.php")
    Call<AppointSyncResponse<ArrayList<MappedClientFileBalance>>> postGetClientFileBalance(@Body GetClientFileBalance body);

    @POST("file/updateCurrency.php")
    Call<AppointSyncResponse<Success>> postPostCurrency(@Body PostCurrency body);

    @POST("file/addPaymentReceived.php")
    Call<AppointSyncResponse<PostPaymentResponse>> postPostPaymentReceived(@Body PostPaymentReceived body);

    @POST("file/addPaymentDue.php")
    Call<AppointSyncResponse<PostPaymentResponse>> postPostPaymentDue(@Body PostPaymentDue body);

    @POST("file/updateFileValue.php")
    Call<AppointSyncResponse<Success>> postPostClientFileUpdate(@Body PostClientFileUpdate body);

    @POST("file/updateFileNote.php")
    Call<AppointSyncResponse<Success>> postPostClientFileUpdateNote(@Body PostClientFileUpdateNote body);

    @Multipart
    @POST("file/addAttachment.php")
    Call<AppointSyncResponse<MappedFileAttachment>> uploadClientFileAttachment(@Part("clientWebID") RequestBody clientWebID, @Part("pageWebID") RequestBody pageWebID, @Part MultipartBody.Part filePart);


//    Appointments APIs

    @POST("appointment/acceptAppointment.php")
    Call<AppointSyncResponse<MappedAppointment>> postAcceptAppointment(@Body AcceptAppointment body);

    @POST("appointment/amendAppointment.php")
    Call<AppointSyncResponse<MappedAppointment>> postPostAmendAppointment(@Body PostAmendAppointment body);

    @POST("appointment/rejectAppointment.php")
    Call<AppointSyncResponse<MappedAppointment>> postRejectAppointment(@Body RejectAppointment body);

    @POST("appointment/deleteAppointment.php")
    Call<AppointSyncResponse<Success>> postDeleteAppointment(@Body DeleteAppointment body);

    @POST("appointment/cancelAmendment.php")
    Call<AppointSyncResponse<MappedAppointment>> postCancelAmendment(@Body CancelAmendment body);

    @POST("appointment/addAppointment.php")
    Call<AppointSyncResponse<MappedAppointment>> postPostNewAppointment(@Body PostNewAppointment body);
    
    @POST("appointment/getNotes.php")
    Call<AppointSyncResponse<ArrayList<MappedAppointmentNotes>>> postGetAppointmentNotes(@Body GetAppointmentNotes body);

    @POST("appointment/getAttachments.php")
    Call<AppointSyncResponse<ArrayList<MappedAppointmentAttachments>>> postGetAppointmentAttachment(@Body GetAppointmentAttachment body);

    @POST("appointment/addNote.php")
    Call<AppointSyncResponse<MappedAppointmentNotes>> postPostAppointmentNote(@Body PostAppointmentNote body);

    @Multipart
    @POST("appointment/addAttachment.php")
    Call<AppointSyncResponse<Success>> uploadAppointmentNoteAttachment(@Part("appointmentWebID") RequestBody appointmentWebID,@Part MultipartBody.Part filePart);



}

package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 1/28/19.
 */

public interface Subject {

    public void register(Observer observer);
    public void unregister(Observer observer);
    public void notifyObservers();

}

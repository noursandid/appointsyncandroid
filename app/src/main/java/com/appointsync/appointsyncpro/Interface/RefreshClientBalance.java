package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/13/19.
 */

public interface RefreshClientBalance {

    public void didFinishAddingPaymentMessage(Observer observer);
    public void notifyObservers();

}

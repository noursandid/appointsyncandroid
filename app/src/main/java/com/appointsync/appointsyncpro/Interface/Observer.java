package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 1/28/19.
 */

public interface Observer {

    public void update();

}

package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/4/19.
 */

public interface MonthDaysRecyclerAdapterDelegate {
     void didSelectDay(int[] selectedYearMonthDay);
}

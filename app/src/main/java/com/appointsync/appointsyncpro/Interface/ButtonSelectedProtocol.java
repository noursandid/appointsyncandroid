package com.appointsync.appointsyncpro.Interface;

import android.media.Image;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appointsync.appointsyncpro.AmendAppointment;
import com.appointsync.appointsyncpro.AppointmentNotes;
import com.appointsync.appointsyncpro.AppointmentView;
import com.appointsync.appointsyncpro.Class.Main.MappedAppointment;
import com.appointsync.appointsyncpro.Class.Main.MappedClient;
import com.appointsync.appointsyncpro.Class.Main.MappedThread;
import com.appointsync.appointsyncpro.ClientProfile;
import com.appointsync.appointsyncpro.ClientsObjects.FilePagesAdapter;
import com.appointsync.appointsyncpro.MessageView;

/**
 * Created by cgsawma on 1/1/19.
 */

public interface ButtonSelectedProtocol {
    public void seeLocationsSelected(Button button);
    public void seeAddNewLocation(ImageView imageView);
    public void seeProfileSettings(ImageView imageView);
    public void seeClientProfile(ClientProfile clientProfile);
    public void seeAppointment(AppointmentView AppointmentView);
    public void seeClientAppointment(Button Button, MappedClient client);
    public void seeThreadMessage(MessageView messageView);

    public void openMessagePage(MappedThread mappedThread);

//Notifications

    public void seeAppointmentFromNotification(AppointmentView AppointmentView);
    public void seeClientProfileNotification(ClientProfile clientProfile);
    public void seeAppointmentFromDayView(AppointmentView AppointmentView);
    public void openAppointmentNote(AppointmentView appointmentView, MappedAppointment mappedAppointment);
    public void seeThreadMessageFromNotification(MessageView messageView);

//    Client

    public void seeAddNewAppointment(Button button, MappedClient client);
    public void seeAppointmentFromClient(AppointmentView AppointmentView);
    public void seeClientSettingsPermission(Button button, MappedClient client);
    public void seeClientSendMessage(Button button, MappedClient client);
    public void seeClientFile(MappedClient client);
    public void seeClientAddFilePage(MappedClient client, UpdateFilePageView observer);
    public void seeClientFileBalance(MappedClient client);
    public void seeClientLinkToOnlineAccount(Button button, MappedClient client);
    public void seeAddNewClient();
    public void openClientsPage(MappedClient client);
    public void seeClientBalanceAddPaymentDue(MappedClient client);
    public void seeClientBalanceAddPaymentReceived(MappedClient client);

// Appointment

    public void seeAmendAppointment(AmendAppointment amendAppointment);
    public void seeAppointmentNotes(AppointmentNotes appointmentNotes);

  //Calendar

    public void seeDayView(int[] yeaDayView);
    public void notifyDayViewToUpdateAppointmentList();
    public void calendarAddNewAppointment();
    public void dayViewAddNewAppointment();



//    Profile Settings

    public void seeProfileSettingSubscription(TextView textView);
    public void seeProfileSettingEditProfile(TextView textView);
    public void seeProfileSettingChangePassword(TextView textView);
    public void seeProfileSettingClientPermission(TextView textView);
    public void seeProfileSettingFileSecurity(TextView textView);
    public void seeProfileSettingNotification(TextView textView);
    public void seeProfileSettingContactUs(TextView textView);
    public void seeProfileSettingHelp(TextView textView);
    public void seeProfileSettingAbout(TextView textView);
    public void seeProfileSettingPrivacyPolicy(TextView textView);
    public void seeProfileSettingTermsAndCondition(TextView textView);
    public void seeNotPremiumUser(TextView textView);
    public void seeNotPremiumUserClientProfile();
    public void seeRenewPremiumAccount(Button button);
    public void seeRenewProAccount(Button button);
    public void seeRenewPlanPayment(boolean isUpgrade,int type,String subscriptionPackage,RenewPlanPaymentDelegate delegate);

}
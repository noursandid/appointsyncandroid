package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/19/19.
 */

public interface NoSubscriptionObserver {
    void notifyEndOfSubscription();
}

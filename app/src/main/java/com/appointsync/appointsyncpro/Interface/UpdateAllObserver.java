package com.appointsync.appointsyncpro.Interface;

/**
 * Created by cgsawma on 2/8/19.
 */

public interface UpdateAllObserver {
    void updateAll();
}

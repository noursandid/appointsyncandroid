package com.appointsync.appointsyncpro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.appointsync.appointsyncpro.Adapters.AddClientPageAdapter;
import com.appointsync.appointsyncpro.Adapters.RegistrationPackagesAdapter;
import com.appointsync.appointsyncpro.Class.Main.Controller;
import com.appointsync.appointsyncpro.Interface.Observer;

import java.util.ArrayList;

public class RegistrationPackages extends AppCompatActivity {
    private RegistrationPackagesAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_packages);

        viewPager = (ViewPager) findViewById(R.id.viewPagerRegistration);
        tabLayout = (TabLayout) findViewById(R.id.tabLayoutRegistration);
        adapter = new RegistrationPackagesAdapter(getSupportFragmentManager());

        RegistrationAppointSyncPro registrationAppointSyncPro = new RegistrationAppointSyncPro();
        RegistrationAppointSyncProPremium registrationAppointSyncProPremium = new RegistrationAppointSyncProPremium();
        adapter.addFragment(registrationAppointSyncPro, "AppointSync Pro");
        adapter.addFragment(registrationAppointSyncProPremium, "AppointSync Pro Premium");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }


        @Override
        public void onBackPressed(){

            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

     if(Controller.noActiveSubscription!= null && Controller.noActiveSubscription.equals(true)){
         Controller.noActiveSubscription = false;
         Intent intent = new Intent(RegistrationPackages.this, Login.class);
         startActivity(intent);
     }else{
         if(Controller.noActiveSubscription!= null){
             Controller.noActiveSubscription = false;
         }
         finish();
     }

    }
}

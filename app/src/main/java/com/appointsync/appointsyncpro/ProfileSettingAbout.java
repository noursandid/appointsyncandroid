package com.appointsync.appointsyncpro;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSettingAbout extends Fragment {

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

    }
    Button btnBack;
    WebView webview;



    public ProfileSettingAbout() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile_setting_about, container, false);

        final ImageView profileSettingAboutBackArrow = view.findViewById(R.id.profileSettingAboutBackArrow);


        webview=(WebView)view.findViewById(R.id.webViewAbout);
        webview.setInitialScale(1);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setDefaultFontSize(42);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webview.setScrollbarFadingEnabled(false);
        webview.setWebViewClient(new MyWebViewClient());
        openURL();

        profileSettingAboutBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ProfileSettingAbout f = (ProfileSettingAbout) fm.findFragmentByTag("profileSettingAboutFragment");
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                ft.remove(f);
                ft.commit();


            }
        });


        return view;
    }

    private void openURL() {
        webview.loadUrl("https://www.appointsync.com/About.html");
        webview.requestFocus();
    }

}
